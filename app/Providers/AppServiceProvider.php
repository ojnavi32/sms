<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Laravel\Dusk\DuskServiceProvider;
use Illuminate\Support\Facades\Schema;
use Laravel\Horizon\Horizon;
use Response;
use App\Contact;

use Illuminate\Support\Facades\Validator;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Schema::defaultStringLength(191);

        Horizon::auth(function ($request) {

            // @TODO @besingamkb real authorization on horizon dashboad
            $allowed_emails = [
                'mark@intergo.com.cy',
                'besingamkb@gmail.com',
                'mike@intergo.com.cy',
                'hafsalmetvr@gmail.com',
                'italos@intergo.com.cy',
                'manny@intergo.com.cy',
                'guillermo@intergo.com.cy',
                'tapas@intergo.com.cy',
                'admin@sms.to' // admin/manager account
            ];

            if ($request->user()) {
                return in_array($request->user()->email, $allowed_emails);
            }
            return false;
        });

        // Custom for caching findOrFail Exception
        $this->app->make('api.exception')->register(function (\Illuminate\Database\Eloquent\ModelNotFoundException $e) {
            return abort(404);
        });
        Validator::extend('smsto_valid_phone', function ($attribute, $value, $parameters, $validator) {
            return Contact::isValidNumber($value);
        }, 'Invalid Phone Number!');
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        if ($this->app->environment('local', 'testing')) {
            $this->app->register(DuskServiceProvider::class);
        }
        if ($this->app->environment('production'))
        {
            $this->app->alias('bugsnag.logger', \Illuminate\Contracts\Logging\Log::class);
            $this->app->alias('bugsnag.logger', \Psr\Log\LoggerInterface::class);
        }
    }
}
