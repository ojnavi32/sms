<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class ComposerServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        // manager breadcrumbs
        view()->composer('manager.*', function($view){
            $breadcrumbs[] = $this->_managerLink('manager', 'Manager');
            switch($view->name()) {

                case 'manager.sms.batches.index':
                    $breadcrumbs[] = 'SMS Broadcasts';
                    break;

                case 'manager.sms.batches.messages.index':
                    $breadcrumbs[] = $this->_managerLink('manager/clients', 'SMS Broadcasts');
                    $breadcrumbs[] = $this->_managerLink('manager/clients', 'SMS Broadcasts #' . $view->batches->sms_broadcast_id);
                    $breadcrumbs[] = 'Batch #' . $view->batches->id;
                    break;

                // Clients
                case 'manager.clients.index':
                    $breadcrumbs[] = 'Clients';
                    break;
                case 'manager.clients.view':
                    $breadcrumbs[] = $this->_managerLink('manager/clients', 'Clients');
                    $breadcrumbs[] = $view->client->name;
                    break;
                case 'manager.clients.update':
                    $breadcrumbs[] = $this->_managerLink('manager/clients', 'Clients');
                    $breadcrumbs[] = $this->_managerLink('manager/clients/'.$view->client->id.'/view', $view->client->name);
                    $breadcrumbs[] = 'Update';
                    break;
                case 'manager.clients.settings.index':
                    $breadcrumbs[] = $this->_managerLink('manager/clients', 'Clients');
                    $breadcrumbs[] = $this->_managerLink('manager/clients/'.$view->client->id.'/view', $view->client->name);
                    $breadcrumbs[] = 'Settings';
                    break;
                case 'manager.clients.settings.adjust_rates.index':
                    $breadcrumbs[] = $this->_managerLink('manager/clients', 'Clients');
                    $breadcrumbs[] = $this->_managerLink('manager/clients/'.$view->client->id.'/view', $view->client->name);
                    $breadcrumbs[] = $this->_managerLink('manager/clients/'.$view->client->id.'/settings', 'Settings');
                    $breadcrumbs[] = 'Adjust Rates';
                    break;
                case 'manager.clients.settings.edit_funds':
                    $breadcrumbs[] = $this->_managerLink('manager/clients', 'Clients');
                    $breadcrumbs[] = $this->_managerLink('manager/clients/'.$view->client->id.'/view', $view->client->name);
                    $breadcrumbs[] = $this->_managerLink('manager/clients/'.$view->client->id.'/settings', 'Settings');
                    $breadcrumbs[] = 'Edit Funds';
                    break;
                 case 'manager.clients.settings.update_OptoutNumber':
                    $breadcrumbs[] = $this->_managerLink('manager/clients', 'Clients');
                    $breadcrumbs[] = $this->_managerLink('manager/clients/'.$view->client->id.'/view', $view->client->name);
                    $breadcrumbs[] = $this->_managerLink('manager/clients/'.$view->client->id.'/settings', 'Settings');
                    $breadcrumbs[] = 'Optout Number';
                    break;
                case 'manager.clients.fax_broadcasts.index':
                    $breadcrumbs[] = $this->_managerLink('manager/clients', 'Clients');
                    $breadcrumbs[] = $this->_managerLink('manager/clients/'.$view->client->id.'/view', $view->client->name);
                    $breadcrumbs[] = 'Fax Broadcasts';
                    break;

                // Number banlist
                case 'manager.number_banlist.index':
                    $breadcrumbs[] = 'Number Banlist';
                    break;
                case 'manager.number_banlist.edit':
                    $breadcrumbs[] = $this->_managerLink('manager/number-banlist', 'Number Banlist');
                    $breadcrumbs[] = 'New';
                    break;

                // Rates
                case 'manager.rates.index':
                    $breadcrumbs[] = 'Rates';
                    break;
                case 'manager.rates.create':
                    $breadcrumbs[] = $this->_managerLink('manager/rates', 'Rates');
                    if ($view->name) {
                        $breadcrumbs[] = $view->name;
                        $breadcrumbs[] = 'Update';
                    } else {
                        $breadcrumbs[] = 'Create';
                    }
                    break;

                // Fax Broadcasts
                case 'manager.fax_broadcasts.index':
                    $breadcrumbs[] = 'Fax Broadcasts';
                    break;
                case 'manager.fax_broadcasts.view':
                    $breadcrumbs[] = $this->_managerLink('manager/fax-broadcasts', 'Fax Broadcasts');
                    $breadcrumbs[] = 'Broadcast #'.$view->fax_broadcast->id;
                    break;
                case 'manager.fax_broadcasts.batch_messages':
                    $breadcrumbs[] = $this->_managerLink('manager/fax-broadcasts', 'Fax Broadcasts');
                    $breadcrumbs[] = $this->_managerLink('manager/fax-broadcasts/'.$view->fax_broadcast_batch->fax_broadcast->id.'/view', 'Broadcast #'.$view->fax_broadcast_batch->fax_broadcast->id);
                    $breadcrumbs[] = 'Batch #'.$view->fax_broadcast_batch->id;
                    break;

                // IP banlist
                case 'manager.ip_banlist.index':
                    $breadcrumbs[] = 'IP Banlist';
                    break;
                case 'manager.ip_banlist.edit':
                    $breadcrumbs[] = $this->_managerLink('manager/ip-banlist', 'IP Banlist');
                    $breadcrumbs[] = 'New';
                    break;
            }

            $view->with('breadcrumbs', $breadcrumbs);
        });

        // client profile tabs
        view()->composer('client.profile.*', function($view){
            // url, icon, title
            $profile_tabs_collection = collect([
                [url('client/profile'), 'fa-user', 'Profile', 'client.profile.index'],
                //[url('client/profile/edit'), 'fa-user', 'Edit Profile', 'client.profile.edit'],
                [url('client/profile/change-password'), 'fa-lock', 'Change Password', 'client.profile.change_password'],
            ]);
            $client_profile_tabs = '<ul class="nav nav-tabs">';
            $profile_tabs_collection->each( function ($tab) use (&$client_profile_tabs, $view) {
                $client_profile_tabs .= ($view->name() == $tab[3] ? '<li class="active">' : '<li>');
                $client_profile_tabs .= '<a href="'.$tab[0].'"><i class="fa '.$tab[1].'"></i>'.$tab[2].'</a></li>';
            });
            $client_profile_tabs .= '</ul>';
            $view->with('client_profile_tabs', $client_profile_tabs);
        });

    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    private function _managerLink($url, $text)
    {
        return '<a href="'.url($url).'">'.$text.'</a>';
    }

}
