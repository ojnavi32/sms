<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use URL;


class ContactUsMailToSmsTo extends Mailable
{
    //use Queueable, SerializesModels;
    public $name;
    public $email;
    public $message;
    public $phone;
    
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($name,$email,$message,$phone)
    {
        $this->name = $name;
        $this->email = $email;
        $this->message = $message;
        $this->phone = $phone;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    { 
        return $this->subject("Inquiry from " . $this->name . "<". $this->email .">")->markdown('emails.contact_us_smsto');
    }
}
