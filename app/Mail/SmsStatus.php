<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use URL;


class SmsStatus extends Mailable
{
    //use Queueable, SerializesModels;
    public $userName;
    public $deliveryCount;
    public $failedCount;
    public $listName;
    public $broadcastId;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($userName,$deliveryCount,$failedCount,$listName,$broadcastId)
    {
        $this->userName = $userName;
        $this->deliveryCount = $deliveryCount;
        $this->failedCount = $failedCount;
        $this->listName = $listName;
        $this->broadcastId = $broadcastId;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    { 
        return $this->subject("SMS Broadcast finished - SMS.to")->markdown('emails.sms_status')->with(['url' => url('/').'/app#/reports/'.$this->broadcastId]);
    }
}
