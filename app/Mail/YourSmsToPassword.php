<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use URL;

class YourSmsToPassword extends Mailable
{
    //use Queueable, SerializesModels;
    public $user;
    public $passwordResetlink;
   
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($user,$passwordResetlink)
    {
        $this->user = $user;
        $this->passwordResetlink = $passwordResetlink;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    { 
        return $this->subject('Welcome to SMS.to')->markdown('emails.verify_user_with_password')->with(['url' => URL::to('login')]);
    }
}
