<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use URL;


class FailedBroadcast extends Mailable
{
   
     public $subMessage;
     public $message;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($subMessage,$message)
    {
        $this->subMessage = $subMessage;
        $this->message = $message;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    { 
        return $this->subject($this->subMessage."  - SMS.to")->markdown('emails.sms_failed');
    }
}
