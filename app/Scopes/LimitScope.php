<?php

namespace App\Scopes;

use Illuminate\Database\Eloquent\Scope;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\Schema;

class LimitScope implements Scope
{
    /**
     * Apply the scope to a given Eloquent query builder.
     *
     * @param  \Illuminate\Database\Eloquent\Builder $builder
     * @param  \Illuminate\Database\Eloquent\Model $model
     * @return void
     */
    public function apply(Builder $builder, Model $model)
    {
        // We have limit parameter
        if (request('limit') and is_numeric(request('limit')) and request('limit') > 0) {
            $builder->limit(request('limit'));
        }
        
        // If we have filter parameter
        // We need to identify if the column exists in the table
        // if (in_array($field, config('sort.contacts'))) {
        //     $builder->where($field, 'LIKE', '');
        // }
        
        // Ordering
        if (request('sort')) {
            $firstChar = substr(request('sort'), 0, 1);
            $field = request('sort');

            // We need to check if the  field exists in table
            $order = 'asc';
            if ($firstChar == '-') {
                // Remove the first character
                $field = substr($field, 1);
                $order = 'desc';
            }


            $table = $model->getTable();

            if (Schema::hasColumn($table, $field)) {
                $builder->orderBy($field, $order);
            }
        }
        
    }
}