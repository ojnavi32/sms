<?php 

namespace App\Gateways;

use Illuminate\Http\Request;
use App\Models\Document;
use App\Services\FileConverter;
use App\Services\DocumentManager;

use Storage;

class FileGateway
{
    protected $documents;
    protected $fileConverter;

    public function __construct(Document $documents, FileConverter $fileConverter)
    {
        $this->documents = $documents;
        $this->fileConverter = $fileConverter;
    }

    public function upload($request, $fileField = 'file')
    {
        if ( ! $request->hasFile($fileField)) {
            return response()->json(['status' => 'failed', 'message' => 'You need to upload file.'], 400);
        }
        
        // If the mode is client_mode Like the site is Fax2email
        if (config('fax.mode') == 'client_mode') {
            return fax()->uploadFile($request);
        }

        $fileConverter = new FileConverter($imgk = '');

        if (session('api_sandbox')) {
            $fileConverter->sandbox = true;
        }

        $fileConverter->process($request, $fileField);

        return $fileConverter->document;
    }

    public function cleanRestore(Request $request, $id, $restore)
    {
        // doc_id is required
        $request->request->add([
            'doc_id' => $id,
        ]);
        
        // Return document
        return DocumentManager::getCleanBackground($request, $restore);
    }
    
    public function download(Request $request, $id)
    {
        $document = $this->documents->find($id);
        $file = storage_path('app/documents/'. $document->user_id . '/') . $document->orig_filename . '.preview.jpg';
        if ($request->png_file) {
            $file = 'documents/'. $document->user_id . '/' . $document->filename . '.png';
            if ( Storage::disk('s3-docs')->exists($file) ) {
                return response(Storage::disk('s3-docs')->get($file), 200)
                  ->header('Content-Type', 'image/png');
            }
        }
        return response()->file($file);
    }

    public function coversheet($request, $document)
    {
        $filename = uniqid() . '.mergecoversheet.tiff';
        $outputFile = replaceLocalName(storage_path('app/documents/'. auth()->user()->id . '/') . $document->filename);
        
        $fileConverter = new FileConverter($imgk = '');
        $coversheetFilename = $fileConverter->generateCoverSheet($request, $document->total_pages);
        $filesArray[] = $coversheetFilename;
        $filesArray[] = $document->filename;
        $fileConverter->mergeFiles($filesArray, $outputFile, $dontDeleteFile = $document->filename);

        // Lets upload the new file
        $fileStore = 'documents/' . auth()->user()->id . '/' . $document->filename;
        if ( Storage::exists($fileStore) ) {
            $filesize = Storage::size($fileStore);
        }

        // We put the merged file to S3
        if (config('filesystems.enable_s3')) {
            if ( Storage::exists($fileStore) ) {
                Storage::disk('s3-docs')->put($fileStore, Storage::get($fileStore));
            }
        }
        $document->total_pages += 1;
        $document->save();
        return $document;
    }

    public function deleteFile($id, $app = 'api')
    {
        if ($app == 'web') {
            $document = $this->documents->find($id);
            if ($document) {
                if ( ! $document->has_access ) {
                    abort(404);
                }

                if ($document->fax_jobs()->whereStatus('executed')->count() > 0) {
                    return redirect('member/my-documents')
                        ->with('alert_messages', ['warning' => [trans('messages.errors.deletion_failed_used')]]);
                } else {
                    if ($document->delete()) {
                        // Todo: May be we can also delete the files themselves
                        return redirect('member/my-documents')
                            ->with('alert_messages', ['success' => [trans('messages.success.document_deleted')]]);
                    }
                }
            }

            return redirect('member/my-documents')
                ->with('alert_messages', ['success' => [trans('messages.success.unable_delete_document')]]);
            
        }

        // We need to make sure that the file belongs to user
        if ($id) {
            if (session('api_sandbox')) {
                $document = SandboxDocument::find($id);
            } else {
                $document = Document::find($id);
            }
            if ( ! $document ) {
                return response()->json(['status' => 'failed', 'message' => 'File could not found'], 404);
            }
            if (auth()->user()->id == $document->user_id) {
                if ($document->delete()) {
                    return response()->json(null, 204);
                }
            }
            return response()->json(['status' => 'failed'], 400);
        }
    }

    public function files()
    {
        $filesArray = [];

        if (session('api_sandbox')) {
            $files = auth()->user()->sandboxDocuments;
        } else {
            $files = auth()->user()->documents;
        }

        foreach ($files as $file) {
            $filesArray[] = [
                'id' => (int)$file->id,
                'filename' => $file->filename_display,
                'pages' => $file->total_pages,
                'size' => $file->filesize,
                'uploaded' => $file->created_at->format('Y-m-d'),
            ];
        }

        return response()->json($filesArray, 200);
    }
    
    public function getFile($id)
    {
        $file = $this->documents->where('user_id', auth()->user()->id)->where('id', $id)->first();
        
        if ($file) {
            $fileData = [
                'id' => $file->id,
                'user_id' => $file->user_id,
                'filename' => $file->filename,
                'total_pages' => $file->total_pages,
                'filesize' => $file->filesize,
                'preview_file' => $file->preview_file,
                // 'created_at' => $file->created_at,
                // 'updated_at' => $file->updated_at, 
            ];
            return response()->json($fileData, 200);
        }
        
        return response()->json(['status' => 'failed', 'message' => 'File could not found'], 404);
    }
}