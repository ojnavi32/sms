<?php 

namespace App\Gateways;

use Illuminate\Http\Request;
use App\Services\AgileCrm;
use App\Models\User;
use App\Models\NonUser;
use Mail;
use App\Mail\VerifyEmail;
use Carbon\Carbon;

class UserGateway
{
    protected $agile;
    protected $users;
    protected $nonMembers;

    public function __construct(AgileCrm $agile, User $users, NonUser $nonMembers)
    {
        $this->agile = $agile;
        $this->users = $users;
        $this->nonMembers = $nonMembers;
    }

    public function create($request, $frontendController = null, $userInfo = null)
    {
        if ($frontendController) {
            $frontendController->validate($request, [
                'email' => 'required|email|min:12|max:255|unique:users,email',
                'country' => 'required|min:3|max:3'
            ]);
        }

        $field = 'facebook_id';
        $socialId = null;
        if ($request) {
            $email = $request->email;
        }

        if ($userInfo) {
            $field = $userInfo->service . '_id';
            $email = $userInfo->getEmail();
            $socialId = $userInfo->getId();
        }
        
        $randomPassword = str_random(12);
        $user = new User([
            'role_id' => 2,
            'email' => $email,
            'email_domain' => substr(strrchr($email, "@"), 1),
            $field => $socialId,
            'cash_balance' => config('payment_gateway.default_cash_balance'),
            'agile_tags' => json_encode([]),
            'ip_address' => getUserIp(),
            'last_login_time' => Carbon::now(),
            'last_ip_address' => getUserIp(),
            'country' => getLocation(),
            'currency' => getCurrency(),
            'verification_code' => uniqid(),
        ]);

        $user->save();

        // Dont email if from social login
        if ( ! $userInfo ) {
            Mail::queue(new VerifyEmail($user, $randomPassword));
        }

        // Login user directly
        auth()->login($user);

        // Create agile contact
        $user->name = '';

        $tag = 'user';

        if (session('fax2mail_romania')) {
            $tag = session('fax2mail_romania');
        }

        // Create agile contact
        $this->agile->createContact($user, $tag);

        return $user;
    }
    public function memberVatnumber($request)
    {
       $members=$this->users->orderBy('created_at', 'DESC')->orWhereNotNull('business_vat');
       return [
            'requests' => $request->all(),
            'members' => $members->paginate(15),
            ];   

    }

    public function members($request, ...$params)
    {
        $members = $this->users->orderBy('created_at', 'DESC')->withCount('broadcasts');
        if ($params[0] == 'normal') {
            //$members->whereNull('created_app');
            if ($request->has('name_or_email')) {
                $members->whereRaw("CONCAT(`name`, ' ', `email`) LIKE ?", ['name_or_email' => "%".$request->get('name_or_email')."%"]);
            }
            if ($request->has('developer')) {
                $members->where('developer', 1);
            }
            
            $members->where('type', '<>', 'admin');
            
        } else if($params[0] == 'ios') {
            //$members->where('created_app', 'Mobile iOS');
            if ($request->has('name_or_email')) {
                $members->where('uuid', 'LIKE', $request->get('name_or_email'));
            }
        } else if($params[0] == 'android') {
            //$members->where('created_app', 'Mobile android');
            if ($request->has('name_or_email')) {
                $members->where('uuid', 'LIKE', $request->get('name_or_email'));
            }
        } else if($params[0] == 'vat') {
            
            $members->with('billing_detail');
            $members->validVat();
            
        } else if($params[0] == 'non') {
            return [
                'requests' => $request->all(),
                'members' => $this->nonMembers->paginate(100),
                'name_or_email' => $request->get('name_or_email'),
            ];
        } else if($params[0] == 'developer') {
            $members->where('developer', 1);
        }

        $tags = $request->input('tags');
        
        if ($tags) {
            $whereRaw = '';
            foreach ($tags as $tag) {
                $whereRaw .= ' agile_tags RLIKE "' . $tag . '" OR';
            }
            $whereRaw = substr($whereRaw, 0, -3);
            $members->whereRaw($whereRaw);
        }

        if ($request->input('banned')) {
            $members->where('verified', 2);
        }

        if ($request->input('valid_vat')) {
            $members->validVat();
        }

        if ($request->has('innapp-purchase')) {
            $members->where('cash_balance', '!=', 0);
        }    
        return [
            'requests' => $request->all(),
            'members' => $members->paginate(15),
            'name_or_email' => $request->get('name_or_email'),
        ];
    }

    public function stats()
    {
    }
   
}