<?php 

namespace App\Gateways\Api;

use Illuminate\Http\Request;
use App\Models\User;
use Uuid, Validator, DB, Hash;
use GuzzleHttp\Client;
use Mail;
use App\Mail\VerifyEmail;
use App\Mail\VerifyEmailDeveloper;

class Account
{
    public function balance()
    {
        $balance = floatval(auth()->user()->cash_balance +
                            auth()->user()->cash_balance_sub +
                            auth()->user()->userCashBalance()->sum('amount'));
        
        // If this is a sandbox
        if (session('api_sandbox')) {
            $balance = floatval(auth()->user()->sandbox_cash_balance);
        }
        
        return response()->json([
                'status' => 'success',
                'balance' => $balance,
                ],
        200);
    }

	public function store($request, $os = 'ios')
    {
        if ($request->has('platform')) {
            $os = 'android';
        }
        
        if ($request->uuid) {
            $user = User::where('uuid', $request->uuid)->first();
            if ($user) {
                return response()->json([
                    'status' => 'success',
                    'api_key' => (string)$user->api_key,
                    ],
                    200
                );
            }
        }

        // If the api did not provide any email we create temporary email for them
        if ($request->email == '' and ! $request->developer) {
            $temporaryEmail = $os . 'user' . User::max('id') . str_random(5) . '@fax.to';
            $request->merge(['email' => $temporaryEmail]);
        }

        $validator = Validator::make($request->all(), [
            'email' => 'required|email|max:64|unique:users,email',
        ]);
        
        if ($validator->fails()) {

            //If request comes from Get API KEY Form, Dont Throw Email Unique Error, Instead Update User to developer
            if ($request->get_api_key  && $user = User::where('email', $request->email)->first()){
                $user->verification_code = uniqid();
                $user->verified = 0;
                $user->save();

                Mail::queue(new VerifyEmailDeveloper($user));
                
                return response()->json([
                        'status' => 'success', 
                        'existing_account' => true,
                        ],
                200);

            }

            $messages = $validator->errors();

            return response()->json([
                'status' => 'failed',
                'message' => $messages->first('email'),
                ],
                400
            );
        }

        $createdApp = 'Mobile iOS';
        if ($os == 'android') {
        	$createdApp = 'Mobile android';
        }
        
        $cashBalance = config('payment_gateway.default_cash_balance');
        
        $password = '';
        
        $developer = null;
        
        // If the sign up is from Fax-api page
        if ($request->developer) {
            $developer = 1;
            $createdApp = null;
            $cashBalance = config('payment_gateway.default_cash_balance_developer');
            $cashBalance = 0;
            $randomPassword = str_random(12);
            $password = Hash::make($randomPassword);
        }

        $user = new User([
            'role_id' => 2,
            'email' => $request->input('email'),
            'cash_balance' => $cashBalance,
            'agile_tags' => json_encode([]),
            'ip_address' => getUserIp(),
            'country' => getLocation(),
            'verification_code' => uniqid(),
            'default_provider' => env('API_DEFAULT_PROVIDER'),
            'uuid' => $request->uuid,
            'api_key' => $apiKey = Uuid::generate(),
            'api_enabled' => 1,
            'developer' => $developer,
            'created_app' => $createdApp,
            'password' => $password,
        ]);

        $user->save();
        
        if ($request->developer) {
            
            auth()->login($user); // Login
            //Mail::queue(new VerifyEmail($user, $randomPassword));
            
            return response()->json([
                    'status' => 'success', 
                    'api_key' => (string)$apiKey,
                    'user_id' => encrypt($apiKey . ':' . $user->id),
                    ],
            200);
        }
        
        return response()->json([
                'status' => 'success', 
                'api_key' => (string)$apiKey,
                ],
        200);
    }

    public function login($request)
    {
        if ($request->uuid) {
            $user = user::where('uuid', $request->uuid)->first();
            if ($user) {
                return response()->json([
                    'status' => 'success', 
                    'api_key' => (string)$user->api_key,
                    ],
                    200
                );
            }
        }

        $user = DB::table('users')->where('email', $request->input('email'))->first();

        if ($user) {
            // We need to send the password reset via email if the password is blank
            if ($user->password == '') {
                $client = new Client();

                $response = $client->post(url('password/email'), [
                    'body' => [
                        'email' => $user->email,
                        '_token' => csrf_token(),
                    ]
                ]);

                return response()->json([
                    'status' => 'failed',
                    'message' => 'We send password reset link via email',
                    ],
                    403
                );
            }
        }

        $credentials = $request->only('email', 'password');

        if (auth()->attempt($credentials)) {
            return response()->json([
                'status' => 'success', 
                'api_key' => (string)auth()->user()->api_key,
                ],
                200
            );
        }

        return response()->json([
                'status' => 'failed',
                'message' => 'Invalid Email or Password',
                ],
                403
        );
    }

    public function changePassword($request)
    {
        if ( ! Hash::check($request->input('current_password'), auth()->user()->password)) {
            return response()->json([
                'status' => 'failed',
                'message' => 'Current password incorrect',
                ],
                400
            );
        }

        $validator = Validator::make($request->all(), [
            'new_password' => 'required|confirmed|min:6',
        ]);

        if ($validator->fails()) {
            $m = $validator->errors();
            $messages = '';
            foreach ($m->all() as $message) {
                $messages .= $message;
            }
            
            return response()->json([
                'status' => 'failed',
                'message' => $messages,
                ],
                400
            );
        }

        auth()->user()->password = bcrypt($request->input('new_password'));
        auth()->user()->api_key = Uuid::generate();
        auth()->user()->save();

        return response()->json([
            'status' => 'success', 
            'api_key' => (string)auth()->user()->api_key,
            ],
            200
        );
    }
}