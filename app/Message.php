<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Scopes\LimitScope;

class Message extends Model
{
	use SoftDeletes;
	
    protected $table = 'sms_broadcasts_messages';
    
    protected $fillable = [
        'user_id', 'list_id', 'delivered', 'failed', 'message', 'status',
    ];
    
    /**
     * The "booting" method of the model.
     *
     * @return void
     */
    protected static function boot()
    {
        parent::boot();

        static::addGlobalScope(new LimitScope);
    }
    
    public function user()
    {
    	return $this->belongsTo(User::class);
    }
    
    public function list()
    {
    	return $this->belongsTo(Lists::class);
    }
}
