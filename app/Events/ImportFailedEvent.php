<?php

namespace App\Events;

use App\Lists;
use App\Models\User;
use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class ImportFailedEvent implements ShouldBroadcast
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $user;
    public $list;
    public $title;
    public $message;

    /**
     * Create a new event instance.
     *
     * @param User $user
     * @param Lists $list
     * @param $message
     */
    public function __construct(User $user, Lists $list, $message)
    {
        $this->user = $user;
        $this->list = $list;
        $this->title = "Contact Import Failed.";
        $this->message = $message;
    }

//    /**
//     * The event's broadcast name.
//     *
//     * @return string
//     */
//    public function broadcastAs()
//    {
//        return 'import.failed';
//    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel("user." . $this->user->id . ".import");
    }
}
