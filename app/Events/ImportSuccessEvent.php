<?php

namespace App\Events;

use App\Lists;
use App\Models\User;
use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class ImportSuccessEvent implements ShouldBroadcast
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $user;
    public $list;
    public $title;
    public $message;

    /**
     * Create a new event instance.
     *
     * @param User $user
     * @param Lists $list
     */
    public function __construct(User $user, Lists $list, $message = null)
    {
        $this->user = $user;
        $this->list = $list;
        $this->title = "Contact Import";
        $this->message = $this->list->name . " has been imported.";
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel("user." . $this->user->id . ".import");
    }
}
