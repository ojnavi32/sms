<?php

namespace App\Events\Events;

use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class SuccessImportEvent
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $list_id;
    public $imported_count;

    /**
     * Create a new event instance.
     *
     * @param $list_id
     * @param $imported_count
     */
    public function __construct($list_id, $imported_count = 0)
    {
        $this->list_id = $list_id;
        $this->imported_count = $imported_count;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        // we can notify users per import on mongodb if you want with socket.io
        return new PrivateChannel('channel-name');
    }
}
