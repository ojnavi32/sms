<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Scopes\LimitScope;

class Lists extends Model
{
	use SoftDeletes;

    protected $table = 'lists';
    
    protected $fillable = [
        'user_id', 'name', 'description','is_importing'
    ];
    
    /**
     * The "booting" method of the model.
     *
     * @return void
     */
    protected static function boot()
    {
        parent::boot();

        static::addGlobalScope(new LimitScope);
    }
    
    public function user()
    {
    	return $this->belongsTo(User::class);
    }
    
    public function listContacts()
    {
    	return $this->hasMany(ListContact::class, 'list_id');
    }
    
    public function contacts()
    {
        return $this->belongsToMany(Contact::class,'lists_contacts','list_id','contact_id')->whereNull('lists_contacts.deleted_at');
    }

    public function getTotalContactsAttribute()
    { 
        return $this->listContacts()->count();
    }

    public function scopeSearch($query, $srchPrm, $value)
    { 
        if ($srchPrm != null && $value != null) {
            return $query->where($srchPrm, 'LIKE', '%' . $value . '%');
        }
        
        return $query;
    }
}
