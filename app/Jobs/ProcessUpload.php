<?php

namespace App\Jobs;

use App\Models\User;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use App\Services\ContactService;

class ProcessUpload implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $file_path;
    public $list_id;
    public $user;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($file_path, $list_id, User $user)
    {
        $this->file_path = $file_path;
        $this->list_id = $list_id;
        $this->user = $user;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
         ExtractContacts::dispatch($this->file_path, $this->list_id, $this->user)->onQueue('uploadExtract');
//        $job = new ExtractContacts($this->file_path, $this->list_id, $this->user);
//        $job->dispatch()->onQueue('uploadExtract');
//        return $job->getResponse();

//        dispatch(new \App\Jobs\ContactCsvImport($this->filePath, $this->user, $this->list_id, ContactService::class));
    }
}
