<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Http\Request;
use Carbon\carbon;

use App\Jobs\Job;
use App\ListContact;
use App\Models\Template;

use App\Services\CountryInfo;
use App\Services\SMS\Calculators\RateCalculator;
use App\Models\{
    SmsBroadcastMessage, SmsBroadcast, SmsBroadcastBatch
};
use App\User;
use App\Contact;
use App\Lists;
use App\Models\CashHistory;
use Validator;
use App\Models\CreditPackage;
use App\Models\Setting;
use App\Models\Rate;
use App\Models\Provider;


class SendSms implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $user;
    protected $request;
    protected $smsPerBatch;
    protected $contacts;
    public $message;
    public $templateId;
    public $sms_total_cost;
    public $costPerCredits;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(User $user, $contacts, $request = null)
    {
        $this->user = $user;
        $this->contacts = $contacts;
        $this->request = $request;
        $this->smsPerBatch = config('sms.sms_per_batch');
        $this->costPerCredits = CreditPackage::minimumCostPerCredit();
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(Request $request)
    {
        $user = $this->user;
        $status = 'ongoing';
        if (@$this->request['sender_id'])
            $senderId = @$this->request['sender_id'];
        else
            $senderId = null;

        if (@$this->request['template_id']) {
            $template = $this->user->templates()->find(@$this->request['template_id']);

            $this->request['message'] = $template->messages;

        }

        if (!$this->request['send_immediately']) //if scheduled, then validate the schedule date
        {
            $status = 'scheduled';
            $timeZone = $this->request['timezone'];
            $date = $this->request['send_later'];

            $time = Carbon::now();
            $time->setTimezone($timeZone); //To validate with the corresponding timezone whether future time or not

            // $request->validate(['send_later' => 'required|date|after:'.$time->format('Y-m-d H:i:s')],
            //                        ['send_later.after' => 'The schedule time should be a future time']);


            $schedule_time = Carbon::parse($date); //scheduled time for sending fax

            $to_utc = Carbon::createFromFormat('Y-m-d H:i:s', $schedule_time, $timeZone); //creates time with the given timezone

            $to_utc = $to_utc->setTimezone(config('app.timezone')); //converts time to UTC

        }

        $message = $this->formatMessage(@$this->request['message'], $this->user->id, @$this->request['list_id']);
        $smsBroadcast = SmsBroadcast::create([
            'user_id' => $this->user->id,
            'lists_id' => @$this->request['list_id'],
            'message' => $message,
            'template_id' => @$this->request['template_id'],
            'sched_date' => Carbon::parse(@$this->request['send_later'])->format('Y-m-d H:i:s'),
            'time_zone' => @$this->request['timezone'],
            'scheduled_server_time' => @$to_utc ? $to_utc->format('Y-m-d H:i:s') : null,
            'send_later' => @$this->request['send_later'],
            'manual_contacts' => @$this->request['contacts'],
            'total_cost' => 0,
            'status' => $status,
        ]);

        if (!$this->request['send_immediately']) {
            return;
        }

        $broadcastTotalCosts = 0;
        $broadcastCosts = 0;
        $smsMessagesBatches = [];
        $smsMessagesBatchesIndex = 0;
        $smsMessageCount = 0;
        $manualContacs = [];
        $billType = $this->user->billing_type;
        $rate_calculator = new RateCalculator;

        $providerUsed = Provider::getproviderOfCountry($this->contacts[0]->country_code);


        $provider = $providerUsed->with('providerCurrency')->getProviderByName($providerUsed->name)->first();

        $contacts = $rate_calculator->setSmsCost($this->contacts, $billType, $this->formatMessage(@$this->request['message'], $this->user->id, @$this->request['list_id']), $provider,$this->user->id);

        foreach ($contacts as $contact) {
            $smsMessageCount++;

            // calculate total cost
            $rate_calculator->min_cost = 0;
            // $rate_calculator->countryCode = $contact->country_code;
            // $rate_calculator->message = $smsBroadcast->message;
            // $rate_calculator->networkName = $contact->network_name;


            $smsMessage = new SmsBroadcastMessage([
                'sms_broadcast_id' => $smsBroadcast->id,
                'cost' => $contact->cost, // Estimate cost
                'our_cost' => $contact->ourCost, // Estimate our cost
                'credits' => ceil($contact->ourCost / $this->costPerCredits), // Estimate credits
                'user_id' => $smsBroadcast->user_id,
//                        'contact_id' => (string) $contact->_id,
                'contact_id' => null,
                'send_to' => $contact->phone,
                'country_dial_code_id' => $contact->country_code,
            ]);
            $broadcastTotalCosts += $smsMessage->our_cost;
            $broadcastCosts += $smsMessage->cost;

            if (!$request->list_id) {
                // insert to a batch
                $manualContacts[$smsMessagesBatchesIndex][] = $contact->phone;
            }

            $smsMessagesBatches[$smsMessagesBatchesIndex][] = $smsMessage;
            if (($smsMessageCount % $this->smsPerBatch) == 0) {
                $smsMessagesBatchesIndex++;
            }
        }

        // check client credit balance
        if ($smsBroadcast->user->cash_balance < $broadcastTotalCosts) {
            $funds_needed = $broadcastTotalCosts - $smsBroadcast->user->cash_balance;
            $smsBroadcast->status = 'error';
            return $smsBroadcast->save();
        } else {
            $smsBroadcast->user->cash_balance -= $broadcastTotalCosts; // Deduct estimate credits from user's credit balance
            $smsBroadcast->user->save();

            $historyData = [
                'client_id' => $smsBroadcast->user->id,
                'original_amount' => $smsBroadcast->user->cash_balance,
                'amount' => $broadcastTotalCosts,
                'fax_broadcast_id' => $smsBroadcast->id, //TODO change column name
                'type' => 0, // lets do 0 for deduct
                // 'by' => 1, // 1 for admin
            ];

            // Save cash balance history
            CashHistory::create($historyData);

            $smsBroadcast->total_cost = $broadcastTotalCosts; //TODO :Need to rename column as total credit
            $smsBroadcast->cost = $broadcastCosts;
            $smsBroadcast->status = "ongoing";
            $smsBroadcast->batches = count($smsMessagesBatches);
            $smsBroadcast->sent_ip = request()->ip();
            if ($smsBroadcast->save()) {
                foreach ($smsMessagesBatches as $batchIndex => $smsMessagesBatch) {
                    $smsBatch = new SmsBroadcastBatch;
                    $smsBatch->sms_broadcast_id = $smsBroadcast->id;
                    $smsBatch->message = $request->message;
                    $smsBatch->status = 'new';
                    $smsBatch->save();


                    $smsBatch->smsMessages()->saveMany($smsMessagesBatch);

                    sleep(config('fax.broadcasting.fax_send_delay'));//TODO fix

                    // $manualContactsBatch = isset($manualContacts[$batchIndex])?$manualContacts[$batchIndex]:[];

                    dispatch(new \App\Jobs\SendSmsBatch($smsBatch, $senderId));
                }

            }
        }

    }

    private function formatMessage($message, $user_id, $list_id)
    {
        $formattedMessage = "";
        $optout_url = "";
        if (!empty($list_id)) {
            if (strpos($message, "{optout}") !== false) {
                if ($user_id !== "" && $list_id !== "") {

                    $optout_url = route("optout", [
                        'optout_string' => "u" . $user_id . "l" . $list_id
                    ]);
                }
                $formattedMessage = str_replace("{optout}", "\n" . $optout_url, $message);
                return $formattedMessage;
            }

            return str_replace("{optout}", "", $message);
        }

        return $message;
    }

    private function _nextCommProvider($fax_job)
    {
        // If the fax job is from API and API_RETRIES is enabled
        if ($fax_job->job_api == 1 and env('API_RETRIES', false)) {
            return $this->_nextAPICommProvider($fax_job);
        }

        $provider_priorities = $fax_job->provider_priorities();

        foreach ($provider_priorities->get() as $provider_priority) {
            $provider_used = $fax_job->fax_job_transactions->where('comm_provider_id', $provider_priority->id)->first();

            // Check if the provider was not already used
            // So we can use the next provider
            if (!$provider_used) {
                $fax_handler = new FaxHandler($fax_job, $provider_priority);
                $fax_handler->comm_provider->document = $fax_job->document;
                $response = $fax_handler->sendFax();
                $fax_job_transaction = new FaxJobTransaction([
                    'comm_provider_id' => $provider_priority->id,
                    'transaction_id' => @$response['transaction_id'],
                    'response' => json_encode($response),
                ]);
                $fax_job->fax_job_transactions()->save($fax_job_transaction);
                return TRUE;
                break;
            }
        }

        return FALSE;
    }

}
