<?php

namespace App\Jobs;

use App\Jobs\Job;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Storage;
use LaravelMonopondFax;
use App\Models\SmsBroadcastBatch;
use Carbon\Carbon;
use App\Models\Setting;
use App\Services\SMS\Providers\InfoBip;
use App\Models\CreditPackage;
use App\Services\SMS\Calculators\RateCalculator;


class BatchDeliveryUpdate extends Job implements ShouldQueue
{
    use InteractsWithQueue, SerializesModels;

    public $response;
    public $batch;
    public $costPerCredits;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($response, $batch)
    {
     $this->response = $response;
     $this->batch = $batch;
     $this->costPerCredits = CreditPackage::minimumCostPerCredit();
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    { 
      $margin = Setting::getSetting('MARGIN_RATE');
      $profit = 1+ (1*$margin)/100;
      $billingType = $this->batch->smsBroadcast->user->billing_type;
    

      try{
            // Take the messages under each batch
            $messages = $this->batch->smsMessages;
            // Update each message cost and status
            foreach($messages as $message)
               { 
                if(isset($this->response[$message->message_id])) {
                    $message->status = $this->response[$message->message_id]['status'];
                  if($billingType == 'PER_CARRIER'){
                    $message->cost = $this->response[$message->message_id]['cost']; // Real cost from provider
                    $message->our_cost = $this->response[$message->message_id]['cost'] * $profit; // our cost calculating
                    $message->credits = ceil($message->our_cost/$this->costPerCredits); // credits calculation
                  }
 
                    if($message->status == "DELIVERED" || $message->status == "SENT"){
                      $message->delivered = 1;
                      $message->failed = 0;
                    }
                    else if($message->status == "REJECTED" || $message->status == "UNDELIVERABLE" ){
                      $message->failed = 1;
                      $message->delivered = 0;
                    }

                  }
               }

              //Update Status of each Messages under batch
              $this->batch->smsMessages()->saveMany($messages);

             // If there is no pending messages under batch then update batch status to 'done'
              $batchStatus = $this->batch->smsMessages->contains('status','PENDING');
                if(!$batchStatus){
                  $this->batch->status = 'done';
                  $this->batch->save();
                }


                //After updating status & real price of messages of batches ,
                //update total sms cost in broadcast table and adjust user balance
                $broadcast =  $this->batch->smsBroadcast;
                $realCredit = $broadcast::calculatePrice($broadcast);
                $totalCost = $broadcast::calculateTotalPrice($broadcast);

                $cashBalance = $broadcast->user->cash_balance;

                $realEstimateDiff = $broadcast->total_cost - $realCredit; // TODO total cost where actually storing total credits 
                
                $broadcast->user->cash_balance = $cashBalance + $realEstimateDiff;
                $broadcast->user->save();

                $broadcast->total_cost = $realCredit;
                $broadcast->cost = $totalCost;
                $broadcast->total_messages =  $broadcast->messages()->count();
                $broadcast->save();


            
             } catch (\Exception $e){
                  // echo 'Message: ' .$e->getMessage();
            }
        
        }

}