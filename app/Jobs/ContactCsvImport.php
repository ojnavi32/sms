<?php

namespace App\Jobs;

use App\Jobs\Job;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Storage;
use LaravelMonopondFax;
use Auth;
use Excel;
use App\Services\ContactService;
use App\Contact;
use App\Lists;
use App\ListContact;
use Mail;
use DB;
use App\Mail\ImportContact;


class ContactCsvImport extends Job implements ShouldQueue
{
    use InteractsWithQueue, SerializesModels;

    public $file;
    public $user;
    public $listId;
    public $contactService;
    public $count;
    public $total;
    public $lastInsertId;
    public $date;
    public $lists;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($file, $user, $listId, $contactService)
    {
        $this->file = storage_path('app/' . $file);
        $this->user = $user;
        $this->listId = $listId;
        $this->contactService = $contactService;
        $this->total = 0;
        $this->lastInsertId = 0;
        $this->lists = Lists::findOrFail($this->listId);
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {

        $this->lists->is_importing = 1;
        $this->lists->save();

        Excel::filter('chunk')->load($this->file)->chunk(2000, function ($results) {
            $date = \Carbon\Carbon::now();
            $lastContact = DB::table('contacts')->orderBy('id', 'DESC')->first();

            if (isset($lastContact))
                $this->lastInsertId = $lastContact->id;

            $contacts = [];
            foreach ($results as $row) {
                if (empty(trim($row[0]))) continue;

                $row['phone'] = '+' . trim($row[0]);
                $contact = new contact;
                $contact = $contact->setContactNetwork($row, $this->user);

                if ($contact) {
                    $contacts[] = [
                        'user_id' => $this->user->id,
                        'name' => isset($row[1]) ? $row[1] : '',
                        'phone' => $contact->phone,
                        'email' => isset($row[2]) ? $row[2] : '',
                        'country_code' => $contact->country_code,
                        'network_name' => $contact->network_name,
                        'created_at' => $date,
                        'updated_at' => $date
                    ];
                }

            }

            try {

                DB::beginTransaction();

                DB::table('contacts')->insert($contacts);

                $count = count($contacts);
                $this->total += $count;

                if ($this->listId != 'undefined') {

                    $listContacts = [];
                    for ($i = 1; $i <= $count; $i++) {

                        $listContacts[] = ['user_id' => $this->user->id,
                            'list_id' => $this->listId,
                            'contact_id' => ($this->lastInsertId) + $i,
                            'created_at' => $date,
                            'updated_at' => $date
                        ];

                    }

                    DB::table('lists_contacts')->insert($listContacts);
                }

                DB::commit();

            } catch (\Exception $e) {
                DB::rollback();
            }


        }, $shouldQueue = false);

        $count = ListContact::where('list_id', $this->listId)->count();
        $this->lists->is_importing = 0;
        $this->lists->number_contacts = $count;
        $this->lists->save();

        try {
            Mail::to($this->user->email)->send(new ImportContact($this->user, $this->total, $this->lists->name));

        } catch (\Exception $e) {
            // echo 'Message: ' .$e->getMessage();
        }

    }


}
