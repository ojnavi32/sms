<?php

namespace App\Jobs;

use App\Jobs\Job;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Storage;
use LaravelMonopondFax;
use App\Models\SmsBroadcastBatch;
use Carbon\Carbon;
use App\Models\Setting;
use Mail;
use App\Services\SMS\Providers\InfoBip;
use Auth;
use App\Models\RateProviderPriority;
use App\Models\Provider;
use App\Models\Rate;
use App\Services\SmsHandler\SmsHandler;

class SendSmsBatch extends Job implements ShouldQueue
{
    use InteractsWithQueue, SerializesModels;

    public $smsBroadcast;
    public $smsBatch;
    public $smsPerBatch;
    public $ref_suffix;
    public $contacts;
    public $senderId;
    public $provider;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(SmsBroadcastBatch $smsBatch, $senderId, $manualContacts = null, $provider = null)
    {
        $this->smsBatch = $smsBatch;
        $this->smsPerBatch = config('sms.sms_per_batch');
        $this->ref_suffix = env('TEST_BROADCAST_REFERENCE_SUFFIX');
        $this->senderId = $senderId;
        $this->manualContacts = $manualContacts;
        $this->senderId = $senderId;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    { 
        $mapping = [];
        $this->SmsBroadcast = $this->smsBatch->smsBroadcast;

        // Set the time when the queue ran
       
        $msgContent = $this->SmsBroadcast->message;
        $messages = $this->smsBatch->smsMessages; //of only subscribed numbers

        // We can check here what provider to use
        if ($this->provider != null and $this->provider == 'sms_kent') {
            return $this->useSmsKent($messages, $msgContent, $this->senderId);
        }
        if ($messages->count()) {

            $providerUsed = Provider::getproviderOfCountry($messages[0]->country_dial_code_id);
                
        }
                

        $contactNumbs = $messages->pluck('send_to')->toArray();

        $this->smsBatch->provider_id =  $providerUsed->id;
        $this->smsBatch->save();

        $provider = new SmsHandler($providerUsed->name); 
        $sendResp = $provider->commProvider->sendsms($contactNumbs, $msgContent, $this->senderId);

        $this->updateMessagesWithSendAPIResponse($messages,$this->SmsBroadcast,$sendResp,$this->smsBatch);
        
    }

    public function updateMessagesWithSendAPIResponse($messages,$SmsBroadcast,$sendResp,$smsBatch)
    {
        $cnt =0;
        foreach($messages as $message) {
                    $message->message = $SmsBroadcast->message;
                    $message->send_to = '+'.$sendResp['messageResps'][$cnt]['sendTo'];
                    $message->message_id = $sendResp['messageResps'][$cnt]['mId'];
                    $message->status = $sendResp['messageResps'][$cnt]['status'];
                    if($message->status == 'REJECTED'){ 
                        $message->cost = 0;
                        $message->credits = 0; 
                        $message->failed = 1;
                        $message->delivered = 0;
                    }
                    $cnt++;               
        }

        $smsBatch->smsMessages()->saveMany($messages);  

        $smsBatch->status = "ongoing";
        $smsBatch->success  = 1;
        $smsBatch->total_messages = $smsBatch->smsMessages()->count();
        $smsBatch->failed  = 0;
        $smsBatch->message = $SmsBroadcast->message;
        $smsBatch->time_run = Carbon::now();
        $smsBatch->bulk_id = $sendResp['bulkId'];
        $smsBatch->save(); 
    }

    public function useSmsKent($messages, $msgContent, $senderId)
    {
        if ($messages->count()) {

            if($this->manualContacts) {
                $contactNumbs = $this->manualContacts;
            } else {
                foreach($messages as $message) {
                    $numbers[$message->id] = $message->contact->phone;
                }
            }
            
            $sms = new SmsKent();
            $sendResp = $sms->sendsms($contactNumbs, $msgContent, $senderId);  
            $cnt = 0;

            foreach($messages as $message) {
                $message->message = $this->SmsBroadcast->message;
                $message->send_to = '+'.$sendResp['messageResps'][$cnt]['sendTo'];
                $message->message_id = $sendResp['messageResps'][$cnt]['mId'];
                $message->status = $sendResp['messageResps'][$cnt]['status'];
                // if($message->status == 'REJECTED'){ 
                //     $message->cost = 0;
                //     $message->credits = 0;
                //     $message->failed = 1;
                //     $message->delivered = 0;
                // }
                $cnt ++;
            }

            $this->smsBatch->smsMessages()->saveMany($messages);
                     
            $this->smsBatch->status = 'ongoing';
            $this->smsBatch->success  = 1;
            $this->smsBatch->total_messages = $this->smsBatch->smsMessages()->count();
            $this->smsBatch->failed  = 0;
            $this->smsBatch->message = $this->SmsBroadcast->message;
            $this->smsBatch->time_run = Carbon::now();
            $this->smsBatch->bulk_id = $sendResp['bulkId'];
            $this->smsBatch->save();
        
        }

    }


}