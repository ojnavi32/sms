<?php

namespace App\Jobs;

use App\Events\ImportSuccessEvent;
use App\Lists;
use App\Models\User;
use App\Mail\ImportContact;
use App\SmstoMongo;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\Mail;
use MongoDB;

class LastBatch implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $list;
    public $user;
    public $total;

    /**
     * Create a new job instance.
     *
     * @param Lists $list
     * @param User $user
     */
    public function __construct(Lists $list, User $user, $total)
    {
        $this->list = $list;
        $this->user = $user;
        $this->total = $total;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {

        $collection_name = "lists_" . $this->list->id . "_user_" . $this->user->id . "_contacts";

        $collection = (new SmstoMongo)->db()->{$collection_name};

        $this->list->is_importing = 0;
        // $this->list->number_contacts = $collection->count();
        $this->list->save();

        ImportSuccessEvent::dispatch($this->user, $this->list);
        Mail::to($this->user->email)->send(new ImportContact($this->user, $this->total, $this->list->name));
    }
}
