<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Gravatar;
use Propaganistas\LaravelPhone\PhoneNumber;
use App\Scopes\LimitScope;
use DB;
use App\Services\ApiResponse;
use App\Exceptions\CustomException;
use Auth;

class Contact extends Model
{
    use SoftDeletes;

    protected $table = 'contacts';

    protected $fillable = ['user_id', 'name', 'surname', 'email', 'phone', 'photo', 'country_code', 'network_name', 'opt_out', 'opt_out_var'];

    public $sortable = ['id', 'name', 'email', 'network_name', 'country_code', 'created_at', 'updated_at'];

    /**
     * The "booting" method of the model.
     *
     * @return void
     */
    protected static function boot()
    {
        parent::boot();

        static::addGlobalScope(new LimitScope);
    }

    /*function for set the network name and country code to contacts
     *@param contact number
     *@return network name and country code
    */
    public static function setContactNetwork($contact, $user, $save = 0)
    {
        $phoneNumberUtil = \libphonenumber\PhoneNumberUtil::getInstance();
        $phone = gettype($contact) == "object" ? $contact->phone : $contact['phone'];

        $validTypes = [1, 2];

        if (preg_match("~^00\d+$~", $phone)) {
            $phone = preg_replace('/^00?/', "+", $phone);
        }
        try {
            $number = $phoneNumberUtil->parse($phone, null);
            $isValid = $phoneNumberUtil->getNumberType($number);

            if (!in_array($isValid, $validTypes)) {
                $phone = abs($phone);
                if ($user->default_prefix) {
                    $number = $phoneNumberUtil->parse($phone, $user->default_prefix);
                    $isValid = $phoneNumberUtil->getNumberType($number);

                }

            }

            if (in_array($isValid, $validTypes)) {

                $carrierMapper = \libphonenumber\PhoneNumberToCarrierMapper::getInstance();

                $networkName = $carrierMapper->getNameForNumber($number, "en");

                $countryCode = $number->getCountryCode();
                $nationaNumber = $number->getNationalNumber();
                $phone = '+' . $countryCode . $nationaNumber;

                $contact->phone = $phone;
                $contact->network_name = $networkName;
                $contact->country_code = $countryCode;

                if ($save) {
                    $contact->save();
                }
                return $contact;

            }

            return false;

        } catch (\Exception $e) {

        }


    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function listContact()
    {
        // return $this->hasMany(ListContact::class, 'id', 'contact_id');
        return $this->belongsTo(ListContact::class, 'id', 'contact_id');
    }

    public function getListAttribute()
    {
        return $this->listContact->list->name ?: false;
    }

    public function setPhotoAttribute($value)
    {
        if ($value == null) {
            if ($this->email) {
                $this->attributes['photo'] = Gravatar::get($this->email);
            }
        }
    }

    public function setPhoneAttribute($value)
    {
        if ($value != null) {
            $this->attributes['phone'] = (string)PhoneNumber::make($value);
        }
    }

    public function scopeDuplicates()
    {
        return $this->select(DB::raw('count(*) -1 as count, phone'))->havingRaw('COUNT(phone) > 1')->groupby('phone');
    }

    // Scopes
    public function scopeListBy($query, $by = null)
    {
        if ($by) {
            //return $query->where();
        }
    }

    public function scopeContactsSortBy($query, $by = null, $order = 'asc')
    {
        if ($by) {
            return $query->orderBy($by, $order);
        }
    }

    public function scopeSearch($query, $search)
    {
        if ($search != null) {
            return $query->where('name', 'LIKE', '%' . $search . '%')
                ->orWhere('email', 'LIKE', '%' . $search . '%')
                ->orWhere('phone', 'LIKE', '%' . $search . '%');
        }

        return $query;
    }

    public function scopeOrSearch($query, $srchPrm, $value)
    {
        if ($srchPrm != null && $value != null) {
            return $query->orWhere($srchPrm, 'LIKE', '%' . $value . '%');
        }

        return $query;
    }

    public function lists()
    {
        return $this->belongsToMany(Lists::class, 'lists_contacts', 'contact_id', 'list_id');
    }

    public function activeLists()
    {
        return $this->belongsToMany(Lists::class, 'lists_contacts', 'contact_id', 'list_id')->whereNull('lists_contacts.deleted_at');
    }

    public static function convertPhoneNumbersToContacts($request, $user)
    {
        $numbers = explode(',', $request->contacts);
        $contacts = [];

        $numbers = array_map(function ($value) use ($request) {

            if (substr($value, 0, 2) === "00" || substr($value, 0, 1) === "+") {
                return $value;
            } else {
                return '+' . $value;
            }
        }, $numbers);

        if ($request->default_prefix) {
            $request->default_prefix = $request->default_prefix ? $request->default_prefix : null;
        }

        $numbers = array_unique($numbers);

        foreach ($numbers as $number) {
            $number = trim($number);
            $number = str_replace(" ", "", $number);

            if (!empty($number)) {
                $contact = new contact;
                $contact->phone = $number;

                $contact = $contact->setContactNetwork($contact, $request);

                if (!$contact)
                    throw new CustomException(json_encode(['text' => "List contains invalid numbers!", 'invalidNumbers' => $number]));

                $contacts[] = $contact;
            }
        }
        return $contacts;
    }

}