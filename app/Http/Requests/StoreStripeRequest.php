<?php

namespace App\Http\Requests;

// use Illuminate\Foundation\Http\FormRequest;

use Dingo\Api\Http\FormRequest;
use Illuminate\Validation\Rule;
use Auth;

class StoreStripeRequest extends FormRequest {
    // Whether you want to use other status code other than 422.

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize() {
        return auth()->check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules() {
        return [
            'cardNum' => 'required',
            'expMnth' => 'required',
            'expYear' => 'required',
            'cvc' => 'required',
            'amount' => 'required',
        ];
    }
    public function withValidator($validator) {

        $validator->after(function ($validator) {
            if(!Auth::user()->business_address){
                  throw new \Exception('Please enter billing address.');
            }
        });
    }

}
