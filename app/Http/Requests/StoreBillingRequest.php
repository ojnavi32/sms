<?php

namespace App\Http\Requests;

// use Illuminate\Foundation\Http\FormRequest;
use Dingo\Api\Http\FormRequest;
use Illuminate\Validation\Rule;
use App;

class StoreBillingRequest extends FormRequest {

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize() {
        return auth()->check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules() {
        return [
            'business_name' => 'required|max:50',
            'business_address' => 'required',
            'business_postal_code' => 'required',
            'business_country' => 'required',
        ];
    }

    public function withValidator($validator) {

        $validator->after(function ($validator) {
            $this->checkVatNumberValid($validator);
        });
    }

    private function checkVatNumberValid($validator) {

        $vatEu = App::make('App\Services\VatEu');
        $vatNumber = $this->input('business_vat');

        $euCountry = isEUcountry($this->input('business_country'));
        if ($vatNumber && $euCountry && !$vatEu->validateVatNumber($vatNumber)) {
            $validator->errors()->add('business_vat', 'Invalid VAT number');
        }
    }

}
