<?php

namespace App\Http\Requests;

// use Illuminate\Foundation\Http\FormRequest;
use App\Rules\ListFormat;
use Dingo\Api\Http\FormRequest;
use Illuminate\Validation\Rule;

class UploadContactListRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return auth()->check();
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {

        return [
            'list_id.integer' => "Please select a list.",
            'file_name.mimes' => "Please select CSV file type."
        ];
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [

            'list_id' => [
                'integer'
            ],
            'file_name' => [
//                'required|file|mimes:csv,txt'
                'required',
                'file',
                'mimes:csv,txt',
                new ListFormat
            ]
        ];
    }
}
