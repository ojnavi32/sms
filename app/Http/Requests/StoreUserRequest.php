<?php

namespace App\Http\Requests;

//use Illuminate\Foundation\Http\FormRequest;
use Dingo\Api\Http\FormRequest;
use Illuminate\Validation\Rule;

class StoreUserRequest extends FormRequest {

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize() {
        return auth()->check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules() {
        return [
            'first_name' => ['required', 'max:50'],
            'last_name' => ['required', 'max:50'],
        ];
    }

}
