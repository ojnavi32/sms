<?php

namespace App\Http\Middleware;

use Closure;
use Log;
use App\Models\ApiKeyLog;

class ApiLog
{
    public $startTime;
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $this->startTime = microtime(true);

        return $next($request);
    }

    public function terminate($request, $response)
    {
        $endTime = microtime(true);
        ApiKeyLog::create([
            'user_id'=> @auth()->user()->id,
            'url' => $request->fullUrl(),
            'ip_address' => $request->getClientIp(),
            'payload' => json_encode($request->all()),
            'method' => $request->getMethod(),
            'status' => $response->getStatusCode(),
            'status_code' => $response->getStatusCode(),
        ]);
    }
}
