<?php

namespace App\Http\Controllers;
use DB;

use Illuminate\Http\Request;
use App\Models\Rate;
use App\Models\ExchangeRate;

class CostCalculatorController extends Controller
{
    public function __construct()
    {
        
    }

    
    public function getAvailableCountries()
    {
        $providerId = request('provider_id', 1);
        
        $data = [
            'available_countries' => Rate::selectRaw('country_dial_codes.country, country_dial_codes.a2_code')
            			->join('country_dial_codes', 'country_dial_codes.id', '=', 'rates.country_dial_code_id')
                        ->groupBy('country', 'a2_code')
                        ->with(['pRate' => function ($query) use ($providerId) {
                            $query->where('provider_id', $providerId);
                        }])
                        ->get()
        ];
        
        $data['provider_id'] = $providerId;

        return $data;
    }

    public function getLowestRate($country)
    {
        $countryName = getCountryName($country);

        $rate = Rate::select('base_rate')->where('name',$countryName)->max('base_rate');
        $rateInEuro = $rate/43837.8 * 1.21;

        // $providerId = request('provider_id', 1);
         
        return response()->json(['cost_per_sms' => round($rateInEuro, 2)]);
    }
}
