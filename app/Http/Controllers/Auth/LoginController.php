<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use JWTAuth;
use App\Models\NonUser;
use App\Models\UserSession;
use App\User;
use Jenssegers\Agent\Agent;
use Carbon\Carbon;
use Mail;
use App\Mail\VerifyMail;
use App\Models\VerifyUser;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/dashboard';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        // I cant add env value on protected $redirectTo so I just add it here
        // $this->redirectTo = env('LOGIN_REDIRECT'); // redirect
        // We may need to change the $this->redirectTo depending on user type
        
        $this->middleware('guest')->except('logout');
    }
    
    public function login(Request $request)
    {
        $this->redirectTo = route('app.dashboard');
        
        $this->validateLogin($request);

        // If the class is using the ThrottlesLogins trait, we can automatically throttle
        // the login attempts for this application. We'll key this by the username and
        // the IP address of the client making these requests into this application.
        if ($this->hasTooManyLoginAttempts($request)) {
            $this->fireLockoutEvent($request);

            return $this->sendLockoutResponse($request);
        }

        if ($this->attemptLogin($request)) {
            
            // Generate jwt token
            $token = JWTAuth::fromUser(auth()->user());
            
            // Register the original user_id
            session(['original_user_id' => auth()->user()->id]);
            
            // Register the last login date and time. Also the last IP Address
            auth()->user()->last_login_time = Carbon::now();
            auth()->user()->last_ip_address = getUserIp();
            auth()->user()->email_domain = substr(strrchr($request->get('email'), "@"), 1);
            auth()->user()->save();
            
            // Store some user sessions data
            $agent = new Agent();
            UserSession::create([
                'user_id' => auth()->user()->id, 
                'browser' => $agent->browser() . ' on ' . $agent->platform(),
                'location' => getLocation(),
            ]);
            
            return $this->sendLoginResponse($request);
        }

        // If the login attempt was unsuccessful we will increment the number of attempts
        // to login and redirect the user back to the login form. Of course, when this
        // user surpasses their maximum number of attempts they will get locked out.
        $this->incrementLoginAttempts($request);
        
        //
        $user = User::where('email', $request->email)->first();

        if ( ! $user ) {
            NonUser::firstOrCreate(['email' => $request->email, 'country' => getLocation()]);
        }
        
        return $this->sendFailedLoginResponse($request);
    }

    public function authenticated(Request $request, $user)
    { 
        if (!$user->verified) {
            auth()->logout();

            $verifyUser = VerifyUser::create([
            'user_id' => $user->id,
            'token' => str_random(40)
        ]);

            Mail::to($user->email)->send(new VerifyMail($user));
            return back()->with('warning', 'You need to confirm your account. We have sent you an activation link, please check your email.');
        }
        return redirect()->intended($this->redirectPath());
    }
}
