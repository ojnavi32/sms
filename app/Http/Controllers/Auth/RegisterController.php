<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use App\Models\UserSession;
use Jenssegers\Agent\Agent;
use App\Models\VerifyUser;
use Mail;
use App\Mail\VerifyMail;
use App\Mail\YourSmsToPassword;
use App\Mail\WelcomeEmail;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/dashboard';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        // I cant add env value on protected $redirectTo so I just add it here
        //$this->redirectTo = env('LOGIN_REDIRECT'); // redirect
        $this->redirectTo = route('app.dashboard');
        // We may need to change the $this->redirectTo depending on user type
        
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6|confirmed',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return User
     */
    protected function create(array $data)
    { 
        $user = User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'is_get_started' => isset($data['is_get_started'])??0,
            'password' => bcrypt($data['password']),
            'terms_conditions' => $data['terms_conditions'],
            'special_offers' => isset($data['special_offers'])??0,
        ]);

        // Store verify_users data
        $verifyUser = VerifyUser::create([
            'user_id' => $user->id,
            'token' => str_random(40)
        ]);

        Mail::to($user->email)->send(new VerifyMail($user));

        // Store some user sessions data
        $agent = new Agent();
         UserSession::create([
                'user_id' => $user->id, 
                'browser' => $agent->browser() . ' on ' . $agent->platform(),
                'location' => getLocation(),
            ]);

          return $user;
    }

    public function verifyUser($token)
    { 
        $passwordResetlink =  url('/').'/password/reset';
        $verifyUser = VerifyUser::where('token', $token)->first();
        if(isset($verifyUser) ){
            $user = $verifyUser->user;
            if(!$user->verified) {
                $verifyUser->user->verified = 1;
                $verifyUser->user->save();
                $status = "Your e-mail is verified. You can now login.";
                if($user->is_get_started){
                      $status = $this->getStartedEmail($user);
                }
                else{
                    Mail::to($user->email)->send(new WelcomeEmail($user,$passwordResetlink));
                }      
            }else{

                $status = "Your e-mail is already verified. You can now login.";
            }
        }else{
            return redirect('/login')->with('message', "Sorry your email cannot be identified.");
        }
 

        return redirect('/login')->with('status', $status);
    }

    protected function registered(Request $request, $user)
    {
        $this->guard()->logout();
        return redirect('/login')->with('message', 'We sent you an activation link. Check your email and click on the link to verify.');
    }

    public function getStartedRequest(Request $request)
    { 

        $validator = Validator::make($request->all(), [
            'email' => 'required|string|email|max:255|unique:users',
            ]);

        if($validator -> fails()) {
            return \Redirect::back()->with('error', 'Your email is already registered with us,Please login to continue.')
                ->withErrors($validator)
                ->withInput();
        }

        $name = explode("@", $request->email);
        $password = substr(md5(mt_rand()), 0, 7);

        $data['name'] = $name[0];
        $data['email'] = $request->email;
        $data['is_get_started'] = 1;
        $data['terms_conditions'] = 1;
        $data['special_offers'] = 1;
        $data['password'] = bcrypt($password);

        
        $this->create($data);
        return redirect('/')->with('status', 'We sent you an activation link. Check your email and click on the link to verify.');
    }

    public function getStartedEmail($user)
    {
            $passwordResetlink =  url('/').'/password/reset';
            $password = substr(md5(mt_rand()), 0, 7);
            $user->password = bcrypt($password);
            $user->save();
            $user->tempPassword = $password;
            Mail::to($user->email)->send(new YourSmsToPassword($user,$passwordResetlink));
            $status = "Your account is activated. A password sent to your e-mail";
            return $status;
        
    }

}
