<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Mail\ContactUs;
use App\Mail\ContactUsMailToSmsTo;
use Mail;

class ContactUsController extends Controller
{
	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{

	}

	public function showContactUs()
	{
		return view('landing/contact-us/index');
	}

	public function  submitContactUs(Request $request)
	{
    	$validator = \Validator::make($request->all(),[
    		'g-recaptcha-response' => 'required',
    		'name' => 'required|max:255',
            'email' => 'required|email|max:255',
            'phone' => 'required',
            'message' => 'required',
    	]);
	
	if($validator->fails())
		return redirect('/contact-us')
				->withErrors($validator)
				->withInput();

        // Email to staff
        Mail::to('support@sms.to')->send(new ContactUsMailToSmsTo($request->name,$request->email,$request->message,$request->phone));

        // Auto respond email to sender
        Mail::to($request->email)->send(new ContactUs($request->name));
        
        return back()->with('success','Thank you for your interest in SMS.to.We received you inquiry. You will hear from us soon!');


	}

}
