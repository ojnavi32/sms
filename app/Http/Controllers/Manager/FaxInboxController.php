<?php 

namespace App\Http\Controllers\Manager;

use App\Http\Controllers\Controller;
use App\Models\FaxJob;
use App\Models\UserInbox;
use App\Models\Document;
use App\Models\User;
use Illuminate\Http\Request;
use Carbon\Carbon;
use App\Models\FaxInbox;

class FaxInboxController extends Controller
{
    protected $inbox;
    protected $users;

    public function __construct(FaxInbox $inbox, User $users)
    {
        $this->inbox = $inbox;
        $this->users = $users;
    }

    public function getIndex(Request $request)
    {
        $now = Carbon::now();
        $startDate = $request->input('start_date', '2016-02-13');
        $endDate = $request->input('end_date', $now->format("Y-m-d"));

        $faxes = $this->inbox->whereBetween('created_at', [$startDate . ' 00:00:00', $endDate . ' 23:59:59'])->has('user')->orderBy('created_at', 'DESC');

        $status = $request->input('status');

        if ($status) {
            $faxes->whereIn('status', $status);
        }

        $paginationAppends = [
            'page' => $request->page, 
            'start_date' => $startDate, 
            'end_date' => $endDate, 
            'name_email' => $request->name_email
        ];

        $data = [
            'start_date' => $startDate,
            'end_date' => $endDate,
            //'total' => $faxes->byUser($userIds)->count(),
            'total' => '',
            'faxes' => $faxes->paginate(100), // 100
            'paginationAppends' => $paginationAppends,
        ];

        $data['document_name'] = $request->input('document_name');
        $data['status'] = $request->input('status');
        return view('manager.fax_inbox.index', $data);
    }

    public function getView($id)
    {
        $data = ['fax_job' => FaxJob::find($id)];
        return view('manager.fax_jobs.view', $data);
    }

}
