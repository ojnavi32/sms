<?php 

namespace App\Http\Controllers\Manager;

use Redirect;
use DB;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use App\Models\CountryDialCode;
use App\Models\Rate;
use App\Models\RateProviderPriority;
use App\Models\Provider;
use App\Models\ProviderRate;

use Illuminate\Http\Request;

class CountryRatesController extends Controller {

    public function getIndex(Request $request)
    {   
        $coutrySelected=$request->country; 
                   
        $rate = Rate ::when($coutrySelected, function ($query) use ($coutrySelected) {
                    return $query->where('name', 'like', '%' .$coutrySelected. '%');
                }
            )->paginate(15); 

        return view('manager.country_rates.index',['rates' => $rate]); 
        
    }
    public function create()
    {
        
        return view('manager.country_rates.create');
        
    }
    public function postCreate(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'code' => 'required',
            'country' => 'required',
            'cost' => 'required',
            ]);

        if($validator -> fails()) { 
                return \Redirect::back()
                ->withErrors($validator)
                ->withInput();
        }
        $credit = Rate:: create([
        'country_dial_code_id' => $request->code,
        'name' => $request->country,
        'base_rate' => $request->cost,
        'multiplier' => 1.0,
        'plan' => 0
        ]);
        return redirect()->route('manager.country-rates');
    }

    public function edit($id)
    { 
        $rates = Rate::find($id);

        return view('manager.country_rates.edit', ['rates' => $rates]);
    }

    public function update($id, Request $request)
    {
        $validator = Validator::make($request->all(), [
            'code' => 'required',
            'country' => 'required',
            'cost' => 'required',
            ]);

        if($validator -> fails()) { 
                return \Redirect::back()
                ->withErrors($validator)
                ->withInput();
        }
        
        $rate = Rate:: where('id', $id)
        ->update([
        'country_dial_code_id' => $request->code,
        'name' => $request->country,
        'base_rate' => $request->cost
        ]);

        return redirect()->route('manager.country-rates');
    }

    public function postDelete(Request $request, $id)
    { 
            $rate = Rate::find($id);
            if ($rate) {
                $rate->delete();
                return redirect('manager/country-rates')->with('alert_messages',['success'=>['Record deleted!']]);
            }
    }
    
    public function postUpdateProviders($id, Request $request)
    { 
        if ($request->has('action')) {
            switch ($request->input('action'))
            {
                case 'move_up':
                    $rate_provider_priority = RateProviderPriority::find($request->input('rate_provider_priority_id'));
                    $next_rate_provider_priority = RateProviderPriority::whereRateId($id)
                            ->wherePriority($rate_provider_priority->priority - 1)
                            ->increment('priority');
                    $rate_provider_priority->decrement('priority');
                    return Redirect::back();
                    break;

                case 'move_down':
                    $rate_provider_priority = RateProviderPriority::find($request->input('rate_provider_priority_id'));
                    $next_rate_provider_priority = RateProviderPriority::whereRateId($id)
                            ->wherePriority($rate_provider_priority->priority + 1)
                            ->decrement('priority');
                    $rate_provider_priority->increment('priority');
                    return Redirect::back();
                    break;

            }
        }
    }

    public function getproviders($id)
    { 
        $rateProvider = RateProviderPriority::where('rate_id',$id)->with('provider')->orderBy('priority')->get();
        
        return view('manager.country_rates.country_providers',['providers' => $rateProvider,'rate_id' => $id]); 
               
    }

    public function addProviders(Request $request)
    { 
        $providerCount = RateProviderPriority::providerCount($request);

        RateProviderPriority:: create([
        'provider_id' => $request->provider,
        'rate_id' => $request->rate_id,
        'priority' => $providerCount + 1,
        ]);

        return 'Successfully Added';
    }

    public function deleteProviders($id)
    { 
           $providerPriority = RateProviderPriority::find($id);
          
            if ($providerPriority) {
                $providerPriority->delete();

                RateProviderPriority::where('rate_id',$providerPriority->rate_id)
                                ->where('priority','>',$providerPriority->priority)
                                ->decrement('priority');
                return  \Redirect::back()->with('alert_messages',['success'=>['Record deleted!']]);
            }
    } 

    
    public function ratesUpload()
    {
        return view('manager.rates_upload');
    }

    public function rateFileUpload(Request $request)
    {  
        $validator = Validator::make($request->all(), [
            'csv' => 'required',
            ]);

        if($validator -> fails()) {
            return redirect(route('manager.ratesUpload'))
                ->withErrors($validator)
                ->withInput();
        }
        // $provider = Provider::find($request->provider_id);

        $countries = getAllCountries();
        $countries  = array_flip($countries);

        $csvFile = $request->file('csv');
        
        $path = $request->file('csv')->storeAs('csv',str_random(16) . '.txt');

        $fullPath = storage_path('app/'.$path);

        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        
        if (($handle = fopen($fullPath, "r")) !== FALSE) { 
         $cnt =0;
         $dataArray = [];

          while (($data = fgetcsv($handle, 1500, ",")) !== FALSE) {
            if(!$cnt) { $cnt++; continue;}
              try{
                   $countryName = strtoupper($data[0]);
                   $dataArray[] = [
                                'country_dial_code_id' => $countries[$countryName], 'base_rate' => $data[1], 'name' => $data[0], 'plan' => 0, 'multiplier' =>1, 'currency' => 'euro',
                              ];
             
               }catch (\Exception $e) {
                  report($e);

                }
            }
                    Rate::truncate();
                    Rate::insert($dataArray);
                    fclose($handle);
            }

           DB::statement('SET FOREIGN_KEY_CHECKS=1;');

        return redirect()->route('manager.country-rates');
    }


}
