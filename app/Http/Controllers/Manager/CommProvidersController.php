<?php 

namespace App\Http\Controllers\Manager;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\CommProvider;

use Illuminate\Http\Request;

class CommProvidersController extends Controller {

    public function getIndex()
	{
        $data['comm_providers'] = CommProvider::all();
		return view('manager.providers.index', $data);
	}

}
