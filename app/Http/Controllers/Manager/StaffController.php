<?php 

namespace App\Http\Controllers\Manager;

use Redirect;
use App\Http\Controllers\Controller;
use App\Models\CountryDialCode;
use App\Models\User;
use App\Models\CommProvider;
use App\Models\RateProviderPriority;

use Illuminate\Http\Request;

class StaffController extends Controller {

    public function getIndex()
	{
        $data['users'] = User::where('role_id', 1)->get();
        return view('manager.staff.index', $data);
	}

    public function getCreate(Request $request)
	{
        $data = [
            'name' => $request->old('name'),
            'username' => $request->old('username'),
            'email' => $request->old('email'),
        ];
        return view('manager.staff.create', $data);
	}

    public function postCreate(Request $request)
    {
		$this->validate($request, [
			'name' => 'required',
            'username' => 'required',
            'email' => 'required|email|max:255|unique:users',
            'password' => 'required|min:6',
		]);

        $input = $request->except('_token');
        $input['password'] = bcrypt($request->input('password'));
        $input['role_id'] = 1;
        $input['access'] = json_encode($request->input('access', []));

        if (User::create($input)) {
            return redirect('manager/staff')->with('alert_messages',['success'=>['New Staff created!']]);
        }
    }

    public function getUpdate($id)
	{
        if ( ! empty($id) ) {
            $user = User::find($id);
            $data = [
                'name' => $user->name,
                'username' => $user->username,
                'email' => $user->email,
                'access' => $user->access,
            ];

            $data['members_checked'] = in_array('members', json_decode($user->access)) ? "checked" : "";
            $data['vault_checked'] = in_array('vault', json_decode($user->access)) ? "checked" : "";

            return view('manager.staff.update', $data);
        }
	}

    public function postUpdate($id, Request $request)
    {
        if ( !empty($id) ) {
            $this->validate($request, [
                'name' => 'required',
                'username' => 'required',
                'email' => 'required|email',
            ]);

            $user = User::find($id);
            $user->name = $request->input('name');
            $user->username = $request->input('username');
            $user->email = $request->input('email');
            if ($request->input('password') != '') {
                $user->password = bcrypt($request->input('password'));
            }
            
            $user->access = json_encode($request->input('access', []));

            if ($user->save()) {
                return redirect('manager/staff')->with('alert_messages',['success'=>['Staff updated!']]);
            }
        }
    }

    public function postDelete(Request $request)
    {
        if ($request->has('id')) {
            $user = User::find($request->input('id'));
            if ($user->delete()) {
                return redirect('manager/staff')->with('alert_messages',['success'=>['Record deleted!']]);
            }
        }
    }
}
