<?php 

namespace App\Http\Controllers\Manager;

use Redirect;
use DB;
use App\Http\Controllers\Controller;
use App\Models\CountryDialCode;
use App\Models\Rate;
use App\Models\RateProviderPriority;
use App\Models\Provider;
use App\Models\ProviderRate;
use App\Models\SmsBroadcast;

use Illuminate\Http\Request;

class SmsController extends Controller {

    public function index()
	{
        
        $data['broadcasts'] = SmsBroadcast::with('user')->orderBy('created_at', 'desc')->paginate(15);
        
        
        return view('manager.sms.index', $data);
	}

    public function create(Request $request)
	{
        $data = [
            'id' => 0,
            'country_dial_codes' => CountryDialCode::all(),
            'country_dial_code_id' => $request->old('country_dial_code_id'),
            'name' => $request->old('name'),
            'plan' => $request->old('plan'),
            'base_rate' => $request->old('base_rate'),
            'multiplier' => $request->old('multiplier'),
            'provider' => Provider::find(request('provider_id')),
        ];
        return view('manager.rates.create', $data);
	}

    public function postCreate(Request $request)
    {
		$this->validate($request, [
			'country_dial_code_id' => 'required',
            'name' => 'required',
            //'plan' => 'required',
            'base_rate' => 'required',
		]);

        if (Rate::create($request->except('_token'))) {
            return redirect('manager/rates')->with('alert_messages',['success'=>['New rate created!']]);
        }
    }

    public function edit($id)
	{
        if (!empty($id)) {
            $rate = Rate::find($id);
            
            $providerRate = ProviderRate::where('rate_id', $rate->id)->where('provider_id', request('provider_id', 1))->first();
            $data = [
                'id' => $id,
                'country_dial_codes' => CountryDialCode::all(),
                'country_dial_code_id' => $rate->country_dial_code_id,
                'name' => $rate->name,
                'plan' => $rate->plan,
                'base_rate' => $providerRate->base_rate,
                'our_rate' => $providerRate->our_rate,
                'credits' => $providerRate->credits,
                'multiplier' => $providerRate->multiplier,
                // 'rate_provider_priorities' => $rate->provider_priorities()->orderBy('priority')->get(),
                'providersRates' => $rate->providersRate,
                'provider_id' => request('provider_id'),
                'rate_provider_priorities' => [],
                'provider' => Provider::find(request('provider_id')),
            ];
            return view('manager.rates.update', $data);
        }
	}

    public function update($id, Request $request)
    {
        if ($id != '') {
            $this->validate($request, [
                'country_dial_code_id' => 'required',
                'name' => 'required',
                'base_rate' => 'required',
                'our_rate' => 'required',
                'credits' => 'required',
            ]);

            $rate = Rate::find($id);
            
            $rate->country_dial_code_id = $request->input('country_dial_code_id');
            $rate->name = $request->input('name');
            $rate->plan = $request->input('plan');
            $rate->base_rate = $request->input('base_rate');
            $rate->credits = $request->input('credits');
            $rate->multiplier = $request->input('multiplier');
            
            $providerRate = ProviderRate::where('rate_id', $rate->id)
                ->where('provider_id', request('provider_id'))
                ->first();
                
            $providerRate->base_rate = $request->input('base_rate');
            $providerRate->our_rate = $request->input('our_rate');
            $providerRate->credits = $request->input('credits');
            
            if ($providerRate->save()) {
                return redirect('manager/rates?provider_id='. request('provider_id'))
                    ->with('alert_messages', ['success'=> ['Rate updated!']]);
            }
        }
    }

    public function postDelete(Request $request)
    {
        if ($request->has('id')) {
            $rate = Rate::find($request->input('id'));
            if ($rate->delete()) {
                return redirect('manager/rates')->with('alert_messages',['success'=>['Record deleted!']]);
            }
        }
    }
    
    public function getUpdateProviders($id, Request $request)
    {
        $data = [
            'rate' => Rate::find($id),
            'providers' => Provider::pluck('name', 'id')->all(),
        ];
        return view('manager.rates.edit_providers', $data);
    }

    public function postUpdateProviders($id, Request $request)
    {
        if ($request->has('action')) {
            switch ($request->input('action'))
            {

                case 'add':
                    $rate = Rate::find($id);
                    $rate_providers = $rate->provider_priorities();
                    $rate_providers->attach($request->input('rate_provider'), ['priority' => ($rate_providers->count() + 1)]);
                    return Redirect::back();
                    break;

                case 'delete':
                    $rate_provider_priority = RateProviderPriority::find($request->input('rate_provider_priority_id'));
                    if ($rate_provider_priority->delete()) {
                        RateProviderPriority::whereRateId($id)
                                ->where('priority','>',$rate_provider_priority->priority)
                                ->decrement('priority');
                    }
                    return Redirect::back();
                    break;

                case 'move_up':
                    $rate_provider_priority = RateProviderPriority::find($request->input('rate_provider_priority_id'));
                    $next_rate_provider_priority = RateProviderPriority::whereRateId($id)
                            ->wherePriority($rate_provider_priority->priority - 1)
                            ->increment('priority');
                    $rate_provider_priority->decrement('priority');
                    return Redirect::back();
                    break;

                case 'move_down':
                    $rate_provider_priority = RateProviderPriority::find($request->input('rate_provider_priority_id'));
                    $next_rate_provider_priority = RateProviderPriority::whereRateId($id)
                            ->wherePriority($rate_provider_priority->priority + 1)
                            ->decrement('priority');
                    $rate_provider_priority->increment('priority');
                    return Redirect::back();
                    break;

            }
        }
    }

}
