<?php 

namespace App\Http\Controllers\Manager\Payments;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\CountryDialCode;

class VatSettingsController extends Controller
{

	public function getIndex()
	{
		$page_data = [
			'country_vats' => CountryDialCode::whereNotNull('vat')->get(),
		];
		return view('manager.payments.vat_settings.index', $page_data);
	}

	public function getCreate()
	{
		$page_data = [
			'country_dial_codes' => CountryDialCode::whereNull('vat')->get(),
		];
		return view('manager.payments.vat_settings.create', $page_data);
	}

	public function postCreate(Request $request)
	{
		$this->validate($request, [
			'country_dial_code_id' => 'required',
			'vat' => 'required|numeric',
		]);

		$country_dial_codes = CountryDialCode::whereIn('id', $request->input('country_dial_code_id'));
		if ($country_dial_codes->update(['vat' => $request->input('vat')])) {
            return redirect('manager/payments/vat-settings')->with('alert_messages', ['success' => ['VAT set for countries ' . implode(', ', $country_dial_codes->pluck('country')->all()) . '!']]);
		}
	}

	public function getUpdate($cdc_id)
	{
		$page_data = [
			'country_dial_code' => CountryDialCode::find($cdc_id),
		];
		return view('manager.payments.vat_settings.update', $page_data);
	}

	public function postUpdate($cdc_id, Request $request)
	{
		$country_dial_code = CountryDialCode::find($cdc_id);
		$country_dial_code->vat = $request->input('vat');
		if ($country_dial_code->save()) {
            return redirect('manager/payments/vat-settings')->with('alert_messages', ['success' => ['VAT for country ' . $country_dial_code->country . ' updated!']]);
		}
	}

	public function postDelete(Request $request)
	{
		if ($request->has('id')) {
            $country_dial_code = CountryDialCode::find($request->input('id'));
            $country_dial_code->vat = NULL;
            if ($country_dial_code->save()) {
                return redirect('manager/payments/vat-settings')->with('alert_messages', ['success' => ['VAT removed for country ' . $country_dial_code->country . '!']]);
            }
		}
	}

}