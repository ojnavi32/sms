<?php 

namespace App\Http\Controllers\Manager\Payments;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Excel;
use App\Models\PaymentHistory;
use App\Models\PaymentHistoryDetail;
use Illuminate\Http\Request;
use Storage;

class PaymentHistoryController extends Controller
{

    public function getIndex(Request $request)
    {
        if ($request->download or $request->comma) {
            return $this->postDownloadCsv($request);
        }
        $payment_history = PaymentHistory::whereNull('ontrial')->orderBy('created_at', 'DESC');
        $month_start = PaymentHistory::whereNull('ontrial')->orderBy('created_at', 'ASC')->first()->created_at;
        $month_end = $payment_history->first()->created_at;
        $month_range[] = $month_start->format('Y-m');
        while($month_start->format('Ym') <= $month_end->format('Ym')) {
            $month_range[] = $month_start->format('Y-m');
            $month_start->addMonth();
        }
        if ($request->has('month')) {
            $payment_history->where('created_at', 'like', $request->get('month') . '%');
        }
        if ($request->has('pending')) {
            $payment_history->where('status', 'pending');
        }

        return view('manager.payments.payment_history.index', [
            'request' => $request->all(),
            'month_range' => array_combine($month_range, $month_range),
            'payment_history' => $payment_history->paginate(30),
        ]);
    }

    public function postDownloadCsv(Request $request)
    {
        $payment_history = PaymentHistory::whereNull('ontrial')
            ->where('payment_method_id', '!=', 7)
            ->orderBy('created_at', 'DESC');
        if ($request->has('month')) {
            $payment_history->where('created_at', 'like', $request->get('month') . '%');
        }
        $fileText = '';
        $csv_data = [];
        foreach ($payment_history->get() as $payment) {
            $row = [
                'Created At' => $payment->created_at,
                'Name' => $payment->user->name,
                'Email' => $payment->user->email,
                'Payment Method' => $payment->payment_method->name,
                'Amount' => $payment->amount,
                'Paypal Email' => NULL,
                'Cardholder Name' => NULL,
                'Street Address' => NULL,
                'Postal Code' => NULL,
                'Country' => NULL,
            ];

            if ($request->comma) {
                @$country = $payment->user->billing_detail->country_dial_code->country;

                $fileText .= $payment->created_at->format('d/m/y H:i') . ',' . 
                            $payment->user->name . ',' . 
                            $country . ',' . 
                            $payment->payment_method->name . ',' . 
                            $payment->amount . "\n";
            }

            $csv_data[] = array_replace($row, $payment->payment_history_details->pluck('info', 'name')->all());
        }

        if ($request->comma) {
            Storage::put('payments.txt', $fileText);
            return response()->download(storage_path('app/payments.txt'));
        }

        Excel::create('Filename', function($excel) use ($csv_data) {
            $excel->sheet('Sheetname', function($sheet) use ($csv_data) {
                $sheet->fromArray($csv_data);
            });
        })->download('csv');
    }


    public function getView($payment_history_id)
    {
        $payment_history = PaymentHistory::find($payment_history_id);
        if ($payment_history->vat) {
            $tax_amount = round(($payment_history->vat/100) * $payment_history->amount, 2);
            $pay_amount = $payment_history->amount + $tax_amount;
        } else {
            $tax_amount = 0.00;
            $pay_amount = $payment_history->amount;
        }
        $data = [
            'payment' => $payment_history,
            'tax_amount' => $tax_amount,
            'pay_amount' => $pay_amount,
        ];
        return view('manager.payments.payment_history.view', $data);
    }

    public function getAuthorize($id)
    {
        $payment = PaymentHistory::find($id);
        $payment->status = 'authorized';
        $payment->save();

        // We update the cash balance of the user;
        $user = $payment->user;
        $user->cash_balance += $payment->amount;
        $user->save();

        return redirect('manager/payments/payment-history?pending=pending')->with('alert_messages',['success'=>['Fund credited to user account!']]);
    }

    public function getCancel($id)
    {
        PaymentHistoryDetail::where('payment_history_id', $id)->delete();
        PaymentHistory::find($id)->delete();
        return redirect('manager/payments/payment-history?pending=pending')->with('alert_messages',['success'=>['Payment has been cancelled!']]);
    }   

}
