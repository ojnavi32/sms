<?php 

namespace App\Http\Controllers\Manager;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\EmailBanlist;
use Illuminate\Http\Request;

class EmailBanlistController extends Controller
{
    public function getIndex()
    {
        return view('manager.email_banlist.index', [
            'banlist' => EmailBanlist::all(),
        ]);
    }

    public function getCreate(Request $request)
    {
        return view('manager.email_banlist.edit', [
            'match_string' => $request->old('match_string'),
        ]);
    }

    public function postCreate(Request $request)
    {
        $this->validate($request, [
            'match_string' => 'required|min:3',
        ]);

        EmailBanlist::create(['match_string' => $request->input('match_string')]);
        return redirect('manager/email_banlist')->with('alert_messages', ['success' => ['New email ban added!']]);
    }

    public function getUpdate($ban_id)
    {
        $banned = EmailBanlist::find($ban_id);
        return view('manager.email_banlist.update', [
            'match_string' => $banned->match_string,
        ]);
    }

    public function postUpdate($ban_id, Request $request)
    {
        $this->validate($request, [
            'match_string' => 'required|min:3',
        ]);

        $member = EmailBanlist::find($ban_id);
        $member->match_string = $request->input('match_string');
        $member->save();
        return redirect('manager/email_banlist')->with('alert_messages', ['success' => ['Ban record updated!']]);
    }

    public function postDelete(Request $request)
    {
        if ($request->has('id')) {
            $banned = EmailBanlist::find($request->input('id'));
            if ($banned->delete()) {
                return redirect('manager/email_banlist')->with('alert_messages', ['success' => ['Ban record deleted!']]);
            }
        }
    }

}
