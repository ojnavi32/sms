<?php 

namespace App\Http\Controllers\Manager;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;

use App\Models\Rate;
use App\Models\CreditPackage;
use Storage;
use Excel;
use DB;
use Auth;


class CreditPackagesController extends Controller
{
	public function create()
	{
		return view('manager.credits.index');
		
	}

	public function store(Request $request)
	{ 
		$validator = Validator::make($request->all(), [
			'package' => 'required',
			'credit' => 'required',
			'price' => 'required',
			]);

		if($validator -> fails()) {
			return redirect(route('manager.creditsPackages.create'))
				->withErrors($validator)
				->withInput();
		}



		$credit = CreditPackage:: create([
        'name' => $request->package,
        'credits' => $request->credit,
        'price' => $request->price
        ]);

        return redirect()->route('manager.creditsPackages.show');
	}

	public function show()
	{ 
		$credit = CreditPackage :: get();
    	return view('manager.credits.listing', ['credits' => $credit]); 
    }

    public function edit($id)
    {
    	$credit = CreditPackage::find($id);

        return view('manager.credits.edit', ['credits' => $credit]);
    }    

    public function update(Request $request,$id)
    { 
        $validator = Validator::make($request->all(), [
            'package' => 'required',
            'credit' => 'required',
            'price' => 'required',
            ]);

        if($validator -> fails()) {
                return \Redirect::back() 
                ->withErrors($validator)
                ->withInput();
        }

        $credit = CreditPackage:: where('id', $id)
        ->update([
        'name' => $request->package,
        'credits' => $request->credit,
        'price' => $request->price
        ]);

        return redirect()->route('manager.creditsPackages.show');

    }

    public function delete(Request $request,$id)
    { 
        $credit = CreditPackage::findOrFail($id);

        if ($credit) {
            $credit->delete();
         }   
        return redirect()->route('manager.creditsPackages.show');

    }

}