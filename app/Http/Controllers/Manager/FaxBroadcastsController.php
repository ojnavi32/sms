<?php 

namespace App\Http\Controllers\Manager;

use App\Http\Controllers\Controller;
use App\Models\FaxJob;
use App\Models\FaxBroadcast;

use Illuminate\Http\Request;

class FaxBroadcastsController extends Controller
{
    public function getIndex(Request $request)
	{
        $page_data = [
            'fax_broadcasts' => FaxBroadcast::orderBy('created_at', 'DESC')->get(),
        ];
		return view('manager.fax_broadcasts.index', $page_data);
	}

    public function getView($id, Request $request)
    {
        $fax_broadcast = FaxBroadcast::find($id);
        $page_data = [
            'fax_broadcast' => $fax_broadcast,
            'executing_faxes' => $fax_broadcast->fax_jobs()->whereStatus('executed')->count(),
            'failed_faxes' => $fax_broadcast->fax_jobs()->whereStatus('failed')->count(),
            'successful_faxes' => $fax_broadcast->fax_jobs()->whereStatus('success')->count(),
            'fax_jobs' => $fax_broadcast->fax_jobs()->paginate(300),
        ];
		return view('manager.fax_broadcasts.view', $page_data);
    }

    public function getFaxJob($fax_broadcast_id, $fax_id)
    {
        $data = [
            'fax_job' => FaxJob::find($fax_id)
        ];
        return view('manager.fax_jobs.view', $data);
    }

}
