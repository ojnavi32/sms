<?php 

namespace App\Http\Controllers\Manager;

use App\Http\Controllers\Controller;
use App\Models\SmsBroadcast;

use Illuminate\Http\Request;

class SmsBatchController extends Controller {

    public function index($id)
	{
        
        $data['broadcasts'] = SmsBroadcast::with('user', 'broadcastBatches')->find($id);
        
        
        return view('manager.sms.batches.index', $data);
	}

}
