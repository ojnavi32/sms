<?php 

namespace App\Http\Controllers\Manager;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\ExchangeRate;
use Illuminate\Http\Request;

class ExchangeRatesController extends Controller
{

    public function getIndex()
    {
        return view('manager.exchange_rates.index', [
            'exchange_rates' => ExchangeRate::all(),
        ]);
    }

}
