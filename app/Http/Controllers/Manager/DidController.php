<?php 

namespace App\Http\Controllers\Manager;

use App\Http\Controllers\Controller;
use Unirest\Request as VoxBoneRest;
use App\Services\VoxBone;
use Illuminate\Http\Request;

use App\Models\PortingRequest;
use App\Models\PortingPrice;
use Carbon\Carbon;
use Mail;
use Excel;
use App\Services\Payments;

class DidController extends Controller {

	protected $voxbone;
    protected $payments;

	public function __construct(VoxBone $voxbone, Payments $payments)
	{
        $this->voxbone = $voxbone;
        $this->payments = $payments;
	}

    public function numbers($request)
    {
        return $this->numbers
            ->activeNumbers()
            ->doesntHave('payment')
            ->trialExpiredNumbers()
            ->searchNumber($request->input('number'))
            ->where('did', '!=', '')
            ->paginate(200);
    }
	public function getIndex(Request $request)
	{
        $dids = $this->numbers($request);

        return view('manager.dids.index', compact('dids'));
	}

    public function getDownload(Request $request)
    {
        $numbers = $this->numbers($request);
        $csvData = [];
        foreach ($numbers as $number) {
            $name = '';
            $email = '';
            if ($number->user) {
                if ($number->user->name == '') {
                    $name = 'No name';
                } else {
                    $name = $number->user->name;
                }

                $email = $number->user->email;
            }
            $row = [
                'DID ID' => $number->did,
                'Country' => $number->country,
                'Number' => $number->did_number,
                'Name' => $name,
                'Email' => $email,
                'Subscription ID' => $number->subscription_id,
            ];
            $csvData[] = $row;
        }

        // Export the numbers into excel file
        Excel::create('Numbers', function($excel) use ($csvData) {
            $excel->sheet('s', function($sheet) use ($csvData) {
                $sheet->setAutoSize(true);
                $sheet->fromArray($csvData);
            });
        })->download('xls');
    }

}
