<?php 

namespace App\Http\Controllers\Manager;

use App\Http\Controllers\Controller;
use Unirest\Request as VoxBoneRest;
use App\Services\VoxBone;
use Illuminate\Http\Request;

use App\Models\PortingPrice;

class PortingController extends Controller {

	protected $voxbone;
	protected $price;

	public function __construct(VoxBone $voxbone, PortingPrice $price)
	{
		$this->voxbone = $voxbone;
		$this->price = $price;
	}

	public function getIndex(Request $request)
	{	
		$status = $request->input('status', 'active');
		if ($status == 'active') {
			$prices = $this->price->withAmount()->paginate(100);
		}

        return view('manager.porting.index', compact('prices'));
	}

    public function getEdit(Request $request, $id)
    {
        $price = $this->price->find($id);

        return view('manager.porting.edit', compact('price'));
    }

	public function postUpdate(Request $request, $id)
    {
        $price = $this->price->find($id);
        $price->country = $request->country;
        $price->amount = $request->amount;
        
        if ($price->save()) {
            return redirect('manager/porting')->with('alert_messages',['success'=>['Price has been saved!']]);
        }
    }

}
