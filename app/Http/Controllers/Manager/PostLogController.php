<?php 

namespace App\Http\Controllers\Manager;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\PostLog;

class PostLogController extends Controller {

	protected $logs;

	public function __construct(PostLog $logs)
	{
		$this->logs = $logs;
	}

	public function getIndex(Request $request)
	{
		$logs = $this->logs->Braintree($request->input('braintree'))->orderBy('id', 'desc')->paginate(50);
		
        return view('manager.post.logs', compact('logs'));
	}
    
}
