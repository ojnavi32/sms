<?php 

namespace App\Http\Controllers\Manager;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use App\Models\DeleteRequest;
use Carbon\Carbon;
use App\User;
use App\Contact;
use App\ListContact;
use App\Models\NotificationSetting;
use App\Models\SmsBroadcastMessage;
use App\Models\Template;
use App\Models\UserSession;
use App\Models\VerifyUser;
use App\Models\ApiKey;
use App\Lists;
use App\Models\ApiKeyLog;
use App\Models\SocialAccount;
use App\Models\CashHistory;
use App\Models\UserRateAdjustment;
use App\Models\UserPaymentMethod;
use App\Models\UserApiRateAdjustment;
use App\Models\ShortLink;
use App\Models\Payment;
use App\Models\SmsBroadcast;
use App\Models\BillingDetail;

class DeleteRequestController extends Controller
{
	
	public function show()
	{ 
        $deleteRequests =  DeleteRequest:: get();

    	return view('manager.delete_request.listing',['deleteRequests' => $deleteRequests]); 
    }

    public function postDelete($id, Request $request)
    {
        $member = User::find($id);
            
        ListContact::where('user_id',$id)->forceDelete();
        Lists::where('user_id',$id)->forceDelete();
        Contact::where('user_id',$id)->forceDelete();

        NotificationSetting::where('user_id',$id)->forceDelete();
        Payment::where('user_id',$id)->forceDelete();
            
        SmsBroadcastMessage::where('user_id',$id)->forceDelete();

        $broadcasts = SmsBroadcast::where('user_id',$id)->get();
        foreach($broadcasts as $broadcast)
        {
            $broadcast->broadcastBatches()->forceDelete();
        }
            

        SmsBroadcast::where('user_id',$id)->forceDelete();
        Template::where('user_id',$id)->forceDelete();

        UserSession::where('user_id',$id)->forceDelete();

        VerifyUser::where('user_id',$id)->forceDelete();
        ApiKey::where('user_id',$id)->forceDelete();
        ApiKeyLog::where('user_id',$id)->forceDelete();
        SocialAccount::where('user_id',$id)->forceDelete();
        CashHistory::where('client_id',$id)->forceDelete();
        BillingDetail::where('user_id',$id)->forceDelete();
        UserPaymentMethod::where('user_id',$id)->forceDelete();
        ShortLink::where('user_id',$id)->forceDelete();


        $deleteRequest = DeleteRequest::where('user_id',$id)
                                      ->update([
                                             'is_deleted' => 1,
                                             'member_deleted_at' => Carbon::now(),
                                    ]);
        

        if ($member->forceDelete()) {
            return redirect('manager/delete-requests');
        }
       
    }

   
}