<?php 

namespace App\Http\Controllers\Manager;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Setting;

use Cache;

class SettingsController extends Controller
{
    public function getIndex(Request $request)
    {
        return view('manager.settings.index', ['settings' => Setting::pluck('value', 'key')->all()]);
    }

    public function postIndex(Request $request)
    {
        foreach ($request->except('_token') as $key => $val) {
            $setting = Setting::firstOrNew(['key' => $key]);
            $setting->value = $val;
            $setting->save();
            
            //Cache::forget('key');
        }
        
        return redirect('manager/settings/settings')->with('alert_messages', ['success' => ['Settings Updated!']]);
    }
}
