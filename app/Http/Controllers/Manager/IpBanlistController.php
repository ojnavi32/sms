<?php 

namespace App\Http\Controllers\Manager;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\IpBanlist;
use Illuminate\Http\Request;
use Cache;

class IpBanlistController extends Controller
{

    public function getIndex()
    {
        return view('manager.ip_banlist.index', [
            'banlist' => IpBanlist::all(),
        ]);
    }

    public function getCreate(Request $request)
    {
        return view('manager.ip_banlist.edit', [
            'ip_address' => $request->old('ip_address'),
        ]);
    }

    public function postCreate(Request $request)
    {
        $this->validate($request, [
            'ip_address' => 'required|ip',
        ]);

        IpBanlist::create(['ip_address' => $request->input('ip_address')]);

        IpBanlist::storeCache();

        return redirect('manager/ip_banlist')->with('alert_messages', ['success' => ['New IP Address ban added!']]);
    }

    public function getUpdate($id)
    {
        $banned = IpBanlist::find($id);
        return view('manager.ip_banlist.update', [
            'ip_address' => $banned->ip_address,
        ]);
    }

    public function postUpdate($id, Request $request)
    {
        $this->validate($request, [
            'ip_address' => 'required|ip',
        ]);

        $member = IpBanlist::find($id);
        $member->ip_address = $request->input('ip_address');
        $member->save();

        IpBanlist::storeCache();

        return redirect('manager/ip_banlist')->with('alert_messages', ['success' => ['Ban record updated!']]);
    }

    public function postDelete(Request $request)
    {
        if ($request->has('id')) {
            $banned = IpBanlist::find($request->input('id'));
            if ($banned->delete()) {
                IpBanlist::storeCache();

                return redirect('manager/ip_banlist')->with('alert_messages', ['success' => ['Ban record deleted!']]);
            }
        }
    }

}
