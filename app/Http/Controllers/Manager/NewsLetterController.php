<?php 

namespace App\Http\Controllers\Manager;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Services\VerifyEmail;
use App\Models\EmailBanlist;
use Illuminate\Http\Request;
use Excel;

class NewsLetterController extends Controller
{

    public function getIndex()
    {
        return view('manager.newsletter.index', [
            'banlist' => EmailBanlist::all(),
        ]);
    }

    public function getCreate(Request $request)
    {
        return view('manager.newsletter.create', [
            'match_string' => $request->old('match_string'),
        ]);
    }

    public function postCreate(Request $request)
    {
        $reader = Excel::load(storage_path('CY-CONTACTS.xlsx'))
            //->select(array('Email', 'company'))
            ->limit(100)
            ->get();
        $emails = [];
        foreach($reader as $row) {
            if (trim($row->email) != '') {
                $emails[] = trim($row->email);
            }
        }

        foreach ($emails as $email) {
            //$ve = new VerifyEmail($email, 'marios@fax.to');
            $ve = new VerifyEmail($email, 'notifications@github.com');
            // notifications@github.com
            $msg = '';
            $msg = $email . 'verified: ';
            $msg .= ($ve->verify() == true) ? 'Yes' : 'No';
            echo $msg . '<br>';
        }
        
        return;
        $this->validate($request, [
            'match_string' => 'required|min:3',
        ]);

        EmailBanlist::create(['match_string' => $request->input('match_string')]);
        return redirect('manager/email_banlist')->with('alert_messages', ['success' => ['New email ban added!']]);
    }
}
