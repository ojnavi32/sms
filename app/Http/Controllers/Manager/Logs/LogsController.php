<?php 

namespace App\Http\Controllers\Manager\Logs;

use App\Http\Controllers\Controller;
use App\Models\FailedTopupLog;

use Illuminate\Http\Request;

class LogsController extends Controller {

    public function getFailedTopups()
	{
        $failed_topups = FailedTopupLog::orderBy('created_at', 'DESC')->whereNull('error_type')->paginate(10);
        return view('manager.logs.failed_topups', compact('failed_topups'));
	}
	
	public function getFailedSubscriptions()
	{
        $failed_topups = FailedTopupLog::orderBy('created_at', 'DESC')->where('error_type', 1)->paginate(10);
        return view('manager.logs.failed_topups', compact('failed_topups'));
	}
    
}
