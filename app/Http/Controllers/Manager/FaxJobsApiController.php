<?php namespace App\Http\Controllers\Manager;

use Carbon\Carbon;
use App\Http\Controllers\Controller;
use App\Models\FaxJob;

use Illuminate\Http\Request;

class FaxJobsApiController extends Controller
{
    public function getIndex()
    {
        return view('manager.fax_jobs.index', $this->platform());
    }
    
    public function ios()
    {
        return view('manager.fax_jobs.index', $this->platform('ios'));
    }
    
    public function android()
    {
        return view('manager.fax_jobs.index', $this->platform('android'));
    }
    
    public function platform($platform = null)
    {
        $faxJobs = FaxJob::where('status', '<>', 'pending')
                ->byPlatform($platform)
                ->where('job_api', '=', 1)
                ->has('user')
                ->whereDoesntHave('fax_broadcast')
                ->orderBy('created_at', 'DESC')
                ->paginate(100);
                
        $data = [
            'hour_ago' => Carbon::now()->subHour(),
            'fax_jobs' => $faxJobs,
        ];
        
        return $data;
        
    }
}
