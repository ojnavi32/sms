<?php 

namespace App\Http\Controllers\Manager;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;

use App\Models\Currency;


class CurrencyController extends Controller
{
	public function create()
	{
		return view('manager.currency.create');
		
	}

	public function store(Request $request)
	{ 
		$validator = Validator::make($request->all(), [
			'currency' => 'required',
			'title' => 'required',
			'iso_code' => 'required',
            'decimal' => 'required',
            'symbol' => 'required',
            'value' => 'required',
			]);

		if($validator -> fails()) {
			return redirect(route('manager.currency.create'))
				->withErrors($validator)
				->withInput();
		}


		$currency = Currency:: create([
        'currency' => $request->currency,
        'title' => $request->title,
        'iso_code' => $request->iso_code,
        'decimal' => $request->decimal,
        'symbol' => $request->symbol,
        'value' => $request->value
        ]);

        return redirect()->route('manager.currency.show');
	}

	public function show()
	{ 
		$currency = Currency :: get();
    	return view('manager.currency.listing', ['currencies' => $currency]); 
    }

    public function edit($id)
    {
    	$currency = Currency::find($id);

        return view('manager.currency.edit', ['currency' => $currency]);
    }    

    public function update(Request $request,$id)
    { 
        $validator = Validator::make($request->all(), [
            'currency' => 'required',
            'title' => 'required',
            'iso_code' => 'required',
            'decimal' => 'required',
            'symbol' => 'required',
            'value' => 'required',
            ]);

        if($validator -> fails()) { 
                return \Redirect::back()
                ->withErrors($validator)
                ->withInput();
        }
        
        $currency = Currency:: where('id', $id)
        ->update([
        'currency' => $request->currency,
        'title' => $request->title,
        'iso_code' => $request->iso_code,
        'decimal' => $request->decimal,
        'symbol' => $request->symbol,
        'value' => $request->value
        ]);

        return redirect()->route('manager.currency.show');

    }

    public function delete(Request $request,$id)
    { 
        $currency = Currency::findOrFail($id);

        if ($currency) {
            $currency->delete();
         }   
        return redirect()->route('manager.currency.show');

    }

}