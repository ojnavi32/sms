<?php 

namespace App\Http\Controllers\Manager;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Excel;
use App\Models\Payment;
use Illuminate\Http\Request;
use app\User;
use Session;
class PaymentHistoryController extends Controller
{

    public function getIndex(Request $request)
    {
        $payment_history = Payment::with('user')->when('created_at', function ($query) {
                  return $query->orderBy('created_at', 'DESC');
        });

        return view('manager.payment_history.index', [
            'request' => $request->all(),
            'payment_history' => $payment_history->paginate(15),
        ]);

    }

    public function postDownloadCsv(Request $request)
    {
        $payment_history = PaymentHistory::orderBy('created_at', 'DESC');
        if ($request->has('month')) {
            $payment_history->where('created_at', 'like', $request->get('month') . '%');
        }

        $csv_data = [];
        foreach ($payment_history->get() as $payment) {
            $row = [
                'Created At' => $payment->created_at,
                'Name' => $payment->user->name,
                'Email' => $payment->user->email,
                'Payment Method' => $payment->payment_method->name,
                'Amount' => $payment->amount,
                'Paypal Email' => NULL,
                'Cardholder Name' => NULL,
                'Street Address' => NULL,
                'Postal Code' => NULL,
                'Country' => NULL,
            ];
            $csv_data[] = array_replace($row, $payment->payment_history_details->pluck('info', 'name')->all());
        }
        Excel::create('Filename', function($excel) use ($csv_data) {
            $excel->sheet('Sheetname', function($sheet) use ($csv_data) {
                $sheet->fromArray($csv_data);
            });
        })->download('csv');
    }


    public function getView($payment_history_id)
    { 
        $data = [
            'payment' => Payment::find($payment_history_id)
        ];
        return view('manager.payment_history.view', $data);
    }

    public function confirmPayment($id)
    { 
        $payment = Payment::where('is_paid',0)->where('id',$id)->first();
        
        if($payment){            
            $payment->user->cash_balance += $payment->amount;
            $payment->user->save(); 

            $payment->is_paid = 1;
            $payment->save();

            return redirect('manager/members/payments')->with('success','Payment Approved!');            
        }
        return redirect('manager/members/payments')->with('danger','Failed to approve!');  

    }

}
