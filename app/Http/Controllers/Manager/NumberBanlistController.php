<?php 

namespace App\Http\Controllers\Manager;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\NumberBanlist;
use Illuminate\Http\Request;

class NumberBanlistController extends Controller
{

    public function getIndex()
    {
        return view('manager.number_banlist.index', [
            'banlist' => NumberBanlist::all(),
        ]);
    }

    public function getCreate(Request $request)
    {
        return view('manager.number_banlist.edit', [
            'match_string' => $request->old('match_string'),
        ]);
    }

    public function postCreate(Request $request)
    {
        $this->validate($request, [
            'match_string' => 'required|min:3',
        ]);

        NumberBanlist::create(['match_string' => $request->input('match_string')]);
        return redirect('manager/number_banlist')->with('alert_messages', ['success' => ['New number ban added!']]);
    }

    public function getUpdate($ban_id)
    {
        $banned = NumberBanlist::find($ban_id);
        return view('manager.number_banlist.update', [
            'match_string' => $banned->match_string,
        ]);
    }

    public function postUpdate($ban_id, Request $request)
    {
        $this->validate($request, [
            'match_string' => 'required|min:3',
        ]);

        $member = NumberBanlist::find($ban_id);
        $member->match_string = $request->input('match_string');
        $member->save();
        return redirect('manager/number_banlist')->with('alert_messages', ['success' => ['Ban record updated!']]);
    }

    public function postDelete(Request $request)
    {
        if ($request->has('id')) {
            $banned = NumberBanlist::find($request->input('id'));
            if ($banned->delete()) {
                return redirect('manager/number_banlist')->with('alert_messages', ['success' => ['Ban record deleted!']]);
            }
        }
    }
}
