<?php 

namespace App\Http\Controllers\Manager\Charts;

use App\Http\Controllers\Controller;
use DB;
use Carbon\Carbon;
use App\Models\CountryDialCode;
use App\Models\FaxJob;

use Illuminate\Http\Request;


class ChartsController extends Controller {


    public function getHeatMap(Request $request)
	{
        $now = Carbon::now();
        $start_date = $request->input('start_date', $now->format("Y-m-d"));
        $end_date = $request->input('end_date', $now->format("Y-m-d"));
        $countries_fax_counts = CountryDialCode::leftJoin('rates', 'rates.country_dial_code_id', '=', 'country_dial_codes.id')
                ->select([
                        'country_dial_codes.a2_code',
                        'country_dial_codes.country',
                        'fax_jobs.created_at',
                        DB::raw("
                            SUM(if(`fax_jobs`.`status` IN ('executed', 'failed', 'success'), 1, 0)) AS `all_faxes`,
                            SUM(if(`fax_jobs`.`status` = 'success', 1, 0)) AS `successful_faxes`,
                            SUM(if(`fax_jobs`.`status` = 'failed', 1, 0)) AS `failed_faxes`,
                            (SUM(if(`fax_jobs`.`status` = 'success', 1, 0))/SUM(if(`fax_jobs`.`status` IN ('executed', 'failed', 'success'), 1, 0))) * 100 AS `success_rate`,
                            (SUM(if(`fax_jobs`.`status` = 'failed', 1, 0))/SUM(if(`fax_jobs`.`status` IN ('executed', 'failed', 'success'), 1, 0))) * 100 AS `fail_rate`,
                            SUM(`documents`.`total_pages`) `faxes_total_pages`
                        ")
                    ])
                ->leftJoin('fax_jobs', 'fax_jobs.rate_id', '=', 'rates.id')
                ->leftJoin('documents', 'documents.id', '=', 'fax_jobs.document_id')
                ->whereNull("fax_jobs.fax_broadcast_id")
                ->whereBetween("fax_jobs.created_at", [$start_date . ' 00:00:00', $end_date . ' 23:59:59'])
                ->groupBy('country_dial_codes.id')
                ->get();
        //var_dump($countries_fax_counts->toArray()); exit;

        $all_faxes = [['Country', 'Total Faxes']];
        $successful_faxes = [['Country', 'Successful Faxes', 'Success Rate (%)']];
        $failed_faxes = [['Country', 'Failed Faxes', 'Fail Rate (%)']];
        $total_pages = [['Country', 'Total Pages']];
        foreach($countries_fax_counts as $country_fax_count) {
            $all_faxes[] = [
                $country_fax_count['a2_code'],
                (int)$country_fax_count['all_faxes'],
            ];
            $successful_faxes[] = [
                $country_fax_count['a2_code'],
                (int)$country_fax_count['successful_faxes'],
                (int)$country_fax_count['success_rate'],
            ];
            $failed_faxes[] = [
                $country_fax_count['a2_code'],
                (int)$country_fax_count['failed_faxes'],
                (int)$country_fax_count['fail_rate'],
            ];
            $total_pages[] = [
                $country_fax_count['a2_code'],
                (int)$country_fax_count['faxes_total_pages'],
            ];
        }

        $page_data = [
            'start_date' => $start_date,
            'end_date' => $end_date,
            'all_faxes' => json_encode($all_faxes),
            'successful_faxes' => json_encode($successful_faxes),
            'failed_faxes' => json_encode($failed_faxes),
            'total_pages' => json_encode($total_pages),
            'countries_fax_counts' => $countries_fax_counts,
        ];
        return view('manager.charts.heat_map', $page_data);
	}
    
}
