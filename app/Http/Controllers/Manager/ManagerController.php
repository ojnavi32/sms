<?php 

namespace App\Http\Controllers\Manager;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Carbon\Carbon;

use Illuminate\Http\Request;
use App\Models\Rate;
use App\Models\CommProvider;
use App\Models\PaymentHistory;
use App\Models\User;
use App\Models\Provider;
use App\Models\SmsBroadcast;

use App\Services\Stats;
use Storage;
use Excel;
use DB;

class ManagerController extends Controller {

    public $paymentHistory;
    public $dids;
    public $stats;

    public function __construct(PaymentHistory $paymentHistory)
    {
    	$this->paymentHistory = $paymentHistory;
    	//$this->stats = $stats;
    }

    public function getIndex()
	{
		
		$today = \Carbon\Carbon::now()->format('Y-m-d');

		$stats = [
			[
				'title' => 'Total Members',
				'icon' => 'fa fa-users fa-3x text-theme-sm',
				'value' => User::count(),
			],
			[
				'title' => 'Total Providers',
				'icon' => 'fa fa-beer fa-3x text-theme-sm',
				'value' => Provider::count(),
			],
			[
				'title' => 'Total SMS Send',
				'icon' => 'fa fa-fax fa-3x text-theme-sm',
				'value' => SmsBroadcast::count(),
			],
			[
				'title' => 'Total SMS Send Today', 
				'icon' => 'fa fa-fax fa-3x text-theme-sm', 
				'value' => SmsBroadcast::where('created_at', 'LIKE', $today.'%')->count(),  
			]];
        return view('manager.index', ['stats'=>$stats]);
	}

    public function getLogin()
	{
		$data['meta_title'] = 'Login';
        return view('manager.login', $data);
	}

    public function postLogin(Request $request)
	{
		$this->validate($request, [
			'username' => 'required', 
			'password' => 'required',
		]);

		$credentials = $request->only('username', 'password');

		if (auth()->attempt($credentials, $request->has('remember'))) {
			return redirect('manager');
		}

		return redirect()->back()->withErrors(['username' => 'Invalid login']);
	}

	public function logout(Request $request)
	{
		auth()->logout();
        
		return redirect('manager/login');
	}

}
