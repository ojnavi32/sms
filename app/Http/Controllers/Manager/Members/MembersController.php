<?php 

namespace App\Http\Controllers\Manager\Members;

use Illuminate\Filesystem\Filesystem;

use App\Http\Controllers\Controller;
use App\Models\Role;
use App\Models\User;
use App\Models\SmsBroadcast;
use App\Models\PaymentHistory;
use App\Models\Payment;
use App\Models\PaymentHistoryDetail;
use App\Models\EmailBanlist;
use App\Models\BillingDetail;
use App\Models\CountryDialCode;
use App\Models\PaymentMethod;
use Illuminate\Http\Request;
use App\Services\Payments;
use Carbon\Carbon;
use Artisan;
use App\Gateways\UserGateway;
use App\Contact;
use App\ListContact;
use App\Models\NotificationSetting;
use App\Models\SmsBroadcastMessage;
use App\Models\Template;
use App\Models\UserSession;
use App\Models\VerifyUser;
use App\Models\ApiKey;
use App\Lists;
use App\Models\ApiKeyLog;
use App\Models\SocialAccount;
use App\Models\CashHistory;
use App\Models\UserRateAdjustment;
use App\Models\UserPaymentMethod;
use App\Models\UserApiRateAdjustment;
use App\Models\ShortLink;
use URL;
use File;



use Mail;
use App\Mail\WelcomeDeveloper;

// For password resets
use Illuminate\Foundation\Auth\SendsPasswordResetEmails;

class MembersController extends Controller
{
    protected $payments;
    protected $userGateway;
    
    use SendsPasswordResetEmails;

    public function __construct(Payments $payments, UserGateway $userGateway) 
    {
        $this->payments = $payments;
        $this->userGateway = $userGateway;
    }

    public function getIndex(Request $request)
    {
        $data = $this->userGateway->members($request, 'normal');
        return view('manager.members.index', $data);
    }

    public function getiOSMembers(Request $request)
    {
        $data = $this->userGateway->members($request, 'ios');
        return view('manager.members.index', $data);
    }

    public function getAndroidMembers(Request $request)
    {
        $data = $this->userGateway->members($request, 'android');
        return view('manager.members.index', $data);
    }

    public function vatMembers(Request $request)
    {
        $data = $this->userGateway->memberVatnumber($request);
        return view('manager.members.vat.index', $data);
    }

    public function nonMembers(Request $request)
    {
        $data = $this->userGateway->members($request, 'non');
        return view('manager.members.non.index', $data);
    }
    
    public function developerMembers(Request $request)
    {
        $data = $this->userGateway->members($request, 'developer');
        
        return view('manager.members.developer.index', $data);
    }

    public function show($id)
    {   
        $today =  \Carbon\Carbon::now();
        $start = Carbon::now()->startOfMonth();
        $end = Carbon::now();//echo Carbon::now()->subMonth()->month;die();
        $data['member'] = User::find($id);
        $data['smsBroadcasts'] = [];
        $data['smstoday'] =SmsBroadcast::whereDate('created_at', Carbon::today())
                ->where('user_id', $id)->count();
        $data['smsthisMonth'] = SmsBroadcast::whereMonth('created_at',$today->month)->where('user_id', $id)->count();
        $data['smslastMonth'] = SmsBroadcast::whereMonth('created_at','=', Carbon::now()->subMonth()->month)->where('user_id', $id)->count();
        $data['smslifeTime'] = SmsBroadcast::where('user_id', $id)->count();
        $data['changes'] = [];
        $data['payments'] = Payment::with('user','paymentMethod')->when('created_at', function ($query) {
                  return $query->orderBy('created_at', 'DESC');
        })->where('user_id',$id)->get();

        $data['failedTopupLogs'] = [];
        $data['failedSubscriptionsLogs'] = [];//print_r($data);die();
        
        return view('manager.members.show', $data);
        
        
        $dt = Carbon::now();
        $today = $dt->format('Y-m-d');
        $month = $dt->year . '-' . sprintf('%02d', $dt->month);
        $dt->subMonth();
        $lastMonth = $dt->year . '-' . sprintf('%02d', $dt->month);
        $faxes = [];
        $faxJob = $member->fax_jobs()->where('status', '=', 'success');
        $faxes['faxToday'] = $faxJob->where('created_at', 'LIKE', $today . '%')->count();
        $faxJob = $member->fax_jobs()->where('status', '=', 'success');
        $faxes['faxMonth'] = $faxJob->where('created_at', 'LIKE', $month . '%')->count();
        $faxJob = $member->fax_jobs()->where('status', '=', 'success');
        $faxes['faxLastMonth'] = $faxJob->where('created_at', 'LIKE', $lastMonth . '%')->count();
        $faxJob = $member->fax_jobs()->where('status', '=', 'success');
        $faxes['faxLifeTime'] = $faxJob->count();

        $faxes['faxReceivedToday'] = $member->inbox()->where('created_at', 'LIKE', $today . '%')->count();
        $faxes['faxReceivedMonth'] = $member->inbox()->where('created_at', 'LIKE', $month . '%')->count();
        $faxes['faxReceivedLastMonth'] = $member->inbox()->where('created_at', 'LIKE', $lastMonth . '%')->count();
        $faxes['faxReceivedLifeTime'] = $member->inbox()->count();
        
        
        if (request('more')) {
            $faxJobs = $member->fax_jobs()->where('status', '<>', 'pending')->orderBy('created_at', 'DESC')->get();
        } else {
            $faxJobs = $member->fax_jobs()->where('status', '<>', 'pending')->orderBy('created_at', 'DESC')->paginate(10);
        }
        
        $data = [
            'member' => $member,
            'fax_jobs' => $faxJobs,
            'changes' => $member->changes()->get(),
            'numbers' => $member->allNumbers()->get(),
            'faxes' => $faxes,
            'payments' => $member->payment_history()->with('payment_method')
                ->whereNull('ontrial')->orderBy('created_at', 'DESC')->get(),
            'failedTopupLogs' => $member->failed_topup_logs()->whereNull('error_type')->get(),
            'failedSubscriptionsLogs' => $member->failed_topup_logs()->where('error_type', 1)->get(),
            'user_id' => $id,
        ];
        return view('manager.members.view', $data);
    }

    public function edit($id)
    {  
        $member = User::find($id);
        $data['id'] = $id;
        $data['member'] = $member;
        $data['billingDetail'] = $member->billing_detail; 
        return view('manager.members.edit', $data);
    }

    public function getPaymentView($id)
    {
        $data['payment'] = PaymentHistory::find($id);
        return view('manager.payment_history.view', $data);
    }

    public function getCreate(Request $request)
    {
        $data = [
            'name' => $request->old('name'),
            'email' => $request->old('email'),
        ];
        return view('manager.members.create', $data);
    }

    public function postCreate(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|max:255',
            'email' => 'required|email|max:255|unique:users',
            'password' => 'required|confirmed|min:6',
        ]);

        $role = Role::whereSlug('member')->first();
        $user = new User([
            'name' => $request->input('name'),
            'email' => $request->input('email'),
            'password' => bcrypt($request->input('password')),
            'verified' => 1,
        ]);
        $role->users()->save($user);
        $user->billing_detail()->save(new BillingDetail);
        return redirect('manager/members')->with('alert_messages', ['success' => ['New member created!']]);
    }

    public function getUpdate($id)
    {  
        $member = User::find($id);
        $data['member'] = $member;
        $data['billingDetail'] = $member->billing_detail;
        return view('manager.members.update', $data);
    }

    public function postUpdate($member_id, Request $request)
    {
       $this->validate($request, [
            'name' => 'required|max:255',
            'email' => 'required|email|max:255|unique:users,email,' . $member_id,
            'password' => 'nullable|confirmed|min:6',
        ]);

        $member = User::find($member_id);
        $member->name = $request->input('name');
        $member->email = $request->input('email');
        if ( ! empty($request->input('password'))) {
            $member->password = bcrypt($request->input('password'));
        }
        //$member->disable_email = $request->disable_email;
        $member->company_name =$request->input('company');
        $member->first_name = $request->input('firstname');
        $member->last_name = $request->input('lastname');
        $member->business_address = $request->input('street_address');
        $member->business_postal_code = $request->input('postal_code');
        $member->business_vat = $request->input('vat_number');
        $member->business_country = $request->input('country_dial_code_id');
        $member->save();
  
        //$this->payments->saveBillingDetails($request, $member->id);

        return redirect('manager/members')->with('alert_messages', ['success' => ['Member updated!']]);
        
    }

    public function getEditFunds($member_id)
    {
        $member = User::find($member_id);
        $data = [
            'cash_balance' => $member->cash_balance,
        ];

        return view('manager.members.edit_funds', $data);
    }

    public function postEditFunds($member_id, Request $request)
    { 
        $this->validate($request, [
            'cash_balance' => 'required|numeric',
        ]);

        $member = User::find($member_id);

        //Add cash balance to payment
        $payment = new Payment;
        $payment->amount = $request->input('cash_balance');
        $payment->user_id = $member_id;
        $payment->vat = 0;
        $paymentMethodId = PaymentMethod::where('slug','admin')->first();
        $payment->payment_method_id = $paymentMethodId->id;
        $member->cash_balance = $request->input('cash_balance');
        $payment->save();

        $member->save();

        return redirect('manager/members')->with('alert_messages', ['success' => ['Funds updated!']]);
    }

    public function postDelete($id, Request $request)
    {
         
       //if ($request->has('id')) {
            $member = User::find($id);
            
            ListContact::where('user_id',$id)->forceDelete();
            Lists::where('user_id',$id)->forceDelete();
            Contact::where('user_id',$id)->forceDelete();

            NotificationSetting::where('user_id',$id)->forceDelete();
            Payment::where('user_id',$id)->forceDelete();
            
            SmsBroadcastMessage::where('user_id',$id)->forceDelete();

            $broadcasts = SmsBroadcast::where('user_id',$id)->get();
            foreach($broadcasts as $broadcast)
            {
                $broadcast->broadcastBatches()->forceDelete();
            }
            

            SmsBroadcast::where('user_id',$id)->forceDelete();
            Template::where('user_id',$id)->forceDelete();

            UserSession::where('user_id',$id)->forceDelete();

             VerifyUser::where('user_id',$id)->forceDelete();
             ApiKey::where('user_id',$id)->forceDelete();
             ApiKeyLog::where('user_id',$id)->forceDelete();
             SocialAccount::where('user_id',$id)->forceDelete();
             CashHistory::where('client_id',$id)->forceDelete();
             BillingDetail::where('user_id',$id)->forceDelete();
             UserPaymentMethod::where('user_id',$id)->forceDelete();
             ShortLink::where('user_id',$id)->forceDelete();


            if ($member->forceDelete()) {
                return redirect('manager/members')->with('alert_messages', ['success' => ['Record deleted!']]);
            }
       //}
    }

    public function getVerify($member_id)
    {
        $member = User::find($member_id);

        if ($member) {
            $member->verified = 1;
            if ($member->save()) {
                return redirect('manager/members')->with('alert_messages', ['success' => ['User has been verified!']]);
            }
        }
        abort(404);
    }
    
    public function verify(Request $request)
    {
        $user = User::find($request->id);

        if ($user) {
            $user->verified = 1;
            $user->cash_balance = config('payment_gateway.default_cash_balance_developer');
            if ($user->save()) {
                // Email the developer
                Mail::queue(new WelcomeDeveloper($user));
                return $user;
            }
        }
        abort(404);
    }

    public function postBan(Request $request)
    {
        $member = User::find($request->input('id'));
        $member->verified = 2;
        $member->ban_reason = $request->ban_reason;
        $member->date_banned = date('Y-m-d');
        $member->save();
        EmailBanlist::firstOrCreate(['match_string' => $member->email]);
        return redirect('manager/members')->with('alert_messages', ['success' => ['User has been banned!']]);
    }

    public function getTrust($member_id)
    {
        $member = User::find($member_id);

        if ($member) {
            $member->verified = 1;
            $member->trusted = 1;
            $member->temporary_ban = null;
            if ($member->save()) {
                return redirect('manager/members/view/' . $member_id)->with('alert_messages', ['success' => ['User has been trusted!']]);
            }
        }
        abort(404);
    }

    public function getImpersonate($id)
    {
        if (auth()->user()->role->slug != 'super-administrator') {
            return response('Unauthorized.', 401);
        }
        session(['admin_id' => auth()->user()->id]);
        auth()->loginUsingId($id);
        return redirect('member/profile');
    }

    public function getBan($id)
    {
        return $this->banOrUnbanned($id, 2);
    }

    public function getUnban($id)
    {
        return $this->banOrUnbanned($id, 1);
    }

    public function banOrUnbanned($id, $ban = 1)
    {
        $member = User::find($id);

        if ($member) {
            $member->verified = $ban;
            if ($ban == 1) {
                $member->date_banned = null;
                $member->temporary_ban = null;
            }
            if ($member->save()) {
                return redirect('manager/members/view/' . $id)->with('alert_messages', ['success' => ['User has been saved!']]);
            }
        }

        abort(404);
    }

    public function getCreateInvoice($userId)
    {
        $billingDetail = $this->payments->getBillingDetails($userId);

        $user = User::find($userId);

        $data = [
            'billing_detail' => $billingDetail,
            'user_id' => $userId,
            'me' => $user,
            'country_dial_codes' => CountryDialCode::all(),
            'subscriptions' => $user->numbers()->where('subscription_id', '!=', '')->pluck('subscription_id', 'did_number'),
        ];

        return view('manager.payment_history.create', $data);
    }

    public function postCreateInvoice(Request $request, $userId)
    {
        $this->validate($request, [
            'firstname' => 'required',
            'lastname' => 'required',
            'street_address' => 'required',
            'postal_code' => 'required',
            'country_dial_code_id' => 'required',
            'amount' => 'required|numeric',
        ]);

        $requestData['currency'] = 'EUR';
        $requestData['country_dial_code_id'] = $request->input('country_dial_code_id');
        
        $vat = $this->payments->calculateVat($requestData, $request->input('amount'));

        $vat = $vat['vat'];

        $billingDetail = $this->payments->saveBillingDetails($request, $userId);

        $user = User::find($userId);

        $paymentHistory = $user->payment_history()->create([
            'payment_method_id' => $request->input('payment_method_id'),
            'amount' => $request->input('amount'),
            'vat' => $vat,
            'subscription_id' => $request->input('subscription_id'),
        ]);

        if ($request->has('subscription_id') and $request->input('subscription_id') != '') {
            // $number = UserDids::where('subscription_id', $request->input('subscription_id'))->first();
            // if ($number) {
            //     $number->payment_failed = NULL;
            //     $number->failed_reason = null;
            //     $number->save();
            // }
        }

        // If billing details is provided
        if ( ! empty($billingDetail) ) {

            $paymentHistory->payment_history_details()->saveMany([
                new PaymentHistoryDetail(['name' => 'Braintree Transaction ID', 'info' => '']),
                new PaymentHistoryDetail(['name' => 'Name', 'info' => $billingDetail->firstname . ' ' . $billingDetail->lastname]),
                new PaymentHistoryDetail(['name' => 'Street Address', 'info' => $billingDetail->street_address]),
                new PaymentHistoryDetail(['name' => 'Postal Code', 'info' => $billingDetail->postal_code]),
                new PaymentHistoryDetail(['name' => 'Country', 'info' => $billingDetail->country_dial_code->country]),
            ]);
        }

        return redirect('manager/members/view/' . $userId)->with('alert_messages', ['success' => ['Invoice has been saved!']]);
    }

    public function getPaymentEdit($id)
    {
        $payment = PaymentHistory::find($id);
        
        $data = [
            'payment' => $payment,
            'user_id' => $payment->user_id,
            'me' => $payment->user,
            'country_dial_codes' => CountryDialCode::all(),
        ];
        return view('manager.payment_history.edit', $data);
    }

    public function postPaymentEdit(Request $request, $id)
    {
        $this->validate($request, [
            'amount' => 'required|numeric',
        ]);

        $payment = PaymentHistory::find($id);
        $payment->amount = $request->input('amount');
        $payment->vat = $request->input('vat');
        $payment->created_at = $request->input('invoice_date');
        $payment->save();
        
        return redirect('manager/members/view/' . $request->input('user_id'))->with('alert_messages', ['success' => ['Invoice has been saved!']]);
    }
    public function getSettings($id)
    {
        $user = User::select('billing_type')->where('id',$id)->first();
        
        return view('manager.members.settings', ['billingType' => $user->billing_type]);
    }
    public function storeSettings(Request $request,$id)
    {
        $user = User::find($id);
        $user->billing_type = $request->billing_type;
        $user->save();

        return redirect()->route('manager.members.settings', $id)->with('alert_messages', ['success' => ['Setting has been saved!']]);;
    }
    public function getVerified(Request $request,$id)
    {
        $member = User::find($id);
        $member->verified =1;
        $member->save();
        return redirect()->route('manager.members');
    }
    public function downloadMemberDetails(Request $request,$id)
    {
        
        $path = storage_path('app/public').'/user-data/'.$id;
        if(is_dir($path))
        {
            $file = new Filesystem;
            $file->cleanDirectory($path);
        }
        File::makeDirectory($path, $mode = 0777, true, true);
        
        // user personal data
        $userData = User::where('id',$id)->first();

        $userFileName = 'user_data.csv';

        $handle = fopen($path.'/'.$userFileName, 'w+');
        fputcsv($handle, array('Name', 'Email Id', 'Cash Balance', 'ip Address', 'Last Login Time', 'Created Date'));

        fputcsv($handle, array($userData->name, $userData->email, $userData->cash_balance, $userData->last_ip_address, $userData->last_login_time, $userData->created_at ));
        
        // sending history
        $userBroadcastHistorys = SmsBroadcast::where('user_id',$id)->get();

        $handle = fopen($path.'/'.'user_broadcast_history', 'w+');
        fputcsv($handle, array('Name','List Name', 'Status', 'Message', 'Cost', 'Total Messages', 'Manual Contcats', 'Created Date'));

        foreach ($userBroadcastHistorys as $userBroadcastHistory) {

            fputcsv($handle, array($userBroadcastHistory->user->name,isset($userBroadcastHistory->list->name) ? $userBroadcastHistory->list->name:'No List', $userBroadcastHistory->status, $userBroadcastHistory->message, $userBroadcastHistory->cost, $userBroadcastHistory->total_messages, $userBroadcastHistory->manual_contacts, $userBroadcastHistory->created_at));
        }

        // Payment History
        $payment_history = Payment::with('user')->when('created_at', function ($query) {
                  return $query->orderBy('created_at', 'DESC');
        })->where('user_id',$id)->get();


        $handle = fopen($path.'/'.'user_payment_history.csv', 'w+');
        fputcsv($handle, array('Name', 'Payment Method', 'Amount', 'Paid Date'));

       
        foreach ($payment_history as $payment) {

            fputcsv($handle, array($payment->user->name, $payment->paymentMethod->name, $payment->amount, $payment->created_at));
        }

        // User Session Data

        $userSessionDetails = UserSession::where('user_id',$id)->get();

        $userFileName = 'user_session.csv';

        $handle = fopen($path.'/'.'user_session.csv', 'w+');
        fputcsv($handle, array('Name', 'Browser', 'Location', 'Date'));

        foreach ($userSessionDetails as $userSessionDetail) {

            fputcsv($handle, array($userSessionDetail->user->name, $userSessionDetail->browser, $userSessionDetail->locaton, $userSessionDetail->created_at));
        }


        fclose($handle);

         // download as zip
        $files = storage_path('app/public').'/user-data/'.$id;
        \Zipper::make($path.'/'.$userData->name.'_smsto'.'.zip')->add($files)->close();
        return response()->download($path.'/'.$userData->name.'_smsto'.'.zip');
    }
}
