<?php 

namespace App\Http\Controllers\Manager\Members;

use App\Http\Controllers\Controller;
use App\Models\User;
use App\Models\UserCashBalance;
use Illuminate\Http\Request;
use App\Services\Payments;
use Carbon\Carbon;

class NumberController extends Controller
{
    protected $payments;
    protected $users;

    public function __construct(Payments $payments, User $users) 
    {
        // if ( ! can('members')) {
        //     die('Unauthorized!');
        // }

        $this->payments = $payments;
        $this->users = $users;
    }
    
    public function getEdit($memberId, $id)
    {
        $number = UserDids::find($id);

        // Set to active if not yet expired
        if (Carbon::now()->toDateString() < $number->expiration_date) {
            $number->status = 1;
        }

        $data = [
            'user_id' => $memberId,
            'number' => $number,
        ];
        return view('manager.numbers.edit', $data);
    }

    public function postEdit(Request $request, $memberId, $id)
    {
        $number = UserDids::find($id);
        $number->fill($request->all());
        $number->payment_failed = $request->payment_failed;
        $number->save();

        // Monthly recurring sub balance
        $number->user->monthly_cash = $request->input('monthly_cash');
        $number->user->save();

        UserCashBalance::where('user_id', '=', $memberId)->update(['amount' => NULL]);

        $balance = UserCashBalance::where('user_id', '=', $memberId)
                    ->orderBy('id', 'desc')
                    ->first();

        if ($balance) {
            $balance->amount = $request->input('cash_balance_sub');
            $balance->save();
        }

        return redirect('manager/members/view/' . $memberId);
    }
    
    public function getPayBalance($memberId, $id)
    {
        $number = UserDids::find($id);

        $data = [
            'user_id' => $memberId,
            'number' => $number,
        ];
        
        return view('manager.numbers.pay_balance', $data);
    }
    
    public function postPayBalance(Request $request, $memberId, $id)
    {
        $number = UserDids::find($id);
        
        $amountArray = config('voxbone.amount_array');
        $amount = $amountArray[request('contract_type')];
        
         // Get VAT if exist
        $requestData['currency'] = request('currency', 'EUR');
        $requestData['country_dial_code_id'] = $number->user->billing_detail->country_dial_code_id;
        $vat = $this->payments->calculateVat($requestData, $amount);
        
        $details = $this->payments->payUsingBalance($number->user, $amount, $vat, $number->id);
        
        if (@$details['reactivate_balance']) {
            return redirect('manager/members/view/' . $memberId)->with('alert_messages', ['success' => ['Number reactivated!']]);
        }
        
        if (@$details['reactivate_balance_not_enough']) {
            return redirect()->back()->withInput()->withErrors(explode(PHP_EOL, $details['message']));
        }
    }

    public function getRetryCharge($id)
    {
        $number = UserDids::find($id);

        $result = $this->payments->retryCharge($number->subscription_id);

        if ($result !== true) {
            return redirect()->back()->withInput()->withErrors(explode(PHP_EOL, $result));
        }
        
        $memberId = $number->user_id;
        
        return redirect('manager/members/edit-number/' . $memberId . '/' . $id);
    }

    public function getAssign(Request $request, $id)
    {
        $nums = $this->numbers
            ->where('status', 6)
            ->where('did_number', '!=', '')
            ->select('did_number', 'id', 'country')
            ->orderBy('country')
            ->get();

        $numbers = [];

        foreach ($nums as $number) {
            $numbers[$number->id] = $number->country . ' ' . $number->did_number;
        }

        $data = [
            'user_id' => $id,
            'numbers' => $numbers,
        ];

        return view('manager.numbers.assign', $data);
    }

    public function postAssign(Request $request)
    {
        $this->validate($request, [
            'email' => 'required',
        ]);

        $months = 3;

        if ($request->input('contract_type')) {
            // 3m will become 3 or 6 or 12
            $months = str_replace('m', '', $request->input('contract_type'));
            $months = intval($months);
        }

        $carbon = new Carbon();

        $carbon->addMonths($months);

        $expirationDate = $carbon->toDateString();

        $number = UserDids::find($request->input('numbers'));
        $number->fill($request->all());
        $number->expiration_date = $expirationDate;
        $number->save();

        // Monthly recurring sub balance
        $user = $this->users->find($request->input('user_id'));
        $user->monthly_cash = $request->input('monthly_cash');
        $user->save();

        UserCashBalance::where('user_id', '=', $request->input('user_id'))->update(['amount' => NULL]);

        $balance = UserCashBalance::where('user_id', '=', $request->input('user_id'))
                    ->orderBy('id', 'desc')
                    ->first();

        if ($balance) {
            $balance->amount = $request->input('cash_balance_sub');
            $balance->save();
        }

        return redirect('manager/members/view/' . $request->input('user_id'));
    }

}
