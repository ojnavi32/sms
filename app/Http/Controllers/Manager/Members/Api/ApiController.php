<?php 

namespace App\Http\Controllers\Manager\Members\Api;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;

class ApiController extends Controller
{
    protected $users;

    public function __construct(User $users) 
    {
        $this->users = $users;
    }
    
    public function cash(Request $request)
    {
        $user = $this->users->find($request->user_id);
        $user->cash_balance = $request->cash_balance;
        $user->save();
    }
    
    public function allowedPending(Request $request)
    {
        $user = $this->users->find($request->user_id);
        $user->allowed_pending = $request->allowed_pending;
        $user->save();
    }
    
    public function deactivate(Request $request)
    {
        $user = $this->users->find($request->user_id);
        $user->verified = 3; // deactivated, 2 ban, 1 active
        $user->save();
    }
}
