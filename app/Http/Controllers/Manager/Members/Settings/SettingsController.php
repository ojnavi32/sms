<?php 

namespace App\Http\Controllers\Manager\Members\Settings;

use App\Http\Controllers\Controller;
use DB, Uuid;
use App\Models\UserRateAdjustment;
use App\Models\User;
use Illuminate\Http\Request;

class SettingsController extends Controller
{
    public function getIndex($member_id)
    {
        $member = User::find($member_id);
        $pageData = [
            'member' => $member,
            'rate_adjustments' => $member->user_rate_adjustments(),
        ];
        return view('manager.members.settings.index', $pageData);
    }

    public function postIndex($member_id, Request $request)
    {
        $member = User::find($member_id);
        $member->broadcast_enabled = $request->input('broadcast_enabled');
        $member->api_key = $request->input('api_key');
        $member->api_key_send = $request->input('api_key_send');
        $member->api_enabled = $request->input('api_enabled');
        $member->provision_numbers = $request->input('provision_numbers');
        $member->number_cost = $request->number_cost;
        $member->default_provider = env('API_DEFAULT_PROVIDER');
        $member->save();
        return redirect('manager/members')->with('alert_messages', ['success' => ['Member Settings updated!']]);
    }

}
