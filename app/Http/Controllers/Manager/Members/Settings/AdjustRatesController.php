<?php

namespace App\Http\Controllers\Manager\Members\Settings;

use App\Http\Controllers\Controller;
use DB;
use App\Models\Rate;
use App\Models\ProviderRate;
use App\Models\User;
use App\Models\UserRateAdjustment;
use App\Models\UserApiRateAdjustment;
use Illuminate\Http\Request;

class AdjustRatesController extends Controller {

    public function getIndex($memberId, Rate $rates, Request $request) {
        $country_rates = $rates->pluck('name', 'id')->all();
        $pageData = [
            'member' => User::find($memberId),
            'rates_select' => $country_rates,
        ];
        $pageData['adjustments'] = User::find($memberId)->user_rate_adjustments;

        return view('manager.members.settings.adjust_rates.index', $pageData);
    }

    public function getCreate($member_id, Request $request) {
        $pageData = [
            'member' => User::find($member_id),
            'rate' => Rate::find($request->input('rate_id')),
            'carrier_select' => ProviderRate::getCountryNetworks($request->input('rate_id'))
        ];
        return view('manager.members.settings.adjust_rates.create', $pageData);
    }

    public function postCreate($memberId, Request $request) {
        $member = User::find($memberId);
        $adjustmentClass = new UserRateAdjustment;
        $data = [
            'user_id' => $memberId,
            'rate_id' => $request->input('rate_id'),
            'billing_type' => $member->billing_type
        ];
        if($member->billing_type == 'PER_CARRIER') {
            $data['network'] = $request->input('network');
        }
        $userRateAdjustment = $adjustmentClass->where($data)->first();

        if (!$userRateAdjustment) {
            $data['base_rate'] = $request->input('base_rate');
            $data['multiplier'] = 1;
            $userRateAdjustment = $adjustmentClass->create($data);
        } else {
            $userRateAdjustment->base_rate = $request->input('base_rate');
            $userRateAdjustment->save();
        }
        return redirect('manager/members/' . $memberId . '/settings/adjust-rates')->with('alert_messages', ['success' => ['Rate adjustment added']]);
    }

    public function getUpdate($member_id, $user_rate_adjustment_id, Request $request) {
        $adjustment = UserRateAdjustment::find($user_rate_adjustment_id);

        $pageData = [
            'member' => User::find($member_id),
            'carrier_select' => ProviderRate::getCountryNetworks($adjustment->rate_id),
            'user_rate_adjustment' => $adjustment,
        ];
        return view('manager.members.settings.adjust_rates.update', $pageData);
    }

    public function postUpdate($member_id, $user_rate_adjustment_id, Request $request) {
        $adjustmentClass = new UserRateAdjustment;

        $user_rate_adjustment = UserRateAdjustment::find($user_rate_adjustment_id);
        $user_rate_adjustment->base_rate = $request->input('base_rate');
        $user_rate_adjustment->network = $request->input('network');
        $user_rate_adjustment->save();
        return redirect('manager/members/' . $member_id . '/settings/adjust-rates')->with('alert_messages', ['success' => ['Rate adjustment updated']]);
    }

    public function postDelete($member_id, $user_rate_adjustment_id, Request $request) {
        if ($user_rate_adjustment_id) {

            $adjustmentClass = new UserRateAdjustment;
            $user_rate_adjustment = $adjustmentClass::find($user_rate_adjustment_id);
            if ($user_rate_adjustment->delete()) {
                return redirect('manager/members/' . $member_id . '/settings/adjust-rates')->with('alert_messages', ['success' => ['Rate adjustment Deleted']]);
            }
        }
    }

}
