<?php 

namespace App\Http\Controllers\Manager;

use DB;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\CountryDialCode;
use App\Models\ExchangeRate;
use Illuminate\Http\Request;

class CountryDialCodesController extends Controller
{
    public function getIndex()
    {
        return view('manager.country_dial_codes.index', [
            'country_dial_codes' => CountryDialCode::all(),
        ]);
    }

    public function getEditExchangeRate($countryDialCodeId)
    {
        $countryDialCode = CountryDialCode::find($countryDialCodeId);
        $data = [
            'exchange_rates' => ExchangeRate::get(['*', DB::raw("CONCAT('(', `currency_code`, ') ', `currency_name`) AS `currency`")])->pluck('currency', 'id')->all(),
            'exchange_rate' => @$countryDialCode->exchange_rate->id,
        ];
        return view('manager.country_dial_codes.edit_exchange_rate', $data);
    }

    public function postEditExchangeRate($countryDialCodeId, Request $request)
    {
        $this->validate($request, [
            'exchange_rate_id' => 'required|numeric',
        ]);

        $countryDialCode = CountryDialCode::find($countryDialCodeId);
        $countryDialCode->exchange_rate_id = $request->input('exchange_rate_id');
        $countryDialCode->save();
        return redirect('manager/country_dial_codes')->with('alert_messages', ['success' => ['Exchange rate set!']]);
    }

}
