<?php 

namespace App\Http\Controllers\Manager;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Job;

class QueueController extends Controller {

	protected $jobs;

	public function __construct(Job $jobs)
	{
		$this->jobs = $jobs;
	}

	public function getIndex(Request $request)
	{
		$jobs = $this->jobs->paginate(50);
		
        return view('manager.queues.index', compact('jobs'));
	}
    
}
