<?php 

namespace App\Http\Controllers\Manager;

use Carbon\Carbon;
use App\Http\Controllers\Controller;
use App\Models\FaxJob;
use App\Models\Document;
use Illuminate\Http\Request;
use App\Services\FaxHandlers\MonopondFaxHandler;
use PhaxioAPI;

class FaxJobsController extends Controller
{
    public function getIndex(Request $request)
    {
        if ($request->has('document_name')) {
            $ids = Document::with('fax_jobs')
                ->where('filename_uploaded', 'LIKE', '%' . $request->input('document_name') . '%')
                ->whereNotNull('user_id')
                ->pluck('id');

            $fax_jobs = FaxJob::where('status', '<>', 'pending')
                ->where('job_api', '=', 0)
                ->whereIn('document_id', $ids)
                ->has('user')
                ->whereDoesntHave('fax_broadcast')
                ->orderBy('created_at', 'DESC');

        } else {
            $fax_jobs = FaxJob::where('status', '<>', 'pending')
                ->where('job_api', '=', 0)
                ->has('user')
                ->whereDoesntHave('fax_broadcast')
                ->orderBy('created_at', 'DESC');
        }

        $status = $request->input('status');

        if ($status) {
            $fax_jobs->whereIn('status', $status);
        }

        $data = [
            'hour_ago' => Carbon::now()->subHour(),
            'fax_jobs' => $fax_jobs->paginate(100), // 100
        ];
        $data['document_name'] = $request->input('document_name');
        $data['status'] = $request->input('status');
        return view('manager.fax_jobs.index', $data);
    }

    public function getView($id)
    {
        $fax_job = FaxJob::find($id);
        return view('manager.fax_jobs.view', compact('fax_job'));
    }

    public function getResend($id)
    {
        $fax_job = FaxJob::find($id);
        return view('manager.fax_jobs.view', compact('fax_job'));
    }

    public function getGetStatus($fax_job_id)
    {
        // Get the last provider used
        $last_fax_job_transaction = FaxJob::find($fax_job_id)->fax_job_transactions()->orderBy('created_at', 'DESC')->with('comm_provider')->first();
        if ($last_fax_job_transaction) {
            $return = [
                'provider' => $last_fax_job_transaction->comm_provider,
                'transaction_id' => $last_fax_job_transaction->transaction_id,
            ];

            // handle status retrieval based on the provider found
            switch ($last_fax_job_transaction->comm_provider->slug) {
                case 'asterisk-ca-voxbeam':
                case 'asterisk-de-didlogic':
                case 'asterisk-sf-flowroute':
                    $url = 'http://'.$last_fax_job_transaction->comm_provider->url.':9999/doneq.php?jobid='.$last_fax_job_transaction->transaction_id;
                    $response_string = file_get_contents($url);
                    $response = json_decode($response_string, TRUE);
                    $return['response'] = $response;
                    if($response['statuscode'] == 0) {
                        $return['status'] = 'success';
                    } else {
                        $return['status'] = 'failed';
                        $return['message'] = $response['status'];
                    }
                    break;

                case 'phaxio':
                    $response = PhaxioAPI::faxStatus($last_fax_job_transaction->transaction_id);
                    $response_data = $response->getData();
                    if ($response_data) {
                        $return['response'] = $response_data;
                        $recipient_data = current($response_data['recipients']);
                        switch ($recipient_data['status']) {
                            case 'success':
                                $return['status'] = 'success';
                                break;
                            case 'failure':
                                $return['status'] = 'failed';
                                $return['message'] = $recipient_data['error_code'];
                                break;
                        }
                    }
                    break;

                case 'monopond':
                    $response = (new MonopondFaxHandler)->getFaxStatus($last_fax_job_transaction->transaction_id);
                    if ($response) {
                        $result = last($response->faxResults);
                        $return['response'] = $response;
                        switch ($result->result) {
                            case 'success':
                                $return['status'] = 'success';
                                break;
                            case 'blocked':
                            case 'failed':
                                $return['status'] = 'failed';
                                $return['message'] = trans('monopond.errors.'.$result->error->code);
                                break;
                        }
                    }
                    break;

                default:
                    break;
            }
            return $return;
        } else {
            abort(404);
        }
    }

    public function postSetStatus(Request $request)
    {
        $faxJob = FaxJob::find($request->fax_job_id);
        if ($faxJob) {
            $faxJob->status = 'failed';
            $faxJob->save();
        }
        
        return redirect()->to('manager/fax_jobs')
            ->with('alert_messages',['success'=>['Fax Job set to failed.']]);
        //dd($request->all());
    }

}
