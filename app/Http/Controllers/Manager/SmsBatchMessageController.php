<?php 

namespace App\Http\Controllers\Manager;

use App\Http\Controllers\Controller;
use App\Models\SmsBroadcastBatch;
use App\Models\SmsBroadcastMessage;

use Illuminate\Http\Request;

class SmsBatchMessageController extends Controller {

    public function index($broadcastId, $batchId)
	{
        $data['batches'] = SmsBroadcastBatch::with('smsBroadcast')->find($batchId);

        $data['messages'] = SmsBroadcastMessage::where('sms_broadcast_batch_id', $batchId)->get();

        
        return view('manager.sms.batches.messages.index', $data);
	}

    public function status(Request $request)
	{
        
        
	}


}
