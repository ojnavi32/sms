<?php 

namespace App\Http\Controllers\Manager;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\IpBanlist;
use Illuminate\Http\Request;
use DB, Artisan;

class SiteTranslationController extends Controller
{
    public function getIndex()
    {
        $groups = DB::table('ltm_translations')->distinct()->get(['group']);

        return view('manager.translations.index', compact('groups'));
    }

    public function getDeploy($group)
    {
        Artisan::call('translations:export', ['group' => $group]);
        return redirect('manager/translations/lists')->with('alert_messages', ['success' => ['Language file for group ' . $group . ' has been deployed!']]);
    }

}
