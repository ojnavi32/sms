<?php 

namespace App\Http\Controllers\Manager;

use Redirect;
use DB;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use App\Models\CountryDialCode;
use App\Models\Rate;
use App\Models\RateProviderPriority;
use App\Models\Provider;
use App\Models\ProviderRate;
use App\Services\Rates\CarrierRates;
use Illuminate\Http\Request;

class RatesController extends Controller {

    public function getIndex(Request $request)
	{ 
        $rate = CarrierRates::rates($request);
        $providerId = $request->provider_id;

        return view('manager.rates.index',['rates' => $rate,'provider_id' => $providerId]); 
        
	}

    public function edit($id)
	{
        $providerRates = ProviderRate::with('rate')->find($id);

        return view('manager.rates.edit', ['providerRates' => $providerRates]);
	}

    public function update($id, Request $request)
    { 
        $validator = Validator::make($request->all(), [
            'code' => 'required',
            'country' => 'required',
            'network' => 'required',
            'cost' => 'required',
            ]);

        if($validator -> fails()) { 
                return \Redirect::back()
                ->withErrors($validator)
                ->withInput();
        }
        
        $rate = Rate:: where('id', $id)
        ->update([
        'country_dial_code_id' => $request->code,
        'name' => $request->country,
        ]);

        $providerRate = ProviderRate:: where('id', $id)
        ->update([
        'network' => $request->network,
        'base_rate' => $request->cost    
            ]);
        return redirect()->route('manager.rates');
    }
    
    public function getUpdateProviders($id, Request $request)
    {
        $data = [
            'rate' => Rate::find($id),
            'providers' => Provider::pluck('name', 'id')->all(),
        ];
        return view('manager.rates.edit_providers', $data);
    }

    public function postUpdateProviders($id, Request $request)
    { 
        if ($request->has('action')) {
            switch ($request->input('action'))
            {

                case 'add':
                    $rate = Rate::find($id);
                    $rate_providers = $rate->provider_priorities();
                    $rate_providers->attach($request->input('rate_provider'), ['priority' => ($rate_providers->count() + 1)]);
                    return Redirect::back();
                    break;

                case 'delete':
                    $rate_provider_priority = RateProviderPriority::find($request->input('rate_provider_priority_id'));
                    if ($rate_provider_priority->delete()) {
                        RateProviderPriority::whereRateId($id)
                                ->where('priority','>',$rate_provider_priority->priority)
                                ->decrement('priority');
                    }
                    return Redirect::back();
                    break;

                case 'move_up':
                    $rate_provider_priority = RateProviderPriority::find($request->input('rate_provider_priority_id'));
                    $next_rate_provider_priority = RateProviderPriority::whereRateId($id)
                            ->wherePriority($rate_provider_priority->priority - 1)
                            ->increment('priority');
                    $rate_provider_priority->decrement('priority');
                    return Redirect::back();
                    break;

                case 'move_down':
                    $rate_provider_priority = RateProviderPriority::find($request->input('rate_provider_priority_id'));
                    $next_rate_provider_priority = RateProviderPriority::whereRateId($id)
                            ->wherePriority($rate_provider_priority->priority + 1)
                            ->decrement('priority');
                    $rate_provider_priority->increment('priority');
                    return Redirect::back();
                    break;

            }
        }
    }

    public function ratesUpload()
    { 
        return view('manager.rates.rates_upload');
    }

    public function providerRateFileUpload(Request $request)
    { 
        $validator = Validator::make($request->all(), [
            'csv' => 'required',
            'provider_id' => 'required',
            ]);

        if($validator -> fails()) {
            return redirect(route('manager.rates.ratesUpload'))
                ->withErrors($validator)
                ->withInput();
        }
        $provider = Provider::find($request->provider_id);

        $countries = getAllCountries();
        $countries  = array_flip($countries);

        $csvFile = $request->file('csv');
        
        $path = $request->file('csv')->storeAs('csv',str_random(16) . '.txt');

        $fullPath = storage_path('app/'.$path);

        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        
        if (($handle = fopen($fullPath, "r")) !== FALSE) { 
         $cnt =0;
         $dataArray = [];

          while (($data = fgetcsv($handle, 1500, ",")) !== FALSE) {
            if(!$cnt) { $cnt++; continue;}
              try{
                   $countryName = strtoupper($data[1]);
                   $rate = Rate::where('name',$data[1])->first();
                   
                   $dataArray[] = [
                               'network' => $data[0], 'base_rate' => $data[5], 'rate_id' => $rate->id,'provider_id' => $request->provider_id,
                              ];
                   
             
               }catch (\Exception $e) {
                  report($e);

                }
            }

            DB::table('providers_rates')->where('provider_id',$request->provider_id)->delete();
                    // ProviderRate::truncate();
                    ProviderRate::insert($dataArray);
                    fclose($handle);
            }

           DB::statement('SET FOREIGN_KEY_CHECKS=1;');

        return redirect()->route('manager.rates');
    }


}
