<?php 

namespace App\Http\Controllers\Manager;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use App\Models\Provider;

use Illuminate\Http\Request;

class ProviderController extends Controller {

    public function getIndex()
	{
        $data['providers'] = Provider::all();
		return view('manager.providers.index', $data);
	}

	public function edit($id)
	{
        $data['provider'] = Provider::find($id);
        $data['id'] = $id;

		return view('manager.providers.edit', $data);
	}

	public function create()
	{
		return view('manager.providers.create');
		
	}

	public function store(Request $request)
	{ 
		$validator = Validator::make($request->all(), [
			'name' => 'required',
			'slug' => 'required',
			'url'  => 'required',
			'currency' => 'required',
			]);

		if($validator -> fails()) {
			return redirect(route('manager.providers.create'))
				->withErrors($validator)
				->withInput();
		}

		$provider = Provider:: create([
        	'name' => $request->name,
        	'slug' => $request->slug,
        	'url' => $request->url,
        	'active' => 1,
        	'currency' => $request->currency, 
        ]);

        return redirect()->route('manager.providers');
	}

	public function update($id)
	{
        $provider = Provider::find($id);

        $provider->name = request('name');
        $provider->slug = request('slug');
        $provider->sms_second = request('sms_second');
        $provider->save();

        return redirect()->route('manager.providers');
	}

	public function delete(Request $request,$id)
    { 
        $provider = Provider::findOrFail($id);

        if ($provider) {
            $provider->delete();
         }   
        return redirect()->route('manager.providers');

    }
}
