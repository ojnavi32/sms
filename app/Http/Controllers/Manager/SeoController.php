<?php 

namespace App\Http\Controllers\Manager;

use App\Http\Controllers\Controller;
use App\Models\Seo;
use Illuminate\Http\Request;
use App\Gateways\UserGateway;

class SeoController extends Controller
{
    protected $seo;

    public function __construct(Seo $seo) 
    {
        $this->seo = $seo;
    }

    public function index(Request $request)
    {
        $data['rows'] = $this->seo->lang(request('locale', 'en'))->paginate(300);
        return view('manager.seo.index', $data);
    }

    public function update(Request $request)
    {
        return $this->seo->find($request->id);
    }
}
