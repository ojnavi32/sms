<?php 

namespace App\Http\Controllers\Manager;

use App\Http\Controllers\Controller;
use App\Models\FaxJob;
use App\Models\UserInbox;
use App\Models\Document;
use App\Models\User;
use Illuminate\Http\Request;
use Carbon\Carbon;
use App\Repositories\Contracts\InboxRepositoryInterface;

class FaxReceivedController extends Controller
{
    protected $inbox;
    protected $users;
    protected $numbers;

    public function __construct(InboxRepositoryInterface $inbox, User $users)
    {
        $this->inbox = $inbox;
        $this->users = $users;
    }

    public function getIndex(Request $request)
    {
        $now = Carbon::now();
        $startDate = $request->input('start_date', '2016-02-13');
        $endDate = $request->input('end_date', $now->format("Y-m-d"));

        $faxes = $this->inbox->findBetween($startDate, $endDate);
        $userIds = null;
        if ($request->has('name_email')) {
            $userIds = $this->users->whereRaw("CONCAT(`name`, ' ', `email`) LIKE ?", ['name_or_email' => "%".$request->name_email ."%"])->pluck('id');
            
            if ($userIds->count() == 0) {
                $userIds = null;
                // We search for number
                $numbers = $this->numbers->where('did_number', $request->name_email)->first();

                if ($numbers) {
                    $faxes->byNumber($numbers->did_number);
                }
            }
        } else {
            $faxes = $this->inbox->findBetween($startDate, $endDate);
        }

        $status = $request->input('status');

        if ($status) {
            $faxes->whereIn('status', $status);
        }
        $paginationAppends = [
            'page' => $request->page, 
            'start_date' => $startDate, 
            'end_date' => $endDate, 
            'name_email' => $request->name_email
        ];

        $data = [
            'start_date' => $startDate,
            'end_date' => $endDate,
            'total' => $faxes->byUser($userIds)->count(),
            'faxes' => $faxes->byUser($userIds)->paginate(100), // 100
            'paginationAppends' => $paginationAppends,
        ];

        $data['document_name'] = $request->input('document_name');
        $data['status'] = $request->input('status');
        return view('manager.fax_received.index', $data);
    }

    public function getView($id)
    {
        $data = [
            'fax_job' => FaxJob::find($id)
        ];
        return view('manager.fax_jobs.view', $data);
    }

}
