<?php 

namespace App\Http\Controllers\Manager;

use App\Http\Controllers\Controller;
use Unirest\Request as VoxBoneRest;
use App\Services\VoxBone;
use Illuminate\Http\Request;

use App\Models\UserDids;
use App\Models\User;
use App\Models\PortingRequest;
use App\Models\Subscription;
use App\Models\RegulationAddress;
use Carbon\Carbon;
use Mail;
use App\Mail\NumberOrderSuccessful;
use App\Mail\PortingSuccessful;
use App\Mail\PortingInProgress;
use App\Mail\FailedCharge;
use Excel, Storage;
use App\Services\Payments;
use Braintree_Subscription;

class VaultController extends Controller {

	protected $voxbone;
	protected $userDids;
    protected $payments;

	public function __construct(VoxBone $voxbone, UserDids $userDids, Payments $payments)
	{
        $this->voxbone = $voxbone;
		$this->userDids = $userDids;
        $this->payments = $payments;
	}
    
	public function getIndex(Request $request)
	{
        $dids = $this->getNumbers($request);
        
        return view('manager.vault.index', compact('dids'));
	}
    
    public function api(Request $request)
    {
        $dids = $this->getNumbers($request, 1);

        return view('manager.vault.api', compact('dids'));
    }
    
    public function getNumbers($request, $api = null)
    {
        $paginate = 50;
        $status = $request->input('status', 'active');
       
        if ($request->search) {
            if ($request->tags) {
                $status = $request->tags;
            }
        }
        
        return $this->userDids
            ->byStatus($status)
            ->ApiNumbers($api)
            ->searchNumber($request->number)->with('user')
            ->orderBy('expiration_date', request('sort', 'asc'))
            ->paginate($paginate);
        
    }

    public function getDetails($id)
    {
        $dids = $this->userDids->with('portingRequest')->find($id);
        $details = $dids->portingRequest;

        return view('manager.vault.view', compact('dids', 'details'));
    }

    public function postDetails($id, Request $request)
    {
        $dids = $this->userDids->find($id);
        if ($request->has('ongoing')) {
            $dids->ongoing = 1;
            $dids->save();
            Mail::queue(new PortingInProgress($dids));
            return redirect('manager/vault?status=pending')->with('alert_messages',['success'=>['The number is set to ongoing']]);
        }
        $details = PortingRequest::find($dids->porting_request_id);
        if ($dids) {
            // May need to change the expiration date of the number
            $countryCode = filter_var($dids->country, FILTER_SANITIZE_NUMBER_INT);
            // $didNumber = $countryCode . $dids->area_code . $dids->did_number;
            //$didNumber = $dids->did_number;
            
            // $n = $this->voxbone->getSingleNumber('%25' . $dids->did_number);
            
            // Check the number in voxbone to get the did_group_id and did

            // Get the didid if exists
            $dids->status = 1;
            $dids->ongoing = null;
            // $dids->did_group_id = null; // todo
            // $dids->did = null; // todo
            //$dids->did_number = $didNumber;
            $dids->save();

            Mail::queue(new PortingSuccessful($dids));

            return redirect('manager/vault?status=pending')->with('alert_messages',['success'=>['The number is now active']]);
        }
        return view('manager.vault.view', compact('details'));
    }

    // Remove the number totally to the user
	public function postDisconnect(Request $request)
    {
        if ($request->has('id')) {
            $did = $this->userDids->find($request->input('id'));
            if ( ! $did) {
                return redirect('manager/vault?status=active')->with('alert_messages',['success'=>['Record could not found!']]);
            }
            $did->status = 2; // 2 for disconnected / cancelled
            $did->admin_cancelled = 1; // cancelled by admin
            $did->expiration_date = date('Y-m-d'); // expired immediately
            // Cancel subscription
            $this->payments->cancelSubscription($did->subscription_id);

            if ($did->save()) {
                // If disconnected from user view
                if ($request->has('user_id')) {
                    return redirect('manager/members/view/' . $request->input('user_id'))->with('alert_messages',['success'=>['Record disconnected!']]);
                }
                return redirect('manager/vault?status=active')->with('alert_messages',['success'=>['Record disconnected!']]);
            }
        }
    }

    // Just cancel subscription
    public function postCancelSubscription(Request $request)
    {
        if ($request->has('id')) {
            $did = $this->userDids->find($request->input('id'));
            // Cancel subscription
            $this->payments->cancelSubscription($did->subscription_id);

            if ($did->save()) {
                // If disconnected from user view
                if ($request->has('user_id')) {
                    return redirect('manager/members/view/' . $request->input('user_id'))->with('alert_messages',['success'=>['Subscription Cancelled!']]);
                }
                return redirect('manager/vault?status=active')->with('alert_messages',['success'=>['Subscription Cancelled!']]);
            }
        }
    }

    public function postEmail(Request $request)
    {
        if ($request->has('id')) {
            $did = $this->userDids->find($request->input('id'));
            $email = $did->user->email;
            $emails[] = $email;
            $emails[] = 'support@fax.to';
            Mail::queue(new FailedCharge($emails, $did->did_number));
            if ($did->save()) {
                return redirect('manager/vault?status=active')->with('alert_messages',['success'=>['Email has been sent!']]);
            }
        }
    }

    public function postReactivate(Request $request)
    {
        if ($request->has('id')) {
            $did = $this->userDids->find($request->input('id'));
            $did->status = 1; // 1 for activate
            if ($did->save()) {
                return redirect('manager/vault?status=active')->with('alert_messages',['success'=>['Record reactivated!']]);
            }
        }
    }
    
    public function postReconnect(Request $request)
    {
        if ($request->has('id')) {
            $did = $this->userDids->find($request->input('id'));
            $did->status = 5; // expired
            if ($did->save()) {
                return redirect('manager/vault?status=deleted')->with('alert_messages',['success'=>['Record Reconnected!']]);
            }
        }
    }
    
    public function postDelete(Request $request)
    {
    	if ($request->has('id')) {
            $did = $this->userDids->find($request->input('id'));
            if ($request->input('permanent') == 1) {
            	$did->status = 3; // 3 permanent delete
            	$message = 'Record has been permanently deleted!';

            	// Remove the number to voxbone also
            	$result = $this->voxbone->cancelDid($didIds = array($did->did));
            } else {
            	$did->deletion_date = Carbon::now()->addDays(30)->toDateString();
            	$message = 'Record set for deletion!';
            }
            if ($did->save()) {
                return redirect('manager/vault?status=expired')->with('alert_messages',['success'=>[$message]]);
            }
        }
    }

    public function postFree(Request $request)
    {
        if ($request->has('id') or $request->has('expired_ids')) {
            
            if ($request->has('expired_ids')) {
                $this->userDids->whereIn('id', $request->input('expired_ids'))->update(['status' => 6]);
                $message = 'DIDs are now free!';
                return redirect('manager/vault?status=expired')->with('alert_messages',['success'=>[$message]]);
            }

            $did = $this->userDids->find($request->input('id'));

            $did->status = 6; // 6 free
            $message = 'DID is now free!';

            // Remove the number to voxbone also
            if ($did->save()) {
                return redirect('manager/vault?status=expired')->with('alert_messages',['success'=>[$message]]);
            }
        }
    }

    public function postDeleteNumber(Request $request)
    {
        if ($request->has('id')) {

            $did = $this->userDids->find($request->input('id'))->delete();
            
            return redirect('manager/vault?status=expired')->with('alert_messages',['success'=>['DID has been deleted!']]);
        }
    }

    public function postAuthorize(Request $request)
    {
        if ($request->has('id')) {
            $did = $this->userDids->find($request->input('id'));

            // Check if we have free did in the vault
            $free = $this->userDids->where('status', 6) // 6 is free
                 ->where('did_group_id', $did->did_group_id)
                 ->first();
                 
            // Need to check if that free number is in the voxbone inventory
            $inInventory = true;
            if ($free) {
                $didNumber = str_replace('+', '', $free->did_number);
                $didNumber = '%25' . $didNumber;
                $numbersResult = $this->voxbone->getSingleNumber($didNumber);
                // If the number is not in inventory
                if (count($numbersResult) == 0) {
                    $inInventory = false;
                }
            }
            
            if ($free and $inInventory) {
                $numbers[0]['didid'] = $free->did;
                $numbers[0]['number'] = $free->did_number;
            } else {
                $numbers = $this->voxbone->provision($did->did_group_id);
            }
            
            if ( ! $numbers ) {
                return redirect()->back()->withInput()->withErrors(explode(PHP_EOL, 'Failed to authorize! ' . $this->voxbone->errorMessage));
            }

            $didid = $numbers[0]['didid'];
            $didNumber = $numbers[0]['number'];
            $did->did = $didid;
            $did->did_number = $didNumber;
            $did->status = 1; // 1 for activate
            if ($did->save()) {
                if ($free) {
                    $this->userDids->find($free->id)->delete();
                }

                // Update subscriptions table
                $subscription = Subscription::where('braintree_subscription_id', $did->subscription_id)->first();

                if ($subscription) {
                    $subscription->did = $did->did;
                    $subscription->save();
                }

                session()->forget('cart_id');
                $carbon = new Carbon();
                $carbon->addMonth();
                $expirationDate = $carbon->toDateString();
                $user = User::find($did->user_id);

                $allowanceAmount = 9;
                // Add new cash balance
                $user->userCashbalance()->create([
                    'amount' => $allowanceAmount,
                    'month' => date('m'),
                    'year' => date('Y'),
                    'subscription_id' => $did->subscription_id,
                    'expiration_date' => $expirationDate,
                    'status' => 'Active',
                ]);

                // We then email the user
                Mail::queue(new NumberOrderSuccessful($did));
                return redirect('manager/vault?status=pending')->with('alert_messages',['success' => ['Record Activated!']]);
            }
        }
    }

    public function postDecline(Request $request)
    {
        if ($request->has('id')) {
            $did = $this->userDids->find($request->input('id'));
            
            if ($did) {
                // Cancel subscription
                $this->payments->cancelSubscription($did->subscription_id);
                if ($did->delete()) {
                    // We then email the user
                    return redirect('manager/vault?status=pending')->with('alert_messages',['success' => ['Number Declined!']]);
                }
            }
        }
    }

    public function postDownloadCsv(Request $request)
    {
        // 1 active, 2 non active, 3 delted
        $statuses = [1 => 'Active', 2 => 'Inactive', 3 => 'Deleted'];
        $csvData = [];
        $stat = $request->status;
        //foreach ($statuses as $status => $label) {
            //$dids = UserDids::orderBy('expiration_date')->where('status', '!=', 4)->where('status', '!=', 5);
            //if ($request->status == 'deleted') {
            $dids = $this->userDids->byStatus($request->status)->orderBy('expiration_date')->get();
            //}
            //$dids->where('status', $status);
            $csvData = [];
            foreach ($dids as $did) {
                $ownerName = '';
                if ($did->user) {
                    $ownerName = $did->user->name;
                }
                $row = [
                    'ID' => $did->id,
                    'Name' => $did->name,
                    'DID Number' => $did->did_number,
                    'Owner Name' => $ownerName,
                    'Email' => $did->email,
                    'Expiration Date' => $did->expiration_date,
                    'Created At' => $did->created_at,
                ];
                $csvData[] = $row;
            }
        //}
        Excel::create('Numbers', function($excel) use ($csvData, $stat) {
            //foreach ($statuses as $status => $label) {
                $excel->sheet($stat, function($sheet) use ($csvData) {
                    $sheet->setAutoSize(true);
                    $sheet->fromArray($csvData);
                });
            //}
        })->download('xls');
    }

    public function getRegulation($id)
    {
        $dids = UserDids::find($id);
        $address = RegulationAddress::find($dids->address_id);

        return view('manager.vault.regulation', compact('dids', 'address'));
    }

    public function postRegulation($id, Request $request)
    {
        $dids = UserDids::find($id);
        $address = RegulationAddress::find($dids->address_id);
        $address->didType = $address->did_type;
        $addressId = $this->voxbone->createRegulationAddress($address);

        if ($address) {
            $file = storage_path('app/documents/' . $address->user_id . '/' . $address->filename);
            $this->voxbone->addressProof($addressId, $file); //  Address proof
            $this->voxbone->verify($addressId); // submit for verification
            $dids->address_id_submitted = 1;
            $dids->save();

            $address = RegulationAddress::find($dids->address_id);
            $address->address_id = $addressId;
            $address->save();
            return redirect('manager/vault/regulation/' . $id)->with('alert_messages',['success'=>['Address has been submitted!']]);
        }
    }

    public function getRegulationFile($id)
    {
        $address = RegulationAddress::find($id);
        $file = Storage::get('documents/' . $address->user_id . '/' . $address->filename);
        $mime = Storage::mimeType('documents/' . $address->user_id . '/' . $address->filename);
        return response($file, 200)->header('Content-Type', $mime);
    }
    
    public function getRegulationFilename(Request $request, $filename)
    {
        $number = UserDids::where('proof_filename', $filename)->first();
        if ($request->last_invoice) {
            $number = UserDids::where('last_invoice', $filename)->first();
        }
        return response()->download(storage_path('app/documents/' . $number->user_id . '/' . $filename));
    }
}
