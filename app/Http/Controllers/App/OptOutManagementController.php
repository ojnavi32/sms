<?php

namespace App\Http\Controllers\App;


use App\Lists;
use App\Models\User;
use App\SmstoMongo;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use MongoDB;

class OptOutManagementController extends Controller
{
    public function optout($optout_string)
    {
        $optout_array = explode("l", explode("u", $optout_string)[1]);

        $user = User::find($optout_array[0]);
        $list = Lists::where('user_id', $user->id)->find($optout_array[1]);
        if (!is_null($user) || !is_null($list)) {
            if ($list->user_id !== $user->id) {
                abort(404);
            }
        } else {
            abort(404);
        }

        return view('app.optout', [
            'user_id' => $user->id,
            'list_id' => $list->id
        ]);
    }

    public function optoutPost(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'phonenumber' => 'phone:AUTO|required',
            'user_id' => 'required|exists:users,id',
            'list_id' => 'required|exists:lists,id'
        ]);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput();
        }

        $collection_name = "lists_" . $request->list_id . "_user_" . $request->user_id . "_contacts";
        $collection = (new SmstoMongo)->db()->{$collection_name};

        $updateData = $collection->updateOne(
            [
                'phone' => $request->phonenumber
            ],
            [
                '$set' => [
                    'optedout' => true,
                    'optout_date' => Carbon::now()
                ]
            ]
        );

        if ($updateData->isAcknowledged()) {
            // Your number has been opted out successfully. You will not receive anymore SMS from us
            return redirect()->back()->with([
                'optout_success_message' => 'Your number has been opted out successfully. You will not receive anymore SMS from us.'
            ]);
        }

        return response();
    }
}
