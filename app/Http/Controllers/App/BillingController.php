<?php

namespace App\Http\Controllers\App;

use App\Http\Controllers\Controller;

use Illuminate\Http\Request;

class BillingController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function index()
    {
        return view('app.billing.index');
    }
    
    public function payment()
    {
        return view('app.billing.payment');
    }
    
    public function add()
    {
        return view('app.billing.add');
    }
    
    public function addCc()
    {
        return view('app.billing.add-cc');
    }
    
    public function addPaypal()
    {
        return view('app.billing.add-paypal');
    }
}
