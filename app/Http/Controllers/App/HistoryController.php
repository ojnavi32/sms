<?php

namespace App\Http\Controllers\App;

use App\Http\Controllers\Controller;

use Illuminate\Http\Request;

class HistoryController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function index()
    {
        return view('app.history.index');
    }
    
    public function resend()
    {
        return view('app.history.resend');
    }
    
    public function download()
    {
        return view('app.history.download');
    }
    
    public function report()
    {
        return view('app.history.report');
    }
}
