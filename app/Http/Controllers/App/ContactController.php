<?php

namespace App\Http\Controllers\App;

use App\Http\Controllers\Controller;

use Illuminate\Http\Request;

use Dingo\Api\Routing\Helpers;

use JWTAuth;

class ContactController extends Controller
{
    use Helpers;
    
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function index()
    {
        $token = JWTAuth::fromUser(auth()->user());
        //$token = JWTAuth::fromUser(auth()->user());
        //$rows = $this->api->get('api/v1/lists?token=' . $token);
        
        $data['contacts'] = $this->api->be(1)->get('contacts');
        $data['lists'] = $this->api->be(1)->get('lists');
        
        return view('app.contacts.index', $data);
    }
    
    public function contactNew()
    {
        return view('app.lists.contacts.new');
    }
    
    public function contactEdit()
    {
        return view('app.lists.contacts.edit');
    }
    
    public function import()
    {
        return view('app.lists.contacts.import');
    }
    
    public function importUpload()
    {
        return view('app.lists.contacts.import-upload');
    }
    
    public function sendEmail()
    {
        return view('app.lists.contacts.send-email');
    }
    
    public function sendSms()
    {
        return view('app.lists.contacts.send-sms');
    }
    
    public function manage()
    {
        return view('app.lists.manage');
    }
    
    public function move()
    {
        return view('app.lists.contacts.move');
    }
}
