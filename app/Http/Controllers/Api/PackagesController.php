<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\CreditPackage;


class PackagesController extends Controller
{
	protected $packages;
	
	/* Function to display available credit packages
	*  Return JSON response
	*/

	public function index()
	{
		$creditPackages = CreditPackage::get();

		return response()->json(['data' => $creditPackages], 200);
	}

	/* Function to return single credit package
	*  @param id
	*  Return JSON response
	*/

	public function show(Request $request, $id)
	{
		$package = CreditPackage::find($id);
		
		if($package) {
			return response()->json(['data' => $package], 200);
		}

		abort(404, 'Package not found.');
	}
}
