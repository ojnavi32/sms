<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Transformers\UserSessionTransformer;
use Illuminate\Http\Request;
use Dingo\Api\Routing\Helpers;
use App\Models\UserSession;
use App\User;
use JWTAuth;
use App\Services\ApiResponse;

class SessionController extends Controller
{
    use Helpers;
    
    protected $usersSessions;
    protected $user;
    
    public function __construct(UserSession $usersSessions)
    {
        $this->middleware(function ($request, $next) {
            $this->user = jwt_user();
            return $next($request);
        });
        
        $this->usersSessions = $usersSessions;
    }
    
    public function index()
    {
    	$usersSessions = $this->usersSessions
            ->where('user_id', $this->user->id)
            ->paginate(num_rec());
        return $this->response->paginator($usersSessions, new UserSessionTransformer);
    }
    
    public function show(Request $request, $id)
    {
        $settings = [];
        $notifications = NotificationSetting::where('user_id', $this->user->id)->pluck('val', 'key')->toArray();
        $types = config('settings.notification_types');
        
        foreach ($types as $key => $val) {
            
            if (array_key_exists($key, $notifications)) {
                $settings[] = ['key' => $key, 'text' => $val, 'enabled' => (bool)$notifications[$key]];
            } else {
                $settings[] = ['key' => $key, 'text' => $val, 'enabled' => false];
            }
        }
        
        // $notifications = NotificationSetting::where('user_id', $this->user->id)->get();
        
        // return $this->response->collection($notifications, new NotificationSettingTransformer);
        return $settings;
    }
    
    public function update(Request $request, $id)
    {
        $settings = $request->except('token');
        
        foreach ($settings as $setting) {
            $notification = NotificationSetting::firstOrNew(['user_id' => $this->user->id, 'key' => $setting['key']]);
            $notification->val = $setting['enabled'];
            $notification->save();
        }
        
        return ['updated' => true];
    }
    
    public function delete(Request $request, $id)
    {
        return $this->deleteSessions($id);
    }
    
    public function deleteAll(Request $request)
    {
        return $this->deleteSessions();
    }
    
    public function deleteSessions($id = null)
    {
        if ($id) {
            $session = $this->usersSessions->findOrFail($id);
        } else {
            $session = $this->usersSessions->where('user_id', '=', $this->user->id);
        }
        
        $session->delete();
        
        return $this->response->noContent(); // 204
    }
}