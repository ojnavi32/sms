<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\StoreContactRequestV2;
use App\Services\Models\Contact;
use App\Transformers\ContactTransformer;
use Illuminate\Http\Request;
use Dingo\Api\Routing\Helpers;
use App\User;
use App\ListContact;
use Illuminate\Support\Collection;
use JWTAuth;
use DB;
use Gravatar;
use App\Http\Requests\FileUpload;
use Illuminate\Validation\Rule;
use App\Http\Requests\StoreContactRequest;
use MongoDB;
use App\Services\ImportContact;
use App\Services\ContactService;
use Auth;
use App\Lists;
use App\SmstoMongo;
use MongoDB\BSON\ObjectId;

/**
 * @Resource("Contacts", uri="/contacts")
 */
class ContactController extends Controller
{

    use Helpers;

    protected $contacts;
    protected $user;
    public $import;
    protected $contactService;

    public function __construct(Contact $contacts, ImportContact $import, ContactService $contactService)
    {
        $this->contacts = $contacts;
        $this->import = $import;
        $this->contactService = $contactService;

        $this->middleware(function ($request, $next) {
            $this->user = jwt_user();
            return $next($request);
        });
    }

    /**
     * Show all contacts for particular users
     *
     * Get a JSON representation of all the contacts of the users.
     *
     * @Get("/")
     * @Versions({"v1"})
     * @Response(200, body={"id": 10, "username": "foo"})
     * @Parameters({
     *      @Parameter("page", type="integer", description="The page of results to view.", default=1),
     *      @Parameter("limit", type="integer", description="The amount of results per page.", default=10)
     * })
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Request $request)
    {
        $contacts = [];
        $paginator = [
            'count' => 0,
            'current_page' => (int)$request->get('page') ?: 1,
            'links' => [],
            'per_page' => 50,
            'total' => 0,
            'total_pages' => 0
        ];

        $query = [];

        if ($request->has('search')) {
            $query['$or'] = [
                [
                    'phone' => $request->get('search')
                ]
            ];
        }

        $user = $request->user();

        $mongo = new SmstoMongo;
        $db = $mongo->db("smsto");
        $listsCount = $user->lists()->count();
        $perListLimit = $this->preventDivisionByZero($paginator['per_page'], $listsCount);
        foreach ($user->lists()->get() as $list) {
            $collection_name = "lists_" . $list->id . "_user_" .$user->id . "_contacts";
            $collection = $db->{$collection_name};
            $q = $collection->find(
                $query,
                [
                    'limit' => $perListLimit,
                    'skip' => $perListLimit * ($paginator['current_page'] - 1)
                ]
            );
            $a = 0;
            foreach ($q as $item) {
                $a++;
                array_push($contacts, [
                    'id' => (string) $item['_id'],
                    'user_id' => $item['user_id'] ?: null,
                    'list_id' => $item['list_id'] ?: null,
                    'list_name' => $list->name ?: null,
                    'phone' => $item['phone'],
                    'firstname' => $item['firstname'] ?: null,
                    'lastname' => $item['lastname'] ?: null,
                    'email' => $item['email'] ?: null,
                    'country_code' => $item['country_code'],
                    'network_name' => $item['network_name'],
                    'optouted' => $item['optedout'],
                    'optout_data' => $item['optout_date'],
                    'created_at' => $item['created_at'],
                    'updated_at' => $item['updated_at']
                ]);
            }
        }
        return response()->json([
            'data' => $contacts,
            'meta' => [
                'pagination' => $paginator
            ]
        ]);
    }

    /**
     * Get avatar equivalent of the email
     *
     * Get avatar equivalent of the email
     *
     * @Get("/{email}/gravatar")
     * @Versions({"v1"})
     * @Response(200, body={"url": ""})
     */
    public function gravatar(Request $request, $email)
    {
        return response()->json(['url' => Gravatar::get($email)], 200);
    }

    public function show(Request $request, $id)
    {
        $params = explode("_list_id_", $request->id);
        $contact_id = null;
        $list_id = null;
        $list = null;

        if (count($params) == 2) {
            $contact_id = $params[0];
            $list_id = $params[1];
            $list = Lists::find($list_id);
        }

        if (!is_null($contact_id) && !is_null($list_id)) {
            $connection = new SmstoMongo;
            $collection_name = "lists_" . $list_id . "_user_" . $request->user()->id . "_contacts";
            $collection = $connection->db()->{$collection_name};
            $item = $collection->findOne([
                '_id' => new ObjectId($contact_id)
            ]);

            return response()->json([
                'data' => [
                    'id' => (string) $item['_id'],
                    'user_id' => $item['user_id'] ?: null,
                    'list_id' => $item['list_id'] ?: null,
                    'list_name' => $list->name,
                    'phone' => $item['phone'],
                    'firstname' => $item['firstname'] ?: null,
                    'lastname' => $item['lastname'] ?: null,
                    'email' => $item['email'] ?: null,
                    'country_code' => $item['country_code'],
                    'network_name' => $item['network_name'],
                    'optouted' => $item['optedout'],
                    'optout_data' => $item['optout_date'],
                    'created_at' => $item['created_at'],
                    'updated_at' => $item['updated_at']
                ]
            ]);
        }



        return response()->json([
            $request->id
        ]);
        $contacts = Contact::findOrFail($id);

        return $this->response->item($contacts, new ContactTransformer);

        return $this->response->error('Record not found.', 404);
    }

    public function store(StoreContactRequestV2 $request)
    {
        $contact = [];
        $date = \Carbon\Carbon::now();
        $user = $request->user();
        $list = $user->lists()->find($request->lists);
        $phoneData = is_valid_number($request->phone, $user->default_prefix);

        if (isset($phoneData['valid']) && $phoneData['valid']) {
            $contact['user_id'] = $user->id;
            $contact['list_id'] = $list->id;
            $contact['phone'] = $phoneData['phonenumber'];
            $contact['firstname'] = $request->name;
            $contact['lastname'] = $request->surname;
            $contact['email'] = $request->email;
            $contact['country_code'] = $phoneData['country_code'];
            $contact['network_name'] = $phoneData['network_name'];
            $contact['optedout'] = false;
            $contact['optout_date'] = null;
            $contact['created_at'] = $date;
            $contact['updated_at'] = $date;

            $collection_name = "lists_" . $list->id . "_user_" . $user->id . "_contacts";
            $connection = new SmstoMongo;
            $collection = $connection->db()->{$collection_name};

            $insetOne = $collection->insertOne($contact);

            if ($insetOne->isAcknowledged()) {

                #Add contact to list contacts
                // $listContacts = [
                //     'user_id' => $user->id,
                //     'list_id' => $list->id,
                //     'contact_id' => (int) $insetOne->getInsertedId(),
                //     'phone' => $contact['phone']
                // ];
                var_dump($insetOne->getInsertedId());
                // $list->listContacts()->create($listContacts);
                
                $list->number_contacts = $list->number_contacts + $insetOne->getInsertedCount();
                $list->save();
                return response()->json([
                    $contact
                ]);
            }
        } else {
            return response()->json([
                'message' => 'Phone # is not valid'
            ], 500);
        }




//        $contact = new Contact;
//
//        $contact->name = $request->name;
//        $contact->surname = $request->surname;
//        $contact->email = $request->email;
//        $contact->phone = $request->phone;
//
//        $contact = $contact->setContactNetwork($contact, $user);
//
//        // We can use list_id also depending on params used
//        if ($request->lists)
//            $this->contactService->setListsIds($request->lists);
//
//        $contact = $this->contactService->create($contact, $this->user);
//        return $this->response->item($contact, new ContactTransformer)->setStatusCode(201);
    }

    public function update(Request $request, $id)
    {
        // Phone number is required
        $this->validate($request, [
            'phone' => 'required',
        ]);

        $params = explode("_list_id_", $request->id);
        $contact_id = null;
        $list_id = null;

        if (count($params) == 2) {
            $contact_id = $params[0];
            $list_id = $params[1];
        }

        if (!is_null($contact_id) && !is_null($list_id)) {
            $connection = new SmstoMongo;
            $collection_name = "lists_" . $list_id . "_user_" . $request->user()->id . "_contacts";
            $collection = $connection->db()->{$collection_name};
            $update = $collection->updateOne(
                ['_id' => new ObjectId($contact_id)],
                ['$set' => [
                    'firstname' => $request->firstname,
                    'lastname' => $request->lastname,
                    'email' => $request->email,
                    'phone' => $request->phone
                ]]
            );

            $item = $collection->findOne(
                ['_id' => new ObjectId($contact_id)]
            );

            return response()->json([
                'data' => [
                    'id' => (string) $item['_id'],
                    'user_id' => $item['user_id'] ?: null,
                    'list_id' => $item['list_id'] ?: null,
                    'phone' => $item['phone'],
                    'firstname' => $item['firstname'] ?: null,
                    'lastname' => $item['lastname'] ?: null,
                    'email' => $item['email'] ?: null,
                    'country_code' => $item['country_code'],
                    'network_name' => $item['network_name'],
                    'optouted' => $item['optedout'],
                    'optout_data' => $item['optout_date'],
                    'created_at' => $item['created_at'],
                    'updated_at' => $item['updated_at']
                ]
            ]);
        }

        $contact = Contact::findOrFail($id);
        $contact->fill($request->all());
        $contact->save();

        // When updating contact theres an option to add or move the contact to a list
        // Contacts can be in multiple lists
        if ($request->has('lists')) {
            if (is_array($request->lists)) {
                foreach ($request->lists as $listId) {
                    ListContact::firstOrCreate([
                        'user_id' => $this->user->id,
                        'list_id' => $listId,
                        'contact_id' => $contact->id,
                    ]);
                }
            }
        }


        $contacts = $this->import->process($request, $this->user);

        if ($contacts) {
            return 'ok';
        }

        return 'not ok';
        // types
        // gmail, yahoo, file
        // Import upload csv, xls ods
        // Process the file
        return $this->response->created();
    }

    public function sendEmail(Request $request, $id)
    {
        $contact = Contact::findOrFail($id);

        if ($contact) {
            if ($contact->user_id != $this->user->id) {
                return $this->response->errorBadRequest();
            }

            $contact->email;

            return $this->response->created(); // 201
        }
        // params email, message
    }

    public function sendSms(Request $request, $id)
    {
        $contact = Contact::findOrFail($id);

        if ($contact) {
            if ($contact->user_id != $this->user->id) {
                return $this->response->errorBadRequest();
            }

            $contact->phone;

            return $this->response->created(); // 201
        }
        // params phone_number, message
    }

    public function deleteContacts(Request $request, $id)
    {
        $params = explode("_list_id_", $request->id);
        $contact_id = null;
        $list_id = null;

        if (count($params) == 2) {
            $contact_id = $params[0];
            $list_id = $params[1];
        }

        if (!is_null($contact_id) && !is_null($list_id)) {
            $connection = new SmstoMongo;
            $collection_name = "lists_" . $list_id . "_user_" . $request->user()->id . "_contacts";
            $collection = $connection->db()->{$collection_name};

            $item = $collection->deleteOne(
                ['_id' => new ObjectId($contact_id)]
            );

            return response()->json([
                'data' => []
            ]);
        }


        $contact = Contact::findOrFail($id);

        // Check if the delete request is multiple ids
        if ($contact) {
            if ($contact->user_id != $this->user->id) {
                return $this->response->errorBadRequest();
            }

            $contact->delete();
            return $this->response->noContent(); // 204
        }

        return $this->response->errorBadRequest();
        return $this->response->error('Record not found.', 404);
    }

    public function moveContacts(Request $request)
    {
        if (is_array($request->lists_contact_ids)) {
            foreach ($request->lists_contact_ids as $id) {
                ListContact::where('id', $id)->update(['list_id' => $request->list_id]);
            }
        }

        // Move from one list to another
        return ['moved' => true];
    }

    // todo:
    public function mergeContacts(Request $request)
    {
        // Get duplicate contacts if any
        $contacts = DB::table('contacts')
            ->select(DB::raw('name, phone, COUNT( * )'))
            ->where('user_id', 6)
            ->groupBy(DB::raw('name, phone'))
            ->having(DB::raw('phone > 1'))
            ->get();

        return ['merge' => true];
    }

    public function contactsRaw(Request $request, $listsId)
    {
        $user = $request->user();
        $contacts = [];
        $collection_name = "lists_" . $listsId . "_user_" . $user->id . "_contacts";
        $mongo = new SmstoMongo();
        $db = $mongo->db();
        $collection = $db->{$collection_name};
        $q = $collection->find([
            []
        ]);
        $c = 0;
        foreach ($q as $item) {
            $c++;
            array_push($contacts, $item);
        }
        echo $c;
        return response()->json([
            $contacts
        ]);

    }

    public function contacts(Request $request, $listsId)
    {
        $contacts = [];
        $paginator = [
            'count' => 0,
            'current_page' => (int)$request->get('page') ?: 1,
            'links' => [],
            'per_page' => 50,
            'total' => 0,
            'total_pages' => 0
        ];

        $query = [];

        if ($request->has('search')) {
            $query['$or'] = [
                [
                    'phone' => $request->get('search')
                ]
            ];
        }

        $collection_name = "lists_" . $listsId . "_user_" . $this->user()->id . "_contacts";
        $mongo = new SmstoMongo();
        $db = $mongo->db();
        $collection = $db->{$collection_name};
        $q = $collection->find(
            $query,
            [
                'limit' => $paginator['per_page'],
                'skip' => $paginator['per_page'] * ($paginator['current_page'] - 1)
            ]
        );

        $collectionCount = $collection->count();
        $paginator['total'] = $collectionCount;
        $paginator['total_pages'] = abs(floor($collectionCount / $paginator['per_page']));

        # Get lists and include to response
        $user = $request->user();
        $lists = $user->lists;

        $count = 0;
        $contactsContainer = [];
        foreach ($q as $item) {
            $count++;
                $item['lists'] = $lists;
                array_push($contactsContainer, $item);
        }
        
        $numberContacts = array_column($contactsContainer, 'phone');
        $listContacts = ListContact::whereIn('phone', $numberContacts)
                ->whereNull('deleted_at')
                ->pluck('phone')
                ->toArray();

        $containerTemp = [];
        foreach ($contactsContainer as $k => $r) {
            if (in_array($r['phone'], $listContacts)) {
                array_push($containerTemp, $r);
            }
        }
        $contacts = $containerTemp;
                
        $paginator['count'] = $count;

        return response()->json([
            'data' => $contacts,
            'meta' => [
                'pagination' => $paginator
            ],
            'request' => $request->all()
        ]);
    }

    /*
    * Function used to add lists to a contact
    * @param contactId, Request
    * @return JSON
    */
    public function addList(Request $request, $id)
    {
        if (is_array($request->lists)) {
            foreach ($request->lists as $list) {
                ListContact::firstOrCreate([
                    'user_id' => $this->user->id,
                    'list_id' => $list['id'],
                    'contact_id' => $id,
                ]);

                $list = Lists::findOrFail($list['id']);
                $list->number_contacts += 1;
                $list->save();
            }
        }
        return ['update' => true];
    }

    /*
    *Function is used to fetch contacts info
    *@param null
    *@return JSON
    */
    public function contactsInfo()
    {
        $lists = $this->user()->lists()->get();
        $count = 0;
        foreach ($lists as $list) {
            $collection_name = "lists_" . $list->id . "_user_" . $this->user()->id . "_contacts";
            $collection = (new SmstoMongo)->db()->{$collection_name};
            $count += $collection->count();
        }

        $contactsCount = $count;

        $totalLists = $this->user()->lists()->count();

        return response()->json(['totalLists' => $totalLists, 'total' => $contactsCount], 200);
    }

    /**
     * Prevents division by zero
     *
     * @param integer $a first number
     * @param integer $b second number
     *
     * @author Vladimir Nikolic <nezaboravi@gmail.com>
     *
     * @return float|int
     */
    public function preventDivisionByZero($a, $b)
    {
        return !empty($b) ? $a / $b : 0;
    }
}