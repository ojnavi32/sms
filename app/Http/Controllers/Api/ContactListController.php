<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Transformers\ContactListTransformer;
use Dingo\Api\Routing\Helpers;
use App\ContactList;
use App\User;
use JWTAuth;

class ContactListController extends Controller
{
    use Helpers;
    
    public function getLists()
    {
    	$lists = ContactList::paginate(num_rec());
        return $this->response->paginator($lists, new ContactListTransformer);
    }
}