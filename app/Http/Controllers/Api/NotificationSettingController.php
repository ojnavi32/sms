<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Transformers\NotificationSettingTransformer;
use Illuminate\Http\Request;
use Dingo\Api\Routing\Helpers;
use App\Models\NotificationSetting;
use App\User;
use JWTAuth;
use App\Services\ApiResponse;
use Carbon\Carbon; 

class NotificationSettingController extends Controller
{
    use Helpers;
    
    protected $templates;
    protected $templatesFields;
    protected $user;
    
    public function __construct()
    {
        // $this->templates = $templates;
        // $this->templatesFields = $templatesFields;
        $this->middleware(function ($request, $next) {
            $this->user = jwt_user();
            return $next($request);
        });
    }
    
    public function index()
    {
    	// $templates = $this->templates
     //        ->where('user_id', $this->user->id)
     //        ->paginate(num_rec());
     //    return $this->response->paginator($templates, new NotificationSettingTransformer);
    }
    
    public function show(Request $request, $id)
    {
        $settings = [];
        $notifications = NotificationSetting::where('user_id', $this->user->id)->pluck('val', 'key')->toArray();
        $types = config('settings.notification_types');
        
        foreach ($types as $key => $val) {
            
            if (array_key_exists($key, $notifications)) {
                $settings[] = ['key' => $key, 'text' => $val, 'enabled' => (bool)$notifications[$key]];
            } else {
                $settings[] = ['key' => $key, 'text' => $val, 'enabled' => false];
            }
        }
        
        // $notifications = NotificationSetting::where('user_id', $this->user->id)->get();
        
        // return $this->response->collection($notifications, new NotificationSettingTransformer);
        return $settings;
    }
    
    public function update(Request $request, $id)
    {
        $settings = $request->except(['token','q']);

        foreach ($settings as $setting) {
            $notification = NotificationSetting::firstOrNew(['user_id' => $this->user->id, 'key' => $setting['key']]);
            $notification->val = $setting['enabled'];

      
            if($setting['enabled'] && $setting['key'] == 'invitations'){
                $notification->email_Marketing_consent_timestamp = Carbon::now();
                $notification->email_Marketing_consent_ip = \Request::ip();
            }
            elseif(!$setting['enabled'] && $setting['key'] == 'invitations'){
                $notification->email_Marketing_consent_unsubscribe_timestamp = Carbon::now();
                $notification->email_Marketing_consent_unsubscribe_ip = \Request::ip();
            }
                
            $notification->save();
            
        }
        
        return ['updated' => true];
    }
}