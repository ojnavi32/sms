<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use Carbon\Carbon;

use Dingo\Api\Routing\Helpers;
use JWTAuth;

use App\Http\Controllers\Controller;
use App\Models\SmsBroadcast;
use App\Models\Payment;
use App\User;
use Illuminate\Http\Response;
use App\Services\SMS\Providers\InfoBip;
use App\Services\SMS\Reports\SMSBroadcastMessageReports;
use App\Services\SMS\Reports\Heatmap;
use App\Services\SMS\Reports\CostActivity;
use App\Services\SMS\Logger\SentLogs;
use App\Models\SmsBroadcastMessage;
use URL;
use File;
use App\Transformers\PaymentTransformer;
use Auth;
use App\Transformers\BillingTransformer;


class DashboardController extends Controller
{
    use Helpers;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //dd(auth()->user()->id);
        return 'api dashboard' .  auth()->user()->id;
        return view('home');
    }

    public function smsSend()
    {
       $sms =  new InfoBip();
       $sms->sendsms('+918606695615', 'Hey');
    }
 
    /**
     * Show the users information
     *
     * @return \Illuminate\Http\Response
     */
    public function userInfo()
    {
        $costPerSms = 0.25; //TODO get average of all providers cost;

        $balanceCash = auth()->user()->cash_balance;

        $smsRemaining =  $balanceCash/$costPerSms;  

        $cashBalance = auth()->user()->cash_balance;             

        $data = array('cashBalance' => round($balanceCash, 2), 'remainingSms' => round($smsRemaining, 2), 'creditBalance' => round($cashBalance, 2));                        
        return response()->json($data);                        
    }

    /**
     * Show the sms logs in application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
     public function smsLogs(SentLogs $smsLogs)
     {
        $today = $smsLogs->getToday();
        $yesterday = $smsLogs->getYesterday();
        $last7Day = $smsLogs->getLast7Day();
        $month = $smsLogs->getMonth();

        $data = ['today' => $today, 'yesterday' => $yesterday, 'last7Days' => $last7Day, 'month' => $month];
        return response()->json($data);
     }

     /**
     * Show the chart sms search
     *
     * @return \Illuminate\Http\Response
     */

     public function chartSmsReach(Request $request)
     { 
        $data = Heatmap::getHeatmapData($request->filter);
        
        return response()->json($data);
     }

     /**
     * Show the chart cost activity
     *
     * @return \Illuminate\Http\Response
     */

     public function chartCostActivity(Request $request)
     { 
        $data = CostActivity::getCostActivityData($request->filter); 
                                         
        return response()->json($data);
    }
    /**
     * Show the chart send activity
     *
     * @return \Illuminate\Http\Response
     */

    public function chartSendActivity(Request $request)
    {
        $startDate = $request->start_date;
        $endDate = $request->end_date;

        $method = 'generate'.$request->filter;

        $result = SMSBroadcastMessageReports::$method( $startDate, $endDate );

        return response()->json($result);
    }

      /**
     * Show the phone number prefix
     *
     * @return \Illuminate\Http\Response
     */
     public function phonePrefix()
     {
        $phonePrefix = getPhoneNumberPrefixes();

        return response()->json(['data' => $phonePrefix]);
     }
     
    /**
     * Show the timezones
     *
     * @return \Illuminate\Http\Response
     */
    public function timezones()
    {
        return response()->json(['data' => getTimezones()]);
    }

    /**
     * Show the payments
     *
     * @return \Illuminate\Http\Response
     */
    public function payments(Request $request)
    { 
        $sortBy = null;

        $sort = ltrim($request->sort, '-');

        $type = 'ASC';
        if($sort == 'date') {
            $sortBy = 'created_at';            
            if(substr($request->sort, 0, 1) == '-') {
                $type = 'DESC';
            }
        }
        
        $entries = Payment::with('paymentMethod')->when($sortBy, function ($query) use ($sortBy, $type) {
                  return $query->orderBy($sortBy, $type);
        })->where('user_id',auth()->user()->id)->paginate(num_rec());

        return $this->response()->paginator($entries, new PaymentTransformer);
    }
    /**
     * Show the payment details
     *
     * @return \Illuminate\Http\Response
     */
    public function getPaymentDetails($id)
    {
        $payment = Payment::with('PaymentMethod','User')->where('user_id',Auth::id())->find($id);
        
        if($payment){
            return $this->response()->item($payment, new BillingTransformer);
        }
        return $this->response->error('Record not found.', 404);
    }


    public function downloadPayment(Request $request)
    { 
        $basePath =  URL::to('/');

        $path = storage_path('app/public').'/csv-payments';
        File::makeDirectory($path, $mode = 0777, true, true);
        
        $payment_history = Payment::with('user')->when('created_at', function ($query) {
                  return $query->orderBy('created_at', 'DESC');
        })->where('user_id',auth()->user()->id)->get();

        $paymentFileName = str_random(16) . '.csv';

        $handle = fopen($path.'/'.$paymentFileName, 'w+');
        fputcsv($handle, array('Name', 'Payment Method', 'Amount', 'Paid Date'));

       
        foreach ($payment_history as $payment) {

            fputcsv($handle, array($payment->user->name, $payment->paymentMethod->name, $payment->amount, $payment->created_at));
        }


        fclose($handle);

        return response()->json(['downloadFile' => $basePath .'/storage/csv-payments/'.$paymentFileName]);
    }

    public function pdfDownload($id)
    {
        $payment = Payment::with('PaymentMethod','User')->where('user_id',Auth::id())->find($id);
        $createdDate = date('d/m/Y', strtotime($payment->created_at->toDateString()));
        $dt = \DateTime::createFromFormat('!d/m/Y', $createdDate);
        $payment->date = strtoupper($dt->format('M j Y')); 
        $payment->path = base_path().'/public/images/logo.png';

        view()->share('payment',$payment);
        
        \PDF::setOptions(['dpi' => 150, 'defaultFont' => 'sans-serif']);
        $pdf = \PDF::loadView('pdfview');
        return $pdf->download('SMSto Invoice-'.$id.'.pdf');
        
    }

    public function getCountry()
    { 
        $countryName = [];
        $phonePrefix = getPhoneNumberPrefixes();
        foreach($phonePrefix as $key=>$value)
        {
            $countryName[$key] = $value['name'];
        }
       
        return response()->json(['data' => $countryName]);
    }
}
