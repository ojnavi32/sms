<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Transformers\UserTransformer;
use Dingo\Api\Routing\Helpers;
use Illuminate\Http\Request;
use App\User;
use Hash;
use Illuminate\Foundation\Auth\ResetsPasswords;
use Storage;
use App\Services\VatEu;
use App\Http\Requests\StoreUserRequest;
use App\Exceptions\CustomException;
use Carbon\Carbon;
use App\Models\DeleteRequest;
use Auth;
use App\Http\Requests\StoreBillingRequest;

class UserController extends Controller {

    use Helpers;
    use ResetsPasswords;

    protected $user;
    protected $vatEu;

    public function __construct(User $users, VatEu $vatEu) {
        $this->vatEu = $vatEu;

        $this->middleware(function ($request, $next) {
            $this->user = jwt_user();
            return $next($request);
        });
    }

    public function getUsers() {
        // We can use item, collection or paginator
        $users = User::paginate(1);
        return $this->response->paginator($users, new UserTransformer);
    }

    public function show(Request $request, $id) {
        $users = User::findOrFail($id);

        return $this->response->item($users, new UserTransformer);

        return $this->response->error('Resource not found.', 404);
    }

    public function update(Request $request, $id) {
        $phone = $request->phone;
        $validator = \Validator::make($request->all(), [
                    'first_name' => 'required|max:255',
                    'last_name' => 'required',
                    'phone' => 'required',
        ]);

        $invalid = false;
        if ($validator->fails()) {
            $invalid = true;
        }

        if ($request->phone) {

            if (preg_match("~^00\d+$~", $phone))
                $phone = preg_replace('/^00?/', "+", $phone);

            $phoneNumberUtil = \libphonenumber\PhoneNumberUtil::getInstance();

            try {
                $number = $phoneNumberUtil->parse($phone, null);
            } catch (\Exception $e) {
                $validator->errors()->add('phone', 'Invalid Phone Number');
                $invalid = true;
            }
        }


        if ($invalid)
            return response()->json(['errors' => $validator->errors(), 'message' => '422 Unprocessable Entity', 'status_code' => 422], 422);

        $users = User::findOrFail($id);
        $data = $request->all();

        unset($data['avatar']);
        $users->fill($data);
        $users->save();

        return $this->response->item($users, new UserTransformer);

        return $this->response->error('Resource not found.', 404);
    }

    public function billingupdate(StoreBillingRequest $request, $id) {

        $data = $request->all();
        $euCountry = isEUcountry($request->business_country);

        if (!$euCountry) {
            $data['business_vat'] = NULL;
        }

        $users = User::findOrFail($id);
        unset($data['avatar']);
        $users->fill($data);
        $users->save();

        return $this->response->item($users, new UserTransformer);
    }

    public function avatar(Request $request, $id = null) {
        $rules = array(
            'image' => 'required', // max 10000kb
        );

        $validator = \Validator::make($request->all(), $rules);

        $invalid = false;
        if ($validator->fails())
            $invalid = true;

        $img = $request->image;

        $image_parts = explode(";base64,", $img);

        $image_type_aux = explode("data:", $image_parts[0]);

        $image_type = $image_type_aux[1];


        $validType = array('image/png', 'image/jp', 'image/jpeg', 'image/bmp', 'image/gif', 'image/svg');

        if (!in_array($image_type, $validType)) {
            $validator->errors()->add('image', 'Image must be in a valid image format!');
            $invalid = true;
        }

        if ($invalid)
            return response()->json(['errors' => $validator->errors(), 'message' => '422 Unprocessable Entity', 'status_code' => 422], 422);

        $users = User::findOrFail($this->user->id);
        $image = preg_replace('#^data:image/[^;]+;base64,#', '', $request->image);


        $avatar = str_random(16) . '.jpg';
        $decoded = base64_decode($image);

        Storage::disk('public')->put('avatar/' . $avatar, $decoded);

        $users->avatar = $avatar;
        $users->save();

        return $this->response->item($users, new UserTransformer);

        return $this->response->error('Resource not found.', 404);
    }

    public function changePassword(Request $request, $id) {
        $users = User::findOrFail($id);

        // If current password is not the same as old password
        if (!Hash::check($request->current_password, $users->password)) {
            return $this->response->error('Current password doesnt match', 400);
        }

        if (($request->password != $request->password_confirmation ) or ( $request->password == '' or $request->password_confirmation == '')) {
            return $this->response->error('Password and confirm password should match.', 400);
        }

        if ($users) {
            $users->password = bcrypt($request->password);
            $users->save();

            return $this->response->item($users, new UserTransformer);
        }

        return $this->response->error('Resource not found.', 404);
    }

    public function resetPassword(Request $request) {
        // return $this->reset($request);
        // field - 'email', 'password', 'password_confirmation', 'token'
    }

    public function deleteRequest(Request $request) {
        $user = User::find(Auth::id());

        DeleteRequest::firstOrCreate([
            'user_id' => Auth::id(),
            'name' => $user->name,
            'email' => $user->email,
            'time_stamp' => Carbon::now(),
            'request_ip' => \Request::ip(),
        ]);

        return ['updated' => true];
    }

}
