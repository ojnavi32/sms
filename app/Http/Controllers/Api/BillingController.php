<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Transformers\UserTransformer;
use Dingo\Api\Routing\Helpers;
use Illuminate\Http\Request;
use App\User;
use App\BillingDetail;

class BillingController extends Controller
{
    use Helpers;
    
    protected $billing;
    
    protected $user;
    
    public function __construct(BillingDetail $billing)
    {
    	$this->billing = $billing;
        $this->middleware(function ($request, $next) {
            $this->user = jwt_user();
            return $next($request);
        });
    }
    
    public function store($userId)
    {
    	$data = request()->all();
    	$data['user_id'] = $userId;
    	return BillingDetail::create($data);
    }
    
    public function update($userId, $id)
    {
    	$billing = BillingDetail::find($id);
    	$billing->fill(request()->all());
    	$billing->save();
    }
}