<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Transformers\BroadcastMessageTransformer;
use Illuminate\Http\Request;
use Dingo\Api\Routing\Helpers;
use App\Models\SmsBroadcast;
use App\Models\SmsBroadcastMessage;
use App\User;
use JWTAuth;
use App\Services\ApiResponse;  

class SmsBroadcastBatchController extends Controller
{
   
    use Helpers;
    
    protected $broadcasts;
    
    public function __construct(SmsBroadcast $broadcasts)
    {
        $this->broadcasts = $broadcasts;
    }

    /*
    *Function to fetch the messages under the broadcast batches
    *@param batch Id
    *@return Json
    */
    public function messages($id)
    { 
       $messages = $this->user()->broadcastMessages()->where('sms_broadcast_batch_id',$id)->paginate(num_rec());
                                 
       return $this->response->paginator($messages, new BroadcastMessageTransformer);

    }

}