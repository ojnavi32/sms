<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Dingo\Api\Routing\Helpers;
use App\Models\UserPaymentMethod;
use App\Transformers\PaymentMethodTransformer;
use App\User;
use JWTAuth;
use App\Services\ApiResponse;

class PaymentMethodController extends Controller
{
    use Helpers;
    
    protected $usersPaymentsMethods;
    
    protected $user;
    
    public function __construct(UserPaymentMethod $usersPaymentsMethods)
    {
        $this->usersPaymentsMethods = $usersPaymentsMethods;
        
        $this->middleware(function ($request, $next) {
            $this->user = jwt_user();
            return $next($request);
        });
        
    }
    
    public function index()
    {
    	$paymentMehods = $this->usersPaymentsMethods
            ->where('user_id', $this->user->id)
            ->paginate(num_rec());
            
        return $this->response->paginator($paymentMehods, new PaymentMethodTransformer);
    }
    
    public function show(Request $request, $id)
    {
        $paymentMehods = $this->usersPaymentsMethods->find($id);
        
        if ($paymentMehods) {
            return $this->response->item($paymentMehods, new PaymentMethodTransformer);
        }
        
        return $this->response->error('Record not found.', 404);
    }
    
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'messages' => 'required',
        ]);
        
        $paymentMehods = $this->usersPaymentsMethods->create([
            'user_id' => $this->user->id,
            'name' => $request->name,
            'messages' => $request->messages,
        ]);
        
        return $this->response->created();
    }
    
    public function update(Request $request, $id)
    {
        // Name is required
        $this->validate($request, [
            'name' => 'required',
            'messages' => 'required',
        ]);
        
        $paymentMehods = $this->paymentMehods->find($id);
        
        $paymentMehods->fill($request->all());
        $paymentMehods->save();
        
        return ['updated' => true];
    }
    
    public function delete(Request $request, $id)
    {
        $this->user = jwt_user();
        
        $paymentMehods = $this->paymentMehods->find($id);
        
        if ($paymentMehods) {
            if ($paymentMehods->user_id != $this->user->id) {
                return $this->response->errorBadRequest();
            }
            
            $paymentMehods->delete();
            return $this->response->noContent(); // 204
        }
        
        return $this->response->errorBadRequest();
    }
}