<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Transformers\BroadcastMessageTransformer;
use App\Transformers\SmsBroadcastTransformer;
use Illuminate\Http\Request;
use Dingo\Api\Routing\Helpers;
use App\Models\SmsBroadcast;
use App\User;
use JWTAuth;
use App\Services\ApiResponse;
use Carbon\Carbon; 
use URL;
use File;
use Excel;

class SmsBroadcastController extends Controller
{
    use Helpers;
    
    protected $broadcasts;
    
    public function __construct(SmsBroadcast $broadcasts)
    {
        $this->broadcasts = $broadcasts;
    }
    
    public function index(Request $request)
    {

        $filter = FALSE;
        $sortBy = null;
        $startDate = null;
        $endDate = null; 

        $sort = ltrim($request->sort, '-');

        $type = 'ASC';
        if($sort == 'messages_total') {
            $sortBy = 'messages_count';            
            if(substr($request->sort, 0, 1) == '-') {
                $type = 'DESC';
            }
        }
        if($sort == 'delivered') { 
            $sortBy = 'delivered_messages_count';            
            if(substr($request->sort, 0, 1) == '-') {
                $type = 'DESC';
            }
        }
        if($sort == 'failed') {
            $sortBy = 'failed_messages_count';            
            if(substr($request->sort, 0, 1) == '-') {
                $type = 'DESC';
            }
        }
        if($request->filter == 'week') {
             $filter = TRUE;
             $startDate = Carbon::now()->startOfWeek()->toDateString(); 
             $endDate = Carbon::now()->toDateString();
        }
        else if($request->filter == 'month') {
              $filter = TRUE;
              $startDate = Carbon::now()->startOfMonth()->toDateString();
              $endDate = Carbon::now()->toDateString();
        
        }
           

        $broadcasts = $this->broadcasts->withCount(['messages', 'deliveredMessages', 'failedMessages'])->when($sortBy, function ($query) use ($sortBy, $type) {
                          return $query->orderBy($sortBy, $type);
                })->when($filter, function ($query) use ($startDate, $endDate) {
                          return $query->whereBetween('created_at', [$startDate, $endDate]);
                })->where('user_id', auth()->user()->id)->paginate(num_rec());
      
        return $this->response->paginator($broadcasts, new SmsBroadcastTransformer);
    }
    
    public function show(Request $request, $id)
    {
        $broadcasts = $this->broadcasts->withCount(['deliveredMessages', 'failedMessages'])->findOrFail($id);
        
        if ($broadcasts) {
            return $this->response->item($broadcasts, new SmsBroadcastTransformer);
        }
        
        return $this->response->errorBadRequest();
        return $this->response->error('Record not found.', 404);
    }
    
    public function store(Request $request)
    {
        $this->validate($request, [
            'list_id' => 'required',
            'message' => 'required',
            'total_cost' => 'required',
        ]);
        
        $broadcasts = $this->broadcasts->create([
            'user_id' => auth()->user()->id,
            'lists_id' => $request->list_id,
            'message' => $request->message,
            'total_cost' => $request->total_cost,
        ]);
        
        return $this->response->created();
    }
    
    public function update(Request $request, $id)
    {
        // name is required
        $this->validate($request, [
            //'name' => 'required',
            'message' => 'required',
        ]);
        
        $broadcast = $this->broadcasts->findOrFail($id);
        
        $broadcast->fill($request->all());
        $broadcast->save();
        
        return ['updated' => true];
    }
    
    public function delete(Request $request, $id)
    {
        $broadcast = $this->broadcasts->findOrFail($id);
        
        if ($broadcast) {
            if ($broadcast->user_id != auth()->user()->id) {
                return $this->response->errorBadRequest();
            }
            
            $broadcast->delete();
            return $this->response->noContent(); // 204
        }
        
        return $this->response->errorBadRequest();
        return $this->response->error('Record not found.', 404);
    }

    /*
    *Function to fetch the batches under the broadcast
    *@param braodcast Id
    *@return Json
    */
    public function broadcastDetails(Request $request,$id)
    {
       $batches = $this->user()->broadcasts()->find($id)->messages()->paginate(num_rec());
       
       return $this->response->paginator($batches, new BroadcastMessageTransformer);

    }

    public function downloadBroadcastAsCsv(Request $request,$id)
    { 
        $basePath =  URL::to('/');

        $dir = storage_path('app/public').'/broadcasts/csv/'.$id;

        if (is_dir($dir)) {
           $downloadFile = File::allFiles(base_path().'/public/storage/broadcasts/csv/'.$id.'/');
           $broadcastCsvFile = $downloadFile[0]->getFilename();
           $path = $basePath .'/storage/broadcasts/csv/'.$id.'/'.$broadcastCsvFile;
        }
        else{
            $path = null;
        }

        return response()->json(['downloadFile' => $path ]);

    }

    public function downloadBroadcastAsExcel(Request $request,$id)
    {
        $basePath =  URL::to('/');

        $dir = storage_path('app/public').'/broadcasts/excel/'.$id;

        if (is_dir($dir)) {
           $downloadFile = File::allFiles(base_path().'/public/storage/broadcasts/excel/'.$id.'/');
           $broadcastCsvFile = $downloadFile[0]->getFilename();
           $path = $basePath .'/storage/broadcasts/excel/'.$id.'/'.$broadcastCsvFile;
        }
        else{
            $path = null;
        }

        return response()->json(['downloadFile' => $path ]);

    }
}