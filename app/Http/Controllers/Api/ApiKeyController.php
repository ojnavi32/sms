<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Transformers\ApiKeyTransformer;
use Illuminate\Http\Request;
use Dingo\Api\Routing\Helpers;
use App\Models\ApiKey;
use App\User;
use JWTAuth;
use App\Services\ApiResponse;

class ApiKeyController extends Controller
{
    use Helpers;
    
    protected $apiKeys;
    
    protected $user;
    
    public function __construct(ApiKey $apiKeys)
    {
        $this->apiKeys = $apiKeys;
        $this->middleware(function ($request, $next) {
            $this->user = jwt_user();
            return $next($request);
        });
    }
    
    public function index()
    {
    	$apiKeys = $this->apiKeys
            ->where('user_id', $this->user->id)
            ->paginate(num_rec());
            
        return $this->response->paginator($apiKeys, new ApiKeyTransformer);
    }
    
    public function show(Request $request, $id)
    {
        $lists = $this->apiKeys->where('user_id', $this->user->id)->find($id);
        
        if ($lists) {
            return $this->response->item($lists, new ApiKeyTransformer);
        }
        
        return $this->response->errorBadRequest();
    }
    
    public function store(Request $request)
    {
        $this->validate($request, [
            'description' => 'required',
            'mode' => 'required',
        ]);
        
        $this->apiKeys->create([
            'user_id' => $this->user->id,
            'description' => $request->description,
            'mode' => $request->mode,
            'status' => $request->status,
            'access_key' => $request->access_key,
        ]);
        
        return $this->response->created();
    }
    
    public function update(Request $request, $id)
    {
        // name is required
        $this->validate($request, [
            'description' => 'required',
            'mode' => 'required',
        ]);
        
        $key = $this->apiKeys->findOrFail($id);
        
        $key->fill($request->all());
        $key->save();
        
        return ['updated' => true];
    }
    
    public function delete(Request $request, $id)
    {
        $key = $this->apiKeys->findOrFail($id);
        
        if ($key) {
            if ($key->user_id != auth()->user()->id) {
                return $this->response->errorBadRequest();
            }
            
            $key->delete();
            return $this->response->noContent(); // 204
        }
        
        return $this->response->errorBadRequest();
    }
}