<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;

use Illuminate\Http\Request;

class ApiController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        return view('api.index');
    }
    
    public function apiKey()
    {
        return view('api.key');
    }
    
    public function apiKeyLog()
    {
        return view('api.key-log');
    }
    
    public function data()
    {
        return view('api.data');
    }
    
    public function log()
    {
        return view('api.log');
    }
}
