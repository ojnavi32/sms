<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Transformers\TemplateTransformer;
use Illuminate\Http\Request;
use Dingo\Api\Routing\Helpers;
use App\Models\Template;
use App\Models\TemplateField;
use App\User;
use JWTAuth;
use App\Services\ApiResponse;
use App\Http\Requests\StoreTemplateRequest;

class TemplateController extends Controller {

    use Helpers;

    protected $templates;
    protected $templatesFields;
    protected $user;

    public function __construct(Template $templates, TemplateField $templatesFields) {
        $this->templates = $templates;
        $this->templatesFields = $templatesFields;
        $this->middleware(function ($request, $next) {
            $this->user = jwt_user();
            return $next($request);
        });
    }

    public function index(Request $request) {
        $sortBy = null;
        $sort = ltrim($request->sort, '-');
        $type = 'ASC';
        if ($sort == 'messages_length') {
            $sortBy = 'messages_length';
            if (substr($request->sort, 0, 1) == '-') {
                $type = 'DESC';
            }
        }

    	$templates = $this->user->templates()->when($sortBy, function ($query) use ($sortBy, $type) {
                               return $query->orderByRaw("CHAR_LENGTH(messages)  $type");
                      })->where(function ($query) use($request){
                          return $query->where('messages','LIKE', '%' . $request->search . '%')
                                ->orWhere('name','LIKE', '%' . $request->search . '%');
                      })->paginate(num_rec());                        

        return $this->response->paginator($templates, new TemplateTransformer);
    }

    public function show(Request $request, $id) {
        $templates = $this->templates->findOrFail($id);

        return $this->response->item($templates, new TemplateTransformer);
    }

    public function store(StoreTemplateRequest $request) {
        //throw new \Dingo\Api\Exception\StoreResourceFailedException('Could not create new template.');

        $template = $this->templates->create([
            'user_id' => $this->user->id,
            'name' => $request->name,
            'messages' => $request->messages,
        ]);

        // Save some templates fields if any
        $fields = [];

        if (str_contains($request->messages, '|*NAME*|')) {
            $fields[] = 'NAME';
        }

        if (str_contains($request->messages, '|*SURNAME*|')) {
            $fields[] = 'SURNAME';
        }

        if (!empty($fields)) {
            $this->templatesFields->create([
                'template_id' => $template->id,
                'fields' => json_encode($fields)
            ]);
        }

        return $this->response->item($template, new TemplateTransformer)->setStatusCode(201);
    }

    public function update(Request $request, $id) {
        // Name is required
        $this->validate($request, [
            'name' => 'required',
            'messages' => 'required',
        ]);

        $templates = $this->templates->findOrFail($id);

        $templates->fill($request->all());
        $templates->save();

        return ['updated' => true];
    }

    public function delete(Request $request, $id) {
        $templates = $this->templates->findOrFail($id);

        if ($templates->user_id != $this->user->id) {
            return $this->response->errorBadRequest();
        }

        $templates->delete();
        return $this->response->noContent(); // 204


        return $this->response->errorBadRequest();
    }

}
