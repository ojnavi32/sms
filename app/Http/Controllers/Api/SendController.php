<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\SmstoMongo;
use Illuminate\Http\Request;
use Dingo\Api\Routing\Helpers;
use App\ListContact;
use App\User;
use JWTAuth;
use App\Services\ApiResponse;
use App\Services\ContactService;
use App\Services\SMS\Calculators\RateCalculator;

use App\Jobs\SendSms;
use Carbon\Carbon;
use App\Models\SmsBroadcast;
use App\Models\ShortLink;
use Auth;
use App\Models\SmsBroadcastMessage;
use App\Contact;
use App\Models\Rate;
use App\Models\Provider;
use MongoDB;


class SendController extends Controller
{
    use Helpers;

    public $sms_per_batch;
    protected $user;
    protected $contactService;
    protected $rateCalculator;
    protected $smsBroadcast;

    public function __construct(ContactService $contactService, RateCalculator $rateCalculator, SmsBroadcast $smsBroadcast)
    {
        $this->smsBroadcast = $smsBroadcast;
        $this->rateCalculator = $rateCalculator;
        $this->contactService = $contactService;
        $this->sms_per_batch = config('sms.sms_per_batch');
        $this->middleware(function ($request, $next) {
            $this->user = jwt_user();
            return $next($request);
        });

    }

    public function index(Request $request)
    {
        // If sender_id Exists
        if ($request->sender_id) {
            if (!preg_match('/^[a-zA-Z][a-zA-Z0-9_-]{3,11}$/', $request->sender_id)) {
                return $this->response->error('invalid sender id', 500);
            }
        }

        if (!$request['message'] && !$request['template_id']) {
            return $this->response->error('template or message is missing', 500);
        }

        //If List ID Exists
        if ($request->list_id) {
            $contacts = collect([]);

            $collection_name = "lists_" . $request->list_id . "_user_" . $request->user()->id . "_contacts";
            $collection = (new SmstoMongo)->db()->{$collection_name};
            $items = $collection->find([
                'optedout' => false,
                'optout_date' => null
            ]);

            foreach ($items as $item) {
                $contacts->push($item);
            }

        } elseif ($request->contacts) {
            // If List Id Not Exist and manually Entered contact Exists
            $contacts = Contact::convertPhoneNumbersToContacts($request, $this->user);
        } else {
            return $this->response->error('missing list or contacts', 500);
        }

        //Dispatch Broadcast SMS Job
        dispatch(new SendSms($this->user, $contacts, $request->all()));

        return 'sent';
    }

    /*Function to find out estimate cost

      @param list_id,message,template_id,contacts
    */
    public function previewCost(Request $request)
    {
        $mapping = [];
        $estimatedTotalCosts = 0;
        $estimatedCost = 0;
        $totalMessages = 0;
        $contacts = [];

        //Validate Given Data
        $message = $request->message;
        if ($request->template_id && Auth::user()->templates()->find($request->template_id)) {
            $template = Auth::user()->templates()->find($request->template_id);
            $message = $template->messages;
        }

        if (is_null($message) || (!$request->list_id and !$request->contacts)) {
            return response()->json(['estimatedCost' => 0, 'totalMessages' => 0, 'error' => NULL]);
        }

        //try {
        if ($request->list_id) {
            $list = Auth::user()->lists()->find($request->list_id);
            $total = $this->estimateTotalCostOfList($list->id, $message);

            $error = Auth::user()->cash_balance < $total['cost'] ? 'No funds available' : NULL;

            $mssagesPerContact = ceil(strlen($message) / 160);

            $totalMessages = $total['count'] * $mssagesPerContact;


            return response()->json(['estimatedCost' => round($total['cost'], 5), 'totalMessages' => $totalMessages, 'error' => $error]);


        } else if ($request->contacts) {
            $user = Auth::user();
            $contacts = Contact::convertPhoneNumbersToContacts($request, $user);
        }

        if ($contacts) {
            $providerUsed = Provider::getproviderOfCountry($contacts[0]->country_code);

            $provider = $providerUsed->with('providerCurrency')->getProviderByName($providerUsed->name)->first();

            // To find the message character exceeds the limit(160),if it exceeds the total message count increment
            $mssagesPerContact = ceil(strlen($message) / 160);

            $totalMessages = count($contacts) * $mssagesPerContact;

            $cashBalance = Auth::user()->cash_balance;

            $rate_calculator = new RateCalculator;
            $estimatedTotalCosts = $rate_calculator->getTotalCostOfContacts($contacts, Auth::user()->billing_type, $message, $provider,Auth::id());

            // check if the credit balance is greater than estimate credits

            $error = $cashBalance < $estimatedTotalCosts ? 'No funds available' : NULL;
            return response()->json(['estimatedCost' => number_format($estimatedTotalCosts, 5), 'totalMessages' => $totalMessages, 'error' => $error]);
        }

        //} catch(Exception $e) {
//
        //}
    }

    public function estimateTotalCostOfListV2($list_id, $message)
    {
        $total = 0;
        $count = 0;
        $user = Auth::user();
        $collection_name = "lists_" . $list_id . "_user_" . $user->id . "_contacts";
//        $collection = $db->{$collection_name};
        $collection = (new SmstoMongo)->db()->{$collection_name};
        $collection = collect($collection->find())->chunk(10000);

        $message = format_sms_message($message, $user->id, $list_id);

        $collection->each(function ($contacts) use ($message, $user, &$total, &$count) {

            $estimatedTotalCosts = 0;

            $providerUsed = Provider::getproviderOfCountry($contacts->first()->country_code);

            $provider = $providerUsed->with('providerCurrency')->getProviderByName($providerUsed->name)->first();
            $count = $count + count($contacts);


            $rate_calculator = new RateCalculator;
            $contactsCost = $rate_calculator->getTotalCostOfContacts($contacts, $user->billing_type, $message, $provider,Auth::id());
            $total = $total + $contactsCost;
        });
        return ['cost' => $total, 'count' => $count];
    }

    public function estimateTotalCostOfList($list_id, $message)
    {
        return $this->estimateTotalCostOfListV2($list_id, $message);
        $total = 0;
        $count = 0;
        Contact::join('lists_contacts', 'lists_contacts.contact_id', '=', 'contacts.id')->where('list_id', $list_id)->chunk(10000, function ($contacts) use ($message, &$total, &$count) {
            $estimatedTotalCosts = 0;
            $providerUsed = Provider::getproviderOfCountry($contacts[0]->country_code);

            $provider = $providerUsed->with('providerCurrency')->getProviderByName($providerUsed->name)->first();
            $count = $count + count($contacts);

            $rate_calculator = new RateCalculator;
            $contactsCost = $rate_calculator->getTotalCostOfContacts($contacts, Auth::user()->billing_type, $message, $provider, Auth::id());
            $total = $total + $contactsCost;
        });
        return ['cost' => $total, 'count' => $count];
    }

    public function shortLink()
    {
        $link = new ShortLink;

        return response()->json(['data' => ['shortlink' => $link->generateLink(request('url'))]], 200);
    }


}

