<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Services\ContactImport;
use App\Models\ShortLink;
use App\Models\Payment;
use Auth;

class HomeController extends Controller
{
	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		// $this->middleware('auth');
		
		// These were in routes/web.php
		// SMS::driver('cheapglobalsms'); //
		// SMS::driver('websms');
	}

	/**
	 * Show the application dashboard.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index()
	{
		return view('landing/index');
	}
	
	public function import(Request $request)
	{
		// get data from request
		$code = $request->code;

		// get google service
		$googleService = \OAuth::consumer('Google');

		$contactImport = new ContactImport;
		
		if ($request->code) {
			return $contactImport->gmail($code);
		}
		
		// if not ask for permission first
		// get googleService authorization
		$url = $googleService->getAuthorizationUri();

		// return to google login url
		return redirect((string)$url);
		
	}

	public function showSmsApi()
	{
		return view('landing/sms-api');
	}
	
	public function showFaq()
	{
		return view('landing/faq');
	}

	public function showPricingPlans()
	{
		return view('landing/pricing/index');
	}

	public function showAbout()
	{
		return view('landing/about/index');
	}

	public function showPrivacyPolicy()
	{
		return view('landing/privacy-policy/index');
	}

	public function showRefundPolicy()
	{
		return view('landing/refund-policy');
	}

	public function showTerms()
	{
		return view('landing/terms/index');
	}

	public function showPress()
	{
		return view('landing/press/index');
	}
	
	public function shortLink($shortLink)
	{
		$link = ShortLink::where('shortlink', $shortLink)->first();
		if ($link) {
			// Increment the clicks
			$link->clicks += 1;
			$link->save(); 
			return redirect()->to($link->url);
		}

		die(404);
	}

	public function opOut($shortLink)
	{
		die(404);
	}
	public function showGDPRCompliance()
	{
		return view('landing/GDPR/index');
	}
}
