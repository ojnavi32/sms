<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;
use Storage;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        Commands\RecurringSms::class,
        Commands\UpdateExchangeRates::class,
        Commands\SmsSchedule::class,
        Commands\UpdateSmsStatus::class,
        Commands\GenerateReport::class,
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
       
        $schedule->command('sms:queue')
                 ->everyMinute();
        $schedule->command('sms:schedule')
                 ->everyMinute();
        $schedule->command('update:sms-status')
                 ->everyMinute();
        $schedule->command('report:generate')
                 ->everyMinute();

        $schedule->command('horizon:snapshot')->everyMinute();
                 
    }

    /**
     * Register the Closure based commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');
        
        require base_path('routes/console.php');
    }
}
