<?php 

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Carbon\carbon;
use Auth;
use App\User;
use App\Services\CountryInfo;
use App\Services\SMS\Calculators\RateCalculator;
use App\Models\{SmsBroadcastMessage, SmsBroadcast, SmsBroadcastBatch};
use App\Models\CashHistory;
use App\Contact;
use App\Models\CreditPackage;
use App\Models\Setting;
use App\Models\Rate;
use App\Models\Provider;

class SmsSchedule extends Command {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'sms:schedule';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Command to send the scheduled sms.';

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function handle()
	{ 
      $margin = Setting::getSetting('MARGIN_RATE');
      $profit = 1+ (1*$margin)/100;
      $senderId = null;

      $smsPerBatch = config('sms.sms_per_batch');  
      $this->costPerCredits = CreditPackage::minimumCostPerCredit();

      $today  = carbon::Now();
      $format1 = 'Y-m-d';
      $format2 = 'H:i:s';

      $date = Carbon::parse($today)->format($format1);
      $time = Carbon::parse($today)->format($format2);


       $smsBroadcasts = SmsBroadcast::where('scheduled_server_time', '<',  $today->format('Y-m-d H:i:s'))->where('schedule_processed',0)->get();
       

       foreach ($smsBroadcasts as $smsBroadcast) {
            $user = $smsBroadcast->user;
            $billType = $user->billing_type;

            $smsBroadcast->schedule_processed = 1;
            $smsBroadcast->save();
            
	       if($smsBroadcast->lists_id) { 
	             $contacts = user::find($smsBroadcast->user_id)->lists()->find($smsBroadcast->lists_id)->contacts;
	           
	             foreach($contacts as $contact)
	             {
	                if(!$contact->network_name || !$contact->country_code)
	                {
	                     $contact = $contact->setContactNetwork($contact,$user,1);

	                }
	             }
	        }elseif($smsBroadcast->manual_contacts) {
           
        // If List Id Not Exist and manually Entered contact Exists
            $numbers = explode(',', $smsBroadcast->manual_contacts);
            $contactsArray = [];
            foreach($numbers as $number)
            {
                $contact = new Contact;
                $contact->phone = $number;

                $contact = $contact->setContactNetwork($contact,$user);

                $contactsArray[] = $contact;  
            } 
        } else {
            return 'something went wrong';
        }





       	    $broadcastTotalCosts = 0;
            $smsMessagesBatches = [];
            $smsMessagesBatchesIndex = 0;
            $smsMessageCount = 0;
            $manualContacs = [];

            $rate_calculator = new RateCalculator;

            foreach($contactsArray as $contactArray) {

                        if(isset($mapping[$contactArray->country_code])) { 
                                $providerUsed =  $mapping[$contactArray->country_code];
                        }

                        else{ 
                                $rate = Rate::where('country_dial_code_id',$contactArray->country_code)->first();

                                $rateProviderPriority = $rate->highestPriorityProvider();

                                $mapping[$contactArray->country_code] = $rateProviderPriority->provider->name;
                                $providerUsed = $mapping[$contactArray->country_code];
                        }
                        
            }

            $provider =  Provider::with('providerCurrency')->getProviderByName($providerUsed)->first();

            $contacts = $rate_calculator->setSmsCost($contactsArray,$billType,@$this->request['message'],$provider);


            foreach($contacts as $contact) {
                    $smsMessageCount ++;
                    // calculate total cost
                    

                    $rate_calculator->min_cost = 0;
                   
                    $smsMessage = new SmsBroadcastMessage([
                        'sms_broadcast_id' => $smsBroadcast->id,
                        'cost' => $contact->cost, // Estimate cost
                        'our_cost' => $contact->ourCost, // Estimate our cost
                        'credits' => ceil($contact->ourCost/$this->costPerCredits), // Estimate credits
                        'user_id' => $smsBroadcast->user_id,
                        'contact_id' => $contact->id,
                        'send_to' => $contact->phone,
                        'country_dial_code_id' => $contact->country_code
                    ]);

                    $broadcastTotalCosts += $smsMessage->our_cost;

                    if(!$smsBroadcast->list_id) {
                        // insert to a batch
                        $manualContacts[$smsMessagesBatchesIndex][]=$contact->phone;
                    }

                    $smsMessagesBatches[$smsMessagesBatchesIndex][] = $smsMessage;
                    if (($smsMessageCount % $smsPerBatch) == 0) {
                        $smsMessagesBatchesIndex ++;
                    } 
            }

              // check client cash balance
            if ($smsBroadcast->user->cash_balance < $broadcastTotalCosts) { 
                        $funds_needed = $broadcastTotalCosts - $smsBroadcast->user->cash_balance;
                        $smsBroadcast->status = 'error';
                        $smsBroadcast->save();
                        return;
            } else {
                    $smsBroadcast->user->cash_balance -=  $broadcastTotalCosts; // Deduct estimate credits from user's credit balance
                    $smsBroadcast->user->save();
             
                    $historyData = [
                        'client_id' => $smsBroadcast->user->id,
                        'original_amount' => $smsBroadcast->user->cash_balance,
                        'amount' => $broadcastTotalCosts,
                        'fax_broadcast_id' => $smsBroadcast->id, //TODO change column name
                        'type' => 0, // lets do 0 for deduct
                        // 'by' => 1, // 1 for admin
                    ];
                    
                    // Save cash balance history
                    CashHistory::create($historyData);
                   
                    $smsBroadcast->total_cost = $broadcastTotalCosts; //TODO :Need to rename column as total credit
                    $smsBroadcast->status = "ongoing";
                    $smsBroadcast->batches = count($smsMessagesBatches);
                    $smsBroadcast->sent_ip = request()->ip();
            if ($smsBroadcast->save()) {
                foreach ($smsMessagesBatches as $batchIndex => $smsMessagesBatch) {
                    $smsBatch = new SmsBroadcastBatch;
                    $smsBatch->sms_broadcast_id = $smsBroadcast->id;
                    $smsBatch->message = $smsBroadcast->message;
                    $smsBatch->status = 'new';
                    $smsBatch->save();

                 
                    $smsBatch->smsMessages()->saveMany($smsMessagesBatch);

                    sleep(config('fax.broadcasting.fax_send_delay'));

                    // $manualContactsBatch = isset($manualContacts[$batchIndex])?$manualContacts[$batchIndex]:[];
                   
                    dispatch(new \App\Jobs\SendSmsBatch($smsBatch, $senderId));
                }

                    // dispatch(new \App\Jobs\UpdateBroadcastRealPrice($smsBroadcast))->delay(150);
            }
        }
          
       }

	}

}
