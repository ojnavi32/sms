<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

use App\Models\SmsBroadcast;
use URL;
use File;
use Excel;

class GenerateReport extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'report:generate';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command to generate broadcast report';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    { 
        $smsBroadcasts = SmsBroadcast::where([['report',0],['status','done']])->get();
        $basePath =  URL::to('/');

            foreach ($smsBroadcasts as $smsBroadcast) {
                $batches = $smsBroadcast->find($smsBroadcast->id)->messages()->get();

                $path = storage_path('app/public').'/broadcasts/csv/'.$smsBroadcast->id;
                File::makeDirectory($path, $mode = 0777, true, true);
                
                $broadcastCsvFile = 'broadcast'. '.csv';

                $handle = fopen($path.'/'.$broadcastCsvFile, 'w+');
                fputcsv($handle, array('Message','Phone','Delivered','Failed','Cost','Send Date','Status'));

               
                foreach ($batches as $batch) {

                    fputcsv($handle, array($batch->message, $batch->send_to, $batch->delivered,$batch->failed,$batch->our_cost,$batch->created_at,$batch->status));
                }


                fclose($handle);
            }    

            $this->excelConversion($smsBroadcasts);
    }

    public function excelConversion($smsBroadcasts)
    {
       $basePath =  URL::to('/');

        foreach ($smsBroadcasts as $smsBroadcast) {
            $batches = $smsBroadcast->find($smsBroadcast->id)->messages()->get();

            $path = storage_path('app/public').'/broadcasts/excel/'.$smsBroadcast->id;
            File::makeDirectory($path, $mode = 0777, true, true);


            $broadcastExcelFile = 'broadcast';


            Excel::create($broadcastExcelFile, function($excel) use ($batches) { 
                $excel->sheet('mySheet', function($sheet) use ($batches)
                { 
                    $sheet->cell('A1', function($cell) {$cell->setValue('Message');   });
                    $sheet->cell('B1', function($cell) {$cell->setValue('Phone');   });
                    $sheet->cell('C1', function($cell) {$cell->setValue('Delivered');   });
                    $sheet->cell('D1', function($cell) {$cell->setValue('Failed');   });
                    $sheet->cell('E1', function($cell) {$cell->setValue('Cost');   });
                    $sheet->cell('F1', function($cell) {$cell->setValue('Send Date');   });
                    $sheet->cell('G1', function($cell) {$cell->setValue('Status');   });
                    
                    if (!empty($batches)) {
                        foreach ($batches as $key => $value) {
                            $i= $key+2;
                            $sheet->cell('A'.$i, $value['message']); 
                            $sheet->cell('B'.$i, $value['send_to']); 
                            $sheet->cell('C'.$i, $value['delivered']); 
                            $sheet->cell('D'.$i, $value['failed']); 
                            $sheet->cell('E'.$i, $value['our_cost']); 
                            $sheet->cell('F'.$i, $value['created_at']);
                            $sheet->cell('G'.$i, $value['status']);
                        }
                    }

                });
            })->store('xlsx',$path.'/');
        $smsBroadcast->report = 1;
        $smsBroadcast->save();
        }
    }
}
