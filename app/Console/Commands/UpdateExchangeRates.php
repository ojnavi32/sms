<?php 

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Services\OpenExchangeRates;
use App\Models\ExchangeRate;

class UpdateExchangeRates extends Command {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'update_exchange_rates';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Command to update exchange rates table in database.';

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function handle()
	{
        $oer = new OpenExchangeRates();
        $response = $oer->getCurrencies();
        $currencies = json_decode($response->getBody(), true);
        foreach ($currencies as $currency_code=>$currency_name) {
            ExchangeRate::updateOrCreate(['currency_code' => $currency_code], ['currency_code' => $currency_code, 'currency_name' => $currency_name]);
        }
        $response = $oer->getLatest();
        $latest = json_decode($response->getBody(), true);
        foreach ($latest['rates'] as $currency_code=>$rate) {
            ExchangeRate::updateOrCreate(['currency_code' => $currency_code], ['currency_code' => $currency_code, 'rate' => $rate]);
        }
		$this->comment('Exchange rates table updated!');
	}

}
