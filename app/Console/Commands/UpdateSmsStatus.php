<?php 

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Carbon\carbon;
use App\Models\SmsBroadcast;
use App\Models\SmsBroadcastBatch;
use App\Models\SmsBroadcastMessage;
use App\Services\SMS\Providers\InfoBip;
use Mail;
use App\Models\Provider;
use App\Services\SmsHandler\SmsHandler;
use App\Mail\SmsStatus;
use App\Mail\FailedBroadcast;
use App\Mail\StatusNotUpdatedBroadcast;

class UpdateSmsStatus extends Command {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'update:sms-status';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Command to update delivery status.';

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function handle()
	{ 

      $batches = SmsBroadcastBatch::where('status','ongoing')->where('created_at', '>=', Carbon::now()->subDays(3)->toDateTimeString())->limit(15)->get();

      foreach($batches as $batch)
      {
            $provider = Provider::find($batch->provider_id);
            $sms = new SmsHandler($provider->name); 
           
            
            if($batch->bulk_id){
            	$response = $sms->commProvider->batchDeliveryStatus($batch->bulk_id);
            }	
            else{
            	$response = NULL;
            	
	            $batchMessage = $batch->smsMessages->where('status','PENDING')->first();
	            
	            if($batchMessage)
	               $response = $sms->commProvider->messageDeliveryStatus($batchMessage->message_id);
	            
        	}

            if($response) {
   			   dispatch(new \App\Jobs\BatchDeliveryUpdate($response, $batch));
            }
     }
     
       $broadcasts = SmsBroadcast::where('status','ongoing')->get();

     	// Check if any ongoing batches under broadcasst, if no, update broadcast status to 'done'
     	foreach($broadcasts as $broadcast){
     		$broadcastStatusCheck = $broadcast->broadcastBatches->where('status', '!=', 'done')->count();
     			if(!$broadcastStatusCheck){
     				$broadcast->status = 'done';        				
				    $broadcast->save(); 

					 $userName = $broadcast->user->name;
	     			 $userEmail = $broadcast->user->email;
	     			 $deliveredCount = $broadcast->deliveredMessages->count();
	     			 $failedCount = $broadcast->failedMessages->count();
	     			 $listName = isset($broadcast->list)?$broadcast->list->name:'';
	     			 $broadcastId = $broadcast->id;

	     			 Mail::to($userEmail)->send(new SmsStatus($userName,$deliveredCount,$failedCount,$listName,$broadcastId));

     			}
     	}



     	$batches = SmsBroadcastBatch::where('status','new')->where('created_at','<=',Carbon::now()->subHour(1)->toDateTimeString())->get();

     	// Check if any ongoing batches under broadcasst, if no, update broadcast status to 'failed'
     	foreach($batches as $batch){
     		
     		$batch->status = 'failed';
			$batch->save();

     		$batch->smsBroadcast->status = 'failed';        				
			$batch->smsBroadcast->save(); 

			$userName = $batch->smsBroadcast->user->name;
			$subMessage = 'Failed To Send SMS To Provider';
			$message = 'SMS failed to send from '.$userName ;

			Mail::to('support@sms.to')->send(new FailedBroadcast($subMessage,$message));
					     			
     	}

     	$batches = SmsBroadcastBatch::where('status','ongoing')->where('created_at','<=',Carbon::now()->subHour(1)->toDateTimeString())->get();

     	// Check if any ongoing batches under broadcasst, if no, update broadcast status to 'failed'
     	foreach($batches as $batch){
     		
     		$batch->status = 'failed';
			$batch->save();

     		$batch->smsBroadcast->status = 'failed';        				
			$batch->smsBroadcast->save(); 

			$userName = $batch->smsBroadcast->user->name;
			$subMessage = 'Failed To Update SMS Status';
			$message = 'SMS from user '. $userName .' is ongoing for over 1 hour';

			Mail::to('support@sms.to')->send(new FailedBroadcast($subMessage,$message));
					     			
     	}

	}
}
