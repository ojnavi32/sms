<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\SmsBroadcast;
use Carbon\Carbon;

class RecurringSms extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'sms:queue';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send SMS';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        // Check for any scheduled SMS
        $date = Carbon::now();
        $smsSchedules = SmsBroadcast::where('sched_date', $date->toDateString())->limit(100)->get();
        
        foreach ($smsSchedules as $sms) {
            // We need to put the sms broadcasts to a queue
            if ($ms->time == $date->toDateString()) {
                
            }
        }
        
        $this->comment('sms sent!');
    }
}
