<?php 

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Mail\ContactUs;
use App\Mail\ContactUsMailToSmsTo;
use App\Mail\ImportContact;
use App\Mail\SmsStatus;
use App\Mail\SuccessAddedBalance;
use App\Mail\VerifyMail;
use App\Mail\WelcomeEmail;
use App\Mail\YourSmsToPassword;
use App\User;
use Mail;
use App\Models\SmsBroadcast;
use App\Models\VerifyUser;
use Illuminate\Support\Facades\Password;

class EmailSend extends Command {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	// protected $name = 'email:sender {email = gfgffg}';
	protected $signature = 'email:sender {email}';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Command to send all email.';

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function handle()
	{

		$email = $this->argument('email');

		$user = User::where('email', 'admin@sms.to')->first();
		if(!$user)
		{
			die();
		}

		$credentials = ['email' => $email];
        $response = Password::sendResetLink($credentials, function (Message $message) {
            $message->subject($this->getEmailSubject());
        });


		$smsSendLink = url('/').'/app#/sms/send'; 
		$passwordResetlink = url('/').'/password/reset';

		$broadcast = SmsBroadcast::first();
		$broadcast_id = isset($broadcast->id) ? $broadcast->id : 1;

		$message = 'Test SMS!';
		$listName = 'Test List';
		$phone = '+918670725097';
		$total = 100;
		$deliveredCount = 2;
		$failedCount = 0;
		$user->tempPassword = str_random(7);


		$data = [
                'name' => $user->name, 
                'balance' => $user->cash_balance, 
                'status' => 'success', 
                'credits' => 50,
                'amount' => 100
            ];

         // Store verify_users data
        $verifyUser = VerifyUser::create([
            'user_id' => $user->id,
            'token' => str_random(40)
        ]);    

		// Email to staff
        Mail::to($email)->send(new ContactUsMailToSmsTo($user->name,$user->email,$message,$phone));

        // Auto respond email to sender
        Mail::to($email)->send(new ContactUs($user->name));

        // Contact Import mail
        Mail::to($email)->send(new ImportContact($user,$total,$listName));

        // email after delivering sms
        Mail::to($email)->send(new SmsStatus($user->name,$deliveredCount,$failedCount,$listName,$broadcast_id));

        // Mail sending after balnace added
        Mail::to($email)->send(new SuccessAddedBalance($data,$smsSendLink));

        // Verify email
        Mail::to($email)->send(new VerifyMail($user));

        // welcome email
        Mail::to($email)->send(new WelcomeEmail($user,$passwordResetlink));

        // password email
        Mail::to($email)->send(new YourSmsToPassword($user,$passwordResetlink));

    }
}
