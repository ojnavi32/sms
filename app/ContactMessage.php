<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ContactMessage extends Model
{
    use SoftDeletes;
    
    protected $table = 'contacts_messages';
    
    protected $fillable = [
        'user_id', 'contact_list_id', 'contact_id', 'message_id', 'rate_id', 'cost',
    ];
    
    public function user()
    {
    	return $this->belongsTo(User::class);
    }
    
    public function list()
    {
    	return $this->belongsTo(Lists::class);
    }
    
    public function contact()
    {
    	return $this->belongsTo(Contact::class);
    }
    
    public function message()
    {
    	return $this->belongsTo(Message::class);
    }
    
    public function rate()
    {
    	return $this->belongsTo(Rate::class);
    }
    
    public function scopeSuccess($query)
    {
    	return $this->where('status', 'success');
    }
    
    public function scopeFailed($query)
    {
    	return $this->where('status', 'failed');
    }
}
