<?php

namespace App\Notifications;

use Illuminate\Notifications\Notification;
use Illuminate\Notifications\Messages\MailMessage;

class ResetPassword extends Notification
{
    public $token;

    public function __construct($token)
    {
        $this->token = $token;
    }

    public function via($notifiable)
    {
        return ['mail'];
    }

    public function toMail($notifiable)
    {
        return (new MailMessage)
            ->subject('Reset your password - SMS.to')
            ->line("We got a password reset request for your account. If you didn't request it, please ignore!")
            ->action('Reset Password', url(config('app.url').route('password.reset', $this->token, false)));
            // ->line('If you did not request a password reset, no further action is required.');
    }        
}