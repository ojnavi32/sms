<?php 

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Postback extends Model {

	use SoftDeletes;

	protected $table = 'postbacks';

	protected $fillable = ['user_id', 'fax_job_id', 'url', 'status', 'msg_code', 'message', 'status_code'];

	public function transcations()
    {
        return $this->hasMany('App\Models\PostbackTransaction');
    }
    
    public function faxJob()
    {
        return $this->belongsTo('App\Models\FaxJob');
    }
}
