<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Scopes\LimitScope;

class Notification extends Model {

	use SoftDeletes;

	protected $table = 'notifications';

	protected $fillable = ['user_id', 'messages', 'read'];

	/**
     * The "booting" method of the model.
     *
     * @return void
     */
    protected static function boot()
    {
        parent::boot();

        static::addGlobalScope(new LimitScope);
    }

	public function user()
    {
        return $this->BelongsTo(User::class);
    }

}
