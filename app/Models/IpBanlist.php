<?php 

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

use Cache;

class IpBanlist extends Model {

    use SoftDeletes;

    protected $table = 'banlist_ip';

    protected $fillable = ['ip_address'];

    public static function storeCache()
    {
    	Cache::forget('banned_ip_address');

        // Lets put the banned ip address to cache
        Cache::rememberForever('banned_ip_address', function() {
            return static::pluck('ip_address');
        });
    }

}
