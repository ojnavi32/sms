<?php 

namespace App\Models;

use DB;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CreditPackage extends Model {

	use SoftDeletes;
	
    protected $table = 'credits_packages';

	protected $fillable = ['name', 'credits', 'price'];

	public static function minimumCostPerCredit()
    {
    	$maxCredit = CreditPackage::orderBy('credits','desc')->first();
    	$minimumCostPerCredit = $maxCredit->price/$maxCredit->credits;
    	
        return $minimumCostPerCredit;
    }
    
}
