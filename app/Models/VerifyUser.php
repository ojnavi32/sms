<?php 

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class VerifyUser extends Model
{
    protected $guarded = [];
    
    protected $table = 'verify_users';

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }
 
}