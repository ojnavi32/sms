<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PaymentHistoryDetail extends Model
{

    protected $table = 'payment_history_details';
    protected $fillable = ['payment_history_id', 'name', 'info', 'created_at'];

    public function payment_history() {
        return $this->belongsTo('App\Models\PaymentHistory');
    }

    public static function updatePaymentHistory($ids, $billingDetail)
    {
    	if (count($ids) > 0) {
            foreach ($ids as $id) {
                
                static::firstOrCreate([
                    'payment_history_id' => $id,
                    'name' => 'Name',
                ]);

                static::where('payment_history_id', $id)
                    ->where('name', 'Name')
                    ->update(['info' => $billingDetail->firstname . ' ' . $billingDetail->lastname]);
                
                if ($billingDetail->company != '') {
                    $company = static::where('payment_history_id', $id)
                        ->where('name', 'Company')
                        ->first();

                    if ( ! $company ) {
                        static::create([
                                'payment_history_id'=> $id,
                                'name' => 'Company',
                                'info' => $billingDetail->company
                        ]);
                    } else {
                    	$company->info = $billingDetail->company;
                    	$company->save();
                    }
                }

                static::firstOrCreate([
                    'payment_history_id' => $id,
                    'name' => 'Street Address',
                    'info' => $billingDetail->street_address,
                ]);

                static::firstOrCreate([
                    'payment_history_id' => $id,
                    'name' => 'Postal Code',
                    'info' => $billingDetail->postal_code
                ]);

                static::firstOrCreate([
                    'payment_history_id' => $id,
                    'name' => 'Country',
                    'info' =>  $billingDetail->country_dial_code->country
                ]);
                
                static::where('payment_history_id', $id)
                    ->where('name', 'Street Address')
                    ->update(['info' => $billingDetail->street_address]);

                static::where('payment_history_id', $id)
                    ->where('name', 'Postal Code')
                    ->update(['info' => $billingDetail->postal_code]);

                static::where('payment_history_id', $id)
                    ->where('name', 'Country')
                    ->update(['info' => $billingDetail->country_dial_code->country]);
            }
        }
    }

}
