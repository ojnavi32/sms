<?php 

namespace App\Models;

use DB;
use App\Models\Provider;

use Illuminate\Database\Eloquent\Model;

class ProviderRate extends Model {

    protected $table = 'providers_rates';

    protected $fillable = ['provider_id', 'rate_id', 'base_rate', 'our_rate', 'network'];

    public function rate()
    {
        return $this->belongsTo('App\Models\Rate','rate_id');
    }

    public function provider()
    {
        return $this->belongsTo(Provider::class);
    }

    public static function getCarrierRate($countryCode,$networkName,$providerId)
    { 
        $providerRate = DB::table('providers_rates')->select('providers_rates.base_rate')
                  ->join('rates', 'providers_rates.rate_id', '=', 'rates.id')
                  ->where('providers_rates.network', 'LIKE', '%'. $networkName. '%')
                  ->where('rates.country_dial_code_id',$countryCode)
                  ->where('providers_rates.provider_id',$providerId)->max('providers_rates.base_rate'); //TODO kobe:-it should be dynamic based on provider
           
        if(!$providerRate)
        { 
            $providerRate = DB::table('providers_rates')
                        ->join('rates', 'providers_rates.rate_id', '=', 'rates.id')
                        ->where('rates.country_dial_code_id',$countryCode)
                        ->where('providers_rates.provider_id',$providerId)
                        ->max('providers_rates.base_rate');                  
        }

        return $providerRate;
    }

    public static function getCountryNetworks($rateId){
             //To get Lists of all Avail;able mobile newtworks for a country, use Infobip rate sheet
             $providerId = Provider::where('slug','infobip')->pluck('id');
             return self::where('provider_id', $providerId)
                          ->where('rate_id', $rateId)
                          ->pluck('network', 'network');

    }
}
