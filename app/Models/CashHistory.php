<?php 

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CashHistory extends Model
{
    protected $table = 'cash_history';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'client_id','original_amount', 'amount', 'fax_broadcast_id', 'type', 'by'
    ];

}