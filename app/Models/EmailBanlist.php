<?php 

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class EmailBanlist extends Model {

    use SoftDeletes;

    protected $table = 'banlist_email';

    protected $fillable = ['match_string'];

}
