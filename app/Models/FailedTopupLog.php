<?php 

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class FailedTopupLog extends Model {

	use SoftDeletes;

    protected $table = 'failed_topup_logs';

	protected $fillable = ['user_id', 'error_type', 'topup_details', 'error_message'];


	public function user()
    {
        return $this->belongsTo('App\Models\User');
    }

}
