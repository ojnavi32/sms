<?php 

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CardExpiryNotifications extends Model {


    protected $table = 'card_expiry_notifications';

	protected $fillable = ['token','day_30','day_15','day_1'];


}
