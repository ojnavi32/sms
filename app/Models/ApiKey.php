<?php 

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ApiKey extends Model {

	use SoftDeletes;

	protected $table = 'api_keys';

	protected $fillable = ['user_id', 'api_key', 'description', 'mode', 'status', 'access_key'];

	public function user()
    {
        return $this->BelongsTo('App\Models\User');
    }
}