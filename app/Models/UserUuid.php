<?php 

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class UserUuid extends Model {

	use SoftDeletes;

	protected $table = 'users_uuids';

	protected $fillable = ['uuid', 'user_id'];

	public function user()
    {
        return $this->BelongsTo('App\Models\User');
    }
}
