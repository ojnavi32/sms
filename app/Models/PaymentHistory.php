<?php

namespace App\Models;
use App\Models\UserDids;

use Illuminate\Database\Eloquent\Model;

class PaymentHistory extends Model
{
    protected $table = 'payment_history';
    
    protected $fillable = ['user_id', 'payment_method_id', 'user_did_id', 'amount', 'vat', 'vat_exempt', 'currency', 
        'status', 'ontrial', 'subscription_id', 'invoice_date', 'android_purchase_token', 'created_at',
    ];

    public function user() {
        return $this->belongsTo('App\Models\User')->withTrashed();
    }

    public function payment_method() {
        return $this->belongsTo('App\Models\PaymentMethod');
    }

    public function payment_history_details() {
        return $this->hasMany('App\Models\PaymentHistoryDetail');
    }

    public function scopeTeam($query)
    {
        return $query->whereIn('user_id', auth()->user()->team_members);
    }

    public function getVatExemptionAttribute()
    {
        if ($this->vat_exempt == true) {
            return 'Yes';
        }

        return 'No';
    }

    public function getDetails($id)
    {
        $payment = $this->find($id);

        if ($payment->vat) {
            $taxAmount = round((($payment->vat/100) * $payment->amount), 2);
            $payAmount = $payment->amount + $taxAmount;
        } else {
            $taxAmount = NULL;
            $payAmount = $payment->amount;
        }
        
        if ($payment) {
            $pageData = [
                'meta_title' => trans('meta.payment_details'),
                'payment' => $payment,
                'tax_amount' => $taxAmount,
                'pay_amount' => $payAmount,
                'description' => 'Credit balance',
            ];

            // If its a subscription
            if ($payment->subscription_id) {
                $did = UserDids::where('subscription_id', $payment->subscription_id)->first();
                $pageData['description'] = '';
                if ($did) {
                    $pageData['description'] = 'Fax Number (' . $did->did_number . ')';
                }
            }
            
            // If its payment using cash balance
            if ($payment->user_did_id) {
                $did = UserDids::find($payment->user_did_id);
                $pageData['description'] = '';
                if ($did) {
                    $pageData['description'] = 'Fax Number (' . $did->did_number . ')';
                }
            }
            

            $pageData['payment_detail'] = $payment->payment_history_details->pluck('info', 'name')->all();
            $pageData['user'] = $payment->user;

            return $pageData;

        } else {
            abort(404);
        }
    }
}
