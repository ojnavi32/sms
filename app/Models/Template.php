<?php 

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Scopes\LimitScope;

use Kyslik\ColumnSortable\Sortable;

class Template extends Model {

	use SoftDeletes, Sortable;
    
	protected $table = 'templates';

	protected $fillable = ['user_id', 'name', 'messages'];
    
    public $sortable = [
                        'id',
                        'name',
                        'created_at',
                        'updated_at'
    ];

     /**
     * The "booting" method of the model.
     *
     * @return void
     */
    protected static function boot()
    {
        parent::boot();

        static::addGlobalScope(new LimitScope);
    }


	public function user()
    {
        return $this->BelongsTo(User::class);
    }
    
    public function words()
    {
        
    }
    
    public function getCharactersAttribute()
    {
    	return str_len($this->messages);
    }
    
    public function scopeSearch($query, $srchPrm, $value)
    { 
        if ($srchPrm != null && $value != null) {
            return $query->where($srchPrm, 'LIKE', '%' . $value . '%');
        }
        
        return $query;
    }

    public function scopeOrSearch($query, $srchPrm, $value)
    { 
        if ($srchPrm != null && $value != null) {
            return $query->orWhere($srchPrm, 'LIKE', '%' . $value . '%');
        }

        return $query;
    }
}
