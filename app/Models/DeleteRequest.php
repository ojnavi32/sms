<?php 

namespace App\Models;

use DB;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class DeleteRequest extends Model {

	use SoftDeletes;
	
    protected $table = 'delete_requests';

	protected $fillable = ['time_stamp', 'request_ip', 'user_id', 'name', 'email','member_deleted_at','is_deleted'];

    public function user(){

    	return $this->hasOne('App\User','id','user_id');
    }
}
