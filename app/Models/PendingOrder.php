<?php 

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Uuid;

class PendingOrder extends Model {

    use SoftDeletes;

    protected $table = 'pending_orders';

    protected $fillable = [
        'user_id', 'document_id', 'trans_type', 'amount', 'did_group_id', 'contract_type', 'did_number', 
        'porting_request_id', 'uuid', 'sessions', 'fulfilled',
    ];

    public function user()
    {
    	return $this->belongsTo(User::class);
    }

    public function document()
    {
        return $this->belongsTo(Document::class);
    }

    public function savePendingOrder()
    {
    	$transType = 'did';
        $sessions = [
            'numberType' => session('numberType'),
            'name' => session('name'),
            'country' => session('country'),
            'areaCode' => session('areaCode'),
            'contractType' => session('contractType'),
            'contract_amount' => session('contract_amount'),
            'destinationEmail' => session('destinationEmail'),
            'didGroupId' => session('didGroupId'),
            'verificationRequired' => session('verificationRequired'),
        ];

        $currentProvider = session('current_provider');
        if ($currentProvider) {
            $transType = 'port';
            $sessions = [
                'count_numbers' => session('count_numbers'),
                'name' => session('name'),
                'number_type' => session('number_type'),
                'country_id' => session('country_id'),
                'country' => session('country'),
                'area_code' => session('area_code'),
                'areaCode' => session('areaCode'),
                'current_provider' => session('current_provider'),
                'numbers' => session('numbers'),
                'contractType' => session('contractType'),
                'contract_amount' => session('contract_amount'),
                'destinationEmail' => session('destinationEmail'),
                'didGroupId' => session('didGroupId'),
            ];
        }

        // Save pending orders for follow up email use
        $orders = $this->firstOrNew([
                    'user_id' => auth()->user()->id, 
                    'did_group_id' => session('didGroupId'), 
                    'contract_type' => session('contractType'),
                    'trans_type' => $transType,
                    'amount' => session('contract_amount'),
        ]);

        $orders->uuid = Uuid::generate();
        $orders->sessions = json_encode($sessions);
        $orders->save();

        // for did
        // "numberType":"GEOGRAPHIC","name":"Numero","country":"USA","areaCode":"337","contractType":"3m",
        // "contract_amount":27,"destinationEmail":"max5@gmail.com","didGroupId":"6557","verificationRequired":false,

        // port
        // "count_numbers":1,"name":"name name","areaCode":"337","contractType":"3m","contract_amount":27,
        // "destinationEmail":"max5@gmail.com","didGroupId":"6557 ","number_type":"GEOGRAPHIC","country_id":[41],
        // "country":"USA","area_code":"337","current_provider":"smart","numbers":"+3232323232"}
    }
}
