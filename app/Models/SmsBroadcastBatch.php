<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Scopes\LimitScope;

class SmsBroadcastBatch extends Model
{
	use SoftDeletes;
	
    protected $table = 'sms_broadcasts_batches';
    
    protected $fillable = [
        'status', 'message', 'time_run',
    ];

    /**
     * The "booting" method of the model.
     *
     * @return void
     */
    protected static function boot()
    {
        parent::boot();

        static::addGlobalScope(new LimitScope);
    }

    public function smsBroadcast()
    {
    	return $this->belongsTo(SmsBroadcast::class);
    }

    public function smsMessages()
    {
        return $this->hasMany('App\Models\SmsBroadcastMessage');
    }

    public function getStatusFormatAttribute()
    {
        if ($this->status == 'success') {
            return 'success';
        }

        if ($this->status == 'failed') {
            return 'danger';
        }

        return 'warning';
    }
    public function messages()
    {
        return $this->hasMany(SmsBroadcastMessage::class);
    }
    public function provider()
    {
        return $this->hasOne(Provider::class,'id','provider_id');
    }

}
