<?php 

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class UserTeam extends Model {

	use SoftDeletes;

	protected $table = 'users_team';

	protected $fillable = ['parent_user_id', 'user_id', 'notify_email', 'send', 'receive'];

	public function user()
	{
		return $this->belongsTo(User::class);
	}
}
