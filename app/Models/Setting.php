<?php 

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Cache;

class Setting extends Model
{
    protected $table = 'settings';

    protected $fillable = ['key', 'value'];

    public function scopeSetting($query, $key)
    {
        return $query->where('key', $key)->first()->value;
    }

    public static function getSetting($key, $default = null)
    {
        return Cache::get($key, function () use ($key, $default) {
            $seting = static::where('key', $key)->first();
            if ($seting) {
                return $seting->value;
            }
            return $default;
        });
    }
}
