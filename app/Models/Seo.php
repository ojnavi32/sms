<?php 

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Seo extends Model {

	use SoftDeletes;

	protected $table = 'seo';

	protected $fillable = ['url', 'locale', 'title', 'description', 'h1'];

	public function scopeLang($query, $lang)
	{
		return $query->where('locale', $lang);
	}
}
