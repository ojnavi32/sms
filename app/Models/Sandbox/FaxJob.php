<?php namespace App\Models\Sandbox;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class FaxJob extends Model
{
    use SoftDeletes;

    protected $table = 'z_fax_jobs';

    protected $fillable = ['user_id', 'from', 'fax_number', 'delete_after_success', 'job_api', 'post_delivered', 'document_id', 'user_ipv4', 'rate_id', 'fax_broadcast_id', 'status', 'message', 'cost'];

    public function user()
    {
        return $this->BelongsTo('App\Models\User');
    }

    public function document()
    {
        return $this->belongsTo('App\Models\Sandbox\Document')->withTrashed();
    }

    public function rate()
    {
        return $this->belongsTo('App\Models\Rate');
    }

    public function provider_priorities()
    {
        return $this->rate->provider_priorities();
    }

    public function fax_job_transactions()
    {
        return $this->hasMany('App\Models\FaxJobTransaction');
    }

    public function getUserIpv4IpAttribute()
    {
        return long2ip($this->attributes['user_ipv4']);
    }

    public function fax_broadcast()
    {
        return $this->belongsTo('App\Models\FaxBroadcast');
    }
}