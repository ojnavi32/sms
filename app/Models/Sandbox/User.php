<?php namespace App\Models\Sandbox;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;

class User extends Model implements AuthenticatableContract, CanResetPasswordContract
{

    use Authenticatable,
        CanResetPassword,
        SoftDeletes;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'z_users';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'email', 'password', 'cash_balance', 'verified', 'verification_code'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = ['password', 'remember_token'];
    public $include_delete_relatives = ['billing_detail', 'fax_jobs'];

    public function role()
    {
        return $this->belongsTo('App\Models\Role');
    }

    public function billing_detail()
    {
        return $this->hasOne('App\Models\BillingDetail');
    }

    public function payments()
    {
        return $this->hasMany('App\Models\Payment');
    }

    public function braintree_customer()
    {
        return $this->hasOne('App\Models\BraintreeCustomer');
    }

    public function payment_history()
    {
        return $this->hasMany('App\Models\PaymentHistory');
    }

    public function documents()
    {
        return $this->hasMany('App\Models\Document');
    }

    public function fax_jobs()
    {
        return $this->hasMany('App\Models\FaxJob');
    }

    public function fax_broadcasts()
    {
        return $this->hasMany('App\Models\FaxBroadcast');
    }

    public function has_pending_fax_jobs()
    {
        return $this->fax_jobs()->whereStatus('pending')->count();
    }

    public function scopeOfRole($query, $role)
    {
        try {
            $id = Role::whereSlug($role)->first()->id;
        } catch(\ErrorException $e) {
            $id = NULL;
        }
        return $query->whereRoleId($id);
    }

    public function user_rate_adjustments()
    {
        return $this->hasMany('App\Models\UserRateAdjustment');
    }

    public function failed_topup_logs()
    {
        return $this->hasMany('App\Models\FailedTopupLog');
    }

    // public function getMainCashBalanceAttribute($value)
    // {
    //     return $this->cash_balance;
    // }

    // public function getCashBalanceAttribute($value)
    // {
    //     return $value + $this->cash_balance_sub;
    // }

    // public function getTotalCashBalanceAttribute($value)
    // {
    //     return $this->cash_balance + $this->cash_balance_sub;
    // }
}
