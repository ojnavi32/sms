<?php namespace App\Models\Sandbox;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Auth, Storage;

class Document extends Model {

	use SoftDeletes;

	protected $table = 'z_documents';

	protected $fillable = ['user_id', 'filename', 'filename_uploaded', 'orig_filename', 'file_extension', 'total_pages', 'filesize', 'preview_in_storage', 'preview_file', 'preview_image'];

	public function user()
    {
        return $this->BelongsTo('App\Models\Sandbox\User');
    }

    public function fax_jobs()
    {
        return $this->hasMany('App\Models\Sandbox\FaxJob');
    }

    public function getFilenameDisplayAttribute()
    {
        if ($this->filename_uploaded != '') {
            return $this->filename_uploaded;
        }
        if ($this->orig_filename != '') {
            return $this->orig_filename;
        }
        return $this->filename;
    }

    public function myDocumentUrl()
    {
        // We can check if the file is not exists
        return 'documents/' . $this->user_id . '/' . $this->filename;
    }
}
