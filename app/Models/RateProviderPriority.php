<?php 

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

use Illuminate\Database\Eloquent\SoftDeletes;

use App\Models\Provider;

class RateProviderPriority extends Model {


	use SoftDeletes;

    protected $table = 'rates_providers_priorities';

	protected $fillable = ['rate_id', 'provider_id', 'priority'];

	public function provider()
    { 
        return $this->belongsTo('App\Models\Provider','provider_id');
    }

    public static function providerCount($request)
    { 
        return $count = RateProviderPriority::where('rate_id',$request->rate_id)->count();
    }

}