<?php 

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Carbon\Carbon;

class UserCashBalance extends Model {

	use SoftDeletes;

	protected $table = 'users_cash_balances';

	protected $fillable = ['user_id', 'amount', 'month', 'year', 'subscription_id', 'expiration_date', 'status'];
}
