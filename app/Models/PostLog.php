<?php 

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Carbon\Carbon;

class PostLog extends Model {

	use SoftDeletes;

	protected $table = 'post_logs';

	protected $fillable = ['braintree', 'content'];

	public function scopeBraintree($query, $braintree = null)
	{
		return $query->where('braintree', $braintree);
	}
}
