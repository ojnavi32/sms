<?php 

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Rate extends Model {

    protected $table = 'rates';

    protected $fillable = ['country_dial_code_id', 'name', 'plan', 'base_rate', 'multiplier','kobe'];

    public $include_delete_relatives = ['rates_providers_priorities', 'user_rate_adjustments'];

    public function country_dial_code()
    {
        return $this->belongsTo(CountryDialCode::class);
    }

    public function fax_jobs()
    {
        return $this->hasMany('App\Models\FaxJob');
    }

    public function rate_provider_priorities()
    {
        return $this->hasMany('App\Models\RateProviderPriority','rate_id')->orderBy('priority');
    }

    public function provider_priorities()
    {
        return $this->belongsToMany('App\Models\CommProvider', 'rate_provider_priorities')->withPivot('id', 'priority')->orderBy('priority');
    }

    public function price_per_page()
    {
        return number_format($this->base_rate * $this->multiplier, 2);
    }

    public function user_rate_adjustments()
    {
        return $this->hasMany(UserRateAdjustment::class);
    }
    
    public function providersRate()
    { 
        return $this->hasMany(ProviderRate::class);
    }

    public function highestPriorityProvider()
    { 
        if($this->rate_provider_priorities)
           return $this->rate_provider_priorities->first();

    }
    
    public function pRate()
    {
        return $this->hasOne(ProviderRate::class, 'rate_id', 'rate_id');
    }

    public static function getCountryRate($countryCode)
    {
        $rate = Rate::where('country_dial_code_id',$countryCode)->first(); //TODO kobe:-it should be dynamic based on provider
                 
        // if(!$rate)
        // { 
        //     $rate = Rate::where('country_dial_code_id',$countryCode)
        //                 ->max('base_rate');                  
        // }

        return $rate->base_rate;
    }
}
