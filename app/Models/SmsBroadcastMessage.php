<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Scopes\LimitScope;

class SmsBroadcastMessage extends Model
{
	use SoftDeletes;
	
    protected $table = 'sms_broadcasts_messages';
    
    protected $fillable = [
        'status', 'message', 'time_run', 'contact_id','sms_broadcast_id','cost','our_cost','credits','user_id','country_dial_code_id','send_to'
    ];

    /**
     * The "booting" method of the model.
     *
     * @return void
     */
    protected static function boot()
    {
        parent::boot();

        static::addGlobalScope(new LimitScope);
    }
    
    public function broadcastBatch()
    {
    	return $this->belongsTo(SmsBroadcastBatch::class);
    }

    public function getStatusFormatAttribute()
    {
        if ($this->status == 'success') {
            return 'success';
        }

        return 'danger';
    }
    public function contact()
    {
        return $this->belongsTo('App\Contact','contact_id');
    }
    public function contactName()
    { 
        if($this->contact_id)
        {
            return $this->contact->name;
        }
    }
    public function rate()
    {
        return $this->hasOne('App\Models\Rate');
    }
    
}
