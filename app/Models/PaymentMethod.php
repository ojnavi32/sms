<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
// use Cviebrock\EloquentSluggable\SluggableInterface;
// use Cviebrock\EloquentSluggable\SluggableTrait;

class PaymentMethod extends Model 
{
    // use SluggableTrait;

    protected $table = 'payment_methods';
    
    protected $fillable = ['name', 'slug', 'active'];
    
    // protected $sluggable = ['build_from' => 'name'];

}
