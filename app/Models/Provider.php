<?php 

namespace App\Models;
use App\Models\Currency;

use Illuminate\Database\Eloquent\Model;

class Provider extends Model {

  protected $table = 'providers';

	protected $fillable = ['name', 'slug', 'active', 'currency'];
	
	public function providersRates()
  {
      return $this->hasMany(ProviderRate::class);
  }

  public function providerCurrency()
  {
    return $this->hasOne(Currency::class, 'currency', 'currency');
  }

  public function scopeGetProviderByName($query, $providerName)
  {
    return $query->where('name', $providerName);
  }
  
  public static function getproviderOfCountry($countryCode)
  {
    $rate = Rate::where('country_dial_code_id',$countryCode)->first();
    $rateProviderPriority = $rate->highestPriorityProvider();

      if($rateProviderPriority)
        $provider = $rateProviderPriority->provider;

      else
        $provider = Provider::where('name','Infobip')->first();

    return $provider;
  }

}
