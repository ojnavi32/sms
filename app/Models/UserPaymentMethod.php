<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserPaymentMethod extends Model
{
    protected $table = 'users_payments_methods';
    
    protected $fillable = [
    		'user_id', 
    		'type', 
    		'ending', 
    		'expiration_date', 
    		'status',
    		'auto_recharge',
    ];
}
