<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class SmsBroadcastMessageTransaction extends Model
{
	use SoftDeletes;
	
    protected $table = 'sms_broadcasts_messages_transactions';
    
    protected $fillable = [
        'message_id', 'provider_id', 'rate_id', 'transcation_id', 'status', 'response',
    ];
    
    public function user()
    {
    	return $this->belongsTo(User::class);
    }
}
