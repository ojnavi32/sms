<?php 

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Carbon\Carbon;

class UserPendingCash extends Model {

	use SoftDeletes;

	protected $table = 'users_pending_cash';

	protected $fillable = ['user_id', 'amount'];
}
