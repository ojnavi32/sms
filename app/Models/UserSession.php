<?php 

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Scopes\LimitScope;

class UserSession extends Model {

	use SoftDeletes;

	protected $table = 'users_sessions';

	protected $fillable = ['user_id', 'browser', 'location', 'recent_activity'];
	
	/**
     * The "booting" method of the model.
     *
     * @return void
     */
    protected static function boot()
    {
        parent::boot();

        static::addGlobalScope(new LimitScope);
    }
    
	public function user()
    {
        return $this->belongsTo(User::class);
    }
}
