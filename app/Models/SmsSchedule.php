<?php 

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class SmsSchedule extends Model {

	use SoftDeletes;

	protected $table = 'sms_schedules';

	protected $fillable = ['sms_broadcast_id', 'date', 'time', 'interval'];

	public function smsBroadcast()
    {
        return $this->belongsTo(SmsBroadcast::class);
    }

}
