<?php 

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class AreaCode extends Model {

	use SoftDeletes;

	protected $table = 'area_codes';

	protected $fillable = ['did_group_id', 'country_code', 'state_id', 'city_name', 'area_code'];
	
	public function scopeDidGroupIds($query, $didGroupIds = array())
	{
		if ( ! empty($didGroupIds)) {
			return $query->whereIn('did_group_id', $didGroupIds);
		}
	}
	
	public function scopeStateId($query, $stateId = null)
	{
		if ($stateId) {
			return $query->where('state_id', $stateId);
		}
	}
	
	public function scopeCityNamePattern($query, $cityNamePattern = null)
	{
		if ($cityNamePattern) {
			return $query->where('city_name', 'LIKE', $cityNamePattern);
		}
	}
	
}
