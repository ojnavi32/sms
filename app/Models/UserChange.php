<?php 

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class UserChange extends Model {

    use SoftDeletes;

    protected $table = 'users_changes';

    protected $fillable = ['user_id', 'name', 'username', 'email'];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    //protected $dates = ['deletion_date'];

    public function user()
    {
        return $this->BelongsTo('App\Models\User');
    }
}
