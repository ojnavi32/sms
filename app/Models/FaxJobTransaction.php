<?php 

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class FaxJobTransaction extends Model
{

    use SoftDeletes;

    protected $table = 'fax_job_transactions';

    protected $fillable = ['fax_job_id', 'comm_provider_id', 'transaction_id', 'response'];


    public function fax_job()
    {
        return $this->BelongsTo('App\Models\FaxJob');
    }


    public function comm_provider()
    {
        return $this->BelongsTo('App\Models\CommProvider');
    }


}