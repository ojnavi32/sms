<?php 

namespace App\Models;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;

use Illuminate\Notifications\Notifiable;
use Mail;
use App\Mail\ForgetPasswordEmail;
use Braintree_Customer;
use Braintree_Exception_NotFound;

//use Laravel\Cashier\Billable;

// use Illuminate\Foundation\Auth\Access\Authorizable;
// use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;

class User extends Model implements AuthenticatableContract, CanResetPasswordContract
{

    use Authenticatable,
        CanResetPassword,
        SoftDeletes,
        Notifiable;
        //Billable;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'users';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['role_id', 'parent_user_id', 'notify_email', 'send', 'receive', 'username', 'facebook_id',
                            'google_id', 'twitter_id', 'linkedin_id', 'phone_number', 'name', 'email', 'email_domain', 'password', 'api_key', 'api_key_send',
                            'api_enabled', 'provision_numbers', 'number_cost', 'fax_receive_callback_url', 'email_fax_enabled', 'uuid', 'pending_cash', 'cash_balance_sub', 'monthly_cash', 'developer',
                            'disable_email', 'cash_balance', 'currency', 'verified', 'trusted', 'verification_code',
                            'api_callback_url', 'resolution', 'number_retries', 'default_provider', 'tsi_number', 'low_balance_amount', 
                            'api_email_notifications', 'sandbox_api_callback_url', 'sandbox_cash_balance',
                            'access', 'team_emails', 'login_first_time', 'agile_id', 'agile_billing', 'agile_tags', 
                            'ip_address', 'created_app', 'country', 'timezone', 'last_login_time', 'last_ip_address'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = ['password', 'remember_token'];
    public $include_delete_relatives = ['billing_detail', 'fax_jobs'];
    
    public function taxPercentage() {
        return session('vat');
    }

    public function billing_detail()
    {
        return $this->hasOne('App\Models\BillingDetail');
    }
    
    public function faxUri()
    {
        return $this->hasOne('App\Models\FaxUri');
    }

    public function payments()
    {
        return $this->hasMany('App\Models\Payment');
    }

    public function braintree_customer()
    {
        return $this->hasOne('App\Models\BraintreeCustomer');
    }

    public function inbox()
    {
        return $this->hasMany('App\Models\UserInbox');
    }

    public function payment_history()
    {
        return $this->hasMany('App\Models\PaymentHistory');
    }

    public function documents()
    {
        return $this->hasMany('App\Models\Document');
    }

    public function sandboxDocuments()
    {
        return $this->hasMany('App\Models\Sandbox\Document');
    }

    public function sandboxFaxJobs()
    {
        return $this->hasMany('App\Models\Sandbox\FaxJob');
    }

    public function myDocuments()
    {
        return $this->hasMany('App\Models\Document')->orderBy('id', 'DESC');
    }

    public function teamDocuments()
    {
        return $this->hasMany('App\Models\Document', 'user_id', 'parent_user_id')->orderBy('id', 'DESC');
    }

    public function changes()
    {
        return $this->hasMany('App\Models\UserChange');
    }

    public function numbers()
    {
        return $this->hasMany('App\Models\UserDids')->where('status', '!=', 6);
    }
    
    public function allNumbers()
    {
        return $this->hasMany('App\Models\UserDids');
    }

    public function fax_jobs()
    {
        return $this->hasMany('App\Models\FaxJob');
    }

    public function fax_broadcasts()
    {
        return $this->hasMany('App\Models\FaxBroadcast');
    }

    public function userCashbalance()
    {
        return $this->hasMany('App\Models\UserCashBalance');
    }

    public function has_pending_fax_jobs()
    {
        return $this->fax_jobs()->whereStatus('pending')->count();
    }

    public function scopeOfRole($query, $role)
    {
        try {
            $id = Role::whereSlug($role)->first()->id;
        } catch(\ErrorException $e) {
            $id = NULL;
        }
        return $query->whereRoleId($id);
    }

    public function scopeValidVat($query)
    {
        return $query->whereHas('billing_detail', function ($q) {
            $q->where('vat_number_validated', true);
        });
    }

    public function user_rate_adjustments()
    {
        return $this->hasMany('App\Models\UserRateAdjustment')->where('billing_type','=', $this->billing_type);
    }

    public function user_api_rate_adjustments()
    {
        return $this->hasMany('App\Models\UserApiRateAdjustment');
    }

    public function failed_topup_logs()
    {
        return $this->hasMany('App\Models\FailedTopupLog');
    }

    public function user_dids()
    {
        return $this->hasMany('App\Models\UserDids');
    }

    // public function getCashBalanceAttribute($value)
    // {
    //     // We get our parent cash balance
    //     if ($this->parent_user_id != NULL and $this->parent_user_id != 0) {
    //         return static::find($this->parent_user_id)->cash_balance;
    //     }

    //     return $value;
    // }

    public function getTeamCashBalanceAttribute()
    {
        // We get our parent cash balance
        if ($this->parent_user_id != NULL and $this->parent_user_id != 0) {
            return static::find($this->parent_user_id)->cash_balance;
        }

        return $this->cash_balance;
    }

    public function getTeamUserIdAttribute()
    {
        // We get our parent cash balance
        if ($this->parent_user_id != NULL and $this->parent_user_id != 0) {
            return static::find($this->parent_user_id)->id;
        }

        return $this->id;
    }

    public function getTeamMembersAttribute()
    {
        if (auth()->user()->parent_user_id != NULL) {
            $parentUserId = auth()->user()->parent_user_id;
        } else {
            $parentUserId = auth()->user()->id;
        }

        $ids = static::where('parent_user_id', $parentUserId)->pluck('id');
        $ids = collect($ids);
        $ids = $ids->merge([$parentUserId]);
        return $ids;
    }

    public function getTeamMembersReceiveAttribute()
    {
        if (auth()->user()->parent_user_id != NULL) {
            $parentUserId = auth()->user()->parent_user_id;
        } else {
            $parentUserId = auth()->user()->id;
        }

        $ids = static::where('parent_user_id', $parentUserId)->where('receive', 1)->pluck('id');
        $ids = collect($ids);
        $ids = $ids->merge([$parentUserId]);
        return $ids;
    }

    public function getIsBannedAttribute()
    {
        $banned = EmailBanlist::whereRaw(':email LIKE `match_string`', ['email' => $this->email]);
        return ($banned->count() ? TRUE : FALSE);
    }

    public function getBrainTreeCustomerIdAttribute()
    {
        $id = @auth()->user()->braintree_customer->customer_id;

        $customerId = NULL;

        try {
            if ($id) {
                $customerId = Braintree_Customer::find($id);
                $customerId = $id;
            }
        } catch(Braintree_Exception_NotFound $e) {
            $customerId = NULL;
        }

        return $customerId;
    }

    public function getEmailNameAttribute()
    {
        if ($this->name == '') {
            return $this->email;
        }
        
        return $this->name;
    }
    
    public function getBannedTextAttribute()
    {
        if ($this->verified == 2) {
            return 'Yes (' . @config('fax.ban_reasons')[$this->ban_reason] . ')';
        }
        if ($this->verified == 3) {
            return 'Yes (Deactivated)';
        }
        
        
        return 'No';
    }

    public function isExists($email)
    {
        $exists = static::where('email', $email)->first();
        return ($exists ? TRUE : FALSE);
    }

    public function isChild($email)
    {
        $child = static::where('email', $email)
            ->where('parent_user_id', auth()->user()->id)
            ->first();
        return ($child ? TRUE : FALSE);
    }

    public function isNotify($email)
    {
        $child = static::where('email', $email)
            ->where('parent_user_id', auth()->user()->id)
            ->where('notify_email', 1)
            ->first();
        return ($child ? 'checked' : '');
    }

    public function isSend($email)
    {
        $child = static::where('email', $email)
            ->where('parent_user_id', auth()->user()->id)
            ->where('send', 1)
            ->first();
        return ($child ? 'checked' : '');
    }

    public function isReceive($email)
    {
        $child = static::where('email', $email)
            ->where('parent_user_id', auth()->user()->id)
            ->where('receive', 1)
            ->first();
        return ($child ? 'checked' : '');
    }
    public function broadcasts()
    {
        return $this->hasMany('App\Models\SmsBroadcast','user_id');
    }

    /**
     * Send the password reset notification.
     *
     * @param  string  $token
     * @return void
     */
    public function sendPasswordResetNotification($token)
    {
        Mail::queue(new ForgetPasswordEmail($this, $token));
    }

    // public function getIdAttribute($value)
    // {
    //     // We are going to use the user_id of the parent
    //     // if the current user is a child
    //     // something like auth()->user()->id
    //     if ($this->parent_user_id != NULL and $this->parent_user_id != 0) {
    //         return $this->parent_user_id;
    //     }
    //     return $value;
    // }

}
