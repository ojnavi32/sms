<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Scopes\LimitScope;
use App\Lists;

class SmsBroadcast extends Model
{
	use SoftDeletes;
	
    protected $table = 'sms_broadcasts';
    
    protected $fillable = [
        'user_id', 'lists_id', 'status', 'message', 'template_id', 'total_cost','sched_date','sched_time','time_zone','scheduled_server_time','send_later','manual_contacts','cost'
    ];
    
    /**
     * The "booting" method of the model.
     *
     * @return void
     */
    protected static function boot()
    {
        parent::boot();

        static::addGlobalScope(new LimitScope);
    }

    public function user()
    { 
    	return $this->belongsTo(User::class);
    }
    
    public function list()
    {
    	return $this->belongsTo(Lists::class,'lists_id');
    }

    public function broadcastBatches()
    { 
        return $this->hasMany(SmsBroadcastBatch::class,'sms_broadcast_id');
    }

    public function getStatusFormatAttribute()
    {
        if ($this->status == 'success') {
            return 'success';
        }

        if ($this->status == 'failed') {
            return 'danger';
        }

        return 'warning';
    }
    public function messages()
    {
        return $this->hasMany(SmsBroadcastMessage::class, 'sms_broadcast_id');
    }
    public function deliveredMessages()
    { 
       return $this->messages()->where('delivered',1);
        
    }
    public function failedMessages()
    { 
       return $this->messages()->where('failed',1);
    }

    public function listColumns()
    {
        return $this->list()->select('id','name');
    }
    public function broadcastLists()
    { 
       if($this->list) {
          return $this->listColumns;
    }
    }

    public static function calculatePrice($broadcast){
       
        $realCost = 0;
        foreach($broadcast->broadcastBatches as $batch)
        {
            $messages = $batch->smsMessages;
            $realCost += $messages->sum('our_cost');

        }
        return $realCost;
    }
    public static function calculateTotalPrice($broadcast){
       
        $realCost = 0;
        foreach($broadcast->broadcastBatches as $batch)
        {
            $messages = $batch->smsMessages;
            $realCost += $messages->sum('cost');

        }
        return $realCost;
    }
    public static function calculateCredits($broadcast){
       
        $realCredit = 0;
        foreach($broadcast->broadcastBatches as $batch)
        {
            $messages = $batch->smsMessages;
            $realCredit += $messages->sum('credits');

        }
        return $realCredit;
    }

}
