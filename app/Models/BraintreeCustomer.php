<?php 

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class BraintreeCustomer extends Model {

	use SoftDeletes;

	protected $table = 'braintree_customers';

	protected $fillable = ['user_id', 'customer_id'];

	public function user()
    {
        return $this->BelongsTo('App\Models\User');
    }

}
