<?php 

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TemplateField extends Model {

	use SoftDeletes;

	protected $table = 'templates_fields';

	protected $fillable = ['template_id', 'fields'];

	public function template()
    {
        return $this->belongsTo(Template::class);
    }

}
