<?php 

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

use App\Models\Subscription;

class Subscription extends Model {

    use SoftDeletes;

    protected $table = 'users_subscriptions';

    protected $fillable = ['user_id', 'braintree_subscription_id', 'expiration_date', 'status', 'did'];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */

    public function user()
    {
        return $this->BelongsTo('App\Models\User');
    }

    public static function createSubscription($number, $status = 'Active')
    {
        Subscription::create([
            'user_id' => $number->user_id,
            'braintree_subscription_id' => $number->subscription_id,
            'expiration_date' => $number->expiration_date,
            'status' => $status,
            'did' => $number->did,
        ]);
    }
}
