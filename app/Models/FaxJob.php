<?php 

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Services\SMS\Calculators\RateCalculator;
use Carbon\Carbon;

class FaxJob extends Model
{

    use SoftDeletes;

    protected $table = 'fax_jobs';

    protected $fillable = ['user_id', 'from', 'fax_number', 'delete_after_success', 'job_api', 'api_callback_url', 'server_fax_job_id', 'platform', 'from_email', 'post_delivered', 
        'document_id', 'user_ipv4', 'channel_event', 'rate_id', 'fax_broadcast_id', 'status', 'message', 'resolution',
        'cost', 'used_sub_balance', 'used_cash_balance_id', 'borrow_cash', 'impersonated'
    ];

    public function user()
    {
        return $this->BelongsTo('App\Models\User');
    }


    public function document()
    {
        return $this->belongsTo('App\Models\Document')->withTrashed();
    }


    public function rate()
    {
        return $this->belongsTo('App\Models\Rate');
    }


    public function provider_priorities()
    {
        return $this->rate->provider_priorities();
    }


    public function fax_job_transactions()
    {
        return $this->hasMany('App\Models\FaxJobTransaction');
    }

    public function faxJobTransactionsWithTrash()
    {
        return $this->hasMany('App\Models\FaxJobTransaction')->withTrashed();
    }


    public function getUserIpv4IpAttribute()
    {
        return long2ip($this->attributes['user_ipv4']);
    }

    public function getCreatedAtFormatAttribute()
    {
        if (auth()->user()->timezone) {
            return $this->created_at->timezone(auth()->user()->timezone);
        }
        
        return $this->created_at;
    }


    public function fax_broadcast()
    {
        return $this->belongsTo('App\Models\FaxBroadcast');
    }

    public function scopeTeam($query)
    {
        return $query->whereIn('user_id', auth()->user()->team_members);
    }

    public function scopeSearch($query, $request)
    {
        if ($request->fax_number) {
            $query->where('fax_number', $request->fax_number);
        }

        $now = Carbon::now();

        $startDate = $request->input('start_date', '2016-02-13');
        $endDate = $request->input('end_date', $now->format('Y-m-d'));

        $query->whereBetween('created_at', [$startDate . ' 00:00:00', $endDate . ' 23:59:59']);

        if ($request->selected) {
            $statuses = explode(',', $request->selected);
            $query->whereIn('status', $statuses);
        }
        
        return $query;
    }
    
    public function scopeByPlatform($query, $platform = null)
    {
        return $query->where('platform', $platform);
    }

    public function saveCost($rateCalculator)
    {
        $this->rate_id = $rateCalculator->rate->id;
        $this->cost = $totalCost;
        $this->save();
    }

    public function getCost($faxNumber, $document)
    {
        $rateCalculator = new RateCalculator;
        return $rateCalculator->getCost($faxNumber, $document);
    }

    public function getRetries()
    {
        $defaultProvider = env('API_DEFAULT_PROVIDER', 13);

        if (env('API_RETRIES', false)) {
            // Changes start
            // Get the number of retries set by the API user
            $numberRetries = $this->user->number_retries;

            // 9 for GLOBAL, 12 for EU
            $defaultProvider = $this->user->default_provider;

            // Get the providers
            $providers = [$defaultProvider];
            $providerPriorities = $this->provider_priorities();
            // Limit the number of records based on the number of retries set by users
            foreach( $providerPriorities->limit($numberRetries)->get() as $provider) {
                // Dont include $defaultProvider
                if ($provider->id != $defaultProvider) {
                    $providers[] = $provider->id;
                }
            }

            $newArray = [];

            while(count($newArray) <= $numberRetries) {
                foreach($providers as $key => $value) {
                    $newArray[] = $value;
                }
            }

            $providers = array_slice($newArray, 0, $numberRetries);

            $finalProviders = [];
            foreach($providers as $provider) {
                // Set the provider as use when its $defaultProvider
                if ($provider == $defaultProvider) {
                    $finalProviders[$defaultProvider] = 1;
                } else {
                    $finalProviders[$provider] = 0;
                }
            }

            $this->api_providers_used = json_encode($finalProviders);
            $this->retries += 1;
            $this->save();
        }

        return $defaultProvider;
    }


}