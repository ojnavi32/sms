<?php 

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Payment extends Model {

	use SoftDeletes;

	protected $table = 'payments';


	protected $fillable = ['user_id', 'payment_method_id', 'amount', 'reference_number', 'is_paid', 'vat','details'];


	public function user()
    {
        return $this->BelongsTo('App\Models\User');
    }

    public function paymentMethod()
    {
        return $this->BelongsTo('App\Models\PaymentMethod');
    }

}
