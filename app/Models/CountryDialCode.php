<?php 

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CountryDialCode extends Model {

    protected $table = 'country_dial_codes';

	protected $fillable = ['country', 'a2_code', 'a3_code', 'dial_code', 'exchange_rate_id'];

    protected $sluggable = ['build_from' => 'country'];

	public function rates()
    {
        return $this->hasMany(Rate::class);
    }

	public function exchange_rate()
    {
        return $this->belongsTo(ExchangeRate::class);
    }

    public function getCountryCodeAttribute()
    {
        return $this->attributes['country'] . ' (' . $this->attributes['a3_code'] . ') +'  . $this->attributes['dial_code'];
    }


}
