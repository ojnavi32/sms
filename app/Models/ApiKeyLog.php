<?php 

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ApiKeyLog extends Model 
{
	/**
     * The connection name for the model.
     *
     * @var string
     */
    //protected $connection = 'mysql';

	use SoftDeletes;

	protected $table = 'api_keys_logs';

	protected $fillable = ['user_id', 'api_key', 'description', 'method', 'status', 'status_code', 'access_key', 'url', 'ip_address', 'payload'];

	public function user()
    {
        return $this->BelongsTo('App\Models\User');
    }
}