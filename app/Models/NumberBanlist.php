<?php 

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class NumberBanlist extends Model
{

    use SoftDeletes;

    protected $table = 'banlist_number';

    protected $fillable = ['match_string'];

    public function scopeIsBanned($query, $fax_number)
    {
        return $query->whereRaw(':number LIKE `match_string`', ['number' => $fax_number]);
    }

    public static function ban($faxNumber, $faxJob)
    {
    	$ban = static::whereRaw(':number LIKE `match_string`', ['number' => $faxNumber])->first();

    	if ( ! $ban) {
    		return false;
    	}

    	$message = trans('messages.errors.cant_fax_destination');
        $faxJob->status = 'failed';
        $faxJob->message = $message;
        $faxJob->save();

        $data['status'] = 'failed';
        $data['msg'] = $message;

        return $data;

    }

}
