<?php 

namespace App\Models;

use DB;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Currency extends Model {

	use SoftDeletes;

    protected $table = 'currency';

	protected $fillable = ['currency', 'title', 'iso_code', 'decimal', 'symbol', 'value'];


	public static function convertCurrencyToEuro($price,$provider)
    {
       $pricInEuro = $price/$provider->providerCurrency->value;
    
       return $pricInEuro;
    }
    
}
