<?php 

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class PostbackTransaction extends Model {

	use SoftDeletes;

	protected $table = 'postbacks_transactions';

	protected $fillable = ['postback_id', 'status_code'];

	public function postback()
    {
        return $this->belongsTo('App\Models\Postback');
    }
    
    
}
