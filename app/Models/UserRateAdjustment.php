<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserRateAdjustment extends Model {

    protected $table = 'users_rates_adjustments';
    protected $fillable = ['user_id', 'rate_id', 'base_rate', 'multiplier', 'network', 'billing_type'];

    public function user() {
        return $this->hasOne('App\Models\User');
    }

    public function rate() {
        return $this->belongsTo('App\Models\Rate');
    }

    public static function getCarrierRate($countryCode, $networkName, $userId) {
        $rate = Rate::where('country_dial_code_id', $countryCode)->first();
        $userRate = UserRateAdjustment::where('network', 'LIKE', '%' . $networkName . '%')
                        ->where('user_id', $userId)
                        ->where('rate_id', $rate->id)
                        ->where('billing_type', 'PER_CARRIER')->max('base_rate');
        return $userRate;
    }

    public static function getCountryRate($countryCode, $userId) {
        $rate = Rate::where('country_dial_code_id', $countryCode)->first();
        $userRate = UserRateAdjustment::where('user_id', $userId)
                        ->where('rate_id', $rate->id)
                        ->where('billing_type', 'PER_COUNTRY')->max('base_rate');
        return $userRate;
    }

}
