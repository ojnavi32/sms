<?php 

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserApiRateAdjustment extends Model {

    protected $table = 'user_api_rate_adjustments';

    protected $fillable = ['user_id', 'rate_id', 'base_rate', 'multiplier'];

    public function user()
    {
        return $this->hasOne('App\Models\User');
    }

    public function rate()
    {
        return $this->belongsTo('App\Models\Rate');
    }
}
