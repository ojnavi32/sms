<?php 

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ShortLink extends Model {

	protected $table = 'shortlinks';

	protected $fillable = ['user_id', 'shortlink', 'url', 'cicks'];

	public function generateLink($url)
	{
		// Check if the link already exists in database
		$link = $this->where('url', $url)->first();
		if ( ! $link ) {
			$shortlink = str_random(5);
			$this->create(['shortlink' => $shortlink, 'url' => $url]);
			return route('short_link', $shortlink);
		}

		return route('short_link', $link->shortlink);
	}
}
