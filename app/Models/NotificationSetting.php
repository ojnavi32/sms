<?php 

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class NotificationSetting extends Model {

	use SoftDeletes;

	protected $table = 'notifications_settings';

	protected $fillable = ['user_id', 'key', 'val','email_Marketing_consent_timestamp', 'email_Marketing_consent_ip', 'email_Marketing_consent_unsubscribe_timestamp', 'email_Marketing_consent_unsubscribe_ip'];

	public function user()
    {
        return $this->belongsTo(User::class);
    }

}
