<?php 

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class NonUser extends Model
{
    use SoftDeletes;

    protected $table = 'non_users';

    protected $fillable = ['email', 'country'];

}