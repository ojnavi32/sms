<?php 

namespace App\Services;

use Braintree_Configuration;

class SmsBraintree
{
    public $environment;
    public $merchant_id;
    public $merchant_id_usd;
    public $public_key;
    public $private_key;

    public function __construct()
    {
        $this->environment = config('payment_gateway.braintree.environment');
        $this->merchant_id = config('payment_gateway.braintree.merchant_id');
        $this->public_key = config('payment_gateway.braintree.public_key');
        $this->private_key = config('payment_gateway.braintree.private_key');
    }

    public function init()
    {
        Braintree_Configuration::environment($this->environment);
        Braintree_Configuration::merchantId($this->merchant_id);
        Braintree_Configuration::publicKey($this->public_key);
        Braintree_Configuration::privateKey($this->private_key);

        return $this;
    }
}