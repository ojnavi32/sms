<?php namespace App\Services;

# Enter your domain name , agile email and agile api key
define('AGILE_DOMAIN', config('agile.agile_domain'));
define('AGILE_USER_EMAIL', config('agile.agile_user_email'));
define('AGILE_REST_API_KEY', config('agile.agile_rest_api_key'));

class AgileCrm
{
    public $statusCode;

    public function __construct()
    {
        
    }

    public function getContact($id, $email = '')
    {
        // by id
        $this->curl_wrap('contacts/' . $id, null, 'GET', 'application/json');

        // by email
        $this->curl_wrap('contacts/search/email/' . $email, null, 'GET', 'application/json');
    }

    public function createContact($user, $tag = null)
    {
        if( ! env('ENABLE_AGILE', false) ) {
            return false;
        }
        
        if ($user->agile_id == NULL) {
            
            $firstName = $user->name;
            $lastName = '';
            $name = explode(' ', $user->name);
            if (trim(@$name[0] . @$name[1]) != '') {
                $firstName = @$name[0];
                $lastName = @$name[1];
            }

            $contactJson = [
              'lead_score' => '0',
              'star_value' => '0',
              'properties' => [
                [
                  'name'=> 'first_name',
                  'value'=> $firstName,
                  'type'=> 'SYSTEM'
                ],
                [
                  'name' => 'last_name',
                  'value'=> $lastName,
                  'type' => 'SYSTEM'
                ],
                [
                  'name' => 'email',
                  'value' => $user->email,
                  'type'=> 'SYSTEM'
                ],
              ]
            ];

            if ($tag != null) {
                $contactJson['tags'] = [$tag];
            }
            $contactJson = json_encode($contactJson);
            $result = $this->curl_wrap('contacts', $contactJson, 'POST', 'application/json');
            if ($this->statusCode == 200) {
                $agile = json_decode($result, false, 512);
                $user->login_first_time = 1;
                $user->agile_id = $agile->id;
                $user->save();
                return json_decode($result, false, 512);
            }
        }
    }

    public function update($id, $data)
    {
        if( ! env('ENABLE_AGILE', false) ) {
            return false;
        }

        $user = auth()->user();
        // If theres no agile then theres no need to update
        if ($id == null) {
            return true;
        }

        // Check if
        if( $user->agile_billing == 1) {
            return true;
        }
        $user->agile_billing = 1;

        $fields = [
            'paymentMethod' => 'Payment Method',
            'firstName' => 'First Name',
            'lastName' => 'Last Name',
            'streetAddress' => 'Street Address',
            'postalCode' => 'Postal Code',
            'country' => 'Country',
        ];

        $contactJson['id'] = $id; // It is mandatory field. Id of contact

        foreach ($fields as $key => $label) {
            $contactJson['properties'][] = [
                    'name' => $label, 
                    'value' => $data[$key], 
                    'type' => 'CUSTOM'
            ];
        }

        $contactJson = json_encode($contactJson);
        $this->curl_wrap('contacts/edit-properties', $contactJson, 'PUT', 'application/json');

        $user->save();
    }

    public function createTag($user, $tags = array())
    {
        if( ! env('ENABLE_AGILE', false) ) {
            return false;
        }

        if ($user->agile_id == NULL) {
            return false;
        }

        // We need to check if the tags exists in our own records
        $ownTags = json_decode($user->agile_tags);

        if (is_array($ownTags)) {
            if (in_array($tags['0'], $ownTags)) {
                return false;
            }
        }

        $contactJson['tags'] = $tags;
        $contactJson['id'] = $user->agile_id;

        $contactJson = json_encode($contactJson);

        $result = $this->curl_wrap('contacts/edit/tags', $contactJson, 'PUT', 'application/json');

        if ($this->statusCode == 200) {
            array_push($ownTags, $tags[0]);
            $user->agile_tags = json_encode($ownTags);
            $user->save();
            return true;
        }
    }

    public function curl_wrap($entity, $data, $method, $content_type) {
        if ($content_type == NULL) {
            $content_type = 'application/json';
        }
        
        $agile_url = "https://" . AGILE_DOMAIN . ".agilecrm.com/dev/api/" . $entity;

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($ch, CURLOPT_MAXREDIRS, 10);
        curl_setopt($ch, CURLOPT_UNRESTRICTED_AUTH, true);
        switch ($method) {
            case "POST":
                $url = $agile_url;
                curl_setopt($ch, CURLOPT_URL, $url);
                curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
                curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
                break;
            case "GET":
                $url = $agile_url;
                curl_setopt($ch, CURLOPT_URL, $url);
                curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
                break;
            case "PUT":
                $url = $agile_url;
                curl_setopt($ch, CURLOPT_URL, $url);
                curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PUT");
                curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
                break;
            case "DELETE":
                $url = $agile_url;
                curl_setopt($ch, CURLOPT_URL, $url);
                curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "DELETE");
                break;
            default:
                break;
        }
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            "Content-type : $content_type;", 'Accept : application/json'
        ));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_USERPWD, AGILE_USER_EMAIL . ':' . AGILE_REST_API_KEY);
        curl_setopt($ch, CURLOPT_TIMEOUT, 120);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        $output = curl_exec($ch);
        
        if ( ! curl_errno($ch) ) {
            $this->statusCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        }

        curl_close($ch);
        return $output;
    }
}
