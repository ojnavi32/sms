<?php

namespace App\Services;

use Maatwebsite\Excel\Files\ExcelFile;

class ContactImport extends ExcelFile
{
    public function __construct()
    {
    	
    }
    
    public function getFile()
    {
        return storage_path('exports') . '/file.csv';
    }

    public function getFilters()
    {
        return [
            'chunk'
        ];
    }
    
    public function gmail($code)
    {
    	// get google service
        $googleService = \OAuth::consumer('Google');
        
    	if ( ! is_null($code)) {
            // This was a callback request from google, get the token
            $token = $googleService->requestAccessToken($code);

            // Send a request with it
            $result = json_decode($googleService->request('https://www.google.com/m8/feeds/contacts/default/full?alt=json&max-results=500'), true);
            //dd($result);

            // Going through the array to clear it and create a new clean array with only the email addresses
            $contacts = []; // initialize the new array
            $i = 0;
            foreach ($result['feed']['entry'] as $contact) {
                // var_dump($contact) . '<br>';
                if (isset($contact['gd$email'])) { // Sometimes, a contact doesn't have email address
                    //$emails[] = $contact['gd$email'][0]['address'];
                    //echo $contact['gd$email'][0]['address'] . '<br>';
                    $contacts[$i]['email'] = $contact['gd$email'][0]['address'];
                }
                
                if (isset($contact['title'])) { // Sometimes, a contact doesn't have email address
                    //$emails[] = $contact['gd$email'][0]['address'];
                    //echo $contact['title']['$t'] . '<br>';
                    $contacts[$i]['name'] = $contact['title']['$t'];
                    //echo $contact['gd$phoneNumber'][0]['$t'] . '<br>';
                }
                
                if (isset($contact['gd$phoneNumber'])) { // Sometimes, a contact doesn't have email address
                    //$emails[] = $contact['gd$email'][0]['address'];
                    //echo $contact['gd$phoneNumber'][0]['uri'] . '<br>';
                    $contacts[$i]['phone'] = $contact['gd$phoneNumber'][0]['uri'];
                    //echo $contact['gd$phoneNumber'][0]['$t'] . '<br>';
                }
                // echo $contact['gd$phoneNumber'][0]['$t'] . '<br>';
                // echo $contact['gd$phoneNumber'][0]['uri'] . '<br>';
                
                $i ++;
            }
            
            // Filter all contacts with numbers
            return $collection = collect($contacts);
            
            $contacts = $collection->filter(function ($value, $key) {
			    return $value->phone != '';
			});

			return $contacts->all();

        }
    }
}
