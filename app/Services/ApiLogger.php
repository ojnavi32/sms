<?php 

namespace App\Services;

use GuzzleHttp\Client;
use Exception;

class ApiLogger
{

    public function __construct()
    {
        
    }

    public function apiCallBack($faxTransaction, $faxJob = null)
    {
        // If faxJob is null we use the faxtransaction or else we use $faxJob param
        if ($faxJob == null) {
            $faxJob = $faxTransaction->fax_job;
        }
        
        // If the fax job is from API call
        if ($faxJob->job_api == 1) {
            //$document = $faxJob->document;
            $user = $faxJob->document->user;
            // Lets post to the URL provided by API user
            if ($user->api_callback_url != '') {
                $client = new Client();

                $message = $faxJob->message;
                $msgCode = 'ok';

                if ($faxJob->status == 'failed') {
                    // Messages
                    $messages = [
                        'unknown' => "Fax Failed - Unknown Error ( We don’t know exactly what went wrong - contact us for more info )",
                        'no_answer' => 'Fax Failed - No Answer from Fax machine',
                        'busy' => 'Fax Failed - Line Busy ( The fax machine could be busy )',
                        'file_error' => 'Fax Failed - File conversion failed. Re-upload and Retry',
                        'hangup' => 'Fax Failed - Early Hangup',
                    ];

                    $message = $messages['unknown'];
                    $msgCode = 'unknown';

                    $words = [
                        'no answer' => 'no_answer',
                        'busy' => 'busy',
                        'Can not open' => 'file_error',
                        'prematurely' => 'hangup',
                        'hang-up' => 'hangup',
                    ];

                    foreach ($words as $word => $key) {
                        // A match was found
                        if (preg_match("/\b" . $word . "\b/i", $faxJob->message)) {
                            $message = $messages[$key];
                            $msgCode = $key;
                            break;
                        }
                    }
                }
                
                $statusCode = 200;
                
                $status = $faxJob->status;
                
                if ($msgCode == 'ok') {
                    $status = 'success';
                } else {
                    $status = 'failed';
                }
                
                $formParams = [
                        'form_params' => [
                            'fax_job_id' => $faxJob->id,
                            'status' => $status,
                            'msg_code' => $msgCode,
                            'message' => $message,
                        ]
                ];
                
                try {
                    
                    $apiCallBackUrl = $user->api_callback_url;
                    
                    if ($faxJob->api_callback_url) {
                        $apiCallBackUrl = $faxJob->api_callback_url;
                    }
                    
                    $response = $client->post($apiCallBackUrl, $formParams);
                    
                } catch(Exception $ex) {
                    echo $ex->getMessage();
                    //echo $ex->getStatus();
                    $statusCode = 500;
                }
                
                $data = [
                    'user_id' => $user->id,
                    'fax_job_id' => $faxJob->id,
                    'url' => $user->api_callback_url,
                    'status' => $faxJob->status,
                    'msg_code' => $msgCode,
                    'message' => $message,
                ];
                
                // Log the POST request and continue sending until we get 200
                api_logger($data, $statusCode);
            }
        }
    }
    
}
