<?php

namespace App\Services;

use Laravel\Socialite\Contracts\User as ProviderUser;
use App\Models\SocialAccount;
use App\User;

class SocialAccountService
{
    public function createOrGetUser(ProviderUser $providerUser, $driver = 'facebook')
    {
        // We need email address
        if ($providerUser->getEmail() == null) {
            return false;
        }
        
        $account = SocialAccount::whereService($driver)
            ->whereSocialId($providerUser->getId())
            ->first();

        if ($account) {
            return $account->user;
        } else {

            $account = new SocialAccount([
                'social_id' => $providerUser->getId(),
                'service' => $driver
            ]);

            $user = User::whereEmail($providerUser->getEmail())->first();

            if ( ! $user ) {

                $user = User::create([
                    'email' => $providerUser->getEmail(),
                    'name' => $providerUser->getName(),
                    'password' => bcrypt(str_random() . time()),
                ]);
            }

            $account->user()->associate($user);
            $account->save();

            return $user;

        }

    }
}