<?php 

namespace App\Services;
use App\Contact;
use App\ListContact;
use App\Lists;
use Gravatar;
use App\Services\CountryInfo;
use App\Services\SMS\Calculators\RateCalculator;
use Propaganistas\LaravelPhone\PhoneNumber;

class ContactService {

    protected $client;
    
    public $listIds = null;
    
    public $import = false;

    public function __construct()
    {
        
    }
    
    public function setListsIds($listIds)
    {
        $this->listIds = $listIds;    
    }

    public function create($row, $user, $import = false)
    {
        $contact = Contact::create([
            'user_id' => $user->id,
            'name' => $row->name,
            'surname' => $row->surname,
            'email' => $row->email,
            'phone' => (string) $row->phone,
            'photo' => $row->photo,
            'country_code' => $row->country_code,
            'network_name' => $row->network_name,
        ]);
       
            
        // We should check of country column to identify the country code to use
        $contact->phone = $this->identifyCountry((string)$row->phone, @$row->country);
        $contact->save();
        
        // When creating contact theres an option to add the contact to a list
        // Contacts can be in multiple lists
        $this->associateToList($contact, $user);
        
        if ($import == false) {
            return $contact;
        }
    }
    
    public function associateToList($contact, $user)
    {
        if ($this->listIds) {
            if (is_array($this->listIds)) {
                $listIds = $this->listIds;
            } else {
                $listIds[] = $this->listIds;
            }
            
            foreach ($listIds as $listId) {
                $contact->listContact()->firstOrCreate([
                    'user_id' => $user->id,
                    'list_id' => $listId,
                    'contact_id' => $contact->id,
                    'phone' => $contact->phone,
                ]);

                $list = Lists::findOrFail($listId);
                $list->number_contacts += 1;
                $list->save();
            }
        }
    }
    
    public function identifyCountry($phone, $country = null)
    {
        if ($country) {
            if (substr($phone, 0, 1) == '0') {
                return (string) PhoneNumber::make($phone)->ofCountry($country);  // +3212345678
            }
        }
        
        return (string)$phone;
    }
}