<?php

namespace App\Services\SMS\Calculators;

use App\Services\CountryInfo;
use App\Models\Rate;
use App\Models\ProviderRate;
use App\Models\UserRateAdjustment;
use App\Models\Setting;
use App\Models\Provider;
use App\Models\Currency;
use DB,
    Cache;
use App\Models\CreditPackage;
use Auth;

class RateCalculator {

    public $rate;
    public $rate_per_page;
    public $cost;
    public $credits;
    public $totalCredits = 0;
    private $currency;
    public $provider;
    public $margin;
    public $profit;
    public $costPerCredits;

    public function __construct() {
        $this->margin = Setting::getSetting('MARGIN_RATE');
        $this->profit = 1 + (1 * $this->margin) / 100;
        $this->costPerCredits = CreditPackage::minimumCostPerCredit();
    }

    public function setRate($rate) {
        $this->rate = $rate;
    }

    /* Function calculate rate of each message according to carrier
      @param $countryCode,$networkName,$message
      @return $total
     */

    public function getCarrierSmsRate($countryCode, $networkName, $message, $provider, $userId) {

        $rateInEuro = UserRateAdjustment::getCarrierRate($countryCode, $networkName, $userId);
        if (!$rateInEuro) {
            $carrierRate = ProviderRate::getCarrierRate($countryCode, $networkName, $provider->id);
            $rateInEuro = Currency::convertCurrencyToEuro($carrierRate, $provider);
        }
        $mssagesCount = ceil(strlen($message) / 160);
        return $mssagesCount*$rateInEuro;
    }

    /* Function to get country sms rate in Euro
      @param $countryCode
      @return $total
     */

    public function getCountrySmsRate($countryCode, $userId, $message) {

        $rateInEuro = UserRateAdjustment::getCountryRate($countryCode, $userId);
        if (!$rateInEuro) {
            $rateInEuro = Rate::getCountryRate($countryCode);
        }
        $mssagesCount = ceil(strlen($message) / 160);
        return $mssagesCount*$rateInEuro;
    }

    public function getRateId() {
        $this->rate->id;
    }

    public function getCredit($contact, $user = null) {
        $countryInfo = new CountryInfo($contact);

        if ($countryInfo->hasCountryRates()) {
            // Get rate to use
            $useRate = $countryInfo->rateToUse();

            // Base on rate id we need to get the provider_rates table
            // providers_rates table
            $useRate = $countryInfo->providerRate($user);
            $totalCost = $useRate->credits;

            // Function for Cost:  IF PROVIDER COST * MARGINRATE < $0.01 THEN COST = 1 CREDIT
            // IF PROVIDER COST * MARGINRATE < $0.02 THEN COST = 2 CREDIT and so on.
            // MARGINRATE will be added from manager et.c MARGINRATE = 1.2 
            $credits = 1;
            if (($useRate->base_rate * Setting::getSetting('MARGIN_RATE')) < Setting::getSetting('COST_PER_CREDIT')) {
                $credits = 1;
            }

            return $credits;
        }
    }

    public function getTotalCredits($contacts, $user = null) {
        foreach ($contacts as $contact) {
            $this->totalCredits += $this->getCredit($contact, $user);
        }

        return $this->totalCredits;

        return $countryInfo->countryDialCodeId;
        return $prefix;

        return $countryInfo->dialCodeFromNum();

        // $countryInfo->dialCodeFromNum();
        // $prefix = $countryInfo->prefix;
        // look up rates table
        // then look up providers_rates table
    }

    public function getTotalCosts() {
        return $this->totalCredits * Setting::getSetting('COST_PER_CREDIT');
    }

    public function getTotalCostOfContacts($contacts, $billType, $message, $provider, $userId) {
        switch ($billType) {
            case 'PER_CARRIER':

                return $ourCost = $this->getTotalCarrierCost($contacts, $message, $provider, $userId);

                break;

            case 'PER_COUNTRY';

                return $ourCost = $this->getTotalCountryCost($contacts, $userId, $message);

                break;
        }
    }

    public function getTotalCarrierCost($contacts, $message, $provider, $userId) {
        $mapping = [];
        $cost = 0;

        foreach ($contacts as $contact) {

            if (!isset($mapping[$contact->network_name])) {

                $contactCost = $this->getCarrierSmsRate($contact->country_code, $contact->network_name, $message, $provider, $userId);

                $mapping[$contact->network_name] = $contactCost;
            }

            $cost += $mapping[$contact->network_name];
        }
        return $cost;
    }

    public function getTotalCountryCost($contacts, $userId, $message) {
        $mapping = [];
        $cost = 0;

        foreach ($contacts as $contact) {

            if (!isset($mapping[$contact->country_code])) {

                $countryCost = $this->getCountrySmsRate($contact->country_code, $userId, $message);

                $mapping[$contact->country_code] = $countryCost;
            }

            $cost += $mapping[$contact->country_code];
        }
        return $cost;
    }

    public function getTotalCashOfContacts($contacts, $billType, $message) {
        switch ($billType) {
            case 'PER_CARRIER':

                $contacts = $this->setCarrierCost($contacts, $message);

                $contacts = collect($contacts);

                return $contacts->sum('ourCost');

                break;

            case 'PER_COUNTRY';

                $contacts = $this->setCountryCost($contacts);
                $contacts = collect($contacts);

                return $contacts->sum('ourCost');

                break;
        }
    }

    public function setSmsCost($contacts, $billType, $message, $provider, $userId) {
        switch ($billType) {
            case 'PER_CARRIER':

                return $contacts = $this->setCarrierCost($contacts, $message, $provider, $userId);

                break;

            case 'PER_COUNTRY';

                return $contacts = $this->setCountryCost($contacts, $userId, $message);

                break;
        }
    }

    public function setCarrierCost($contacts, $message, $provider, $userId) {
        $mapping = [];
        foreach ($contacts as $contact) {

            if (!isset($mapping[$contact->network_name])) {

                $cost = $this->getCarrierSmsRate($contact->country_code, $contact->network_name, $message, $provider, $userId);

                $mapping[$contact->network_name] = $cost;
            }

            $contact->cost = $mapping[$contact->network_name];
            $contact->ourCost = $mapping[$contact->network_name];
            $contact->credits = $this->calculateCredit($contact->ourCost);
        }

        return $contacts;
    }

    public function setCountryCost($contacts, $userId, $message) {
        $mapping = [];
        foreach ($contacts as $contact) {

            if (!isset($mapping[$contact->country_code])) {

                $cost = $this->getCountrySmsRate($contact->country_code, $userId, $message);

                $mapping[$contact->country_code] = $cost;
            }

            $contact->cost = $mapping[$contact->country_code];
            $contact->ourCost = $mapping[$contact->country_code];
            $contact->credits = $this->calculateCredit($contact->ourCost);
        }

        return $contacts;
    }

    public function calculateOurCost($cost) {
        return $ourCost = $cost * $this->profit;
    }

    public function calculateCredit($ourCost) {
        return $credit = ceil($ourCost / $this->costPerCredits);
    }

}
