<?php 

namespace App\Services\SMS\Providers;

use Twilio\Rest\Client;
use Twilio\Exceptions\RestException;
use Storage;

class Twilio extends CommProviderInstance implements SmsHandlerContract {

    public function sendSms()
    {
        $sid = config('services.twilio.account_sid');
        $token = config('services.twilio.auth_token');
        $client = new Client($sid, $token);
        
        $jsonResponse = [];
        
        try {
            
            $resolution = 'standard';
            if ($this->fax_job->resolution == 'fine') {
                $resolution = 'fine';
            }
            
            $options = [
                'quality' => $resolution,
                'statusCallback' => config('services.twilio.notify_url')
            ];
            
            $fax = $client->fax->v1->faxes->create(
                "+15017250604",
                $this->fax_number,
                //"https://www.twilio.com/docs/documents/25/justthefaxmaam.pdf"
                $this->document->myDocumentUrl($url = true),
                $options
            );
            
            $this->successWithFrontend($jsonResponse, $fax->sid);
            
        } catch(RestException $e) {
            
            // echo $e->getStatusCode();
            // echo $e->getMessage();
            $jsonResponse['message'] = $e->getMessage();
            $this->failedWithFrontend($jsonResponse);
        }
        
        // Then we make the document private again
        $filePath = 'documents/' . $this->document->user_id . '/' . $this->document->orig_filename;
        Storage::disk('s3-docs')->setVisibility($filePath, 'private');
        
        return $jsonResponse;
        
    }

    public function getSmsStatus($sid)
	{
        $sid = config('services.twilio.account_sid');
        $token = config('services.twilio.auth_token');
        $client = new Client($sid, $token);
        
        $jsonResponse = [];
        $fax = $client->fax->v1
            ->faxes($sid)
            ->fetch();
            
        // echo $fax->status;
            
        $jsonResponse['transaction_id'] = $sid;
        
        switch ($fax->status)
        {
            case 'ongoing':
                $this->ongoingWithFrontend($jsonResponse);
                break;
            case 'answered':
                $this->answeredWithFrontend($jsonResponse);
                break;
            default:
                $this->failedWithFrontend($jsonResponse);
                break;
        }
        
        return $jsonResponse;
 
	}


    public function getFaxRate($dest)
	{
        // $client = new GuzzleHttp\Client();
        // $response = $client->post(config('services.hoiio.api_url').'fax/get_rate', [
        //     'body' => [
        //         'app_id' => config('services.hoiio.app_id'),
        //         'access_token' => config('services.hoiio.access_token'),
        //         'incoming' => $dest,
        //     ]
        // ]);
        // $json_response = $response->json();
        // return $json_response;
	}


}
