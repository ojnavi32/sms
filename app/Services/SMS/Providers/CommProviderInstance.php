<?php 

namespace App\Services\SMS\Providers;

use Auth;

abstract class CommProviderInstance {

    public $fax_job;
    public $comm_provider_model;
    public $fax_number;
    public $document;

    public function __construct($fax_number = NULL)
    {
        if ($fax_number)
            $this->fax_number = $fax_number;
    }


    public function ongoingWithFrontend(&$json_response)
    {
        $json_response['front_end_status'] = 'ongoing';
        return $json_response;
    }


    public function successWithFrontend(&$json_response, $transaction_id)
    {
        $json_response['front_end_status'] = 'success';
        $json_response['transaction_id'] = $transaction_id;
        return $json_response;
    }

    public function answeredWithFrontend(&$json_response)
    {
        $json_response['front_end_status'] = 'answered';
        return $json_response;
    }


    public function failedWithFrontend(&$json_response)
    {
        $json_response['front_end_status'] = 'failed';
        return $json_response;
    }


}
