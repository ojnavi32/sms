<?php

namespace App\Services\SMS\Providers;

use Symfony\Component\Process\Process;
use Config;
use Exception;
use App\Models\Currency;

//require_once '../../../vendor/autoload.php';



class InfoBip implements SmsHandlerContract
{ 
    private $currency;

    // public $client;
    
     public function __construct()
     { 
        $this->currency = Currency::where('currency', 'kobe')->first();
        $this->client = new \infobip\api\client\SendSingleTextualSms(new \infobip\api\configuration\BasicAuthConfiguration(config('services.infobip.username'), config('services.infobip.password')));
        
     }


    public function sendsms($contactNums, $msgContent, $senderId)
    {   
        $requestBody = new \infobip\api\model\sms\mt\send\textual\SMSTextualRequest();

        if($senderId)
            $requestBody->setFrom($senderId);

        $requestBody->setTo($contactNums);
        $requestBody->setText($msgContent);

        $response = $this->client->execute($requestBody);
        $messages = $response->getMessages();
        $messageResps =array();
        foreach($messages as $message){
            $mId =  $message->getMessageId();
            $messageResps[] = [ 'mId' => $mId, 'status' => $message->getStatus()->getGroupName(), 'sendTo' => $message->getTo()];
        }
        return ['bulkId'=> $response->getBulkId(), 'messageResps' => $messageResps];
    }

     public function numberLookup($numbers)
    { 

        $requestBody = new \infobip\api\model\nc\query\NumberContextRequest();
        // $requestBody->setFrom(FROM);
        $requestBody->setTo($numbers);
        // $requestBody->setText($msg_content);

        $response = $this->client->execute($requestBody);
        $x = $response->getResults();
        // print_r($x[0]->getMccMnc());die('ppp');
        
    }
    public function batchDeliveryStatus($bulkId)
    { 
        $this->client = new \infobip\api\client\GetSentSmsDeliveryReports(new \infobip\api\configuration\BasicAuthConfiguration(config('services.infobip.username'), config('services.infobip.password')));
        
        $requestBody = new \infobip\api\model\sms\mt\reports\GetSentSmsDeliveryReportsExecuteContext();

        $requestBody->setBulkId($bulkId);

        $response = $this->client->execute($requestBody);

        $messages = $response->getResults();

        $messageDeliveryResp =array();

        foreach($messages as $message){
             $mId = $message->getMessageId(); 
             $price = $message->getPrice()->getPricePerMessage();
             //Converted to standard price euro
             $price = $price * 100;
             $price = $price/$this->currency->value;
             $status = $message->getStatus()->getGroupName();
             $messageDeliveryResp[$mId] = ['messageId' => $mId, 'status' => $status, 'cost' => $price];    
            
        }
        return $messageDeliveryResp;      

    }

    public function messageDeliveryStatus($messageId)
    { 
        $this->client = new \infobip\api\client\GetSentSmsDeliveryReports(new \infobip\api\configuration\BasicAuthConfiguration(config('services.infobip.username'), config('services.infobip.password')));
        
        $requestBody = new \infobip\api\model\sms\mt\reports\GetSentSmsDeliveryReportsExecuteContext();

        $requestBody->setMessageId($messageId);

        $response = $this->client->execute($requestBody);

        $messages = $response->getResults();
        
        if(isset($messages[0])){

            $message = $messages[0];
   
            $mId = $message->getMessageId(); 
            $price = $message->getPrice()->getPricePerMessage();
            //Converted to standard price euro
            $price = $price * 100;
            $price = $price/$this->currency->value;
            $status = $message->getStatus()->getGroupName();
            $messageDeliveryResp[$mId] = ['messageId' => $mId, 'status' => $status, 'cost' => $price];        

            return $messageDeliveryResp;     
         }

    }

}