<?php

namespace App\Services\SMS\Providers;

use Symfony\Component\Process\Process;
use Config;
use Exception;
use App\Models\Currency;

//require_once '../../../vendor/autoload.php';



class Routee implements SmsHandlerContract
{ 
    private $currency;

    public function __construct()
    { 
        $this->currency = Currency::where('currency', 'kobe')->first();
        $this->client = new \infobip\api\client\SendSingleTextualSms(new \infobip\api\configuration\BasicAuthConfiguration(config('services.infobip.username'), config('services.infobip.password')));
        
    }


    public function sendsms($contactNumbers, $msgContent, $senderId)
    {  
      $msgContent = str_replace("\n"," \n ",$msgContent);

      if(!$senderId)
            $senderId ='SMSTO';


      $authorizationToken = $this->getToken();
      $campaign = str_random(5);
 
     $body =  array("campaignCallback" => array("strategy" => "OnCompletion", "url" => "http://sms.to/mindpuzzle/campaigns"), "callback" =>  array("strategy"=> "OnCompletion", "url"=> "http://www.sms.to.com/message"),
                  "body" => $msgContent ,"campaignName" => $campaign, 
                  "to" => $contactNumbers, "from" => $senderId );


      $curl = curl_init();

      curl_setopt_array($curl, array(
                CURLOPT_URL => "https://connect.routee.net/sms/campaign",
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => "",
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 30,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => "POST",
                CURLOPT_POSTFIELDS => json_encode($body),
                CURLOPT_HTTPHEADER => array(
                  "authorization: Bearer ".$authorizationToken,
                  "content-type: application/json"
                ),
      ));


      $response = curl_exec($curl);
      $err = curl_error($curl);

      curl_close($curl);

      if ($err) {
        echo "cURL Error #:" . $err;
      } else {
          
          $routeeSendSms = json_decode($response,true);
     
          $respNumbers = [];
          if(isset($routeeSendSms['to'])) 
            $respNumbers = str_replace("+", "", $routeeSendSms['to']);

          foreach($respNumbers as $respNumber){ 
            $messageResps[] = [ 'mId' => $respNumber, 'status' => strtoupper($routeeSendSms['state']), 'sendTo' => $respNumber];
          }
         
            return ['bulkId'=> $routeeSendSms['trackingId'], 'messageResps' => $messageResps];
        }
    }

    public function batchDeliveryStatus($bulkId)
    { 
        $authorizationToken = $this->getToken();
        $curl = curl_init();

        curl_setopt_array($curl, array(
                CURLOPT_URL => "https://connect.routee.net/sms/tracking/campaign/".$bulkId,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => "",
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 30,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => "GET",
                CURLOPT_HTTPHEADER => array(
                  "authorization: Bearer ".$authorizationToken,
                  "content-type: application/json"
                ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
            echo "cURL Error #:" . $err;
        } else {
            $response = json_decode($response,true);
        }

          
        $respNumbers = $response['content'];

        $messageDeliveryResp =array();

          foreach($respNumbers as $respNumber){
               $mId = str_replace("+", "", $respNumber['to']); 
               $price = $respNumber['price'];
               $status = $respNumber['status']['status'];
               $messageDeliveryResp[$mId] = ['messageId' => $mId, 'status' => strtoupper($status), 'cost' => $price];    
              
          }

        return $messageDeliveryResp;      

    }

    public function messageDeliveryStatus($messageId)
    { 
        $this->client = new \infobip\api\client\GetSentSmsDeliveryReports(new \infobip\api\configuration\BasicAuthConfiguration(config('services.infobip.username'), config('services.infobip.password')));
        
        $requestBody = new \infobip\api\model\sms\mt\reports\GetSentSmsDeliveryReportsExecuteContext();

        $requestBody->setMessageId($messageId);

        $response = $this->client->execute($requestBody);

        $messages = $response->getResults();
        
        if(isset($messages[0])){

            $message = $messages[0];
   
            $mId = $message->getMessageId(); 
            $price = $message->getPrice()->getPricePerMessage();
            //Converted to standard price euro
            $price = $price * 100;
            $price = $price/$this->currency->value;
            $status = $message->getStatus()->getGroupName();
            $messageDeliveryResp[$mId] = ['messageId' => $mId, 'status' => $status, 'cost' => $price];        

            return $messageDeliveryResp;     
         }

    }

     public function encodeBase64()
     { 

       $applicationId = config('services.routee.applicationId');
       $secret = config('services.routee.secret');
       $base64 = $applicationId . ":" . $secret;
       return base64_encode($base64);
      }


    public function getToken()
    {
      $curl = curl_init();

      curl_setopt_array($curl, array(
        CURLOPT_URL => "https://auth.routee.net/oauth/token",
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => "",
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 30,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => "POST",
        CURLOPT_POSTFIELDS => "grant_type=client_credentials",
        CURLOPT_HTTPHEADER => array(
          "authorization: Basic ".$this->encodeBase64(),
          "content-type: application/x-www-form-urlencoded"
        ),
      ));

      $response = curl_exec($curl);
      $err = curl_error($curl);
      curl_close($curl);

      if ($err) {
        echo "cURL Error #:" . $err;
      } else{
        $response = json_decode($response);
      }
     return $response->access_token;

    }

    public function executeRequest($method,$endPoint,$bodyContent=null)
    {
      $authorizationToken = $this->getToken();


        $opts = array(
          CURLOPT_URL => $endPoint,
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => "",
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 30,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => $method,
          CURLOPT_HTTPHEADER => array(
            "authorization: Bearer ".$authorizationToken,
            "content-type: application/json"
          ),
        );

        if ($bodyContent) { 
              $opts[CURLOPT_POSTFIELDS] = json_encode($bodyContent);
        }
        $curlSession = curl_init();
        curl_setopt_array($curlSession, $opts);


        $response = curl_exec($curlSession);

        $err = curl_error($curlSession);

        curl_close($curlSession);

        if ($err) {
             echo "cURL Error #:" . $err;
        } else { 

            return $response = json_decode($response,true);
        }
    }

}
