<?php

namespace App\Services\SMS\Reports;

use Carbon\Carbon;

use App\Models\SmsBroadcastMessage;

class CostActivity 
{
	public static function getCostActivityData($filter)
	{
		$startDate = '';
        $endDate = '';
        
		switch($filter) 
		{
			case 'last_month':
				$startDate = new Carbon('first day of last month');
                $endDate = new Carbon('last day of last month');
            	break;
            case 'this_month':
            	$startDate = Carbon::now()->startOfMonth();
                $endDate = Carbon::now()->toDateTimeString();
            	break;
            case 'last_week':
            	$previous_week = strtotime("-1 week +1 day");

                $start_week = strtotime("last sunday midnight",$previous_week);
                $end_week = strtotime("next saturday",$start_week);

                $startDate = date("Y-m-d",$start_week);
                $endDate = date("Y-m-d",$end_week);
	            break;
	        case 'this_week':
	        	$startDate = Carbon::now()->subDay()->startOfWeek()->toDateString(); // or ->format(..)
                $endDate = Carbon::now()->toDateTimeString();
            	break;
            case 'this_year':
            	$startDate = Carbon::now()->startOfYear();
                $endDate = Carbon::now()->endOfYear();
	           	break;
	        case 'last_year':
	        	$year = date('Y') - 1; // Get current year and subtract 1
                $startDate = Carbon('first day of January'.$year);
                $endDate = Carbon('first day of December'.$year);
                break;
		}
		
		$heatmapCost = SmsBroadcastMessage::selectRaw('country_dial_code_id,sum(cost) as totalCost')
                                         ->when($filter, function ($query) use ($startDate, $endDate) {
                                            return $query->whereBetween('created_at', [$startDate, $endDate]);
                                         })->where('user_id', auth()->user()->id)
                                         ->groupBy('country_dial_code_id') 
                                         ->pluck('totalCost','country_dial_code_id')
                                         ->all();
        
        $data = [];
        
        foreach($heatmapCost as $key =>$value)  
        { 
            $countryCode = getCountryCodeFromDialCode($key);

            if($countryCode) {
                $data[$countryCode] = $value;

            } 
        }  

        return $data;
	}
}