<?php

namespace App\Services\SMS\Reports;

use App\Models\SmsBroadcastMessage;

class SMSBroadcastMessageReports {

    public static function generateWeekly($startDate, $endDate)
	{
		return SmsBroadcastMessage::selectRaw('count(id) as `data`,DATE_FORMAT(created_at, "%Y-%m-%d") as day')
                ->whereBetween('created_at', [$startDate, $endDate])
                ->where('user_id', auth()->user()->id)
                ->groupBy('day')
                ->pluck('data','day')
                ->all();
	}

	public static function generateDaily($startDate, $endDate)
	{
		return SmsBroadcastMessage::selectRaw('count(id) as `data`,DATE_FORMAT(created_at, "%Y-%m-%d") as day')
                    ->whereBetween('created_at', [$startDate, $endDate])
                    ->where('user_id', auth()->user()->id)
                    ->groupBy('day')
                    ->pluck('data','day')
                    ->all();
	}


	public static function generateMonthly($startDate, $endDate)
	{
		return SmsBroadcastMessage::selectRaw('count(id) as `data`, MONTHNAME(created_at) month')
                    ->whereBetween('created_at', [$startDate, $endDate])
                    ->where('user_id', auth()->user()->id)
                    ->groupby('month')
                    ->pluck('data','month')
                    ->all();
	}

}