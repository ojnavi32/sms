<?php 

namespace App\Services;

use App\Models\CountryDialCode;
use App\Models\ProviderRate;
use App\Models\UserRateAdjustment;
use App\Services\SMS\Calculators\RateCalculator;

use Cache;

class CountryInfo
{
    public $contact;
    public $countryRates;
    public $rate;
    public $prefix;
    public $countryDialCodeId;

    public function __construct($contact = NULL)
    {
        $this->contact = $contact ? : $this->contact;
    }

    public function dialCodesFromNum()
    {
        return CountryDialCode::whereRaw(":somevar LIKE CONCAT(`dial_code`,'%')", ['somevar' => str_replace('+', '', $this->contact)]);
    }

    public function dialCodeFromNum()
    {
        // $dialCode = $this->dialCodesFromNum()->first(['dial_code']);
        
        $dialCode = $this->dialCodesFromNum()->first();
        
        //$this->prefix = $this->dialCodesFromNum()->first(['dial_code'])['dial_code'];
        
        // 1 for US 63 for PH
        $this->prefix = $dialCode->dial_code;
        
        $this->countryDialCodeId = $dialCode->id;
        
        return $this->dialCodesFromNum()->first(['dial_code'])['dial_code'];
    }

    public function countryRates()
    {
        return $this->countryRates = $this->dialCodesFromNum()
                        ->join('rates', 'rates.country_dial_code_id', '=', 'country_dial_codes.id')
                        ->get()
                        ->keyBy('plan');
    }

    public function hasCountryRates()
    {
        return (boolean)$this->countryRates()->count();
    }

    public function rateToUse()
    {
        // We can use cache for saving resources
        // $value = Cache::remember('users', $minutes, function () {
        //     return DB::table('users')->get();
        // });
        
        $contact = str_replace('+', '', $this->contact);
        $countryDialCode = $this->dialCodeFromNum();
        
        $rates = $this->countryRates();

        $contact = str_replace('+'. $countryDialCode, '', $this->contact);

        $rateCalculator = new RateCalculator;
        return $this->rate = $rateCalculator->getRateByPrefix($rates, $contact);
    }
    
    public function providerRate($user = null)
    {
        // If we have custome pricing for user
        if ($user) {
            $providerRate = UserRateAdjustment::where('user_id', $user->id)
                ->where('rate_id', $this->rate->id)
                ->first();
                
            if ($providerRate) {
                return $providerRate;
            }
        }
        
        // To do: We can also query orderBy base_rate asc to use the lowest rate
        $providerRate = ProviderRate::where('rate_id', $this->rate->id)->orderBy('priority')->first();
        
        if ($providerRate) {
            return $providerRate;
        }
    }
    
}
