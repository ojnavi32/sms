<?php namespace App\Services;

use GuzzleHttp\Client;

class VatEu {

    protected $client;

    public function __construct(Client $client)
    {
        $this->client = $client;
    }

    public function validateVatNumber($vatNumber)
    {
        //$companyName = $request->input('company');
        $vatNumber = str_replace(' ', '', $vatNumber);
       // $country = $request->country;

        if ($vatNumber == '') {
            return false;
        }
        // http://apilayer.net/api/validate?access_key=345d372cc739b313e7454aa5f1c6ae86&vat_number=CY10352115X
        $response = $this->client->get(config('services.vat_layer.api_url') . 'validate?' . 
            'access_key=' . config('services.vat_layer.api_key') .
            '&vat_number=' . $vatNumber
        );

        //$response = $this->client->get('https://vatlayer.com/php_helper_scripts/vat_api.php?secret_key=f891ab6f971af3c4b104ab8926979613&vat_number=' . $vatNumber);
        if ($response->getStatusCode() == 200) {
            //$json = $response->json();
            $json = json_decode($response->getBody(), true);          
            if (isset($json['valid'])) {
                return $json['valid'];
            }
            return false;
        }
        return false;
    }
}