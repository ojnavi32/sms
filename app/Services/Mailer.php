<?php namespace App\Services;

use Mail;

class Mailer {

    public function __construct()
    {
    }

    public function send(...$params)
    {
        // Its gonna be something like App\Mail\NumberOrderSuccessful
        $class = "\\App\\Mail" . "\\" . $params[0];
        if ( ! class_exists($class)) {
            return false;
        }
        Mail::queue(new $class($params[1]));
        return true;
    }
}