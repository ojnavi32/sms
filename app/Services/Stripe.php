<?php 

namespace App\Services;
use Auth;
use DB, Cache;
use App\Services\Stripe;

class Stripe {
   protected $apiSecret;
    
    public function __construct()
    {
        $this->apiSecret = config('services.stripe.secret');
    }
    
    /*
      *Function for create charge using stripe
      * @parm $request
      * return response
    */
    public function stripeCharge($request)
    {
        \Stripe\Stripe::setApiKey($this->apiSecret);

        $total = $request->total;

        $result = $this->getToken();
        $charge = \Stripe\Charge::create([
          'amount' => $total * 100,
          'currency' => 'eur',
          'source' => $result->id, 
          'description' => request('description')
        ]);
        if($charge->status == 'succeeded') {
          return ['status' => 'success', 'message' => $charge->status];
        }
    }

    public function getToken()
    {
      $user = Auth::user();
      return \Stripe\Token::create([
        'card' => [
        'number' => request('cardNum'),
        'exp_month' => request('expMnth'),
        'exp_year' => request('expYear'),
        'cvc' => request('cvc'),
        'name' => $user->business_name,
        'address_line1' => $user->business_address,
        'address_country' => $user->business_country,
        'address_zip' => $user->business_postal_code
        ]
      ]);
    }
}
