<?php 

namespace App\Services;

use DB;
use Carbon\Carbon;
use App\Models\FaxJob;
use App\Models\Document;

class Stats
{
    public function __construct()
    {
        
    }

    public function revenueDays()
    {
        $date = Carbon::now();
        $date->subDays(20);

        $days = DB::table('payment_history')
                    ->select(DB::raw('created_at AS format, DATE(created_at) AS date, SUM(amount) AS sales'))
                    ->where(DB::raw('date(created_at)'), '>', $date->format('Y-m-d'))
                    ->where('status', '!=', 'pending')
                    ->whereNull('ontrial')
                    ->groupBy('date')
                    ->orderBy('date')
                    ->get();

        $days = collect($days);
        $plots = [];
        foreach ($days as $day) {
            $plots['x'][] = Carbon::createFromFormat('Y-m-d H:i:s', $day->format)->format('F d');
            $plots['y'][] = $day->sales;
        }
        return $plots;
    }

    public function revenueWeeks()
    {
        $date = Carbon::now();
        $date->subWeeks(8);

        $weeks = DB::table('payment_history')
                    ->select(DB::raw('created_at AS format, YEARWEEK(created_at) AS week, SUM(amount) AS sales, '. "CONCAT(DATE_FORMAT(DATE_ADD(created_at, INTERVAL(1-DAYOFWEEK(created_at)) DAY),'%Y-%m-%e'), ' to ',  DATE_FORMAT(DATE_ADD(created_at, INTERVAL(7-DAYOFWEEK(created_at)) DAY),'%Y-%m-%e')) AS date_range"))
                    ->where(DB::raw('date(created_at)'), '>', $date->format('Y-m-d'))
                    ->where('status', '!=', 'pending')
                    ->whereNull('ontrial')
                    ->groupBy('week')
                    ->orderBy('week')
                    ->get();
        $weeks = collect($weeks);
        $plots = [];
        foreach ($weeks as $week) {
            // We remove the year in $day->week so 201623 become 23
            $plots['x'][] = 'Week ' . substr($week->week, 4) . '(' . $week->date_range . ')';
            $plots['y'][] = $week->sales;
        }
        return $plots;
    }

    public function revenueMonths()
    {
        $months = DB::table('payment_history')
                    ->select(DB::raw('created_at AS format, DATE_FORMAT(created_at, "%m-%Y") AS month, SUM(amount) AS sales'))
                    ->where('status', '!=', 'pending')
                    ->groupBy('month')
                    ->whereNull('ontrial')
                    ->orderBy('created_at')
                    ->get();
        $months = collect($months);
        $plots = [];
        foreach ($months as $month) {
            $plots['x'][] = Carbon::createFromFormat('m-Y', $month->month)->format('F Y');
            $plots['y'][] = $month->sales;
        }
        return $plots;
    }

    public function activeSubscribersDays()
    {
        $date = Carbon::now();
        $date->subDays(20);

        $days = DB::table('user_dids')
                    ->select(DB::raw('created_at AS format,DATE(created_at) AS date, COUNT(id) AS sales'))
                    ->where(DB::raw('date(created_at)'), '>', $date->format('Y-m-d'))
                    //->where('status', '!=', 'pending')
                    ->groupBy('date')
                    ->orderBy('date')
                    ->get();

        $days = collect($days);
        $plots = [];
        foreach ($days as $day) {
            $plots['x'][] = Carbon::createFromFormat('Y-m-d H:i:s', $day->format)->format('F d');
            $plots['y'][] = $day->sales;
        }
        return $plots;
    }

    public function activeSubscribersWeeks()
    {
        $date = Carbon::now();
        $date->subWeeks(8);

        $weeks = DB::table('user_dids')
                    ->select(DB::raw('created_at AS format, YEARWEEK(created_at) AS week, COUNT(id) AS sales, '. "CONCAT(DATE_FORMAT(DATE_ADD(created_at, INTERVAL(1-DAYOFWEEK(created_at)) DAY),'%Y-%m-%e'), ' to ',  DATE_FORMAT(DATE_ADD(created_at, INTERVAL(7-DAYOFWEEK(created_at)) DAY),'%Y-%m-%e')) AS date_range"))
                    ->where(DB::raw('date(created_at)'), '>', $date->format('Y-m-d'))
                    //->where('status', '!=', 'pending')
                    ->groupBy('week')
                    ->orderBy('week')
                    ->get();
        $weeks = collect($weeks);
        $plots = [];
        foreach ($weeks as $week) {
            // We remove the year in $day->week so 201623 become 23
            $plots['x'][] = 'Week ' . substr($week->week, 4) . '(' . $week->date_range . ')';
            $plots['y'][] = $week->sales;
        }
        return $plots;
    }

    public function activeSubscribersMonths()
    {
        $months = DB::table('user_dids')
                    ->select(DB::raw('created_at AS format, DATE_FORMAT(created_at, "%m-%Y") AS month, COUNT(id) AS sales'))
                    //->where('status', '!=', 'pending')
                    ->groupBy('month')
                    ->orderBy('created_at')
                    ->get();
        $months = collect($months);
        $plots = [];
        foreach ($months as $month) {
            $plots['x'][] = Carbon::createFromFormat('m-Y', $month->month)->format('F Y');
            $plots['y'][] = $month->sales;
        }
        return $plots;
    }

    public function userFaxPages($userId)
    {
        $date = Carbon::now();
        $date->subDays(30);

        $days = FaxJob::select(DB::raw('created_at AS format, DATE(created_at) AS date, SUM(rate_id) AS pages, (select total_pages from documents where id = fax_jobs.document_id) as total_pages'))
                    ->where(DB::raw('date(created_at)'), '>', $date->format('Y-m-d'))
                    ->where('status', '=', 'success')
                    ->where('user_id', $userId)
                    ->groupBy('date')
                    ->orderBy('date')
                    ->get();

        $days = collect($days);
        $plots = [];
        foreach ($days as $day) {
            $pages = 0;
            $faxDate = Carbon::createFromFormat('Y-m-d H:i:s', $day->format)->format('Y-m-d');
            $plots['x'][] = Carbon::createFromFormat('Y-m-d H:i:s', $day->format)->format('m/d');
            $plots['total_pages'][] = $day->total_pages;
            $docs = FaxJob::where(DB::raw('date(created_at)'), '=', $faxDate)
                    ->where('status', '=', 'success')
                    ->where('user_id', $userId)
                    ->pluck('document_id');

            $doc = Document::whereIn('id', $docs)->pluck('total_pages', 'id')->toArray();

            foreach ($docs as $documentId) {
                if (isset($doc[$documentId])) {
                    $pages += $doc[$documentId];
                }
                
            }

            $plots['y'][] = $pages;
        }

        return $plots;
    }

    public function profileStats($user)
    {
        $dt = Carbon::now();
        $today = $dt->format('Y-m-d');
        $month = $dt->year . '-' . sprintf('%02d', $dt->month);
        $dt->subMonth();
        $lastMonth = $dt->year . '-' . sprintf('%02d', $dt->month);

        $faxSent = [];
        $faxSent['total'] =$user->fax_jobs()->where('status', '=', 'success')->count();
        $faxSent['today'] = $user->fax_jobs()->where('status', '=', 'success')->where('created_at', 'LIKE', $today . '%')->count();
        $faxSent['thisMonth'] = $user->fax_jobs()->where('status', '=', 'success')->where('created_at', 'LIKE', $month . '%')->count();
        $faxSent['lastMonth'] = $user->fax_jobs()->where('status', '=', 'success')->where('created_at', 'LIKE', $lastMonth . '%')->count();
        $faxReceived = [];
        $faxReceived['total'] = $user->inbox()->count();
        $faxReceived['today'] = $user->inbox()->where('created_at', 'LIKE', $today . '%')->count();
        $faxReceived['thisMonth'] = $user->inbox()->where('created_at', 'LIKE', $month . '%')->count();
        $faxReceived['lastMonth'] = $user->inbox()->where('created_at', 'LIKE', $lastMonth . '%')->count();

        return (object) [
            'faxSent' => (object) $faxSent,
            'faxReceived' => (object) $faxReceived
        ];
    }
}
