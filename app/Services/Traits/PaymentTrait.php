<?php

namespace App\Services\Traits;
use Mail;
use App\Mail\Invoice;

trait PaymentTrait
{
    public function getData()
    {
    	$this->getBillingDetails();

        return [
            'me' => auth()->user(),
            'meta_title' => trans('meta.add_funds'),
            'cardholder_name' => old('cardholder_name') ?: auth()->user()->name,
            'country_a2_code' => getCountrya2Code(),
            'country_dial_codes' => countries(),
            'customer_id' => auth()->user()->brain_tree_customer_id,
        ];
    }
    
    public function cashBalanceNumberPayment($number, $user = null)
    {
        // Create invoice
        $this->user = $user;

        // We should create payment history data here
        // Save the payments details
        $billingDetail = @$user->billing_detail;
        $this->paymentMethodId = 7;
        $this->userDidId = $number->id;
        $id = $this->savePaymentHistoryDetails(
            $billingDetail,
            $user->number_cost,
            $vat = null,
            $transactionId = str_random(20),
            $onTrial = NULL,
            $subscriptionId = null
        );
        
        Mail::send(new Invoice($id));
    }
    
    public function payUsingBalance($user, $amount, $vat, $numberId = null)
    {
        // If the cash balance is not enough
        if ($user->cash_balance < $vat['pay_amount']) {
            return ['reactivate_balance_not_enough' => true, 'message' => 'Balance is not enough.'];
        }
        
        $number = $this->dids->find($numberId);
        
        // Set the payment method into cash
        $this->paymentMethodId = 7;
        $this->userDidId = $number->id;
        
        // Save the payments details
        $id = $this->savePaymentHistoryDetails(
                $user->billing_detail,
                $vat['base_amount'],
                $vat['vat'],
                $transactionId = str_random(20),
                $onTrial = null,
                null
        );
        
        Mail::send(new Invoice($id));
        
        // We need to cancel the current subscription first
        $this->cancelSubscription($number->subscription_id);

        $expirationDate = $number->expiration_date;
        
        $number->subscription_id = null;
        $number->status = 1;
        $number->payment_failed = null;
        $number->expiration_date = $expirationDate;
        $number->save();

        // Deduct amount from cash balance
        $user->cash_balance -= $vat['pay_amount'];
        $user->save();

        // Enable all files in users' inbox
        $this->inbox->where('did_id', $number->did)->update(['did_status' => 1, 'payment_failed' => null]);

        $data['reactivate_balance'] = true;
        
        return $data;
    }
    
}