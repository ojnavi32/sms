<?php 

namespace App\Services\SmsHandler;

use Config;
use Auth;
use App\Models\User;
use App\Services\SMS\Providers\InfoBip;
use App\Services\SMS\Providers\Routee;
use App\Services\SMS\Providers\SmsKent\SmsKent;

class SmsHandler
{
    public $providerUsed;
    public $commProvider;

    /**
     * 
     * @param string $comm_provider_slug
     */
    public function __construct($provider)
    {
       
        if ($provider) {
            $this->setProvider($provider);
           
        }
    }

    public function setProvider($provider)
    {
        switch ($provider)
        { 
            case 'Infobip': 
                $this->commProvider = new InfoBip;
                break;
            case 'Routee':
                $this->commProvider = new Routee;
                break;
            case 'Sms Kent':
                $this->commProvider = new SmsKent;
                break;
            default:
                $this->commProvider = NULL;
                break;
        }
    }


    
   
    

}