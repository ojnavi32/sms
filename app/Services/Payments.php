<?php

namespace App\Services;

use Braintree_Customer;
use Braintree_Transaction;
use Braintree_Subscription;
use Braintree_PaymentMethod;
use Braintree_Exception_NotFound;
use Braintree_WebhookNotification;
use Braintree_CreditCard;
use App\Models\Subscription;
use SmsBraintree;
use Carbon\Carbon;

use Illuminate\Http\Request;
use Auth, Storage;
use App\Models\User;
use App\Models\BillingDetail;
use App\Models\CountryDialCode;
use App\Models\PaymentMethod;
use App\Models\PaymentHistoryDetail;
use App\Models\ExchangeRate;
use App\Services\VatEu;
use App\Services\AgileCrm;
use App\Services\Traits\PaymentTrait;

class Payments {
    
	public $customerId;
	public $customer;
	public $paymentInstrumentType = 'paypal';
    public $paymentMethodId;
    public $userDidId = null;
	public $user = null;
	public $token;
    public $merchantAccountId;
    public $paymentStatus = 'authorized';
    public $paymentMethod;
    public $plans = ['3' => 27, '12' => 90];
    public $transactionOptions = [];
    public $vatEu;
    public $currency = 'EUR';
    protected $agile;
    protected $dids;
    protected $inbox;

	public function __construct(User $user, VatEu $vatEu, AgileCrm $agile)
	{
		$this->user = $user;
        $this->vatEu = $vatEu;
        $this->agile = $agile;
        
		SmsBraintree::init();
	}

    use PaymentTrait;

	public function createSubsription($planId, $request, $payAmount, $options = null, $firstBillingDate = null)
	{
		$paymentMethodNonce = $request->input('payment_method_nonce');
        if ($this->token) {

            $braintreeSubscription = [
                'planId' => $planId,
                'paymentMethodToken' => $this->token,
                'price' => $payAmount,
            ];

            // If we have options
            if ( $options != null) {
                $braintreeSubscription['options'] = $options;
            }

            // Use the dollar account merchant account id
            if ( $request->input('currency') == 'USD') {
                $braintreeSubscription['planId'] = $planId . 'd';
                $braintreeSubscription['merchantAccountId'] = env('BRAINTREE_MERCHANT_ACCOUNT_USD', 'intergointeractiveUSD');
            }
            if ( $request->input('currency') == 'GBP') {
                $braintreeSubscription['planId'] = $planId . 'p';
                $braintreeSubscription['merchantAccountId'] = env('BRAINTREE_MERCHANT_ACCOUNT_GBP', 'intergointeractiveGBP');
            }

            $subscription = Braintree_Subscription::create($braintreeSubscription);

            return $subscription;
		}

        $braintreeSubscription = [
                'planId' => $planId,
                'paymentMethodNonce' => $paymentMethodNonce,
                'price' => $payAmount,
        ];

        if ($firstBillingDate != null) {
            $braintreeSubscription['firstBillingDate'] = $firstBillingDate;
        }

        // If we have options
        if ( $options != null) {
            $braintreeSubscription['options'] = $options;
        }

        // Use the dollar account merchant account id
        if ( $request->input('currency') == 'USD') {
            $braintreeSubscription['planId'] = $planId . 'd';
            $braintreeSubscription['merchantAccountId'] = env('BRAINTREE_MERCHANT_ACCOUNT_USD', 'intergointeractiveUSD');
        }
        
        if ( $request->input('currency') == 'GBP') {
            $braintreeSubscription['planId'] = $planId . 'p';
            $braintreeSubscription['merchantAccountId'] = env('BRAINTREE_MERCHANT_ACCOUNT_GBP', 'intergointeractiveGBP');
        }
            
		$subscription = Braintree_Subscription::create($braintreeSubscription);

        if ($subscription->success) {
            $paymentMethod = Braintree_PaymentMethod::find($subscription->subscription->paymentMethodToken);
            $paymentInstrumentType = $this->getPaymentInstrumentType($paymentMethod);
            $this->paymentInstrumentType = $paymentInstrumentType;
        }

        return $subscription;
	}

	public function cancelSubscription($subscriptionId)
	{
		if ($subscriptionId == '') {
			return false;
		}
		try {
            Braintree_Subscription::cancel($subscriptionId);
            return true;
        } catch (Braintree_Exception_NotFound $e) {
            return false;
        }

        return false;
	}

	public function getPaymentInstrumentType($paymentMethod)
	{
		if (get_class($paymentMethod) == 'Braintree_PayPalAccount') {
            return 'paypal';
        }
        if (get_class($paymentMethod) == 'Braintree_CreditCard') {
            return 'credit-card';
        }
	}

	public function saveBillingDetails($request, $userId = NULL)
	{
		$user = Auth::user();
        if ($userId != NULL) {
            $user = $this->user->find($userId);
        }

        if ($this->user->id) {
            $user = $this->user;
        }

        $billingDetail = $user->billing_detail;
        if ( ! $billingDetail ) {
            $billingDetail = BillingDetail::create([
                'user_id' => $user->id,
            ]);
        }

        // If we have save_billing_details and we dont want to save it 
        if ($request->has('save_billing_details') and ! $request->save_billing_details) {
            return $billingDetail;
        }
        $billingDetail->firstname = $request->input('firstname');
        $billingDetail->lastname = $request->input('lastname');
        if ($request->has('company')) {
            $billingDetail->company = $request->input('company');
        }

        $billingDetail->vat_number = $request->input('vat_number');
        $billingDetail->street_address = $request->input('street_address');
        $billingDetail->postal_code = $request->input('postal_code');
        $billingDetail->country_dial_code_id = ($request->input('country_dial_code_id') ?: NULL);
        if ($request->has('auto_recharge_amount')) {
            $billingDetail->auto_recharge_amount = $request->input('auto_recharge_amount');
            $billingDetail->recharge_when_amount = $request->input('recharge_when_amount');
            $billingDetail->currency = $request->input('currency');
            $billingDetail->payment_method_token = $request->input('payment_method_token');
        }

        $billingDetail->save();

        return $billingDetail;
	}

    public function getBillingDetails($userId = NULL)
    {
        $user = auth()->user();
        
        if ($userId != NULL) {
            $user = $this->user->find($userId);
        }

        $name = explode(' ', $user->name);
        $billingDetail = $user->billing_detail;

        if ( ! $billingDetail ) {
            $billingDetail = $user->billing_detail()->create([
                'firstname' => @$name[0]. '',
                'lastname' => @$name[1]. ''
            ]);
        }
        if (trim($billingDetail->firstname . $billingDetail->lastname) == '') {
            $billingDetail->firstname = @$name[0];
            $billingDetail->lastname = @$name[1];
        }

        return $billingDetail;
    }

	public function calculateVat($requestData, $amount)
	{
        // Convert the amount to USD
        if ( $requestData['currency'] == 'USD') {
            $amount = $this->getConversion($amount, 'EUR', 'USD');
        }
        
        // Convert the amount to USD
        if ( $requestData['currency'] == 'GBP') {
            $amount = $this->getConversion($amount, 'EUR', 'GBP');
        }

        // We need to check of the company have a valid VAT Number so we
        // wont charge them VAT
        if (session('vat_number_validated')) {
            return [
                'vat' => null, 
                'tax_amount' => null, 
                'pay_amount' => round($amount, 2), 
                'vat_exempt' => true,
                'base_amount' => $amount,
            ];
        }
        
        $vat = NULL;

        if ($requestData['country_dial_code_id'] != 0) {
            $vat = CountryDialCode::find($requestData['country_dial_code_id'])->vat;
        }

        if ($vat) {
            $tax_amount = round((($vat/100) * $amount), 2);
            $pay_amount = $amount + $tax_amount;
        } else {
            $tax_amount = NULL;
            $pay_amount = $amount;
        }

        return [
            'vat' => $vat, 
            'tax_amount' => $tax_amount, 
            'pay_amount' => round($pay_amount, 2),
            'vat_exempt' => false,
            'base_amount' => $amount,
        ];
	}

	public function createBraintreeCustomer($user)
	{
		$name = explode(' ', $user->name);
		$result = Braintree_Customer::create([
			'email' => $user->email,
			'firstName' => @$name[0],
			'lastName' => @$name[1],
		]);

        if ($result->success) {
            return $result->customer->id;
        }
	}

	public function brainTreeCustomer($paymentMethodNonce)
	{
		$user = Auth::user();
		$name = explode(' ', $user->name);

		if ($user->braintree_customer) {
            $bt_customer_id = $user->braintree_customer->customer_id;
            $customer = Braintree_Customer::find($bt_customer_id);

            $customerId = $bt_customer_id;
            $this->customerId = $customerId;
            
            Braintree_Customer::update(
                $bt_customer_id,
                [
                    'email' => $user->email,
                    'firstName' => @$name[0],
                    'lastName' => @$name[1],
                ]
            );
        } else {
        	// Create customer
        	$customer = Braintree_Customer::create([
                'email' => auth()->user()->email,
                'firstName' => @$name[0],
                'lastName' => @$name[1],
            ]);

            if ($customer->success) {
            	$this->customerId = $customer->customer->id;

                $paymentMethod = Braintree_PaymentMethod::create([
                    'customerId' => $this->customerId,
                    'paymentMethodNonce' => $paymentMethodNonce
                ]);

                if ($paymentMethod->success) {
                    $this->paymentInstrumentType = $this->getPaymentInstrumentType($paymentMethod->paymentMethod);
                    $this->token = $paymentMethod->paymentMethod->token;
                }
            }
        }
	}

	public function setPaymentMethod($paymentMethod)
	{
		$this->paymentMethod = $paymentMethod;
	}

    public function getPaymentMethod($key = '')
    {
        if ($key != '') {
            return $this->paymentMethod->name;
        }

        return $this->paymentMethod;
    }

    public function setPaymentInstrumentType($paymentInstrumentType)
    {
        if ($paymentInstrumentType == 'paypal_account') {
            $this->paymentInstrumentType = 'paypal';
        }
        if ($paymentInstrumentType == 'credit_card') {
            $this->paymentInstrumentType = 'credit-card';
        }
    }

    public function getPaymentMethodId()
    {
        // Cash Balance
        if ($this->paymentMethodId == 7) {
            return $this->paymentMethodId;
        }
        $payment_method = PaymentMethod::whereSlug($this->paymentInstrumentType)->first();

        $this->setPaymentMethod($payment_method);

        $paymentMethodId = $payment_method->id;

        if ($this->paymentMethodId != '') {
            $paymentMethodId = $this->paymentMethodId;
        }

        return $paymentMethodId;
    }

	public function savePaymentHistoryDetails($billingDetail, $amount, $vat, $transactionId, $onTrial = NULL, $subscriptionId = NULL)
	{
        $paymentMethodId = $this->getPaymentMethodId();

        if (auth()->check()) {
            $user = Auth::user();
        } else {
            $user = $this->user;
        }

        $vatAmount = $vat;
        $vatExempt = null;

        if (is_array($vat)) {
            $vatAmount = $vat['vat'];
            $vatExempt = $vat['vat_exempt'];
        }

        $paymentHistory = $user->payment_history()->create([
            'payment_method_id' => $paymentMethodId,
            'user_did_id' => $this->userDidId,
            'amount' => $amount,
            'vat' => $vatAmount,
            'vat_exempt' => $vatExempt,
            'status' => $this->paymentStatus,
            'ontrial' => $onTrial,
            'subscription_id' => $subscriptionId,
        ]);

        // If billing details is provided
        if ( ! empty($billingDetail) ) {

            $paymentHistory->payment_history_details()->saveMany([
                new PaymentHistoryDetail(['name' => 'Braintree Transaction ID', 'info' => ($transactionId ?: '')]),
                new PaymentHistoryDetail(['name' => 'Name', 'info' => $billingDetail->firstname . ' ' . $billingDetail->lastname]),
                new PaymentHistoryDetail(['name' => 'Street Address', 'info' => $billingDetail->street_address]),
                new PaymentHistoryDetail(['name' => 'Postal Code', 'info' => $billingDetail->postal_code]),
                new PaymentHistoryDetail(['name' => 'Country', 'info' => $billingDetail->country_dial_code->country]),
                new PaymentHistoryDetail(['name' => 'Company', 'info' => $billingDetail->company]),
            ]);
        }

        return $paymentHistory->id;
	}

    public function savePaymentHistoryDetailsIos($request, $amount, $vat, $transactionId)
    {
        if (auth()->check()) {
            $user = Auth::user();
        } else {
            $user = $this->user;
        }

        $paymentExists = PaymentHistoryDetail::where('name', 'Braintree Transaction ID')
                ->where('info', $transactionId)
                ->first();
        
        if ($paymentExists) {
            return false;
        }

        $paymentHistory = $user->payment_history()->create([
            'payment_method_id' => $this->paymentMethodId, // in app purchase
            'amount' => $amount,
            'vat' => $vat
        ]);

        $billingDetail = $user->billing_detail;
        if ( ! $billingDetail ) {
            $billingDetail = BillingDetail::create([
                'user_id' => $user->id,
            ]);
        }
        $billingDetail->firstname = $request->input('firstname');
        $billingDetail->lastname = $request->input('lastname');
        $billingDetail->street_address = $request->input('street_address');
        $billingDetail->postal_code = $request->input('postal_code');
        $billingDetail->country_dial_code_id = ($request->input('country_dial_code_id') ?: NULL);
        $billingDetail->save();

        // If billing details is provided
        if ( ! empty($billingDetail) ) {

            $paymentHistory->payment_history_details()->saveMany([
                new PaymentHistoryDetail(['name' => 'Braintree Transaction ID', 'info' => ($transactionId ?: '')]),
                new PaymentHistoryDetail(['name' => 'Name', 'info' => '']),
                new PaymentHistoryDetail(['name' => 'Street Address', 'info' => '']),
                new PaymentHistoryDetail(['name' => 'Postal Code', 'info' => '']),
                new PaymentHistoryDetail(['name' => 'Country', 'info' => '']),
            ]);
        }

        return true;
    }

	public function PaymentHistoryDetail()
	{

	}

    public function getConversion($amount, $fromCurrency = 'EUR', $toCurrency = 'USD')
    {
        $cost = @ExchangeRate::getEquivalentRate($fromCurrency, $amount, [$toCurrency]);
        return $cost[0]['converted_rate'];
    }

    public function updateBraintreeCustomer($user)
    {
        $btCustomerId = $user->braintree_customer->customer_id;
        $customer = Braintree_Customer::find($btCustomerId);
        $name = explode(' ', $user->name);
        Braintree_Customer::update(
            $btCustomerId,
            [
                'email' => $user->email,
                'firstName' => @$name[0],
                'lastName' => @$name[1],
            ]
        );

        return $btCustomerId;
    }

    public function setMerchantAccount($currency, $braintreeTransaction)
    {
        if ( $currency == 'USD') {
            $braintreeTransaction['merchantAccountId'] = env('BRAINTREE_MERCHANT_ACCOUNT_USD', 'intergointeractiveUSD');
        }
        
        if ( $currency == 'GBP') {
            $braintreeTransaction['merchantAccountId'] = env('BRAINTREE_MERCHANT_ACCOUNT_GBP', 'intergointeractiveGBP');
        }

        return $braintreeTransaction;
    }

    public function getBraintreeCustomerId($user)
    {
        if ($user->braintree_customer) {

            $btCustomerId = $this->updateBraintreeCustomer($user);

            $this->transactionOptions = ['submitForSettlement' => TRUE];

        } else {
            $btCustomerId = $this->createBraintreeCustomer($user);

            $this->transactionOptions = [
                'storeInVaultOnSuccess' => TRUE,
                'submitForSettlement' => TRUE,
            ];
        }

        return $btCustomerId;
    }

    public function processPayment($request, $api = false)
    {
        $user = auth()->user();
        $amount = $request->input('amount');

        $agileBilling = [];
        $billingDetail = [];
        // Process billing only when from web site not from API
        if ($api == false) {
            $billingDetail = $this->saveBillingDetails($request);

            $billing = [
                'firstName' => $billingDetail->firstname,
                'lastName' => $billingDetail->lastname,
                'streetAddress' => $billingDetail->street_address,
                'postalCode' => $billingDetail->postal_code,
                'countryCodeAlpha2' => $billingDetail->country_dial_code->a2_code,
                'countryCodeAlpha3' => $billingDetail->country_dial_code->a3_code,
                'country' => $billingDetail->country_dial_code->country,
            ];

            $agileBilling = $billing;
            unset($billing['country']);
        }

        $requestData['currency'] = $request->input('currency', 'EUR');
        $requestData['country_dial_code_id'] = $request->input('country_dial_code_id', 0);
        
        $vat = $this->calculateVat($requestData, $amount);
        $tax_amount = $vat['tax_amount'];
        $pay_amount = $vat['pay_amount'];
        $vatAmount = $vat['vat'];

        if ($this->user->id) {
            $user = $this->user;
            $billingDetail = $user->billing_detail;
        }

        $btCustomerId = $this->getBraintreeCustomerId($user);

        $braintreeTransaction = [
            'amount' => $pay_amount,
            'customerId' => $btCustomerId,
            'paymentMethodNonce' => $request->input('payment_method_nonce'),
            'options' => $this->transactionOptions,
            'taxAmount' => $tax_amount,
        ];

        // Add billing if not from API
        if ($api == false) {
            // If we have save_billing_details and we dont want to save it 
            if ($request->has('save_billing_details') and ! $request->save_billing_details) {
                
            } else {
                $braintreeTransaction['billing'] = $billing;
            }
        }
        
        // Use the dollar account merchant account id
        $braintreeTransaction = $this->setMerchantAccount($request->input('currency', 'EUR'), $braintreeTransaction);
        $sale = Braintree_Transaction::sale($braintreeTransaction);

        if ($sale->success) {
            $topup_amount = $sale->transaction->amount;

            // Trigger FLAG for high amount transactions
            $addCashBalance = $amount;
            $paymentStatus = 'authorized';
            if ($amount > 50) {
                $addCashBalance = 0;
                $addCashBalance = 0.01;
                $this->paymentStatus = 'pending';
            }

            if ($request->input('amount_type') == 'one-time') {
                $user->cash_balance += $request->input('fax_cost');
                $user->currency = $request->input('currency', 'EUR');
            } else {
                $user->cash_balance += $addCashBalance;
                $user->currency = $request->input('currency', 'EUR');
            }
            if ($user->save()) {
                $user->braintree_customer()->firstOrCreate(['customer_id' => $sale->transaction->customerDetails->id]);
                
                $this->setPaymentInstrumentType($sale->transaction->paymentInstrumentType);
                $paymentHistoryId = $this->savePaymentHistoryDetails($billingDetail, $amount, $vat, $sale->transaction->id);
                $topupDetails['amount'] = $amount;
                $topupDetails['payment_method'] = $this->getPaymentMethod('name');
                $tax_amount = 0;
                if ($vatAmount) {
                    $tax_amount = round((($vatAmount/100) * $amount), 2);
                }

                $topupDetails['conversion_value'] = $amount + $tax_amount;
                $topupDetails['transaction_id'] = $sale->transaction->id;
                $topupDetails['payment_history_id'] = $paymentHistoryId;

                // Update agile data if no billing details yet
                $agileBilling['paymentMethod'] = $topupDetails['payment_method'];
                return [
                    'success' => true,
                    'agile_id' => $user->agile_id,
                    'sale' => $sale,
                    'agile_billing' => $agileBilling,
                    'topup_details' => $topupDetails,
                ];
            }
        } else {
            return [
                'success' => false,
                'agile_id' => $user->agile_id, 
                'sale' => $sale,
            ];
        }
    }

    public function setAutoRecharge($request, $api = false)
    {
        $user = auth()->user();
        $amount = $request->input('amount');

        $btCustomerId = $this->getBraintreeCustomerId($user);

        $paymentMethodNonce = $request->input('payment_method_nonce');

        $paymentMethod = Braintree_PaymentMethod::create([
            'customerId' => $btCustomerId,
            'paymentMethodNonce' => $paymentMethodNonce
        ]);

        if ($paymentMethod->success) {
            $this->token = $paymentMethod->paymentMethod->token;
            $request->request->add([
                    'auto_recharge_amount' => $amount, 
                    'currency' => $request->input('currency', 'EUR'),
                    'payment_method_token'=> $this->token
            ]);
            $this->saveBillingDetails($request);
            return [
                'message_title' => trans('messages.info.thank_you'),
                'message_content' => trans('messages.info.successfully_set_recharge'),
            ];
        }

        return false;
    }

    public function processSubscriptionPayment($request)
    {
        $billingDetail = $this->saveBillingDetails($request);
        
        $billing = [
            'firstName' => $billingDetail->firstname,
            'lastName' => $billingDetail->lastname,
            'streetAddress' => $billingDetail->street_address,
            'postalCode' => $billingDetail->postal_code,
            'countryCodeAlpha2' => $billingDetail->country_dial_code->a2_code,
            'countryCodeAlpha3' => $billingDetail->country_dial_code->a3_code,
            'country' => $billingDetail->country_dial_code->country,
        ];

        $agileBilling = $billing;

        $amount = session('contract_amount');
        // If we have porting fee we add it to the amount
        if (session('porting_fee')) {
            $amount += session('porting_fee');
        }
        // If we have more than 1 number ordered on porting
        if (session('count_numbers') and session('count_numbers') > 1) {
            $amount = $amount * session('count_numbers');
        }
        
        if ($request->pay_balance) {
            $amountArray = config('voxbone.amount_array');
            $amount = $amountArray[request('contractType')];
        }

         // Get VAT if exist
        $requestData['currency'] = $request->input('currency');
        $requestData['country_dial_code_id'] = $request->input('country_dial_code_id');
        $vat = $this->calculateVat($requestData, $amount);
        $tax_amount = $vat['tax_amount'];
        $pay_amount = $vat['pay_amount'];
        $vatAmount = $vat['vat'];
        session(['vat' => $vatAmount]);

        $planId = session('contractType', '3m');
        
        // If user pay using balance
        if ($request->pay_balance and session('pay_balance') and session('re-subscribe')) {
            return $this->payUsingBalance(auth()->user(), $amount, $vat, session('re-subscribe-id'));
        }

        $customer = $this->brainTreeCustomer($request->input('payment_method_nonce'));

        $onTrial = 1;
        $options = null;

        $firstBillingDate = null;

        // Start immediately
        if (session('re-subscribe')) {
            $onTrial = null;
            
            // If the number is already expired we bill it immediately
            if (session('number-expired')) {
                $options = ['startImmediately' => true];
            } else {
                $number = $this->dids->find(session('re-subscribe-id'));
                $firstBillingDate = Carbon::createFromFormat('Y-m-d', $number->expiration_date);
            }
        }
        $subscription = $this->createSubsription($planId, $request, $pay_amount, $options, $firstBillingDate);
        $paymentInstrumentType = $this->paymentInstrumentType;

        if ($subscription->success) {

            session(['subscription_id' => $subscription->subscription->id]);
            session(['transaction_id' => @$subscription->subscription->transactions[0]->id]);
            
            $user = auth()->user();
            // We get or create braintree customer in db
            $user->braintree_customer()->firstOrCreate(['customer_id' => $this->customerId]);

            // Save the payments details
            $this->savePaymentHistoryDetails(
                    $billingDetail,
                    $amount,
                    $vat,
                    session('transaction_id'),
                    $onTrial,
                    session('subscription_id')
            );

            session('contract_amount', $pay_amount);

            // Update agile data if no billing details yet
            $agileBilling['paymentMethod'] = $this->getPaymentMethod('name');
            $this->agile->update(auth()->user()->agile_id, $agileBilling);

            $details = [];

            if (session('re-subscribe')) {
                
                // Activate the number
                $number = $this->dids->find(session('re-subscribe-id'));

                // We need to cancel the current subscription first
                $this->cancelSubscription($number->subscription_id);

                $expirationDate = getExpirationDate($number->contract_type);

                if (session('number-expired')) {
                    $expirationDate = getExpirationDate($number->contract_type);
                } else {
                    $expirationDate = $number->expiration_date;
                }

                $number->subscription_id = session('subscription_id');
                $number->status = 1;
                $number->payment_failed = null;
                $number->expiration_date = $expirationDate;
                $number->save();

                Subscription::createSubscription($number);

                // Enable all files in users' inbox
                $this->inbox->where('did_id', $number->did)->update(['did_status' => 1, 'payment_failed' => null]);

                $data['reactivate'] = true;
                return $data;
            }
            
            return $details;
        }

        return ['subscription' => $subscription];
    }

    public function retryCharge($subscriptionId)
    {
        $retryResult = Braintree_Subscription::retryCharge($subscriptionId);
        if ($retryResult->success) {
            $result = Braintree_Transaction::submitForSettlement(
                $retryResult->transaction->id
            );
            if ($result->success ) {
                return true;
            }

            return $result->message;
        }

        return $retryResult->message;
    }

    public static function useCurrency($currency = 'eur', $symbol = '€')
    {

    }
    /*
    * Retrieves all creditcards of the customer
    * @param $request
    * @return Json
    */
    public function getCustomerCreditCards($customer_id)
    {
         $customer = Braintree_Customer::find($customer_id);
          return $customer->creditCards;
    }


    /*
    * Updates the credit card details of the customer
    * @param $request
    * @return bool
    */
    public function updateCreditCard($request)
    {
        
        $data = explode('/', $request->input('expiration'));
       
        $result = Braintree_CreditCard::update(
                           $request->input('card-token'),
                          [ 
                            'billingAddress' => [
                                'postalCode' =>  $request->input('postal-code'),
                                'options' => [
                                    'updateExisting' => true,

                                ]
                            ],
                            'options' => [
                                'verifyCard' => false,
                            ],
                            'expirationMonth' =>  $data[0],
                            'expirationYear' =>  $data[1]
                        ]); 
         if($result->success)
               return "success";


         return "error";
       
    }

    /*
    * Gets the credit card details given the subscription id
    * @param $request
    * @return Json
    */
    public function getCardDetails($request)
    {
        $result = [];
        $result['card_token'] =  Braintree_Subscription::find($request->input('id'))->paymentMethodToken;
        $data = Braintree_CreditCard::find($result['card_token'] );
        $result['expirationMonth'] = $data->expirationMonth;
        $result['expirationYear'] = $data->expirationYear;
        $result['postalCode'] = $data->billingAddress->postalCode;
        $result['last4'] = $data->last4;
        return $result;
       
    }


    /*
     * Get Expiring card in Next 30 days
    */
    public function getExpiryCardCustomers()
    {
        $dt = Carbon::today();

        $now = Carbon::today();

        $end_date = $dt->addDays(30);

        // Get Creadit cards Expiring in next 30 Days
        $result = Braintree_CreditCard::expiringBetween(mktime(0,0,0,$now->month,$now->day,$now->year),mktime(0,0,0,$end_date->month,$end_date->day,$end_date->year));

      
        $BCIds = [];
        foreach ($result as  $value) {
            
            $maxdays = date('t',mktime(0,0,0,$value->expirationMonth,1,$value->expirationYear));
            $end_date->day       = $maxdays;
            $end_date->month     = $value->expirationMonth;
            $length              = $end_date->diffInDays($now);

            $BCIds[$value->customerId] =  [
                                          'expire_in' => $length,
                                          'token' => $value->token,
                                        ];

        }
        return $BCIds;
    }
    
    
}
