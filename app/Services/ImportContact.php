<?php 

namespace App\Services;

use GuzzleHttp\Client;
use Excel;
use App\Services\ContactService;
use App\Contact;
use Mail;
use Storage;

class ImportContact {

    public $fileLocation;
    protected $client;
    protected $excel;
    
    protected $contactService;

    public function __construct(ContactService $contactService)
    {
        $this->contactService = $contactService;
    }
    
    public function setRequest($request)
    {
        $this->request = $request;
    }

    public function process($request, $user)
    {  
        $count = null;

        $filePath = $request->file_name->store('contacts');

        //$file = 'storage/app/' . $this->fileLocation ;

        dispatch(new \App\Jobs\ContactCsvImport($filePath,$user,$request->list_id, $this->contactService));

        // Return number of phone numbers imported
        return true;
    }
}