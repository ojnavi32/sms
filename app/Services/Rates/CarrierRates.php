<?php

namespace App\Services\Rates;

use Carbon\Carbon;

use App\Models\Rate;
use App\Models\ProviderRate;

class CarrierRates 
{
	public static function rates($request)
	{
		$providerId = 0;
        $country = 0;
        $id = 0;
        

        if($request->provider_id)
            $providerId = $request->provider_id;
        if($request->country){
            $country = $request->country;
            $rateData = Rate::where('name',$country)->first();
            $id = $rateData->id;
        }   

        $rate = ProviderRate:: with('rate','provider')->when($providerId, function ($query) use ($providerId) {
                              return $query->where('provider_id', $providerId);
                    })->when($country, function ($query) use ($id) {
                              return $query->where('rate_id', $id);
                    })->paginate(10);
        return $rate;
	}
}