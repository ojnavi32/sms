<?php 

namespace App\Services;

use App\Services\CountryInfo;
use App\Models\Rate;
use DB, Cache;

class RateCalculator1 {

    public $rate;
    public $rate_per_page;
    public $pages;
    public $cost;

    public function setRate($rate)
    {
        $this->rate = $rate;
    }

    public function getRateByPrefix($rates, $faxnum)
    {
        $strlen = strlen($faxnum);
        $num_prefix = NULL;
        $use_rate = NULL;
        for( $i = 0; $i <= $strlen; $i++ ) {
            $num_prefix = $num_prefix . substr( $faxnum, $i, 1 );
            if (isset($rates[$num_prefix])) {
                $use_rate = $rates[$num_prefix];
            }
            if ($i === 5) break;
        }
        if ( ! $use_rate ) {
            if (isset($rates[0])) {
                $use_rate = $rates[0];
            }
        }
        $this->rate = $use_rate;
        return $this->rate;
    }


    public function getRatePerPage()
    {
        @$this->rate_per_page = $this->rate->base_rate * $this->rate->multiplier;
        return $this->rate_per_page;
    }

    public function getTotalCost($doc = NULL)
    {
        $pages = NULL;
        if ($doc) {
            $pages = $doc->total_pages;
        }
        $ratePerPage = $this->rate_per_page ?: $this->getRatePerPage();
        $this->pages = $pages ?: $this->pages;
        $totalCost = ($ratePerPage * 100) * ($this->pages ?: 1);
        $totalCost = (floor($totalCost)/100);
        
        // 50% more cost if fine resolution
        if ($doc) {
            if ($doc->resolution == 'fine') {
                $totalCost = $totalCost * 1.5;
            }
        }
        
        $totalCost = ($totalCost < config('payment_gateway.minimum_fax_cost')) ? config('payment_gateway.minimum_fax_cost') : $totalCost;
        return $totalCost;
    }

    public function getCost($faxNumber, $doc = null, $api = false)
    {
        if (numberIsBanned($faxNumber)) {
            abort(404);
        }
        $countryInfo = new CountryInfo($faxNumber);
        if ( ! $countryInfo->hasCountryRates()) {
            abort(404);
        }
        $this->rate = $countryInfo->rateToUseForFaxNum();
        $this->isApi($api);
        $this->cost = $this->getTotalCost($doc);
        return $this->cost;
    }

    public function getRateId()
    {
        $this->rate->id;
    }

    public function isApi($api)
    {
        if ($api) {
            $this->min_cost = 0;
            // For API price adjustment
            $adjustment = auth()->user()->user_api_rate_adjustments()->whereRateId($this->rate->id)->first();
            if ($adjustment) {
                $this->rate->base_rate = $adjustment->base_rate;
                $this->rate->multiplier = $adjustment->multiplier;
            }
        }
    }

    public function destinations()
    {
        $repositionSlugs = [
            0 => 'united-states',
            7 => 'guam',
        ];

        $destinationsPositioned = Rate::leftJoin('country_dial_codes', 'rates.country_dial_code_id', '=', 'country_dial_codes.id')
                ->whereIn('country_dial_codes.slug', $repositionSlugs)
                ->groupBy('country_dial_codes.id')
                ->orderBy(DB::raw("FIELD(`country_dial_codes`.`slug`, '".implode("', '",$repositionSlugs)."')"))
                ->get(['rates.*', 'country_dial_codes.*', DB::raw("MIN(`base_rate`) AS `base_rate`")])
                ->keyBy('slug');

        $destinationsWoPriority = Rate::leftJoin('country_dial_codes', 'rates.country_dial_code_id', '=', 'country_dial_codes.id')
                ->whereNotIn('country_dial_codes.slug', $repositionSlugs)
                ->groupBy('country_dial_codes.id')
                ->orderBy('dial_code')
                ->get(['rates.*', 'country_dial_codes.*', DB::raw("MIN(`base_rate`) AS `base_rate`")]);

        return [
            'meta_title' => 'Destinations - Pay as you go pricing - Fax Online',
            'reposition_slugs' => $repositionSlugs,
            'list_count' => 0,
            'destinations_positioned' => $destinationsPositioned,
            'destinations' => $destinationsWoPriority,
            'destinations_priority_count' => $destinationsPositioned->count(),
            'destinations_wo_priority_count' => $destinationsWoPriority->count(),
        ];
    }
}
