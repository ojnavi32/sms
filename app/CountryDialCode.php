<?php 

namespace App;

use Illuminate\Database\Eloquent\Model;

class CountryDialCode extends Model {

    protected $table = 'country_dial_codes';

	protected $fillable = ['country', 'a2_code', 'a3_code', 'dial_code', 'exchange_rate_id'];

	public function rates()
    {
        return $this->hasMany('App\Models\Rate');
    }

	public function exchange_rate()
    {
        return $this->belongsTo('App\Models\ExchangeRate');
    }


    public function getCountryCodeAttribute()
    {
        return $this->attributes['country'] . ' (' . $this->attributes['a3_code'] . ') +'  . $this->attributes['dial_code'];
    }


}
