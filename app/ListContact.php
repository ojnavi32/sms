<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Scopes\LimitScope;

class ListContact extends Model
{
    use SoftDeletes;
    
    protected $table = 'lists_contacts';
    
    protected $fillable = [
        'user_id', 'list_id', 'contact_id', 'phone',
    ];
    
    /**
     * The "booting" method of the model.
     *
     * @return void
     */
    // protected static function boot()
    // {
    //     parent::boot();

    //     static::addGlobalScope(new LimitScope);
    // }
    
    public function user()
    {
    	return $this->belongsTo(User::class);
    }
    
    public function list()
    {
    	return $this->belongsTo(Lists::class);
    }
    
    public function contact()
    {
    	return $this->belongsTo(Contact::class);
    }

}
