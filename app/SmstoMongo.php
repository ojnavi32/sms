<?php

namespace App;
use MongoDB;

class SmstoMongo
{
    private $connection;

    /**
     * SmstoMongo constructor.
     */
    public function __construct()
    {
        $this->connection = (new MongoDB\Client(env('MONGODB_HOST', 'mongodb://localhost/'), []));
    }

    /**
     * @param string $db
     * @return MongoDB\Database
     */
    public function db($db = "smsto")
    {
        return $this->connection->{$db};
    }

    /**
     * @param $name
     * @param $arguments
     * @return MongoDB\Database
     */
    public function __call($name, $arguments)
    {
        return $this->db($name)->withOptions(...$arguments);
    }
}