<?php 

namespace App\Facades;

use Illuminate\Support\Facades\Facade;

class SmsBraintreeFacade extends Facade {

    protected static function getFacadeAccessor() { return 'SmsBraintreeFacade'; }
}