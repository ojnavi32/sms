<?php

namespace App\Transformers;

use League\Fractal\TransformerAbstract;
use App\ContactList;

class ContactListTransformer extends TransformerAbstract
{
    public function transform(ContactList $rows)
    {
        return [
            'name' => $rows->name,
            'email' => $rows->email,
            'created_at' => transformer_date_format($rows->created_at),
            // 'created_at' => date('Y-m-d', strtotime($rows->created_at))
        ];
    }
}