<?php

namespace App\Transformers;

use League\Fractal\TransformerAbstract;
use App\Models\SmsBroadcast;

class SmsBroadcastTransformer extends TransformerAbstract
{
    public function transform(SmsBroadcast $rows)
    {
        return [
            'id' => $rows->id,
            'user_id' => $rows->user_id,
            'list' => $rows->broadcastLists(),
            'status' => $rows->status,
            'error_reason' => $rows->status == 'error' ? 'Insufficient Fund' : '',
            'message' => $rows->message,
            'template_id' => $rows->template_id,
            'delivered' => $rows->delivered_messages_count,
            'failed' => $rows->failed_messages_count,
            'total_cost' => round($rows->total_cost, 4),
            'total_messages' => $rows->total_messages,
            'created_at' => transformer_date_format($rows->created_at),
        ];
    }
}