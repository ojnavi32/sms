<?php

namespace App\Transformers;

use League\Fractal\TransformerAbstract;
use App\Models\NotificationSetting;

class NotificationSettingTransformer extends TransformerAbstract
{
    public function transform(NotificationSetting $rows)
    {
        return [
            'id' => $rows->id,
            'user_id' => $rows->user_id,
            'key' => $rows->key,
            // 'messages' => $rows->messages,
            'created_at' => transformer_date_format($rows->created_at),
        ];
    }
}