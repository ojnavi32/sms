<?php

namespace App\Transformers;

use League\Fractal\TransformerAbstract;
use App\Models\SmsBroadcastMessage;

class BroadcastMessageTransformer extends TransformerAbstract
{
    public function transform(SmsBroadcastMessage $rows)
    {
        return [
            'id' => $rows->id,
            'phone_number' => $rows->send_to,
            'status' => $rows->status,
            'name' => $rows->contactName(),
            'date' => transformer_date_format($rows->created_at),
        ];
    }
}