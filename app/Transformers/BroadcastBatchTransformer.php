<?php

namespace App\Transformers;

use League\Fractal\TransformerAbstract;
use App\Models\SmsBroadcastBatch;

class BroadcastBatchTransformer extends TransformerAbstract
{
    public function transform(SmsBroadcastBatch $rows)
    {
        return [
            'id' => $rows->id,
            'sms_broadcast_id' => $rows->sms_broadcast_id,
            'status' => $rows->status,
            'message' => $rows->message,
            'time_run' => $rows->time_run,
            'created_at' => transformer_date_format($rows->created_at),
        ];
    }
}