<?php

namespace App\Transformers;

use League\Fractal\TransformerAbstract;
use App\Models\Template;

class TemplateTransformer extends TransformerAbstract
{
    public function transform(Template $rows)
    {
        return [
            'id' => $rows->id,
            'user_id' => $rows->user_id,
            'name' => $rows->name,
            'messages' => $rows->messages,
            'created_at' => transformer_date_format($rows->created_at),
        ];
    }
}