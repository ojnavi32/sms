<?php

namespace App\Transformers;

use League\Fractal\TransformerAbstract;
use App\Lists;

class ListTransformer extends TransformerAbstract
{
    public function transform(Lists $rows)
    {
        return [
            'id' => $rows->id,
            'user_id' => $rows->user_id,
            'name' => $rows->name,
            'number_contacts' => $rows->number_contacts,
            'favorite' => $rows->favorite,
            'is_importing' => $rows->is_importing,
            'description' => $rows->description,
            'created_at' => transformer_date_format($rows->created_at),
        ];
    }
}
