<?php

namespace App\Transformers;

use League\Fractal\TransformerAbstract;
use App\Models\Payment;

class PaymentTransformer extends TransformerAbstract
{
    public function transform(Payment $rows)
    {
        return [
            'id' => $rows->id,
            'date' => transformer_date_format($rows->created_at),
            'amount' => $rows->amount,
            'user' => $rows->user->name,
            'payment_method' => isset($rows->paymentMethod->name)?$rows->paymentMethod->name:'',
            'invoice_number' => $rows->id,
            'vat' => $rows->vat,
        ];
    }
}