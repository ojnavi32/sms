<?php

namespace App\Transformers;

use League\Fractal\TransformerAbstract;
use App\Models\UserSession;

class UserSessionTransformer extends TransformerAbstract
{
    public function transform(UserSession $rows)
    {
        return [
            'id' => $rows->id,
            'user_id' => $rows->user_id,
            'browser' => $rows->browser,
            'location' => $rows->location,
            'recent_activity' => $rows->recent_activity,
            'created_at' => transformer_date_format($rows->created_at),
        ];
    }
}