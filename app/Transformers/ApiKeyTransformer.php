<?php

namespace App\Transformers;

use League\Fractal\TransformerAbstract;
use App\Models\ApiKey;

class ApiKeyTransformer extends TransformerAbstract
{
    public function transform(ApiKey $rows)
    {
        return [
            'id' => $rows->id,
            'user_id' => $rows->user_id,
            'description' => $rows->description,
            'mode' => $rows->mode,
            'status' => $rows->status,
            'access_key' => $rows->access_key,
            'created_at' => transformer_date_format($rows->created_at),
        ];
    }
}