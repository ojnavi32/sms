<?php

namespace App\Transformers;

use League\Fractal\TransformerAbstract;
use App\Contact;

class ContactTransformer extends TransformerAbstract
{
    public function transform(Contact $rows)
    {
        return [
            'id' => $rows->id,
            'user_id' => $rows->user_id,
            'name' => $rows->name,
            'surname' => $rows->surname,
            'email' => $rows->email,
            'phone' => $rows->phone,
            'photo' => $rows->photo,
            'country_code' => $rows->country_code,
            'network_name' => $rows->network_name,
            'lists' => $rows->activeLists,
            'created_at' => transformer_date_format($rows->created_at),
        ];
    }
}
