<?php

namespace App\Transformers;

use League\Fractal\TransformerAbstract;
use App\Models\UserPaymentMethod;

class PaymentMethodTransformer extends TransformerAbstract
{
    public function transform(UserPaymentMethod $rows)
    {
        return [
            'id' => $rows->id,
            'user_id' => $rows->user_id,
            'type' => $rows->type,
            'ending' => $rows->ending,
            'expiration_date' => $rows->expiration_date,
            'status' => $rows->status,
            'auto_recharge' => $rows->auto_recharge,
            'created_at' => transformer_date_format($rows->created_at),
        ];
    }
}