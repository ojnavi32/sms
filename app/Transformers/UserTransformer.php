<?php

namespace App\Transformers;

use League\Fractal\TransformerAbstract;
use App\User;

class UserTransformer extends TransformerAbstract
{
    public function transform(User $rows)
    {
        return [
            'id' => $rows->id,
            'name' => $rows->name,
            'business_name' => $rows->business_name,
            'first_name' => $rows->first_name,
            'last_name' => $rows->last_name,
            'surname' => $rows->surname,
            'username' => $rows->username,
            'email' => $rows->email,
            'email_domain' => $rows->email_domain,
            'avatar' => $rows->getAvatarUrlAttribute(),
            'avatar_url' => $rows->getAvatarUrlAttribute(),
            'company_name' => $rows->company_name,
            'phone' => $rows->phone,
            'google' => $rows->google,
            'twitter' => $rows->twitter,
            'facebook' => $rows->facebook,
            'cash_balance' => $rows->cash_balance,
            'credit_balance' => $rows->credit_balance,
            'ip_address' => $rows->ip_address,
            'last_login_time' => $rows->last_login_time,
            'last_ip_address' => $rows->last_ip_address,
            'sms_second' => $rows->sms_second,
            'type' => $rows->type,
            'billing_type' => $rows->billing_type,
            'business_address' => $rows->business_address,
            'business_country' => $rows->business_country,
            'business_postal_code' => $rows->business_postal_code,
            'business_vat' => $rows->business_vat,
            'default_prefix' => $rows->default_prefix,
            'country' => $rows->country,
            'created_at' => transformer_date_format($rows->created_at),
            'updated_at' => transformer_date_format($rows->updated_at),
            'deleted_at' => $rows->deleted_at,
        ];
    }
}