<?php

namespace App\Transformers;

use League\Fractal\TransformerAbstract;
use App\Models\payment;

class BillingTransformer extends TransformerAbstract
{
    public function transform(Payment $rows)
    { 
        return [
            'id' => $rows->id,
            'date' => transformer_date_format($rows->created_at),
            'amount' => $rows->amount,
            'user' => $rows->user->name,
            'payment_method' => isset($rows->paymentMethod->name)?$rows->paymentMethod->name:'',
            'invoice_number' => $rows->id,
            'vat' => $rows->vat,
            'billing_address' => $rows->user->business_address,
            'billing_country' => $rows->user->business_country,
            'billing_postal_code' => $rows->user->business_postal_code,
            'billing_vat' => $rows->user->business_vat,
            'billing_name' => $rows->user->business_name,
        ];
    }
}