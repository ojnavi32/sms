<?php

namespace App\Listeners;

use App\Events\Events\SuccessImportEvent;
use App\Lists;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class SuccessImportListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  SuccessImportEvent  $event
     * @return void
     */
    public function handle(SuccessImportEvent $event)
    {
        $list = Lists::find($event->list_id);
        $list->number_contacts += $event->imported_count;
        $list->save();
    }
}
