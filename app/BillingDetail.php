<?php 

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class BillingDetail extends Model {

	use SoftDeletes;

    protected $table = 'billing_details';

	protected $fillable = ['user_id', 'first_name', 'last_name', 'company', 'vat_number', 'vat_number_validated', 
                'vat_number_tries', 'auto_recharge_amount', 'currency', 'recharge_when_amount', 'payment_method_token',
                 'address', 'postal_code', 'country_dial_code_id'];


	public function user()
    {
        return $this->belongsTo(User::class);
    }

	public function country_dial_code()
    {
        return $this->belongsTo(CountryDialCode::class);
    }

}
