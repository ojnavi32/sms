import './bootstrap';

import 'bootstrap-sass/assets/javascripts/bootstrap';

$(document).ready(function() { 
	$(document).on('click', '.btn-submit', function(e) { 
		e.preventDefault();
		var _token = $('input[name="_token"]').val();
		var provider = $('#provider_select').val();
		var rate_id = $('#rate_id').val();

		$.ajax({ 
			url: '/manager/country-rates/add-providers',
			type: 'POST',
			data: { _token: _token, provider: provider, rate_id: rate_id },
			success: function(data) {
				window.location.reload();
				// alert(data);
				if($.isEmptyObject(data.error)) {
					// alert(data.success);
				} else {
					printErrorMsg(data.error);
				}
			}
		});
	}); 
});

$(document).ready(function() { 
	$(document).on('click', 'a.move-up-btn', function(e) { 
		e.preventDefault();
		$.executeAction('move_up', $(this).data('recId'));
	});
	
});

$.executeAction = function(action, rate_provider_priority_id) { 
	$('#form-action').find('input[name=action]').val(action);
	$('#form-action').find('input[name=rate_provider_priority_id]').val(rate_provider_priority_id);
	$('#form-action').submit();
};


