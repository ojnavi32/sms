

export default {
	template: `
		<a href="#" @click.prevent="() => $emit('click', name)" class="feature" :class="{ 'is-active': isActive }">
			<span class="icon">
				<span :class="{ [icon]: true }"></span>
			</span>
			<span class="detail">
				<span class="name">{{name}}</span>
				<span class="desc">{{description}}</span>
			</span>
			<span class="icon arrow" v-if="isActive">
				<span class="icon-arrow-right"></span>
			</span>
		</a>
	`,
	props: {
		icon: {
			type: String,
			required: true
		},
		name: {
			type: String,
			required: true
		},
		description: {
			type: String
		},
		activeId: {
			type: String
		}
	},

	computed: {
		isActive() {
			return this.name === this.activeId;
		}
	}
};
