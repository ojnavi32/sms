import FeatureComponent from './feature';

export default {
	components: {
		oFeature: FeatureComponent
	},

	template: `
		<div class="features">
			<o-feature

				v-for="feature in content"
				:key="feature.name"

				:icon="feature.icon"
				:name="feature.name"
				:description="feature.description"
				:active-id="activeFeature"
				@click="(f) => $emit('update-active', f)"
			></o-feature>
		</div>
	`,

	props: {
		content: {
			type: Array
		},
		activeFeature: {
			type: String
		}
	}
};
