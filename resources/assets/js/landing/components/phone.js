import MessageGroup from './message-group';

export default {
	components: {
		oMessageGroup: MessageGroup
	},

	template: `
		<div class="phone">
			<div class="screen">
				<div class="screen-header">
					<div class="icon is-large" @click.prevent="() => $emit('go-back')">
						<div class="icon-arrow-left"></div>
					</div>
					<div class="title">{{title}}</div>
				</div>
				<div class="screen-content">
					<div class="inner">
						<div class="messages-container">
							<o-message-group v-for="(message, key) in shownMessages" :key="key" :message="message"></o-message-group>
						</div>
					</div>
				</div>
			</div>
		</div>
	`,

	// <div class="announcement">NEW MESSAGES</div>

	props: {
		messages: {
			type: Array
		}
	},

	computed: {
		title() {
			return _.find(this.messages, 'authorName').authorName;
		}
	},

	data() {
		return { shownMessages: [] };
	},

	mounted() {
		this.addNextMessage();
	},

	watch: {
		messages() {
			this.shownMessages = [];
			if(this.timer) {
				clearTimeout(this.timer);
			}
			this.addNextMessage();
		}
	},

	methods: {
		addNextMessage() {
			this.shownMessages = _.slice(this.messages, 0, this.shownMessages.length + 1);
			this.timer = _.delay(() => {
				if(!this || !this.messages || !this.messages.length) {
					return;
				}
				if(this.shownMessages.length === this.messages.length) {
					this.$emit('messages-ended', this.messages);
					return;
				}
				this.addNextMessage();
			}, 3000);
		}
	}
};