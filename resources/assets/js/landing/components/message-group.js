
export default {
	template: `
		<transition name="fadeUp">
			<div class="message-group" :class="{ 'is-received': message.authorAvatar || message.authorName, 'is-important': message.important }">
				<div class="message-content">
					<div class="author-avatar" v-if="message.authorAvatar">
						<div v-if="message.authorAvatar.indexOf('http') === -1">{{message.authorAvatar}}</div>
						<img v-else :src="message.authorAvatar" alt="" />
					</div>
					<div class="messages" v-if="message.messages">
						<div v-for="msg in message.messages">{{msg}}</div>
					</div>
				</div>
				<div class="author-name" v-if="message.authorName">{{message.authorName}}</div>
			</div>
		</transition>
	`,

	props: {
		message: {
			type: Object,
			required: true
		}
	}
};
