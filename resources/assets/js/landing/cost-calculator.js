

import allCountries from '../app/utils/all-countries';

export default {
	el: '#cost-calculator',

	name: 'CostCalculator',

	data() {
		return {
			countries: { list: allCountries.list, selected: null },
			amount: 1000,
			singleSMSCost: 0,

			isLoadingCost: null
		};
	},

	computed: {
		isLoading() {
			return this.isLoadingCost;
		},
		selectedCountry() {
			return this.countries.selected;
		},
		costPerSMS() {
			let itemCost = this.singleSMSCost;
			// if(this.amount >= 10000) {
			// 	itemCost = this.singleSMSCost * 0.95;
			// }
			// if(this.amount >= 50000) {
			// 	itemCost = this.singleSMSCost * 0.90;
			// }
			// if(this.amount >= 100000) {
			// 	itemCost = this.singleSMSCost * 0.80;
			// }
			const value = itemCost;

			return (value && !Number.isInteger(value) && value.toFixed) ? value.toFixed(2) : value;
		},
		showContactUs() {
			return this.amount >= 50000;
		}
	},

	watch: {
		selectedCountry(value) {
			this.getCostToCountry(this.selectedCountry);
		}
	},
	
	created() {
		this.getCostToCountry(this.selectedCountry);
	},

	methods: {
		getCostToCountry(country) {
			if(!country) {
		   		this.singleSMSCost = 0;
				return;
			}
			this.isLoadingCost = true;
			axios.get(window.location.origin + '/estimated/' + country).then(response => {
				this.isLoadingCost = null;
				if(!response || !response.data || !response.data.cost_per_sms) {
			   		this.singleSMSCost = 0.08;
					return;
				}
			    this.singleSMSCost = response.data.cost_per_sms;
			}).catch((error) => {
				this.isLoadingCost = null;
		   		this.singleSMSCost = 0;
				throw error;
			});
		}
	}
};
