import FeaturesComponent from './components/features';
import PhoneComponent from './components/phone';

export default {
	el: '#features',

	name: 'Features',

	components: {
		oFeatures: FeaturesComponent,
		oPhone: PhoneComponent
	},

	data: {
		currentFeature: null,
		features: [
			{
				icon: 'icon-paper-plane',
				name: 'Bulk SMS & SMS Marketing',
				description: 'Send an SMS broadcast to your contact list with merge, tracking, and other powerful features',

				messages: [
					{
						authorAvatar: 'PZ',
						authorName: 'Pizza Store',
						messages: [
							'For the next 24 hours you can get 1 Pizza & 1 Pizza for FREE with Extra Pepperoni',
							'Use PIZZASMS Coupon at checkout to redeem. Optout - sms.to/o/8Vs1RS'
						]
					},
					{
						messages: [
							'Ok. Thank you.'
						]
					}
				]
			},
			{
				icon: 'icon-bell',
				name: 'Notification SMS',
				description: 'Send SMS to your customers when they purchase, make an order, or when their Pizza is ready!',
				messages: [
					{
						authorAvatar: 'IF',
						authorName: 'iFixers',
						messages: [
							'Your iMac is now repaired and shiny as new. John from iFixers!'
						]
					},
					{
						messages: [
							'When can I pick it up?'
						]
					},
					{
						authorAvatar: 'IF',
						messages: [
							'Pickup anytime between 09:00 - 17:00 Mon to Friday from our store'
						]
					}
				]
			},
			{
				icon: 'icon-calendar',
				name: 'Appointment Reminders',
				description: 'Schedule SMS for the future and send reminders to your users to avoid no-shows',
				messages: [
					{
						authorAvatar: 'DO',
						authorName: 'DoctorOffice',
						messages: [
							'This is a reminder for your Appointment with Dr.Smooch at 18:00 - 18:30 on Monday.',
							'To reschedule call our office up to 24 hours before.'
						]
					}
				]
			},
			{
				icon: 'icon-key',
				name: 'Two factor verification & One time passwords',
				description: 'Powerful SMS API gateway to enable Two Factor Authentication and OTP SMS passwords',
				messages: [
					{
						authorAvatar: 'SA',
						authorName: 'SMSAuth',
						important: true,
						messages: [
							'Your blockchain wallet authorization code is: 8KD18'
						]
					}
				]
			},
			{
				icon: 'icon-note',
				name: 'Surveys & Feedback Requests',
				description: 'Quickly run SMS Polls, Surveys, and request feedback with 2-way SMS',
				messages: [
					{
						authorAvatar: '61',
						authorName: '60123',
						messages: [
							'From 1 to 10, how happy were you with the service received at MyAwesomeCoffeeShop?'
						]
					},
					{
						messages: [
							'I really liked the service. My girlfriend got her coffee precisely as she wanted it. I am giving a 10.'
						]
					},
					{
						authorAvatar: '61',
						messages: [
							'Thank you! Looking forward to seeing you again at our lovely coffee shop.'
						]
					},
				]
			}
		]
	},

	computed: {
		activeFeature() {
			if(this.currentFeature) {
				return this.currentFeature.name;
			}
		}
	},

	mounted() {
		this.setActiveFeature(this.features[0].name);
	},

	methods: {
		setActiveFeature(id) {
			if(!id) {
				return;
			}
			this.currentFeature = this.features.find((f) => f.name === id);
		},
		goBack() {
			if(!this.currentFeature) {
				this.setActiveFeature(this.features[0].name);
				return;
			}

			const currentIndex = this.features.indexOf(this.currentFeature);

			const prevId = currentIndex - 1 < 0 ? this.features.length - 1 : currentIndex - 1;

			this.setActiveFeature(this.features[prevId].name);
		},
		setNextFeature() {
			if(!this.currentFeature) {
				this.setActiveFeature(this.features[0].name);
				return;
			}

			const currentIndex = this.features.indexOf(this.currentFeature);

			const nextId = currentIndex + 1 >= this.features.length ? 0 : currentIndex + 1;

			this.setActiveFeature(this.features[nextId].name);
		}
	}
};
