
// FOR THE BURGER MENUs ...
$(function() {
	$('body').on('click', '.navbar-burger', function(event) {
		const $el = $(event.target);
		const target = $el.attr('data-target');
		const $target = $('#' + target);

		// Toggle the class on both the "nav-burger" and the "nav-menu"
		$el.toggleClass('is-active');
		$target.toggleClass('is-active');
	});
});


// NOT SURE IF NEEDED ANYMORE .....
$(function() {
	$('a[data-toggle="tab"]').on('click.tab', function(event) {
		var tabHead = $(this);

		if(tabHead.attr('href').length) {
			var content = $(tabHead.attr('href'));
			if(content.length) {
				event.preventDefault();
				content.parent().children().removeClass('is-active');
				content.addClass('is-active');
				tabHead.closest('li').parent().children().removeClass('is-active');
				tabHead.closest('li').addClass('is-active');
			}
		}
	});
	$('a[data-toggle="display"]').on('click.display', function(event) {
		var subject = $(this);

		if(subject.attr('href').length) {
			var content = $(subject.attr('href'));
			if(content.length && content.hasClass('toggleable')) {
				event.preventDefault();
				content.toggleClass('is-hidden');
			}
		}
	});
	$('.toggleable [data-action="hide-container"]').on('click.hide-container', function(event) {
		event.preventDefault();
		$(this).closest('.toggleable').addClass('is-hidden');
	});
});

$('#email-input').change(function() {
	$('.checkboxDiv').show();
	$('.initial-get').hide();
});
