import DirectiveClickOutside from '../directives/click-outside';

export default function(Vue) {
	Vue.directive('click-outside', DirectiveClickOutside);
};