
export default function(Vue) {
	
	// Layout components
	Vue.component('layout-aside', require('../components/layout/aside.vue'));
	Vue.component('layout-app', require('../components/layout/app.vue'));

	Vue.component('navbar-main', require('../components/navbar-main.vue'));
	Vue.component('navbar-user', require('../components/navbar-user.vue'));
	Vue.component('menu-user', require('../components/menu-user.vue'));

	// Components
	Vue.component('o-google-chart', require('../components/google-chart/google-chart.vue'));

	Vue.component('modal-form', require('../components/modal-form.vue'));

	Vue.component('o-form', require('../components/form/form.vue'));
	Vue.component('form-field', require('../components/form/field.vue'));

	Vue.component('o-image-input', require('../components/image-input.vue'));
	Vue.component('o-switch', require('../components/switch.vue'));
	Vue.component('o-checkbox', require('../components/checkbox.vue'));

	Vue.component('o-stats', require('../components/stats.vue'));

	Vue.component('o-list', require('../components/list/list.vue'));

	Vue.component('o-table', require('../components/table/table.vue'));
	Vue.component('o-table-column', require('../components/table/column.vue'));

	Vue.component('o-dropdown', require('../components/dropdown/dropdown.vue'));
	Vue.component('o-dropdown-item', require('../components/dropdown/item.vue'));


	Vue.component('o-text-truncate', require('../components/text-truncate.vue'));
	Vue.component('o-number-beautifier', require('../components/number-beautifier.vue'));

	Vue.component('o-payment-overview', require('../components/payment-overview.vue'));
};
