import Vue from 'vue';
import Vuex from 'vuex';
import createLogger from '../../../../../node_modules/vuex/dist/logger';
import router from '../router';

// import * as actions from './actions';
// import * as getters from './getters';

import session from './modules/session';
import authenticatedUser from './modules/authenticated-user';
import user from './modules/user';
import contacts from './modules/contacts';
import lists from './modules/lists';
import templates from './modules/templates';
import notificationSetting from './modules/notification-setting';
import sms from './modules/sms';
import shortlinks from './modules/shortlinks';
import charts from './modules/charts';
import reports from './modules/reports';
import reportMessages from './modules/report-messages';
import payments from './modules/payments';
import paymentMethods from './modules/payment-methods';
import timezones from './modules/timezones';
import phonePrefixes from './modules/phone-prefixes';
import countries from './modules/countries';

Vue.use(Vuex);

const debug = process.env.NODE_ENV !== 'production';

export default new Vuex.Store({
	strict: debug,
	// actions,
	// getters,

	modules: {
		session,
		authenticatedUser,
		user,
		contacts,
		lists,
		templates,
		notificationSetting,
		sms,
		shortlinks,
		charts,
		reports,
		reportMessages,
		payments,
		paymentMethods,
		timezones,
		phonePrefixes,
		countries
	},

	plugins: debug ? [createLogger()] : []
});