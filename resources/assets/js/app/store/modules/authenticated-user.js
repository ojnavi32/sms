import SF from '../../utils/store-flows';
import apiEntity from '../../utils/entity';
import * as types from '../mutation-types';

const Store = {
	getters: {
		getAuthenticatedUserAvatar(state, getters) {
			const authenticatedUser = getters.singleAuthenticatedUser();

			if(!authenticatedUser || !authenticatedUser.avatar || !_.isString(authenticatedUser.avatar) || authenticatedUser.avatar.indexOf('//') === -1 || authenticatedUser.avatar.indexOf('/avatar/') === -1) {
				return;
			}

			return authenticatedUser.avatar;
		}
	},
	actions: {
		setAuthenticatedUser({ getters, commit }, { data }) {
			commit(types.SET_SINGLE_AUTHENTICATED_USER, { mode: 'get', inProgress: false, data: { ...getters.singleAuthenticatedUser(), ...data}, error: null });
		},
		logout() {
			return apiEntity.createOne('logout', { }).then(() => {
				window.location.href = window.axios.defaults.baseURL.split('/api')[0];
			});
		}
	}
};

const entity = 'authenticated-user';

SF.get(Store, 'getAuthenticatedUser', { entity });

export default Store;
