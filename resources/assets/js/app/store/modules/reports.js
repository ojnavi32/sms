import SF from '../../utils/store-flows';

const Store = { };

const entity = 'report';

SF.getAll(Store, 'getAllReports', { entity });
SF.get(Store, 'getReport', { entity });
SF.getDownloadLinks(Store, 'getReportDownloadLinks', {
	entity,
	statePropertyName: 'singleDownloadLinks'
});

export default Store;
