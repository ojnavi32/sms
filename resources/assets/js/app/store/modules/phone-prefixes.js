import SF from '../../utils/store-flows';

const Store = { };

const entity = 'phone-prefixes';

SF.getAll(Store, 'getAllPhonePrefixes', { entity });

export default Store;
