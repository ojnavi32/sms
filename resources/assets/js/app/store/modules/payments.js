import SF from '../../utils/store-flows';

const Store = { };

const entity = 'payment';

SF.getAll(Store, 'getAllPayments', { entity });
SF.get(Store, 'getPayment', { entity });
SF.getDownloadLinks(Store, 'getPaymentDownloadLinks', {
	entity,
	statePropertyName: 'singleDownloadLinks'
});
export default Store;
