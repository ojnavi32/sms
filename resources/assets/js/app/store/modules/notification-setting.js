import SF from '../../utils/store-flows';

const Store = { };

const entity = 'notification-setting';
const apiPath = 'users/:authenticatedUserId/settings/notifications';

SF.getAll(Store, 'getAllNotificationSettings', { entity, apiPath });
SF.updateSingleProp(Store, 'updateNotificationSettingEnabled', { entity, apiPath });

export default Store;
