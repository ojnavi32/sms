import SF from '../../utils/store-flows';

const Store = { };

const entity = 'timezone';

SF.getAll(Store, 'getAllTimezones', { entity });

export default Store;
