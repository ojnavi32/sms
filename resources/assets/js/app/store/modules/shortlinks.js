import SF from '../../utils/store-flows';

const Store = { };

const entity = 'shortlink';

// SF.getAll(Store, 'getAllShortlinks', { entity });
// SF.get(Store, 'getShortlink', { entity });
SF.add(Store, 'addShortlink', {
	entity,
	apiPath: 'sms/shortlink'
});

export default Store;
