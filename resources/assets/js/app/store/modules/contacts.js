import SF from '../../utils/store-flows';

const Store = { };

const entity = 'contact';

SF.getAll(Store, 'getAllContacts', { entity });
SF.get(Store, 'getContact', { entity });
SF.add(Store, 'addContact', { entity });
SF.update(Store, 'updateContact', { entity });
SF.delete(Store, 'deleteContact', { entity });
SF.updateSingleProp(Store, 'updateContactLists', { entity });
SF.getStats(Store, 'getContactStats', {
	entity,
	defaultState: {
		data: {
			total: 0,
			totalLists: 0
		}
	}
});
SF.import(Store, 'importContact', { entity });

export default Store;
