import SF from '../../utils/store-flows';
import * as types from '../mutation-types';

function defaultNewOne() {
	return {
		useList: 'custom',
		useTemplate: 'custom',

		single_contact: null,
		list_id: null,
		contacts: null, // should be string!

		default_prefix: null,
		
		template_id: null,
		message: '',

		sender_id: null,

		send_immediately: true,
		send_later: null,
		timezone: null
	};
}

let estimationTimer = null;

const Store = {
	actions: {
		updateSMSNewOne({ dispatch, commit, getters }, data) {
			commit(types.SET_SINGLE_SMS, { mode: 'local', data: { ...getters.singleSms(), ...data } });

			if(estimationTimer) {
				clearTimeout(estimationTimer);
			}
			estimationTimer = _.delay(() => { dispatch('getSmsEstimation', getters.singleSms()); }, 500);
		}
	}
};

const entity = 'sms';

SF.get(Store, 'getSms', { entity });
SF.send(Store, 'sendSms', {
	entity,
	defaultState: {
		data: { ...defaultNewOne() }
	}
});
SF.estimate(Store, 'getSmsEstimation', {
	entity,
	defaultState: {
		data: { estimatedCost: 0, totalMessages: 0 }
	}
});
SF.getStats(Store, 'getSmsStats', {
	entity,
	defaultState: {
		data: {
			today: { value: 0 },
			yesterday: { value: 0 },
			last7Days: { value: 0 },
			month: { value: 0 }
		}
	}
});

export default Store;
