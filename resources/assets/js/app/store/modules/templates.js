import SF from '../../utils/store-flows';

const Store = { };

const entity = 'template';

SF.getAll(Store, 'getAllTemplates', { entity });
SF.get(Store, 'getTemplate', { entity });
SF.add(Store, 'addTemplate', { entity });
SF.update(Store, 'updateTemplate', { entity });
SF.delete(Store, 'deleteTemplate', { entity });

export default Store;
