import SF from '../../utils/store-flows';

const Store = { };

const entity = 'countries';

SF.getAll(Store, 'getAllCountries', { entity });

export default Store;
