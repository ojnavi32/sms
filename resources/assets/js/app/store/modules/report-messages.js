import SF from '../../utils/store-flows';

const Store = { };

const entity = 'report-message';

SF.getAll(Store, 'getAllReportMessages', {
	entity,
	apiPath: 'reports-messages/:id'
});

export default Store;
