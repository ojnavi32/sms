import SF from '../../utils/store-flows';
import * as types from '../mutation-types';

function defaultNewOne() {
	return {
		amount: 10,
		paymentMethod: 'bank-transfer',
	};
}

const Store = {
	state: {
		single: {
			data: { ...defaultNewOne() }
		}
	},
	getters: {
		singleAddFunds: (state) => () => state.single.data
	},
	actions: {
		updateAddFundsNewOne({ dispatch, commit, getters }, data) {
			commit(types.SET_SINGLE_PAYMENT_METHOD, { mode: 'local', data: { ...getters.singleAddFunds(), ...data } });
		}
	}
};

const entity = 'payment-method';

SF.addfunds(Store, 'creditCardPaymentMethod', { entity });
SF.addfunds(Store, 'bankTransferPaymentMethod', { entity });

export default Store;
