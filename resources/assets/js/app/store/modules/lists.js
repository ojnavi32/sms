import SF from '../../utils/store-flows';

const Store = { };

const entity = 'list';

SF.getAll(Store, 'getAllLists', { entity });
SF.get(Store, 'getList', { entity });
SF.add(Store, 'addList', { entity });
SF.update(Store, 'updateList', { entity });
SF.delete(Store, 'deleteList', { entity });
SF.updateSingleProp(Store, 'updateListFavorite', { entity });

export default Store;
