import SF from '../../utils/store-flows';

const Store = { };

const entity = 'session';

SF.getAll(Store, 'getAllSessions', { entity });
SF.get(Store, 'getSession', { entity });
SF.delete(Store, 'deleteSession', { entity });
SF.deleteAll(Store, 'deleteAllSessions', { entity });
SF.getStats(Store, 'getSessionStats', {
	entity,
	apiPath: 'user',
	defaultState: { data: { cashBalance: 0, remainingSms: 0 } }
});

export default Store;
