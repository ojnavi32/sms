import SF from '../../utils/store-flows';

const Store = { };

const entity = 'chart';

SF.getAll(Store, 'getSmsReachChart', {
	entity,
	apiPath: 'chart/geochart/sms-reach'
});

SF.getAll(Store, 'getSendingActivityChart', {
	entity,
	apiPath: 'chart/corechart/sending-activity'
});

SF.getAll(Store, 'getCostActivityChart', {
	entity,
	apiPath: 'chart/corechart/cost-activity'
});

export default Store;
