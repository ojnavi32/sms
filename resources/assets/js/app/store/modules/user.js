import SF from '../../utils/store-flows';

const Store = {};

const entity = 'user';

SF.update(Store, 'updateUserPassword', {
	entity,
	apiPath: 'users/:authenticatedUserId/change-password'
});
SF.update(Store, 'updateUserBasic', {
	entity,
	apiPath: 'users/:authenticatedUserId/update',
	onAfterAction: 'setAuthenticatedUser'
});
SF.update(Store, 'updateUserBusiness', {
	entity,
	apiPath: 'users/:authenticatedUserId/billing/update',
	onAfterAction: 'setAuthenticatedUser'
});
SF.update(Store, 'updateUserAvatar', {
	entity,
	apiPath: 'users/avatar',
	onAfterAction: 'setAuthenticatedUser'
});

export default Store;
