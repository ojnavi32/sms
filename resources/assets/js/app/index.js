import router from './router';
import store from './store';
import apiEntity from './utils/entity';
import locationInfo from './utils/location-info';

import accounting from 'accounting';
import VeeValidate, { Validator } from 'vee-validate';
import { isValidNumber } from 'libphonenumber-js';


import bugsnag from 'bugsnag-js';

import Vue from 'vue';
import VueBlu from 'vue-blu';
import bugsnagVue from 'bugsnag-vue';

import VueGtm from 'vue-gtm';


import InitializeRegisterDirectives from './initializers/register-directives';
import InitializeRegisterComponents from './initializers/register-components';


InitializeRegisterDirectives(Vue);
InitializeRegisterComponents(Vue);

Vue.use(VueBlu);


// Accounting
accounting.settings.currency = { // http://openexchangerates.github.io/accounting.js/#methods
	...accounting.settings.currency,
	symbol : '€',
	// format: '%s %v',
	decimal : ',',
	thousand: '.',
	precision : 2
};

// Vee Validate
const VeeValidateConfig = {
	aria: false,
	classNames: {},
	classes: false,
	delay: 0,
	dictionary: null,
	errorBagName: 'validationErrors', // change if property conflicts
	events: 'input|blur',
	fieldsBagName: 'fields',
	i18n: null, // the vue-i18n plugin instance
	i18nRootKey: 'validations', // the nested key under which the validation messages will be located
	inject: true,
	locale: 'en',
	strict: true,
	validity: false
};


const dictionary = {
	en: {
		attributes: {
			single_contact: 'Number'
		}
	}
};

Validator.localize(dictionary);

// Phone Number Validator
Validator.extend('phoneNumber', {
	getMessage: (fieldName, params, data) => {
		if(data.notValid && data.notValid.length > 1) {
			return `Contains ${data.notValid.length} invalid numbers`;
		}
		return `Not a valid phone number`;
	},
	validate: (value, [options]) => {
		const defaultCountry = options && options.localCountry ? options.localCountry.toUpperCase() : null;

		let numbers = value.split('\n');

		const notValid = [];

		numbers = numbers.map((number) => {
			number = _.trim(number);
			if(!number.length) {
				return;
			}
			const valid = defaultCountry ? isValidNumber(number, defaultCountry) : isValidNumber(number);
			if(!valid) {
				notValid.push(number);
			}
			return { valid, number };
		});

		return Promise.resolve({
			valid: !notValid.length,
			data: {
				result: _.compact(numbers),
				notValid: _.compact(notValid)
			}
		});
	}
});

Vue.use(VeeValidate, VeeValidateConfig);


// Main entry point
if(axios.defaults.headers.common['X-CSRF-TOKEN']) {
	
	const isDevelopment = axios.defaults.baseURL && (axios.defaults.baseURL.indexOf(':3000') !== -1 || axios.defaults.baseURL.indexOf(':8000') !== -1);
	if(!isDevelopment) {
		const bugsnagClient = bugsnag('5ab207ef0f8d1459e7df7b084524b250');

		bugsnagClient.use(bugsnagVue(Vue));

		Vue.use(VueGtm, { // https://github.com/mib200/vue-gtm
			enabled: true,
			debug: false,
			vueRouter: router
			// ignoredViews: ['homepage']
		});
	}

	locationInfo();
	
	apiEntity.lookup('authenticated_user', { limit: 1 }).then((response) => {
		const app = new Vue({
			router,
			store,
			el: '#app',

			beforeMount() {
				// console.log(this.$store._modules.root._children.campaigns._rawModule);
				if(response && response.data) {
					this.$store.dispatch('setAuthenticatedUser', { data: response.data.user });
					this.$store.dispatch('getSessionStats');

					// disable this for now. enable once authentication on socket.io server has been fixed
                    // window.Echo.private('user.' + response.data.user.id + '.import')
					 //    .listen('ImportFailedEvent', (e) => {
					 //    	console.log('ImportFailedEvent', e)
					 //        this.$notify.danger({
						// 		title: e.title,
						// 		content: e.message
						// 	});
					 //    })
						// .listen('ImportSuccessEvent', (e) => {
                    //         console.log('ImportSuccessEvent', e)
                    //         this.$notify.success({
                    //             title: e.title,
                    //             content: e.message
                    //         });
						// })
				}
			}
		});
	});
}
