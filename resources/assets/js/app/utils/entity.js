import axios from 'axios';
import moment from 'moment';

function toEntityPath(entity) {
	entity = entity === 'authenticated-user' ? 'authenticated_user' : entity;
	entity = entity.replace('report', 'broadcast');
	return '/' + (entity === 'user' || entity === 'authenticated_user' || entity === 'sms' || entity === 'sms-sent' || entity === 'phone-prefixes' || entity === 'countries' || entity.indexOf('/') !== -1 || entity.indexOf('_') !== -1 || entity === 'logout' ? entity : entity + 's');
}

function cacheKiller() {
	return new Date().getTime();
}

function prepareParams(data = { }) {
	return { ...data, cachekiller: cacheKiller() };
}

function prepareGeoChartResponse(data, filter) {
	return Object.keys(data).map((key) => ([key, data[key]]));
}

function prepareCoreChartResponse(data, filter) {
	return Object.keys(data).map((key) => ([key, data[key]]));
}

function preparePhonePrefixesResponse(data, filter) {
	if(data.data) {
		data.data.forEach((d) => {
			d.id = d.code;
			d.name = d.name + '(' + d.phone + ')';
		});
	}
	return data;
}

function prepareCountriesResponse(data, filter) {
	if(data.data) {
		data.data.forEach((d) => {
			d.id = d.code;
		});
	}
	return data;
}

function prepareSessionsResponse(data, filter) {
	if(data.data) {
		data.data.forEach((item) => {
			const lastActivityAt = item.last_activity || item.recent_activity;
			if(lastActivityAt) {
				item.last_activity_at = moment.unix(lastActivityAt).fromNow();
			}
			const createdAt = item.created_at;
			if(createdAt) {
				item.created_on = moment(item.created_at).format('MMMM Do YYYY');
			}

			let browser = item.browser;
			if(browser) {
				const browser = (item.browser || '').toLowerCase();
				let icon = 'unknown';
				if(browser.indexOf('firefox') !== -1) {
					icon = 'firefox';
				}
				if(browser.indexOf('chrome') !== -1) {
					icon = 'chrome';
				}
				if(browser.indexOf('safari') !== -1) {
					icon = 'safari';
				}
				if(browser.indexOf('edge') !== -1) {
					icon = 'edge';
				}
				if(browser.indexOf('explorer') !== -1) {
					icon = 'internet-explorer';
				}
				if(browser.indexOf('opera') !== -1) {
					icon = 'opera';
				}
				item.browser_icon = icon;
			}
		});
	}
	return data;
}

export default {
	lookup(entity, data) {
		return axios.get(toEntityPath(entity), { params: prepareParams(data) }).then(response => {
			const responseHasData = response.data;
			if(entity.indexOf('geochart') !== -1 && responseHasData) {
				response.data = prepareGeoChartResponse(response.data, data.filter);
			}
			if(entity.indexOf('corechart') !== -1 && responseHasData) {
				response.data = prepareCoreChartResponse(response.data, data.filter);
			}
			if(entity.indexOf('phone-prefixes') !== -1 && responseHasData) {
				response.data = preparePhonePrefixesResponse(response.data, data.filter);
			}
			if(entity.indexOf('countries') !== -1 && responseHasData) {
				response.data = prepareCountriesResponse(response.data, data.filter);
			}
			if(entity.indexOf('session') !== -1 && responseHasData) {
				response.data = prepareSessionsResponse(response.data, data.filter);
			}
			return response;
		}).catch(error => {
			console.log(error);
			throw error;
		});
	},
	lookupOne(entity, id) {
		return axios.get(toEntityPath(entity) + '/' + id, { params: prepareParams() }).then(response => response).catch(error => {
			console.log(error);
			throw error;
		});
	},
	lookupStats(entity, data) {
		return axios.get(toEntityPath(entity) + '/info',  { params: prepareParams(data) }).then(response => response).catch(error => {
			console.log(error);
			throw error;
		});
	},
	lookupDownloadLinksOne(entity, id) {
		const promises = [
			axios.get(toEntityPath(entity) + '/' + id + '/excel/download'),
			axios.get(toEntityPath(entity) + '/' + id + '/csv/download')
		];

		return axios.all(promises).then(axios.spread((excel, csv) => {
			return {
				data: {
					data: {
						id: id,
						excel: excel.data.downloadFile,
						csv: csv.data.downloadFile
					}
				}
			};
		})).catch(error => {
			console.log(error);
			throw error;
		});
	},
	createOne(entity, data, options) {
		if(!data) {
			return;
		}

		if((entity.indexOf('logout') !== -1 || entity.indexOf('services/') !== -1) && !options) {
			options = {
				baseURL: window.axios.defaults.baseURL.replace('/api/v1', '/')
			};
		}

		return axios.post(toEntityPath(entity), data, options).then(response => response).catch(error => {
			console.log(error);
			throw error;
		});
	},
	update(entity, data, options) {
		if(!data) {
			return;
		}
		return axios.post(toEntityPath(entity), data, options).then(response => response).catch(error => {
			console.log(error);
			throw error;
		});
	},
	updateOne(entity, id, data, options) {
		if(!id || !data) {
			return;
		}
		return axios.post(toEntityPath(entity) + '/' + id, data, options).then(response => response).catch(error => {
			console.log(error);
			throw error;
		});
	},
	deleteOne(entity, id) {
		if(!id) {
			return;
		}
		return axios.delete(toEntityPath(entity) + '/' + id).then(response => response).catch(error => {
			console.log(error);
			throw error;
		});
	},
	deleteAll(entity) {
		return axios.delete(toEntityPath(entity) + '/all').then(response => response).catch(error => {
			console.log(error);
			throw error;
		});
	},

	// Specific calls for damn entities .. should not be here ....
	updateOneFavorite(entity, id, data) {
		if(!id || !data) {
			return;
		}
		return axios.post(toEntityPath(entity) + '/' + id + '/favorite', data).then(response => response).catch(error => {
			console.log(error);
			throw error;
		});
	},
	updateOneLists(entity, id, data) {
		if(!id || !data) {
			return;
		}

		const isDeletion = data.lists.length < data.listsCurrent.length;
		if(isDeletion) {
			const toDelete = _.differenceBy(data.listsCurrent, data.lists, 'id');
			return axios.all(_.map(toDelete, (list) => axios.delete('lists/' + list.id + toEntityPath(entity) + '/' + id))).then((responses) => {
				if(_.isArray(responses) && !_.find(responses, (response) => !response.data || !response.data.deleted)) {
					return { data: { updated: true } };
				}
				throw new Error('Could not update lists of contact');
			}).catch(error => {
				console.log(error);
				throw error;
			});
		}
		console.log(toEntityPath(entity) + '/' + id + '/lists')
		return axios.post(toEntityPath(entity) + '/' + id + '/lists', data).then(response => response).catch(error => {
			console.log(error);
			throw error;
		});
	},
	sendOne(entity, data) {
		if(!data) {
			return;
		}
		return axios.post(toEntityPath(entity) + '/send', data).then(response => response).catch(error => {
			console.log(error);
			throw error;
		});
	},
	estimateOne(entity, data) {
		if(!data) {
			return;
		}
		return axios.post(toEntityPath(entity) + '/send/preview', data).then(response => response).catch(error => {
			console.log(error);
			throw error;
		});
	}
};
