
export default {
	isLocalStorageAvailable() {
		const test = 'test';
		try {
			localStorage.setItem(test, test);
			localStorage.removeItem(test);
			return true;
		} catch(e) {
			return false;
		}
	},
	get(key) {
		if(!this.isLocalStorageAvailable()) {
			return false;
		}

		var value = localStorage.getItem(key);

		if(!value) {
			return;
		}

		// assume it is an object that has been stringified
		if(value[0] === '{') {
			value = JSON.parse(value);
		}

		return value;
	},

	set(key, value) {
		if(!this.isLocalStorageAvailable()) {
			return false;
		}
		if(value === null) {
			value = '';
		}

		if(!key) {
			return;
		}

		if(typeof value === 'object') {
			value = JSON.stringify(value);
		}
		localStorage.setItem(key, value);
	},

	remove(key) {
		if(!this.isLocalStorageAvailable()) {
			return false;
		}
		delete localStorage[key];
	}
};
