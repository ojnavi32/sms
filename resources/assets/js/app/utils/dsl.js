
/*
	DSL: .. adapted from Ember Framework :)
	Brings the nice Ember styled route declarations for Vue...

	const routes = DSL.map(function() {
		this.route('index');
		this.route('lists', function() {
			this.route('new');
			this.route('edit', { path: 'edit/:id' });

			this.route('contacts', { path: '/contactsLELEL', component: 'que' }, function() {
				this.route('new');
				this.route('edit', { path: 'edit/:id' });
			});
		});

		this.route('sms', function() {
			this.route('send');
			this.route('history');
		});

		this.route('api', function() {
			this.route('index');
			this.route('data');
		});
	});
*/

class DSL {
	constructor(name, options) {
		this.parent = name;
		this.matches = [];
		this.options = options;
	}

	route(name, options = {}, callback) {
		if(arguments.length === 2 && typeof options === 'function') {
			callback = options;
			options = {};
		}

		if(callback) {
			let fullName = getFullName(this, name);
			let dsl = new DSL(fullName, this.options);

			callback.call(dsl);

			createRoute(this, name, options, dsl);
		} else {
			createRoute(this, name, options);
		}
	}

	push(path, component, redirect, alias, name, children) {
		const route = { name };
		if(path) {
			if(path === '/index' || path === 'index') {
				path = '';
			}
			if(path === '' && name === 'index') {
				path = '/';
			}
			route.path = path;
		}
		if(redirect) {
			route.redirect = redirect;
		}
		if(alias) {
			route.alias = alias;
		}
		if(children && children.length) {
			route.children = children;
		}

		let componentPath = component ? component : name.toLowerCase().replace(/\./g, '/');
		if(typeof componentPath === 'string') {
			componentPath = require('../routes/' + componentPath + '.vue');
		}

		if(componentPath) {
			route.component = componentPath;
		}
		this.matches.push(route);
	}

	generate() {
		return this.matches;
  }
}

export default DSL;

function canNest(dsl) {
	return dsl.parent !== null;
}

function getFullName(dsl, name) {
	if(canNest(dsl)) {
		return `${dsl.parent}.${name}`;
	} else {
		return name;
	}
}

function createRoute(dsl, name, options = {}, childrenDSL) {
	let fullName = getFullName(dsl, options.name || name);

	if(typeof options.path !== 'string') {
		options.path = `/${name}`;
	}

	const pathStartsWithDash = options.path[0] === '/';
	const isRootRoute = dsl.parent === null;

	if(!pathStartsWithDash && isRootRoute) {
		options.path = '/' + options.path;
	}

	if(!isRootRoute && pathStartsWithDash) {
		options.path = options.path.substr(1);
	}


	dsl.push(options.path, options.component, options.redirect, options.alias, fullName, childrenDSL ? childrenDSL.matches : null);
}


DSL.map = callback => {
	let dsl = new DSL(null, {});
	callback.call(dsl);

	return dsl.matches;
};
