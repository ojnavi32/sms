import expandOptions from './expand-options';
import putInModule from './put-in-module';

import receiveFlow from './flow/receive';
import singleFlow from './flow/single';

const flows = {
	single: singleFlow,
	receive: receiveFlow
};

function wrapFlow(flowName, storeMethod, storeModule, actionName, options) {
	options = expandOptions(storeMethod, actionName, options);

	const result = flows[flowName](storeModule, actionName, storeMethod, options);

	if(storeModule) {
		putInModule(storeModule, result);
	}

	return result;
}

export default {
	getAll(storeModule, actionName, options) {
		return wrapFlow('receive', 'getAll', storeModule, actionName, options);
	},
	getStats(storeModule, actionName, options) {
		return wrapFlow('receive', 'getStats', storeModule, actionName, options);
	},
	estimate(storeModule, actionName, options) {
		return wrapFlow('receive', 'estimate', storeModule, actionName, options);
	},
	get(storeModule, actionName, options) {
		return wrapFlow('single', 'get', storeModule, actionName, options);
	},
	getDownloadLinks(storeModule, actionName, options) {
		return wrapFlow('single', 'getDownloadLinks', storeModule, actionName, options);
	},
	add(storeModule, actionName, options) {
		return wrapFlow('single', 'add', storeModule, actionName, options);
	},
	update(storeModule, actionName, options) {
		return wrapFlow('single', 'update', storeModule, actionName, options);
	},
	updateSingleProp(storeModule, actionName, options) {
		return wrapFlow('single', 'updateSingleProp', storeModule, actionName, options);
	},
	delete(storeModule, actionName, options) {
		return wrapFlow('single', 'delete', storeModule, actionName, options);
	},
	deleteAll(storeModule, actionName, options) {
		return wrapFlow('receive', 'deleteAll', storeModule, actionName, options);
	},
	import(storeModule, actionName, options) {
		return wrapFlow('single', 'import', storeModule, actionName, options);
	},
	send(storeModule, actionName, options) {
		return wrapFlow('single', 'send', storeModule, actionName, options);
	},
	addfunds(storeModule, actionName, options) {
		return wrapFlow('single', 'addfunds', storeModule, actionName, options);
	}
};
