
export default function(storeModule, storeFlow) {
	if(!storeModule || !storeFlow) {
		return;
	}

	if(storeFlow.state) {
		if(!storeModule.state) {
			storeModule.state = { };
		}
		storeModule.state = { ...storeModule.state, ...storeFlow.state };
	}
	if(storeFlow.getters) {
		if(!storeModule.getters) {
			storeModule.getters = { };
		}
		storeModule.getters = { ...storeModule.getters, ...storeFlow.getters };
	}
	if(storeFlow.actions) {
		if(!storeModule.actions) {
			storeModule.actions = { };
		}
		storeModule.actions = { ...storeModule.actions, ...storeFlow.actions };
	}
	if(storeFlow.mutations) {
		if(!storeModule.mutations) {
			storeModule.mutations = { };
		}
		storeModule.mutations = { ...storeModule.mutations, ...storeFlow.mutations };
	}
}
