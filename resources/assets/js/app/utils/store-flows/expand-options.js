import _ from 'lodash';

export default function(flowName, actionName, options) {
	const result = { ...options };

	if(!result.entity) {
		result.entity = 'dummy';
	}
	if(!result.apiPath) {
		result.apiPath = result.entity;
	}

	const entity = _.snakeCase(result.entity);

	const flowIsList = ['getAll', 'getStats', 'estimate'].indexOf(flowName) !== -1;

	let statePropertyName = options.statePropertyName ? _.snakeCase(options.statePropertyName) : 'single';
	if(flowIsList) {
		statePropertyName = _.snakeCase(actionName).replace('get_', '').replace('_' + entity + 's', '').replace('_' + entity, '');
		if(statePropertyName.indexOf('_stats') !== -1) {
			statePropertyName = 'stats';
		}
		if(statePropertyName.indexOf('_estimation') !== -1) {
			statePropertyName = 'estimation';
		}
	}

	let mutationName = null;
	if(flowIsList) {
		if(statePropertyName === 'stats') {
			mutationName = _.snakeCase('receive_' + entity + '_stats');
		} else if(statePropertyName === 'estimation') {
			mutationName = _.snakeCase('receive_' + entity + '_estimation');
		} else {
			mutationName = _.snakeCase(flowName).replace('get_', 'receive_') + '_' + statePropertyName + '_' + entity + 's';
			mutationName = mutationName.replace('all_all_', 'all_');
		}
	} else {
		mutationName = 'set_' + statePropertyName + '_' + entity;
	}
	mutationName = mutationName.toUpperCase();


	if(!result.statePropertyName && statePropertyName) {
		result.statePropertyName = statePropertyName;
	}
	
	result.statePropertyName = _.camelCase(statePropertyName);

	if(!result.singlePropName) {
		const parts = _.snakeCase(actionName).split(entity);
		if(parts.length > 1 && parts[1]) {
			result.singlePropName = parts[1].replace('_', '');
		}
	}

	if(!result.onBefore) {
		result.onBefore = mutationName;
	}
	if(!result.onAfter) {
		result.onAfter = mutationName;
	}
	if(!result.onError) {
		result.onError = mutationName;
	}

	if(options.onAfterAction) {
		result.onAfterAction = options.onAfterAction;
	}

	return result;
}
