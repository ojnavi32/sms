import _ from 'lodash';
import Vue from 'vue';

import GenericStore from '../../genericStore';
import * as types from '../../../store/mutation-types';

/*
	Some description of what this flow creates:
		Having 'contact' as the 'entity', this spits out:
			state:
				{
					all: { status: 'loaded', data: [], pagination: {}, error: {} },
				}
			getters:
				- allContacts
				- allContactsStatus
				- allContactsPagination (except for stats and estimation)
				- allContactsError
			actions:
				- [Given ActionName]
			mutations:
				- RECEIVE_ALL_CONTACTS(newState: Object)
					- newState being the { status, data, pagination, error } from the generated state.
*/

export default function(storeModule, actionName, storeMethod, options) {
	const result = {
		state: { },
		getters: { },
		actions: { },
		mutations: { }
	};

	const statePropertyName = options.statePropertyName;
	const isStats = statePropertyName === 'stats';
	const isEstimation = statePropertyName === 'estimation';

	if(options.defaultState) {
		result.state[statePropertyName] = { ...result.state, ...options.defaultState };
	}

	// getters
	const listGetterName = _.camelCase(isStats || isEstimation ? options.entity + '_' + statePropertyName : statePropertyName + '_' + options.entity + 's');

	options.listGetterName = listGetterName;

	if(isStats || isEstimation) {
		result.getters[listGetterName] = (state) => state[statePropertyName] ? state[statePropertyName].data : null;
		result.getters[listGetterName + 'Status'] = (state) => state[statePropertyName] ? state[statePropertyName].status : null;
		result.getters[listGetterName + 'Error'] = (state) => state[statePropertyName] ? state[statePropertyName].error : null;
	} else {
		result.getters[listGetterName] = (state) => state[statePropertyName] ? state[statePropertyName].data : null;
		result.getters[listGetterName + 'IsFirstLoad'] = (state) => state[statePropertyName] ? typeof state[statePropertyName] === 'undefined' : false;
		result.getters[listGetterName + 'Status'] = (state) => state[statePropertyName] ? state[statePropertyName].status : null;
		result.getters[listGetterName + 'Error'] = (state) => state[statePropertyName] ? state[statePropertyName].error : null;
		result.getters[listGetterName + 'Pagination'] = (state) => state[statePropertyName] ? state[statePropertyName].pagination : null;
		result.getters[listGetterName + 'ShouldReload'] = (state) => state[statePropertyName] ? state[statePropertyName].shouldReload: null;
	}
	// actions
	result.actions[actionName] = function({ getters }) {
		if(getters[statePropertyName] && getters[statePropertyName].status === 'loading') {
			return;
		}
		return GenericStore.actions[storeMethod](options, ...arguments);
	};

	// mutations
	const baseMutation = function() {
		return function(state, newState) {
			let result = null;
			if(!newState) {
				result = { status: 'loading' };
			}

			if(newState && newState.data) {
				result = { status: 'loaded', data: newState.data, pagination: newState.pagination };
			}
			
			if(newState && newState.error) {
				result = { status: 'error', error: newState.error };
			}

			// state[statePropertyName] = newState;
			if(result) {
				Vue.set(state, statePropertyName, { ...state[statePropertyName], ...result });
			} else {
				Vue.set(state, statePropertyName, { ...state[statePropertyName] });
			}
		};
	};

	const mutationNames = _.uniq([options.onBefore, options.onAfter, options.onError]);

	mutationNames.forEach((m) => {
		result.mutations[types[m]] = baseMutation();
	});

	return result;
};
