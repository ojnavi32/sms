import _ from 'lodash';
import Vue from 'vue';

import GenericStore from '../../genericStore';
import * as types from '../../../store/mutation-types';

/*
	Some description of what this flow creates:
		Having 'contact' as the 'entity' and 'single' as the state property base, this spits out:
			state:
				{
					single[_id]: {
						mode: 'get|add|import|addfunds|update|delete|error',
						inProgress: true,
						data: {},
						error: {}
					}
				}
			getters:
				- singleContact(id: String)
				- singleContactMode(id: String)
				- singleContactInProgress(id: String)
				- singleContactError(id: String)
			
			actions:
				- [Given ActionName]
			mutations:
				- SET_SINGLE_CONTACT(id: String, newState: Object)
					- newState being the { mode, inProgress, data, error } from the generated state.
					- it also mutates the other properties of the main state based on the given "mode":
						- on mode "delete", it deletes the instance from everywhere else.
						- on mode "update", it updates the updated properties of itself everywhere it is present.
						- and so on...
*/

export default function(storeModule, actionName, storeMethod, options) {
	const result = {
		state: { },
		getters: { },
		actions: { },
		mutations: { }
	};

	const statePropertyName = options.statePropertyName;
	
	// getters
	const listGetterName = _.camelCase(statePropertyName + '_' + options.entity);
	options.listGetterName = listGetterName;

	result.getters[listGetterName] = (state) => (id) => {
		const key = statePropertyName + (id ? '_' + id : '');
		return state[key] ? state[key].data : null;
	};
	result.getters[listGetterName + 'Mode'] = (state) => (id) => {
		const key = statePropertyName + (id ? '_' + id : '');
		return state[key] ? state[key].mode : null;
	};
	result.getters[listGetterName + 'InProgress'] = (state) => (id) => {
		const key = statePropertyName + (id ? '_' + id : '');
		return state[key] ? state[key].inProgress : false;
	};
	result.getters[listGetterName + 'Error'] = (state) => (id) => {
		const key = statePropertyName + (id ? '_' + id : '');
		return state[key] ? state[key].error : null;
	};

	if(options.defaultState && statePropertyName) {
		result.state[statePropertyName] = { ...result.state, ...options.defaultState };
	}


	// actions
	result.actions[actionName] = function({ getters }) {
		// if(getters[statePropertyName] && getters[statePropertyName].status === 'loading') {
		// 	return;
		// }
		return GenericStore.actions[storeMethod](options, ...arguments);
	};

	// mutations
	const baseMutation = function() {
		return function(state, newState) {
			// CONSIDER EXTRA STUFF WHEN DOING DELETE, UPDATE, .. based on the "mode" being passed in ...

			// state should have the 'id' and 'mode' passed in ...

			let result = null;
			if(!newState.data && !newState.error) {
				result = { inProgress: true };
			}

			if(newState && newState.data) {
				result = { inProgress: false, data: newState.data };
			}
			
			if(newState && newState.error) {
				result = { inProgress: false, error: newState.error };
			}

			const stateName = statePropertyName + (newState.id ? '_' + newState.id : '');

			// state[stateName] = newState ? { ...state[stateName], ...newState } : null;
			if(result) {
				Vue.set(state, stateName, { ...state[stateName], ...result });
			} else {
				Vue.set(state, stateName, { ...state[stateName] });
			}


			if(newState && !result.inProgress) {
				if(newState.mode === 'delete' && newState.id) {
					Object.keys(state).forEach((key) => {
						if(key.indexOf(statePropertyName) !== -1) {
							return;
						}
						if(state && key && state[key] && state[key].data) {
							if(key === 'stats') {
								const result = { ...state[key], data: { ...state[key].data } };
								if(result.data.total) {
									result.data.total = result.data.total - 1;
								}
								Vue.set(state, key, result);
							} else {
								const stored = _.find(state[key].data, entry => (entry.id || entry.key) + '' === (newState.id || newState.key) + '');
								if(stored) {
									const result = { ...state[key], data: _.without(state[key].data, stored) };

									const currentPagination = state[key].pagination;
									if(currentPagination) {
										result.pagination = { ...currentPagination, total: currentPagination.total - 1 };
									}
									Vue.set(state, key, result);
								}
							}
						}
					});
				}
				if(newState.mode === 'update' && newState.id) {
					Object.keys(state).forEach((key) => {
						if(key.indexOf(statePropertyName) !== -1 || key === 'stats' || key === 'estimation') {
							return;
						}
						if(state && key && state[key] && state[key].data) {
							const stored = _.find(state[key].data, entry => (entry.id || entry.key) + '' === (newState.id || newState.key) + '');
							if(stored) {
								const newData = _.isArray(newState.data) ? newState.data[0] : newState.data;
								Object.keys(newData).forEach((propKey) => {
									if(stored[propKey] === newData[propKey] || propKey === 'id' || propKey === 'key') {
										return;
									}
									Vue.set(state[key].data[state[key].data.indexOf(stored)], propKey, newData[propKey]);
								});
							}
						}
					});
				}
				if(newState.mode === 'add') {
					// check 'shouldReload' in the 'list' component
				}

				if(newState.mode === 'send') {
					const result = { ...options.defaultState };
					if(options.entity === 'sms') {
						result.data.template_id = state[stateName].data.template_id;
					}
					Vue.set(state, stateName, result);
				}
			}
		};
	};

	const mutationNames = _.uniq([options.onBefore, options.onAfter, options.onError]);

	mutationNames.forEach((m) => {
		result.mutations[types[m]] = baseMutation();
	});

	return result;
};
