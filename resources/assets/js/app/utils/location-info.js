import axios from 'axios';

import Storage from './storage';

let counter = 0;

export default function() {
	const cachedResult = Storage.get('smsto_user_country');
	if(cachedResult && counter < 5) {
		counter++;
		return Promise.resolve(cachedResult);
	}
	return axios.get('https://ipinfo.io/json').then(response => {
		counter = 0;
		const result = response.data && response.data.country;
		
		Storage.set('smsto_user_country', result);

		return result;
	}).catch(error => {
		counter = 0;
		console.log(error);
		throw error;
	});
};
