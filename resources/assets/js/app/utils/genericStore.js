import _ from 'lodash';

import apiEntity from './entity';

import * as types from '../store/mutation-types';

function resolveEntityData(flowName, entity, { getters, dispatch, commit }, data) {
	let apiPath = null;
	let onBefore = null;
	let onAfter = null;
	let onError = null;
	let onAfterAction = null;
	let singlePropName = null;

	if(entity.entity) {
		singlePropName = entity.singlePropName;
		onBefore = entity.onBefore;
		onAfter = entity.onAfter;
		onAfterAction = entity.onAfterAction;
		onError = entity.onError;
		apiPath = entity.apiPath;
		entity = entity.entity;
	}

	if(!apiPath) {
		apiPath = entity;
	}

	if(apiPath && data && data.pathSegments) {
		Object.keys(data.pathSegments).forEach((segment) => {
			apiPath = apiPath.replace(':' + segment, data.pathSegments[segment]);
		});
	}

	if(apiPath && apiPath.indexOf(':') !== -1) {
		const pathSegments = { authenticatedUserId: (getters.singleAuthenticatedUser() || {}).id };
		Object.keys(pathSegments).forEach((segment) => {
			apiPath = apiPath.replace(':' + segment, pathSegments[segment]);
		});
	}

	if(data && data.filterBy) { // just allowed query params ....
		if(data.filterBy.list) {
			apiPath = 'lists/' + data.filterBy.list + '/' + entity + 's';
		}
	}

	return { entity, apiPath, singlePropName, onBefore, onAfter, onError, onAfterAction, params: data && data.params ? data.params : { limit: 9999999 } };
}

function mapSMSShite(data) {
	const result = { ...data };

	if(result.list_id && result.list_id.id) {
		result.list_id = result.list_id.id;
	}
	if(result.template_id && result.template_id.id) {
		result.template_id = result.template_id.id;
	}

	if(result.contacts && result.contacts.replace) {
		result.contacts = _.map(result.contacts.replace(/\r?\n|\r/g, ',').split(','), (r) => _.trim(r)).join(',');
	}

	if(result.useList === 'custom_single') {
		result.contacts = _.trim(result.single_contact);
		// result.list_id = null;
		delete result.list_id;
	}
	if(result.useList === 'custom') {
		// result.list_id = null;
		delete result.list_id;
	}
	if(result.useList === 'saved') {
		// result.contacts = null;
		delete result.contacts;
	}

	if(result.useTemplate === 'custom') {
		// result.template_id = null;
		delete result.template_id;
	}
	if(result.useTemplate === 'saved') {
		// result.message = null;
		delete result.message;
	}

	if(result.default_prefix && result.default_prefix.id) {
		result.default_prefix = result.default_prefix.id;
	}

	if(result.timezone && result.timezone.id) {
		result.timezone = result.timezone.id;
	}

	return result;
}

// initial state
const state = {
	all: [],
	stats: { total: 0 }
};

// getters
const getters = {
	all: () => state => state.all,
	stats: () => state => state.stats,
	getLocal: () => (state, getters) => (id) => state.all.find(entity => entity.id + '' === id + '')
};

// actions
const actions = {
	getAll(entity, { getters, dispatch, commit }, data) {
		const entityData = resolveEntityData('receive', ...arguments);

		if(entityData.onBefore) {
			commit(types[entityData.onBefore], null);
		}

		return apiEntity.lookup(entityData.apiPath, entityData.params).then(response => {
			if(response.data.data && response.data.data[0] && response.data.data[0].lists) { // for contact lists basically ..
				response.data.data.forEach((contact) => {
					if(!contact || !contact.lists) {
						return;
					}
					contact.lists.forEach((list) => {
						if(!list.id && list.list_id) {
							list.id = list.list_id;
						}
					});
				});
			}

			if(response.data && !response.data.data) {
				const data = { data: response.data, meta: { } };
				if(response.data.meta) {
					data.meta = response.data.meta;
				}
				response.data = data;
			}

			if(entityData.onAfter) {
				commit(types[entityData.onAfter], { data: response.data.data, pagination: response.data.meta ? response.data.meta.pagination : null });
			}
			if(entityData.onAfterAction) {
				dispatch(entityData.onAfterAction);
			}
			return response.data; // has meta info such as pagination
		}).catch((error) => {
			if(entityData.onError) {
				commit(types[entityData.onError], { error: error });
			}
			throw error;
		});
	},
	getStats(entity, { getters, dispatch, commit }, data) {
		const entityData = resolveEntityData('receive', ...arguments);

		if(entityData.onBefore) {
			commit(types[entityData.onBefore], null);
		}

		return apiEntity.lookupStats(entityData.apiPath, data).then(response => {
			if(entityData.onAfter) {
				commit(types[entityData.onAfter], { data: response.data });
			}
			if(entityData.onAfterAction) {
				dispatch(entityData.onAfterAction);
			}
			return response.data;
		}).catch((error) => {
			if(entityData.onError) {
				commit(types[entityData.onError], { error: error });
			}
			throw error;
		});
	},
	getDownloadLinks(entity, { getters, dispatch, commit }, id) {
		if(!id) {
			return;
		}

		const entityData = resolveEntityData('single', ...arguments);

		if(entityData.onBefore) {
			commit(types[entityData.onBefore], { id, mode: 'get' });
		}

		const method = 'lookupDownloadLinksOne';

		return apiEntity[method](entityData.apiPath, id).then((response) => {
			if(response.data.user && !response.data.data) {
				response.data = { data: response.data.user };
			}
			if(entityData.onAfter) {
				commit(types[entityData.onAfter], { id, mode: 'get', data: response.data.data });
			}
			if(entityData.onAfterAction) {
				dispatch(entityData.onAfterAction);
			}
			return response.data;
		}).catch((error) => {
			if(entityData.onError) {
				commit(types[entityData.onError], { id, mode: 'get', error: error });
			}
			throw error;
		});
	},
	get(entity, { getters, dispatch, commit }, id) {
		if(!id) {
			return;
		}

		const entityData = resolveEntityData('single', ...arguments);

		if(entityData.onBefore) {
			commit(types[entityData.onBefore], { id, mode: 'get' });
		}

		const method = 'lookupOne';

		return apiEntity[method](entityData.apiPath, id).then((response) => {
			if(response.data.user && !response.data.data) {
				response.data = { data: response.data.user };
			}
			if(entityData.onAfter) {
				commit(types[entityData.onAfter], { id, mode: 'get', data: response.data.data });
			}
			if(entityData.onAfterAction) {
				dispatch(entityData.onAfterAction);
			}
			return response.data;
		}).catch((error) => {
			if(entityData.onError) {
				commit(types[entityData.onError], { id, mode: 'get', error: error });
			}
			throw error;
		});
	},
	add(entity, { dispatch, commit }, data) {
		if(!data) {
			return;
		}

		const entityData = resolveEntityData('single', ...arguments);

		if(entityData.onBefore) {
			commit(types[entityData.onBefore], { id: data.id, mode: 'add' });
		}

		return apiEntity.createOne(entityData.apiPath, data).then((response) => {
			if(entityData.onAfter) {
				commit(types[entityData.onAfter], { id: data.id, mode: 'add', data: response.data.data || response.data });
			}
			if(entityData.onAfterAction) {
				dispatch(entityData.onAfterAction);
			}
			return response.data;
			// commit(types['ADD_' + _.snakeCase(entity).toUpperCase()], { entity: { ...data } });
		}).catch((error) => {
			if(entityData.onError) {
				commit(types[entityData.onError], { id: data.id, mode: 'add', error: error });
			}
			throw error;
		});
	},
	update(entity, { dispatch, commit }, data) {
		if(!data || (!data.id && entity.singlePropName !=='password')) {
			return;
		}
		if(data.lists) {
			data = { ...data };
			//data.lists = data.lists.map((l) => l.id);
		}

		const entityData = resolveEntityData('single', ...arguments);

		if(entity.entity === 'contact') {
			delete data.lists;
		}

		let payload = data;
		let method = 'updateOne';

		if(['password', 'basic', 'business', 'avatar'].indexOf(entityData.singlePropName) !== -1) {
			method = 'update';
		}

		if(entityData.onBefore) {
			commit(types[entityData.onBefore], { id: data.id || data.key, mode: 'update' });
		}

		const methodArgs = method === 'update' ? [payload] : [data.id, payload];

		return apiEntity[method](entityData.apiPath, ...methodArgs).then(response => {
			let responsePayload = response.data && response.data.data ? response.data.data : { ...data };
			responsePayload = _.isArray(responsePayload) && responsePayload[0] ? responsePayload[0] : responsePayload;
			if(entityData.onAfter) {
				commit(types[entityData.onAfter], { id: data.id, mode: 'update', data: responsePayload });
			}
			if(entityData.onAfterAction) {
				let actionPayload = { ...data };
				if(entityData.singlePropName === 'avatar') {
					actionPayload = { avatar: responsePayload.avatar_url };
				}
				dispatch(entityData.onAfterAction, { data: actionPayload });
			}
			// commit(types[entityData.onAfter], { entity: { ...data } });
		}).catch((error) => {
			if(entityData.onError) {
				commit(types[entityData.onError], { id: data.id, mode: 'error', error: error });
			}
			throw error;
		});
	},
	updateSingleProp(entity, { dispatch, commit }, data) {
		if(!data) {
			return;
		}
		if(data && data.lists) { // for contact lists basically ..
			data = { ...data };
			data.lists = data.lists.map((list) => {
				if(!list.id && list.list_id) {
					list.id = list.list_id;
				}
				return list;
			});
		}
		if(data && data.listsCurrent) { // for contact lists basically ..
			data = { ...data };
			data.listsCurrent = data.listsCurrent.map((list) => {
				if(!list.id && list.list_id) {
					list.id = list.list_id;
				}
				return list;
			});
		}

		const entityData = resolveEntityData('single', ...arguments);

		// const prop = Object.keys(data).find((p) => ['id', 'key'].indexOf(p) === -1);
		const prop = entityData.singlePropName;

		const isNotificationSettingEnabled = entityData.entity === 'notification-setting' && prop === 'enabled';
		const isContactListsUpdate = entityData.entity === 'contact' && data.lists && data.listsCurrent;

		let payload = { [prop]: data[prop] };
		let method = 'updateOne' + _.upperFirst(prop);
		let apiPath = entityData.apiPath;

		if(isNotificationSettingEnabled) {
			payload = [{ key: data.key, [prop]: !data[prop] }];
			method = 'update';
		}

		if(isContactListsUpdate) {
			const currentPropName = prop + 'Current';
			payload[currentPropName] = data[currentPropName];
		}

		if(entityData.onBefore) {
			commit(types[entityData.onBefore], { id: data.id || data.key, mode: 'update' });
		}

		const methodArgs = method === 'update' ? [payload] : [data.id, payload];

		return apiEntity[method](apiPath, ...methodArgs).then(response => {
			let responsePayload = { ...data };
			if(response.data.updated) {
				const reqPayload = _.isArray(payload) && payload[0] ? payload[0] : payload;
				responsePayload = { ...responsePayload, ...reqPayload };
			}
			if(entityData.onAfter) {
				commit(types[entityData.onAfter], { id: data.id || data.key, mode: 'update', data: responsePayload });
			}
			if(entityData.onAfterAction) {
				dispatch(entityData.onAfterAction);
			}
			// commit(types[entityData.onAfter + '_' + prop.toUpperCase()], { entity: responsePayload });
		}).catch((error) => {
			if(entityData.onError) {
				commit(types[entityData.onError], { id: data.id || data.key, mode: 'error', error: error });
			}
			throw error;
		});
	},
	delete(entity, { dispatch, commit }, id) {
		if(!id) {
			return;
		}
		id = id.id ? id.id : id;

		const entityData = resolveEntityData('single', ...arguments);

		if(entityData.onBefore) {
			commit(types[entityData.onBefore], { id, mode: 'delete' });
		}
		return apiEntity.deleteOne(entity.apiPath, id).then((response) => {
			if(entityData.onAfter) {
				commit(types[entityData.onAfter], { id, mode: 'delete', data: id });
			}
			if(entityData.onAfterAction) {
				dispatch(entityData.onAfterAction);
			}
			// commit(types['DELETE_' + _.snakeCase(entity).toUpperCase()], { entity: id });
		}).catch((error) => {
			if(entityData.onError) {
				commit(types[entityData.onError], { id, mode: 'delete', error: error });
			}
			throw error;
		});
	},
	deleteAll(entity, { dispatch, commit }, data) {
		const entityData = resolveEntityData('receive', ...arguments);
		if(entityData.onBefore) {
			commit(types[entityData.onBefore], null);
		}
		return apiEntity.deleteAll(entityData.apiPath).then((response) => {
			if(entityData.onAfter) {
				commit(types[entityData.onAfter], { data: [], pagination: null });
			}
			if(entityData.onAfterAction) {
				dispatch(entityData.onAfterAction);
			}
		}).catch((error) => {
			if(entityData.onError) {
				commit(types[entityData.onError], { error: error });
			}
			throw error;
		});
	},

	import(entity, { commit, getters }, data) {
		const entityData = resolveEntityData('single', ...arguments);

		if(entityData.onBefore) {
			commit(types[entityData.onBefore], { id: data.id, mode: 'import' });
		}
		return new Promise((resolve, reject) => {
			const userId = getters.singleAuthenticatedUser().id;
			if(!userId || !data.file) {
				reject(new Error('No file selected'));
				return;
			}
	                                                              
			const formData = new FormData();
			formData.append('list_id', data.list_id);
			formData.append('file_name', data.file, data.file.name);

			apiEntity.update('contacts/import-upload', formData).then((response) => {
				resolve(true);
			}).catch((error) => {
				// if (error.response.data.errors['file_name'][0]) {
                	// alert(error.response.data.errors['file_name'][0])
				// }
				reject(error);
			});
		}).then((response) => {
			if(entityData.onAfter) {
				commit(types[entityData.onAfter], { id: data.id, mode: 'import', data: data });
			}
			if(entityData.onAfterAction) {
				dispatch(entityData.onAfterAction);
			}
		}).catch((error) => {
			if(entityData.onError) {
				commit(types[entityData.onError], { id: data.id, mode: 'import', error: error });
			}
			throw error;
		});
	},

	addfunds(entity, { commit, dispatch }, data) {
		const entityData = resolveEntityData('single', ...arguments);

		if(entityData.onBefore) {
			commit(types[entityData.onBefore], { id: data.id, mode: 'addfunds' });
		}

		let payload = { ...data };

		const creditCardProperties = ['amount', 'cardHolder', 'cardNum', 'expMnth', 'expYear', 'cvc', 'paymentMethod'];
		const bankTransferProperties = ['amount', 'reference_number', 'description','is_transferred', 'paymentMethod'];

		payload = _.pick(payload, ...creditCardProperties, ...bankTransferProperties);
		const endpoint = (payload.paymentMethod === 'credit-card') ? 'services/stripe/charge' : 'services/bankwire/charge'
		
		return this.add(endpoint, arguments[1], payload).then(() => {
			if(entityData.onAfter) {
				commit(types[entityData.onAfter], { id: data.id, mode: 'addfunds', data: data });				
			}
			if(entityData.onAfterAction) {
				dispatch(entityData.onAfterAction);
			}
			dispatch('getSessionStats');
		}).catch((error) => {
			if(entityData.onError) {
				commit(types[entityData.onError], { id: data.id, mode: 'addfunds', error: error });
			}
			throw error;
		});
	},

	send(entity, { commit, dispatch }, data) {
		const entityData = resolveEntityData('single', ...arguments);

		const payload = mapSMSShite(data);	

		if(entityData.onBefore) {
			commit(types[entityData.onBefore], { id: data.id, mode: 'send' });
		}

		return apiEntity.sendOne(entityData.apiPath, payload).then(response => {
			if(entityData.onAfter) {
				commit(types[entityData.onAfter], { id: data.id, mode: 'send', data: data });
				
			}
			if(entityData.onAfterAction) {
				dispatch(entityData.onAfterAction);
			}
// 			commit(types.ADD_SMS, { entity: { ...data } });
// 			commit(types.UPDATE_SMS_NEWONE, { ...defaultNewOne() });
		}).catch((error) => {
			if(entityData.onError) {
				commit(types[entityData.onError], { id: data.id, mode: 'send', error: error });
			}
			throw error;
		});
	},

	estimate(entity, { commit, getters }, data) {
		const entityData = resolveEntityData('receive', ...arguments);

		if(entityData.onBefore) {
			commit(types[entityData.onBefore], null);
		}
		
		const payload = mapSMSShite(data);

		return apiEntity.estimateOne(entityData.apiPath, payload).then(response => {
			if(entityData.onAfter) {
				commit(types[entityData.onAfter], { data: response.data });
			}
			if(entityData.onAfterAction) {
				dispatch(entityData.onAfterAction);
			}
			return response.data;
		}).catch((error) => {
			if(entityData.onError) {
				commit(types[entityData.onError], { error: error });
			}
			throw error;
		});
	}
};

// mutations
const mutations = {
	receive(storeProp = 'all') {
		return function(state, { entities }) { state[storeProp] = entities; };
	},
	receiveSingle(storeProp = 'me') {
		return function(state, { entity }) { state[storeProp] = entity; };
	},
	add(storeProp = 'all') {
		return function(state, { entity }) { state[storeProp].push(entity); };
	},
	update(storeProp = 'all') {
		return function(state, { entity }) {
			const stored = _.find(state[storeProp], entry => (entry.id || entry.key) + '' === (entity.id || entity.key) + '');
			if(stored) {
				Object.keys(entity).forEach((key) => {
					Vue.set(stored, key, entity[key]);
				});
			}
		};
	},
	updateSingleProp(prop, storeProp = 'all') {
		return function(state, { entity }) {
			const stored = _.find(state[storeProp], entry => (entry.id || entry.key) + '' === (entity.id || entity.key) + '');
			if(stored) {
				Vue.set(stored, prop, entity[prop]);
			}
		};
	},
	updateProps(storeProp = 'me') {
		return function(state, data) {
			Object.keys(data).forEach((key) => {
				Vue.set(state[storeProp], key, data[key]);
			});
		};
	},
	delete(storeProp = 'all') {
		return function(state, { entity }) {
			const stored = _.find(state[storeProp], entry => entry.id + '' === entity + '');
			state[storeProp] = _.without(state[storeProp], stored);
		};
	},
	deleteAll(storeProp = 'all') {
		state[storeProp] = [];
	}
};

export default {
	state,
	getters,
	actions,
	mutations
};

