import Vue from 'vue';
import VueRouter from 'vue-router';
import DSL from './utils/dsl';

Vue.use(VueRouter);

const routes = DSL.map(function() {

	this.route('app', { path: '', redirect: { name: 'app.index' } }, function() {
		this.route('index');
	
		this.route('manage', { redirect: { name: 'app.manage.lists' } }, function() {
			this.route('contacts', function() {
				this.route('new');
				this.route('import');
				this.route('edit', { path: 'edit/:id' });
			});
			this.route('lists', function() {
				this.route('new');
				this.route('import', { name: 'import', component: require('./routes/app/manage/contacts/import.vue') });
				this.route('detail', { path: ':id' });
				this.route('edit', { path: 'edit/:id' });
			});
		});

		this.route('sms', { redirect: { name: 'app.sms.send' } }, function() {
			this.route('send', function() {
				this.route('shortlinks', { redirect: { name: 'app.sms.send.shortlinks.new' } }, function() {
					this.route('new');
				});
			});
			this.route('templates', function() {
				this.route('new');
				this.route('edit', { path: 'edit/:id' });
			});
			this.route('marketplace');
		});

		this.route('reports', function() {
			this.route('download', { path: 'download/:id' });
			this.route('detail', { path: ':id' }, function() {
				this.route('download', { name: 'download', component: require('./routes/app/reports/download.vue') });
			});
		});

		this.route('api', { redirect: { name: 'app.api.dashboard' } }, function() {
			this.route('dashboard');
			this.route('data');
		});
		
		this.route('settings', { redirect: { name: 'app.settings.account' } }, function() {
			this.route('account');
			this.route('security');
			this.route('notifications');
			this.route('sessions');
		});

		this.route('billing', { redirect: { name: 'app.billing.history' } }, function() {	
			this.route('history');			
			this.route('address');
		});

		this.route('payment', { redirect: { name: 'app.payment.detail' } }, function() {	
			this.route('detail', { path: ':id' });		
			this.route('download', { path: '/:id/pdf/download/' } );
		});

		this.route('add-funds', function() {
			this.route(':method(credit-card|paypal|bank-transfer)', { name: 'add', component: require('./routes/app/add-funds/add.vue') });
		});
	});

	this.route('page-not-found', { path: '*' });
});

const router = new VueRouter({
	routes: routes,
	linkActiveClass: 'is-active',
	linkExactActiveClass: 'is-active'
});

window.originalPageTitle = _.trim(_.last(document.title.split('|')));

router.afterEach((to, from) => {
	const routeName = to.name === 'app.index' ? 'dashboard' : to.name;

	const segments = routeName.replace(/(app\.|\.index)/ig, '').split('.');

	const subs = ['new', 'edit', 'detail', 'download', 'send', 'import'];

	const segmentsWithoutSubs = _.without(segments, ...subs);

	let resultSegments = segments;

	subs.forEach((segment) => {
		if(segments.indexOf(segment) !== -1) {
			let entity = _.last(segmentsWithoutSubs) || '';

			if(entity === 'sms') {
				entity = entity.toUpperCase();
			}
			if(entity !== 'SMS' && !(segment === 'import' && entity === 'contacts')) {
				entity = entity.split('');
				if(entity[entity.length - 1] === 's') {
					entity.pop();
				}
			}
			resultSegments = [segment, entity.join ? entity.join('') : entity];
			if(['detail'].indexOf(segment) !== -1) {
				resultSegments = resultSegments.reverse();
			}
		}
	});

	if(resultSegments === segments && resultSegments[0] === 'settings') {
		resultSegments = resultSegments.reverse();

		resultSegments[0] = _.trimEnd(resultSegments[0], 's');
	}

	let result = _.startCase(resultSegments.join(' '));

	if(result.length) {
		result += ' | ';
	}
	result += window.originalPageTitle;

	document.title = result;
});

export default router;
