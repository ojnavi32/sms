import Vue from 'vue';

import Features from './landing/features';
import CostCalculator from './landing/cost-calculator';

import './bootstrap';

Vue.component('form-field', require('./app/components/form/field.vue'));


if($(Features.el).length) {
	const FeaturesApp = new Vue(Features);
}
if($(CostCalculator.el).length) {
	const CostCalculatorApp = new Vue(CostCalculator);
}

require('./jquery');
