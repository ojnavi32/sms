window._ = require('lodash');

/**
 * We'll load jQuery and the Bootstrap jQuery plugin which provides support
 * for JavaScript based Bootstrap features such as modals and tabs. This
 * code may be modified to fit the specific needs of your application.
 */

try {
    window.$ = window.jQuery = require('jquery');

    // require('bootstrap-sass');
} catch (e) {}

/**
 * We'll load the axios HTTP library which allows us to easily issue requests
 * to our Laravel back-end. This library automatically handles sending the
 * CSRF token as a header based on the value of the "XSRF" token cookie.
 */

window.axios = require('axios');

window.axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';

/**
 * Next we will register the CSRF Token as a common header with Axios so that
 * all outgoing HTTP requests automatically have it attached. This is just
 * a simple convenience so we don't have to attach every token manually.
 */

let token = document.head.querySelector('meta[name="csrf-token"]');

if (token) {
    window.axios.defaults.headers.common['X-CSRF-TOKEN'] = token.content;
} else {
    console.error('CSRF token not found: https://laravel.com/docs/csrf#csrf-x-csrf-token');
}

// Auth Token. We can use to request for internal API
let authtoken = document.head.querySelector('meta[name="auth-token"]');
if (authtoken) {
  	window.axios.defaults.headers.common['Authorization'] = 'Bearer '+ authtoken.content;
} 

// Lets define base url
let apiBase = document.head.querySelector('meta[name="api-base"]');
window.axios.defaults.baseURL = apiBase.content;

// default content type for axios
window.axios.defaults.headers.post['Content-Type'] = 'application/json';

/**
 * Echo exposes an expressive API for subscribing to channels and listening
 * for events that are broadcast by Laravel. Echo and event broadcasting
 * allows your team to easily build robust real-time web applications.
 */

// disable this for now. enable once authentication on socket.io server has been fixed
// import Echo from 'laravel-echo';
//
// window.io = require('socket.io-client');
//
// window.Echo = new Echo({
//     broadcaster: 'socket.io',
//     // host: window.location.hostname + ':6001',
//     host: `${window.echoConfig.host}:${window.echoConfig.port}`,
//     auth: {
//         headers: {
//             'Authorization': 'Bearer ' + authtoken.content
//         }
//     }
// });

// window.Echo.private('user.38.import')
//     .listen('ImportFailedEvent', (e) => {
//         alert("hey")
//         console.log(e);
//     });
