@extends('layouts.auth')
@section('title')
<title>Forgotten Password | Bulk SMS Gateway | SMS.To</title>
@endsection	
@section('description') 
	<meta name="description" content="Reset Password and we will send you a cheeky email.SMS.to is a top SMS messaging provider with a simple, reliable and powerful Bulk SMS gateway."/>
@endsection
@section('keywords') 
	<meta name="keywords" content="sms messaging, reset bulk sms password">
@endsection
@section('content')
<section class="section section-auth">
	<div class="container">
		<div class="columns is-vcentered">
			<div class="column is-4 is-offset-4">
				<a class="logo-wrapper" href="{{ url('/') }}">
					@include('components.logo')
				</a>
				<div class="box style-form">
					<div class="box-content">
						<h1 class="title">Reset Password</h1>
						<p class="subtitle">We'll send you a cheeky email</p>

						@include('components.forms.password.email')
					</div>
					<div class="box-actions">
						<p>
							Go back to <a href="{{ route('login') }}"><b>Sign in</b></a> page.
						</p>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
@endsection

