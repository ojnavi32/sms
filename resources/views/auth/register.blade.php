@extends('layouts.auth')

@section('title')
<title>Register for Online Bulk SMS Service Quickly and Easily  | SMS.To</title>
@endsection	
@section('description') 
	<meta name="description" content="Registration is FREE at the cheapest bulk sms service provider - SMS.To. SMS.to is a complete SMS Marketing platform with powerful SMS Gateway features."/>
@endsection
@section('keywords') 
	<meta name="keywords" content="register bulk sms, bulk sms sign up, register at bulk sms, bulk sms service online, online bulk sms, cheap bulk sms service">
@endsection

@section('content')
	<section class="section section-auth">
		<div class="container">
			<div class="columns is-vcentered">
				<div class="column is-4 is-offset-4">
					<a class="logo-wrapper" href="{{ url('/') }}">
						@include('components.logo')
					</a>
					<div class="box style-form">
						<div class="box-content">
							<h1 class="title">Hello Stranger!</h1>
							<p class="subtitle">Create a free account on SMS.to</p>

							@include('components.forms.register')
						</div>
						<div class="box-actions">
							<p>
								Already have an account? <a href="{{ route('login') }}"><b>Sign in</b></a>
							</p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
@endsection
