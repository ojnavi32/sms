@extends('layouts.auth')

@section('title')
<title>Log in To Your SMS.To Account | Cheap Bulk SMS Service | SMS.To</title>
@endsection
@section('description') 
	<meta name="description" content="Log in to the cheap, reliable bulk sms service, SMS.To. Schedule SMS or send sms messages to your contact list with merge, tracking, and powerful features." />
@endsection
@section('keywords') 
	<meta name="keywords" content="schedule sms, sms messages, cheap bulk sms, cheap bulk sms service, bulk sms login">
@endsection

@section('content')
	<section class="section section-auth">
		<div class="container">
			<div class="columns is-vcentered">
				<div class="column is-4 is-offset-4">
					<a class="logo-wrapper" href="{{ url('/') }}">
						@include('components.logo')
					</a>
					<div class="box style-form">
						<div class="box-content">
							<h1 class="title">Welcome back :)</h1>
							<p class="subtitle">Log into your account here</p>

							@if(Session::has('warning'))
								<p class="notification is-danger">{{Session::get('warning')}}</p>
							@elseif(Session::has('status'))
								<p class="notification">{{Session::get('status')}}</p>
							@elseif(Session::has('message'))
								<p class="notification is-danger">{{Session::get('message')}}</p>
							@endif

							@include('components.forms.login')
						</div>
						<div class="box-actions">
							<p>
								Don't have an account? <a href="{{ route('register') }}"><b>Sign up</b></a>
							</p>
						</div>
					</div>
					
				</div>
			</div>
		</div>
	</section>
@endsection
