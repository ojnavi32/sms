@extends('layouts.auth')

@section('content')
	<section class="section section-auth">
		<div class="container">
			<div class="columns is-vcentered">
				<div class="column is-4 is-offset-4">
					<a class="logo-wrapper" href="{{ url('/') }}">
						@include('components.logo')
					</a>
					<div class="box style-form">
						<div class="box-content">
							<div class="box-image">
								<img src="{{ asset('images/ghost-sorry.png') }}" alt="" />
							</div>
							<h1 class="title">404 Error</h1>
							<p class="subtitle is-main">
								Oh noo. We're so sorry but <br />
								apparently we have a problem!
							</p>
							<p class="subtitle">Burt get up here, now...</p>

						</div>
						<div class="box-actions is-spaced">
							<p>
								Please try another page or <br />
								@if (Auth::guest())
									<a href="{{ route('login') }}"><b>Sign in</b></a>
								@else
									<a href="{{ route('app') }}"><b>Go back to Dashboard</b></a>
								@endif
							</p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
@endsection
