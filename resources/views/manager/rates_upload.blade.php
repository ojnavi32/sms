@extends('layouts.manager.layout')

@section('content')


{!! Form::open(array('route' => 'manager.rateFileUpload','enctype' => 'multipart/form-data')) !!}
<h1 class="title">Country Rates Upload</h1>
  <div class="column">
    <div class="col-md-4">
      {!! Form::file('csv', array('class' => 'image, button is-primary')) !!}
    </div>
    
    <div class="col-md-2" style="height:20px;"></div>
      <div class="col-md-4">
        <button type="submit" class="button is-primary">Create</button>
    </div>
  </div>
{!! Form::close() !!}
@endsection
