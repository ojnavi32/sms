@extends('layouts.manager.layout')

@section('page_styles')
<style type="text/css">
    .topup_details em {
        color: red;
        font-weight: bold;
    }
    .topup_details p {
        max-width: 700px;
        word-break: break-all;
    }
</style>
@endsection

@section('content')
<div class="panel panel-default">
    <div class="panel-heading">
    </div>
    <table class="table">
        <thead>
            <tr>
                <th>Time</th>
                <th>User</th>
                <th>Error & Topup Details</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($failed_topups as $failed_topup)
                <tr>
                    <td>
                        <span>{{ $failed_topup->created_at }}</span>
                    </td>
                    <td>
                        <a href="{{ url('manager/members/view/'.$failed_topup->user->id) }}">
                            {{ $failed_topup->user->name }}<br>
                            <small>&lt;{{ $failed_topup->user->email }}&gt;</small>
                        </a>
                    </td>
                    <td class="topup_details">
                        <em>{{ $failed_topup->error_message }}</em><br>
                        <p>{{ $failed_topup->topup_details }}</p>
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>
    <div class="panel-footer text-center">
        {!! $failed_topups->render() !!}
    </div>
</div>
@endsection
