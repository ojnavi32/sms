@extends('layouts.manager.layout')

@section('page_styles')
<style type="text/css">
    .btn-xs .fa {
        margin-right: 0;
    }
</style>
@endsection

@section('content')
<div class="mb-16"><a href="{{ url('manager/staff/create') }}" class="btn btn-primary">Create</a></div>
<div class="panel panel-default">
    <table class="table">
        <thead>
            <tr>
                <th>Name</th>
                <th>Username</th>
                <th>Email</th>
                <th>Can Edit</th>
                <th>&nbsp;</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($users as $user)
                <tr>
                    <td>
                        <span>{{ $user->name }}</span>
                    </td>
                    <td>
                        <span>{{ $user->username }}</span>
                    </td>
                    <td>
                        <span>{{ $user->email }}</span>
                    </td>
                    <td>
                        <span>{{ $user->access }}</span>
                    </td>
                    <td class="text-right action-btns">
                        @if($user->id != 1)
                        <a href="{{ url('manager/staff/update/' . $user->id) }}" class="btn btn-green btn-xs update-btn" title="Update"><i class="fa fa-edit"></i></a>
                        <a href="#" class="btn btn-danger btn-xs delete-btn" title="Delete" data-rec-id="{{ $user->id }}" data-toggle="modal" data-target="#modalConfirm"><i class="fa fa-trash-o"></i></a>
                        @endif
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>
    <div class="panel-footer">
    </div>
</div>
@endsection
