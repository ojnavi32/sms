@extends('layouts.manager.layout')

@section('content')
{!! Form::open(['class' => 'form-bg text-theme']) !!}
    <div class="form-group">
        <label for="name">Name</label>
        <input class="form-control" name="name" id="name" placeholder="Name" type="text" value="{{ $name }}">
    </div>
    <div class="form-group">
        <label for="plan">Username</label>
        <input class="form-control" name="username" id="username" placeholder="Username" type="text" value="{{ $username }}">
    </div>
    <div class="form-group">
        <label for="rate">Email</label>
        <input class="form-control" name="email" id="email" placeholder="Email" type="email" value="{{ $email }}">
    </div>
    <div class="form-group">
        <label for="rate">Password</label>
        <input class="form-control" name="password" id="password" placeholder="Password" type="password" value="">
    </div>

    <div class="form-group">
        <label for="rate">Allow changes in:</label>
        <br>
        <br>
        <input type="checkbox" name="access[]" id="members" value="members"> <label for="members">Members</label>
        <br>
        <input type="checkbox" name="access[]" id="vault" value="vault"> <label for="vault">Vault</label>
    </div>
    <div class="form-group">
    @yield('providers')
    </div>
    <div>
        <button type="submit" class="btn btn-primary text-theme"><i class="fa fa-save"></i>Save</button>
        <a href="{{ url('manager/staff') }}" class="btn btn-default"><i class="fa fa-ban"></i>Cancel</a>
    </div>
</form>
@endsection
