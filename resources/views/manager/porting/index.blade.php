@extends('layouts.manager.layout')

@section('page_styles')
<style type="text/css">
    .btn-xs .fa {
        margin-right: 0;
    }
</style>
@endsection

@section('content')

<div class="panel panel-default">
    <table class="table">
        <thead>
            <tr>
                <th>ID</th>
                <th>DID Number</th>
                <th>Amount</th>
                <th>Lead Time (Working Days)</th>
                <th>&nbsp;</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($prices as $price)
                <tr>
                    <td>
                        <span>{{ $price->id }}</span>
                    </td>
                    <td>
                        <span>{{ $price->country }}</span><br>
                    </td>
                    <td>{{ $price->amount }}</td>
                    <td>
                        <span>{{ $price->lead_time }}</span>
                    </td>
                    <td class="text-right action-btns">
                        <a href="{{ url('manager/porting/edit/'.$price->id ) }}" class="btn btn-green btn-xs" title="Edit Providers"><i class="fa fa-edit"></i></a>
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>
    <div class="panel-footer">

    </div>
</div>
@endsection
