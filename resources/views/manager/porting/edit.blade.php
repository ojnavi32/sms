@extends('layouts.manager.layout')

@section('page_styles')
@endsection

@section('content')
{!! Form::open(['class' => 'form-bg text-theme', 'url' => url('manager/porting/update', $price->id)]) !!}
    <div class="form-group">
        <label for="country_dial_code_id">Update Pricing</label>
    </div>
    <div class="form-group">
        <label for="country">Country</label>
        <input class="form-control" name="country" id="country" placeholder="Country" type="text" value="{{ $price->country }}">
    </div>
    <div class="form-group">
        <label for="amount">Amount</label>
        <input class="form-control" name="amount" id="amount" placeholder="Amount" type="text" value="{{ $price->amount }}">
    </div>
    <div>
        <button type="submit" class="btn btn-primary text-theme"><i class="fa fa-save"></i>Save</button>
        <a href="{{ url('manager/porting') }}" class="btn btn-default"><i class="fa fa-ban"></i>Cancel</a>
    </div>
</form>

@endsection

@section('page_scripts')
<script type="text/javascript">
</script>
@endsection
