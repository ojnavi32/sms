@extends('layouts.manager.layout')

@section('page_styles')

@endsection

@section('content')
                <div class="panel-heading">Credits</div>
                <div class="columns">
                    <form class="form-horizontal" role="form" method="POST" action="{{ route('manager.creditsPackages.store') }}" autocomplete="off">
                        {{ csrf_field() }}
                        <div class="column">
                            <label class="control-label col-sm-2">Package Name</label>
                            <div class="col-md-6">
                                <input class="input" type="text" name="package" placeholder="Enter the name of package" value="{{ old('package') }}">
                            </div>
                        </div>

                         <div class="column">
                            <label class="control-label col-sm-2">Credit</label>
                            <div class="col-md-6">
                                <input class="input" type="text" name="credit" placeholder="Enter the credits" value="{{ old('credit') }}">
                            </div>
                        </div>

                        <div class="column">
                            <label class="control-label col-sm-2">Price</label>
                            <div class="col-md-6">
                                <input class="input" type="text" name="price" placeholder="Enter the Price" value="{{ old('price') }}">
                            </div>
                        </div>
   
                        <div class="column">
                            <div class="col-md-8 col-md-offset-4">
                                <button type="submit" class="button is-primary">
                                    Submit
                                </button>
                                <button type="button" class="button is-primary" onclick="window.location='{{ route("manager.creditsPackages.show") }}'">Credit Lists</button>
                            </div>
                        </div>
                    </form>
                </div>

@endsection
