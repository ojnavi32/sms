@extends('layouts.manager.layout')

@section('content')
<div class="col-md-12">
    <form class="form-horizontal" role="form" method="POST" action="{{ route('manager.creditsPackages.update',$credits->id) }}">
        {{ csrf_field() }}
        <div class="col-md-12">
            <label class="control-label col-sm-2">PACKAGE</label>
            <div class="col-md-6">
                <input type="text" class="input form-control" id="package" name="package" placeholder="Enter the Package" value="{{ @$credits->name }}">
            </div>
        </div>
        <div class="col-md-12">
            <label class="control-label col-sm-2">CREDITS</label>
            <div class="col-md-6">
                <input type="text" class="input form-control" id="credit" name="credit" placeholder="Enter the Credits" value="{{ @$credits->credits }}">
            </div>
        </div>
        <div class="col-md-12">
            <label class="control-label col-sm-2">PRICE $</label>
            <div class="col-md-6">
                <input type="text" class="input form-control" id="price" name="price" placeholder="Enter the Price" value="{{ @$credits->price }}">
            </div>
        </div>
        <div class="col-md-12">
            <div class="col-sm-offset-2 col-sm-10">
                <button type="submit" class="btn btn-primary"><i class="fa fa-save"></i>Update</button>
            </div>
        </div>
    </form>
</div>
@endsection
