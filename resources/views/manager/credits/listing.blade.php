@extends('layouts.manager.layout')

@section('page_styles')

@endsection

@section('content')

<a href="{{ url('manager/credits-packages/create') }}" class="button is-primary">Create</a>
<table class="table is-bordered is-narrow">
    <thead>
        <tr>
            <th>PACKAGE</th>
            <th>CREDITS</th>
            <th>PRICE</th>
            <th>&nbsp;</th>
        </tr>
    </thead>
    <tbody>
        @foreach ($credits as $credit)
            <tr>
                <td>
                    <span>{{ $credit->name }}</span><br>
                </td>
                <td>
                    <span>{{ $credit->credits }}</span><br>
                </td>
                <td>
                    <span>${{ $credit->price }}</span>
                </td>
                
                <td>
                    <a href="{{ route('manager.creditsPackages.edit', $credit->id) }}" class="btn btn-green btn-xs update-btn" title="Update"><i class="fa fa-edit"></i></a>
                    <a href="{{ route('manager.creditsPackages.delete', $credit->id) }}" class="btn btn-danger btn-xs delete-btn" title="Delete" onclick="return confirm('Are you sure?')"><i class="fa fa-trash-o"></i></a>
                </td>
            </tr>
        @endforeach
    </tbody>
</table>

@endsection
