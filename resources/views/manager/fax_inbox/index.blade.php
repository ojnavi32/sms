@extends('layouts.manager.layout')

@section('page_styles')
<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.css">
<style type="text/css">
    .btn-xs .fa {
        margin-right: 0;
    }
    #fax-jobs-list td {
        vertical-align: middle;
    }
    .label.label-failed {
        background-color: #d9534f;
    }
    .label.label-executed {
        background-color: #f0ad4e;
    }
</style>
@endsection

@section('content')
<div class="panel panel-default">
    <div class="panel-heading">
        {!! Form::open(['id' => 'list-filter-form', 'method' => 'GET', 'class' => 'form-horizontal form-bg text-theme']) !!}
            <div class="form-group">
                <label for="inputPassword" class="control-label col-sm-2">Seach</label>
                <div class="col-sm-8">
                    <input type="text" id="name_email" name="name_email" value="{{ request('name_email') }}" placeholder="Search Name, Email or Number" class="form-control">
                </div>
                <div class="col-sm-4">
                   
                </div>
            </div>
            <div class="form-group">
                <label for="inputPassword" class="control-label col-sm-2">Date</label>
                <div class="col-sm-8">
                    <div id="reportrange" class="pull-right" style="background: #fff; cursor: pointer; padding: 5px 10px; border: 1px solid #ccc; width: 100%">
                        <i class="glyphicon glyphicon-calendar fa fa-calendar"></i>&nbsp;
                        <span></span> <b class="caret"></b>
                    </div>
                </div>
                <div class="col-sm-4">
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-10 col-sm-offset-2">
                    <input type="hidden" name="start_date" value="{{ $start_date }}">
                    <input type="hidden" name="end_date" value="{{ $end_date }}">
                    <button class="btn btn-primary">Search</button>
                </div>
            </div>
        </form>
    </div>
    <div>Total: {{ $total }}</div>
    <table id="fax-jobs-list" class="table">
        <thead>
            <tr>
                <th>Created At</th>
                <th>User</th>
                <th>Sender</th>
                <th>Recipient</th>
                <th>Status</th>
                <th>&nbsp;</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($faxes as $fax)
                <tr>
                    <td>{{ $fax->created_at }}</td>
                    <td>
                        {{ $fax->user->name }}<br>
                        <a href="mailto:{{ $fax->user->email }}" class="small">&lt;{{ $fax->user->email }}&gt;</a>
                    </td>
                    <td>{{ $fax->sender }}</td>
                    <td>{{ $fax->recipient }}</td>
                    <td>{{ $fax->status }}</td>
                    <td>
                        <!-- <a href="{{ url('manager/fax_jobs/view/' . $fax->id) }}" class="btn btn-green btn-xs" title="View Details"><span class="fa fa-search-plus"></span></a> -->
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>
    <div class="panel-footer text-center">
        {!! $faxes->appends($paginationAppends)->render() !!}
    </div>
</div>
@endsection

@section('page_scripts')
<script type="text/javascript" src="https://cdn.jsdelivr.net/jquery/1/jquery.min.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.js"></script>
<script type="text/javascript">
$(function(){
    function cb(start, end) {
        $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
    }
    cb(moment('{{ $start_date }}'), moment('{{ $end_date }}'));

    $('#reportrange').daterangepicker({
        startDate: moment('{{ $start_date }}'),
        endDate: moment('{{ $end_date }}'),
        ranges: {
           'Today': [moment(), moment()],
           'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
           'Last 7 Days': [moment().subtract(6, 'days'), moment()],
           'Last 30 Days': [moment().subtract(29, 'days'), moment()],
           'This Month': [moment().startOf('month'), moment().endOf('month')],
           'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
        }
    }, cb);

    $('#reportrange').on('apply.daterangepicker', function(e, picker){
        start_date = picker.startDate.format('YYYY-MM-DD');
        end_date = picker.endDate.format('YYYY-MM-DD');
        name_email = $('#name_email').val();
        window.location.href = '?start_date=' + start_date + '&end_date=' + end_date + '&name_email=' + name_email;
    });

});
</script>
@endsection