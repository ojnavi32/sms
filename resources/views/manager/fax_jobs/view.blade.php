@extends('layouts.manager.layout')

@section('page_styles')
<style type="text/css">
    .label.label-failed {
        background-color: #d9534f;
    }
    .label.label-executed,
    .label.label-requeued,
    .label.label-blocked {
        background-color: #f0ad4e;
    }
</style>
@endsection

@section('content')
<div class="panel panel-light text-theme">
    <div class="panel-heading">
        <h3 class="panel-title">&nbsp;</h3>
    </div>
    <div class="panel-body">
        <form class="form-horizontal">
            <div class="form-group">
                <label class="control-label col-sm-2">Created</label>
                <div class="col-sm-10">
                    <p class="form-control-static">{{ $fax_job->created_at }}</p>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-2">Sender Name</label>
                <div class="col-sm-10">
                    <p class="form-control-static">{{ $fax_job->user->name }}</p>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-2">Sender Email</label>
                <div class="col-sm-10">
                    <p class="form-control-static">{{ $fax_job->user->email }}</p>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-2">Sent from IP</label>
                <div class="col-sm-10">
                    <p class="form-control-static">{{ $fax_job->user_ipv4_ip }}</p>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-2">Recipient</label>
                <div class="col-sm-10">
                    <p class="form-control-static">{{ $fax_job->fax_number }}</p>
                </div>
            </div>
            @if ($fax_job->document()->count())
            <div class="form-group">
                <label class="control-label col-sm-2">Document</label>
                <div class="col-sm-10">
                    <p class="form-control-static">{{ $fax_job->document->filename_display }}</p>
                </div>
            </div>
            @endif
            <div class="form-group">
                <label class="control-label col-sm-2">Status</label>
                <div class="col-sm-10">
                    <p class="form-control-static">
                        <span class="label label-{{ $fax_job->status }}">{{ $fax_job->status }}</span> {{ $fax_job->message }}
                    </p>
                </div>
            </div>
            @if ($fax_job->fax_job_transactions)
            <div class="panel panel-default mt-50">
                <div class="panel-heading">Job Transactions</div>
                <table class="table">
                    <thead>
                        <tr>
                            <th width="100">Datetime</th>
                            <th>Provider</th>
                            <th>Transaction ID</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($fax_job->faxJobTransactionsWithTrash as $fjt)
                        <tr>
                            <td rowspan="2">{{ $fjt->created_at }}</td>
                            <td>{{ $fjt->comm_provider->name }}</td>
                            <td>{{ $fjt->transaction_id }}</td>
                        </tr>
                        <tr>
                            <td colspan="2" style="word-break:break-all;">{{ $fjt->response }}</td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
            @endif
        </form>
    </div>
</div>
@endsection
