@extends('layouts.manager.layout')

@section('page_styles')
<style type="text/css">
    .btn-xs .fa {
        margin-right: 0;
    }
    #fax-jobs-list td {
        vertical-align: middle;
    }
    .label.label-failed {
        background-color: #d9534f;
    }
    .label.label-executed {
        background-color: #f0ad4e;
    }
    #fax-status-modal .retrieved-provider {
        font-weight: bold;
    }
    #fax-status-modal .retrieved-status-data {
        margin-top: 10px;
        display: block;
        word-break: break-word;
    }
    #fax-status-modal .status-set-buttons .checkbox-refund {
        margin-left: 10px;
    }
    #fax-status-modal .status-set-buttons .checkbox-refund input {
        width: 17px;
        height: 17px;
        vertical-align: text-bottom;
    }
</style>
@endsection

@section('content')
<div class="panel panel-default">
    <div class="panel-heading">
        {!! Form::open(['id' => 'list-filter-form', 'method' => 'GET', 'class' => 'form-horizontal form-bg text-theme']) !!}
            <div class="form-group">
                <label for="inputPassword" class="control-label col-sm-2">Document Name</label>
                <div class="col-sm-4">
                    <input type="text" name="document_name" value="{{ @$document_name }}" placeholder="Document Name" class="form-control">
                </div>
                <div class="col-sm-4">
                    <input value="success" type="checkbox" name="status[]" id="success" {{ (in_array('success', request('status', [])) ? 'checked' : '') }}>
                        <label for="success">Success</label><br>
                    <input value="failed" type="checkbox" name="status[]" id="failed" {{ (in_array('failed', request('status', [])) ? 'checked' : '') }}>
                        <label for="failed">Failed</label><br>
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-10 col-sm-offset-2">
                    <button class="btn btn-primary">Search</button>
                </div>
            </div>
        </form>
    </div>
    <table id="fax-jobs-list" class="table">
        <thead>
            <tr>
                <th>Created At</th>
                <th>User</th>
                <th>Recipient</th>
                <th>ReTries</th>
                <th>Status</th>
                <th>&nbsp;</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($fax_jobs as $fax_job)
                <tr>
                    <td>{{ $fax_job->created_at }}</td>
                    <td>
                        {{ $fax_job->user->name }}<br>
                        <a href="mailto:{{ $fax_job->user->email }}" class="small">&lt;{{ $fax_job->user->email }}&gt;</a>
                    </td>
                    <td>{{ $fax_job->fax_number }}</td>
                    <td>{{ $fax_job->job_api ? $fax_job->retries : '' }}</td>
                    <td class="text-center">
                        @if ($fax_job->status != 'success' and $fax_job->status != 'failed')
                        <span class="label label-warning">{{ $fax_job->status }}</span>
                        @else
                        <span class="label label-{{ $fax_job->status }}">{{ $fax_job->status }}</span>
                        @endif
                        @if ($fax_job->status == 'success')
                            <br><small><em>Deducted <strong>&euro;{{ $fax_job->cost }}</strong></em></small>
                        @endif
                    </td>
                    <td>
                        @if($fax_job->status == 'failed')
                        <a href="#" class="btn btn-green btn-xs resend-class" title="Resend Fax" resend="{{ $fax_job->id }}"><span class="fa fa-send"></span></a>
                        @endif
                        <a href="{{ url('manager/fax_jobs/view/' . $fax_job->id) }}" class="btn btn-green btn-xs" title="View Details"><span class="fa fa-search-plus"></span></a><br>
                        @if($fax_job->status == 'executed' && $fax_job->updated_at->lt($hour_ago))
                            <a href="#" data-fax-job-id="{{ $fax_job->id }}" class="btn btn-default btn-xs get-status-btn mt-4">Get Fax Status</a>
                        @endif
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>
    <div class="panel-footer text-center">
        {!! $fax_jobs->appends(['status' => @$status])->render() !!}
    </div>
</div>

<div id="fax-status-modal" class="modal fade" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form action="{{ url()->current() }}/set-status" method="POST">
                {{ csrf_field() }}
                <input type="hidden" name="fax_job_id" id="fax-status-modal-fax_job_id">
                <input type="hidden" name="comm_provider_id">
                <input type="hidden" name="transaction_id">
                <input type="hidden" name="response">
                <input type="hidden" name="status">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Fax Job ID #<span class="fax_job_id"></span> Fax Status</h4>
                </div>
                <div class="modal-body">
                    <div class="progress">
                        <div class="progress-bar progress-bar-striped active" role="progressbar" style="width:100%;background-color:#f0ad4e;">
                            <span>Retrieving...</span>
                        </div>
                    </div>
                    <div class="modal-body-content">
                        Provider: <span class="retrieved-provider"></span><br>
                        Status: <span class="retrieved-status"></span><br>
                        <code class="retrieved-status-data"></code>
                    </div>
                    <div class="no-transaction-found text-center">This fax failed to send.</div>
                </div>
                <div class="modal-footer">
                    <div class="pull-left status-set-buttons">
                        <button type="submit" class="btn btn-primary btn-set-success">Set Success</button>
                        <button type="submit" class="btn btn-primary btn-set-failed">Set Failed</button>
                        <label class="checkbox-refund"><input type="checkbox" name="refund" value="1" checked> Refund fax cost</label>
                    </div>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection

@section('page_scripts')
<script type="text/javascript">
$(function(){
    
    $('.resend-class').click(function(){
        var fax_job_id = $(this).attr('resend');
        $.get(
            '{{ url('service/fax/send-fax') }}',
            { 'fax_job_id': fax_job_id },
            function(data) {
                console.log(data);
                if (data.front_end_status === 'failed') {

                } else {
                }
            }
        );

        $(this).text('Sending...');

        return false;
    });

    // get status modal
    var fs_modal_body_content = $('#fax-status-modal').find('.modal-body .modal-body-content');

    $('.get-status-btn').on('click', function(e){
        e.preventDefault();
        $.clearModalContent();
        var fax_job_id = $(this).data('faxJobId');
        var progress_bar = $('#fax-status-modal').find('.modal-body .progress');
        var fax_status_url = '{{ url('manager/fax_jobs') }}/'+fax_job_id+'/get-status';

        $('#fax-status-modal').find('h4 .fax_job_id').text(fax_job_id);
        $('#fax-status-modal-fax_job_id').val(fax_job_id);
        
        progress_bar.show();
        $('#fax-status-modal').modal();
        $.get(
            fax_status_url
        ).done(function(data){
            progress_bar.hide();

            $('#fax-status-modal').find('input[name=fax_job_id]').val(fax_job_id);
            $('#fax-status-modal').find('input[name=comm_provider_id]').val(data.provider.id);
            $('#fax-status-modal').find('input[name=transaction_id]').val(data.transaction_id);
            $('#fax-status-modal').find('input[name=response]').val(JSON.stringify(data.response));
            $('#fax-status-modal').find('input[name=status]').val(data.status);

            fs_modal_body_content.find('span.retrieved-provider').text(data.provider.name);
            switch (data.status) {
                case 'success':
                    $('#fax-status-modal').find('.status-set-buttons button.btn-set-success').show()
                    $('#fax-status-modal').find('.checkbox-refund input').prop('disabled', true);
                    $('.retrieved-status').html('<span class="label label-success">success</span>');
                    break;
                case 'failed':
                    $.setFailedForm(data.message);
                    break;
                default:
                    $('.retrieved-status').html('<span class="label label-error">Unable to retrieve fax status. Retry later.</span>');
                    break;
            }
            $('.retrieved-status-data').text(JSON.stringify(data.response));
            fs_modal_body_content.show();
        }).fail(function(){
            progress_bar.hide();
            $('#fax-status-modal').find('.no-transaction-found').show();
            $.setFailedForm();
            $('#fax-status-modal').find('.checkbox-refund').hide().find('input').prop('disabled', true);
        });
    });

    // get status modal content clear
    $.clearModalContent = function(){
        fs_modal_body_content.hide();
        fs_modal_body_content.find('[class^=retrieved-]').empty();
        $('#fax-status-modal').find('.status-set-buttons button, .status-set-buttons .checkbox-refund').hide();
        $('#fax-status-modal').find('.no-transaction-found').hide();
    };

    $.setFailedForm = function(message) {
        $('#fax-status-modal').find('.status-set-buttons button.btn-set-failed, .status-set-buttons .checkbox-refund').show()
        $('#fax-status-modal').find('.checkbox-refund input').prop('disabled', false);
        $('.retrieved-status').html('<span class="label label-failed">failed</span> ' + message);
    };

});
</script>
@endsection