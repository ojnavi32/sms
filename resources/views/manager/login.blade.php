@extends('layouts.manager.layout1')

@section('content')
<section class="section section-auth">
    <div class="container">
        <div class="columns is-vcentered">
            <div class="column is-4 is-offset-4">
                <div class="box">
                    <h1 class="title has-text-centered">Site Manager</h1>
                    <form method="POST" action="{{ route('manager.post.login') }}">
                    {{ csrf_field() }}

                    <div class="field">
                        <p class="control has-icons-left">
                            <input class="input{{ $errors->has('username') ? ' is-danger' : '' }}" type="text" name="username" placeholder="Username" value="{{ old('username') }}" required autofocus />
                            <span class="icon is-small is-left">
                                <span class="icon-envelope"></span>
                            </span>
                        </p>
                        @if ($errors->has('email'))
                            <p class="help is-danger">{{ $errors->first('email') }}</p>
                        @endif
                    </div>

                    <div class="field">
                        <p class="control has-icons-left">
                            <input class="input{{ $errors->has('password') ? ' is-danger' : '' }}" type="password" name="password" placeholder="Password" value="" />
                            <span class="icon is-small is-left">
                                <span class="icon-lock"></span>
                            </span>
                        </p>
                        @if ($errors->has('password'))
                            <p class="help is-danger">{{ $errors->first('password') }}</p>
                        @endif
                    </div>
                    
                    <div class="field">
                        <p class="control">
                            <label class="checkbox"><input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}> Remember me </label>
                        </p>
                    </div>

                    <div class="field">
                        <div class="control">
                            <button type="submit" class="button is-primary">Login</button>
                        </div>
                    </div>
                </form>

                </div>
            </div>
        </div>
    </div>
</section>
@endsection