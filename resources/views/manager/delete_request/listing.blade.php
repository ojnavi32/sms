@extends('layouts.manager.layout')

@section('page_styles')

@endsection

@section('content')

<table class="table is-bordered is-narrow">
    <thead>
        <tr>
            <th>USER</th>
            <th>EMAIL</th>
            <th>DELETION REQUEST TIMESTAMP</th>
            <th>REQUEST IP</th>
            <th>&nbsp;</th>
        </tr>
    </thead>
    <tbody>
    @foreach ($deleteRequests as $deleteRequest)
        <tr>
            <td>
               <span>{{ $deleteRequest->name }}</span><br>        
            </td>
            <td>
               <span>{{ $deleteRequest->email }}</span><br>        
            </td>   
            <td>
               <span>{{ $deleteRequest->time_stamp }}</span><br>     
            </td>
            <td>
                <span>{{ $deleteRequest->request_ip }}</span><br> 
            </td>
            @if($deleteRequest->is_deleted ==0)<td><a class="btn btn-danger" href="{{ route('manager.deleteRequest.delete', $deleteRequest->user_id) }}" onclick="return confirm('Are you sure want to permanantly delete this account?')">PERMANENTLY DELETE</td>@endif    
        </tr>
    @endforeach
    </tbody>
</table>

@endsection