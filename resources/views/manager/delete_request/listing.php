@extends('layouts.manager.layout')

@section('page_styles')

@endsection

@section('content')

<table class="table is-bordered is-narrow">
    <thead>
        <tr>
            <th>DELETE REQUESTS</th>
            <th>&nbsp;</th>
        </tr>
    </thead>
    <tbody>
        @foreach ($currencies as $currency)
            <tr>
                <td>
                    <span>{{ $currency->currency }}</span><br>
                </td>
                
                <td>
                    <a href="{{ route('manager.currency.edit', $currency->id) }}" class="btn btn-green btn-xs update-btn" title="Update"><i class="fa fa-edit"></i></a>
                    <a href="{{ route('manager.currency.delete', $currency->id) }}" class="btn btn-danger btn-xs delete-btn" title="Delete" onclick="return confirm('Are you sure?')"><i class="fa fa-trash-o"></i></a>
                </td>
            </tr>
        @endforeach
    </tbody>
</table>

@endsection