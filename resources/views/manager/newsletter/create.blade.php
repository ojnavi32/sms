@extends('layouts.manager.layout')

@section('content')
{!! Form::open(['class' => 'form-bg text-theme']) !!}
    <div class="form-group">
        <!-- <label for="match_string">Match String</label>
        <input class="form-control" name="match_string" id="match_string" placeholder="Match String" type="text" value="{{ $match_string }}"> -->
    </div>
    <div>
        <button type="submit" class="btn btn-primary text-theme"><i class="fa fa-save"></i>Verify</button>
        <a href="{{ url('manager/email_banlist') }}" class="btn btn-default"><i class="fa fa-ban"></i>Cancel</a>
    </div>
</form>
@endsection