@extends('layouts.manager.layout')

@section('page_styles')
<style type="text/css">
    .btn-xs .fa {
        margin-right: 0;
    }
</style>
@endsection

@section('content')
<div class="panel panel-default">
    <table class="table">
        <thead>
            <tr>
                <th>Group</th>
                <th>&nbsp;</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($groups as $banned)
                <tr>
                    <td>
                        <span><a target="_blank" href="{{ url('manager/translations/view/'.$banned->group) }}">{{ $banned->group }}</a></span>
                    </td>
                    <td class="text-right action-btns">
                        <!-- <a href="{{ url('manager/translations/lists/deploy/'. $banned->group) }}" class="btn btn-green btn-xs update-btn" title="Update">Deploy Languages</a> -->
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>
    <div class="panel-footer">

    </div>
</div>
@endsection
