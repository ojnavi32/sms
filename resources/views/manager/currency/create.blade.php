@extends('layouts.manager.layout')

@section('page_styles')

@endsection

@section('content')
                <div class="panel-heading">Currency</div>
                <div class="columns">
                    <form class="form-horizontal" role="form" method="POST" action="{{ route('manager.currency.store') }}" autocomplete="off">
                        {{ csrf_field() }}
                        <div class="column">
                            <label class="control-label col-sm-2">Currency Name</label>
                            <div class="col-md-6">
                                <input class="input" type="text" name="currency" placeholder="Enter the name of currency" value="{{ old('currency') }}">
                            </div>
                        </div>

                         <div class="column">
                            <label class="control-label col-sm-2">Currency Title</label>
                            <div class="col-md-6">
                                <input class="input" type="text" name="title" placeholder="Enter the title of currency" value="{{ old('title') }}">
                            </div>
                        </div>

                        <div class="column">
                            <label class="control-label col-sm-2">ISO Code</label>
                            <div class="col-md-6">
                                <input class="input" type="text" name="iso_code" placeholder="Enter the ISO Code" value="{{ old('iso_code') }}">
                            </div>
                        </div>

                        <div class="column">
                            <label class="control-label col-sm-2">Decimal</label>
                            <div class="col-md-6">
                                <input class="input" type="text" name="decimal" placeholder="Enter the Decimal" value="{{ old('decimal') }}">
                            </div>
                        </div>

                        <div class="column">
                            <label class="control-label col-sm-2">Symbol</label>
                            <div class="col-md-6">
                                <input class="input" type="text" name="symbol" placeholder="Enter the Symbol" value="{{ old('symbol') }}">
                            </div>
                        </div>

                        <div class="column">
                            <label class="control-label col-sm-2">Value</label>
                            <div class="col-md-6">
                                <input class="input" type="text" name="value" placeholder="Enter the Value" value="{{ old('value') }}">
                            </div>
                        </div>
   
                        <div class="column">
                            <div class="col-md-8 col-md-offset-4">
                                <button type="submit" class="button is-primary">
                                    Submit
                                </button>
                                <button type="button" class="button is-primary" onclick="window.location='{{ route("manager.currency.show") }}'">Currency Lists</button>
                            </div>
                        </div>
                    </form>
                </div>

@endsection