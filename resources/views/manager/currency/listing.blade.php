@extends('layouts.manager.layout')

@section('page_styles')

@endsection

@section('content')

<a href="{{ url('manager/currency/create') }}" class="button is-primary">Create</a>
<table class="table is-bordered is-narrow">
    <thead>
        <tr>
            <th>CURRENCY</th>
            <th>CURRENCY TITLE</th>
            <th>ISO CODE</th>
            <th>DECIMAL</th>
            <th>SYMBOL</th>
            <th>VALUE</th>
            <th>&nbsp;</th>
        </tr>
    </thead>
    <tbody>
        @foreach ($currencies as $currency)
            <tr>
                <td>
                    <span>{{ $currency->currency }}</span><br>
                </td>
                <td>
                    <span>{{ $currency->title }}</span><br>
                </td>
                <td>
                    <span>{{ $currency->iso_code }}</span>
                </td>
                <td>
                    <span>{{ $currency->decimal }}</span>
                </td>
                <td>
                    <span>{{ $currency->symbol }}</span>
                </td>
                <td>
                    <span>{{ $currency->value }}</span>
                </td>
                
                <td>
                    <a href="{{ route('manager.currency.edit', $currency->id) }}" class="btn btn-green btn-xs update-btn" title="Update"><i class="fa fa-edit"></i></a>
                    <a href="{{ route('manager.currency.delete', $currency->id) }}" class="btn btn-danger btn-xs delete-btn" title="Delete" onclick="return confirm('Are you sure?')"><i class="fa fa-trash-o"></i></a>
                </td>
            </tr>
        @endforeach
    </tbody>
</table>

@endsection