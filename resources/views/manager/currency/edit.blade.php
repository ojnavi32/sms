@extends('layouts.manager.layout')

@section('content')
<div class="col-md-12">
    <form class="form-horizontal" role="form" method="POST" action="{{ route('manager.currency.update',$currency->id) }}">
        {{ csrf_field() }}
        <div class="col-md-12">
            <label class="control-label col-sm-2">CURRENCY</label>
            <div class="col-md-6">
                <input type="text" class="input form-control" id="currency" name="currency" placeholder="Enter Currency Name" value="{{ @$currency->currency }}">
            </div>
        </div>
        <div class="col-md-12">
            <label class="control-label col-sm-2">CURRENCY TITLE</label>
            <div class="col-md-6">
                <input type="text" class="input form-control" id="title" name="title" placeholder="Enter Currency Title" value="{{ @$currency->title }}">
            </div>
        </div>
        <div class="col-md-12">
            <label class="control-label col-sm-2">ISO CODE</label>
            <div class="col-md-6">
                <input type="text" class="input form-control" id="iso_code" name="iso_code" placeholder="Enter ISO Code" value="{{ @$currency->iso_code }}">
            </div>
        </div>
        <div class="col-md-12">
            <label class="control-label col-sm-2">DECIMAL</label>
            <div class="col-md-6">
                <input type="text" class="input form-control" id="decimal" name="decimal" placeholder="Enter Decimal" value="{{ @$currency->decimal }}">
            </div>
        </div>
        <div class="col-md-12">
            <label class="control-label col-sm-2">SYMBOL</label>
            <div class="col-md-6">
                <input type="text" class="input form-control" id="symbol" name="symbol" placeholder="Enter Symbol" value="{{ @$currency->symbol }}">
            </div>
        </div>
        <div class="col-md-12">
            <label class="control-label col-sm-2">VALUE</label>
            <div class="col-md-6">
                <input type="text" class="input form-control" id="value" name="value" placeholder="Enter Value" value="{{ @$currency->value }}">
            </div>
        </div>
        <div class="col-md-12">
            <div class="col-sm-offset-2 col-sm-10">
                <button type="submit" class="btn btn-primary"><i class="fa fa-save"></i>Update</button>
            </div>
        </div>
       </div> 
    </form>
</div>
@endsection
