@extends('layouts.manager.layout')

@section('content')
{!! Form::open(['class' => 'form-horizontal form-bg text-theme']) !!}
    <div class="form-group">
        <label for="cash_balance" class="control-label col-sm-3">Cash Balance</label>
        <div class="col-sm-9">
            <input type="text" class="form-control" id="cash_balance" name="cash_balance" placeholder="Cash Balance" value="{{ @$cash_balance }}">
        </div>
    </div>
    <div class="form-group">
        <div class="col-sm-offset-2 col-sm-10">
            <button type="submit" class="btn btn-primary text-theme"><i class="fa fa-save"></i>Save</button>
            <a href="{{ url('manager/members') }}" class="btn btn-default"><i class="fa fa-ban"></i>Cancel</a>
        </div>
    </div>
</form>
@endsection
