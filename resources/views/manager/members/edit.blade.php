@extends('layouts.manager.layout')

@section('content')


<form action="{{ route('manager.members.postupdate', $id) }}" method="POST">

{{ csrf_field() }}

    <div class="form-group">
        <label for="name">Name</label>
        <input class="form-control" name="name" id="name" placeholder="Name" type="text" value="{{ $member->name }}">
    </div>
    <div class="form-group">
        <label for="email">Email</label>
        <input class="form-control" name="email" id="email" placeholder="Email" type="text" value="{{ $member->email }}">
    </div>
    <div class="panel panel-default mt-30">
        <div class="panel-heading">
            Leave blank to unchange
        </div>
        <div class="panel-body">
            <div class="form-group">
                <label for="password">Password</label>
                <input class="form-control" name="password" id="password" placeholder="Password" type="password">
            </div>
            <div class="form-group">
                <label for="password_confirmation">Confirm Password</label>
                <input class="form-control" name="password_confirmation" id="password_confirmation" placeholder="Confirm Password" type="password">
            </div>
        </div>
    </div>
    <!--<div class="panel panel-default mt-30">
        <div class="panel-heading">
            Email
        </div>
        <div class="panel-body">
            <div class="form-group">
                <input type="checkbox" value="1" name="disable_email" id="disable_email" {{ ($member->disable_email) ? 'checked' : '' }}> <label for="disable_email">Disable Email</label>
            </div>
        </div>
    </div>-->
    <div class="panel panel-default mt-30">
        <div class="panel-heading">
            Billing Details
        </div>
        <div class="panel-body">
            <div class="form-group">
                <label for="company" class="control-label">Company Name</label>  
                    <input name="company" class="form-control" id="company" placeholder="Company Name" type="text" value="{{ $member->company_name }}">
            </div>

            <div class="form-group">
                <label for="firstname" class="control-label">First Name</label>
                    <input name="firstname" class="form-control" id="firstname" placeholder="First Name" type="text" value="{{ $member->first_name }}">
            </div>
            <div class="form-group">
                <label for="lastname" class="control-label">Last Name</label>
                    <input name="lastname" class="form-control" id="lastname" placeholder="Last Name" type="text" value="{{ $member->last_name }}">
            </div>
            <div class="form-group">
                <label for="street_address" class="control-label">Street Address</label>
                    <textarea name="street_address" class="form-control" id="street_address" placeholder="Street Address">{{ $member->business_address }}</textarea>
            </div>
            <div class="form-group">
                <label for="postal_code" class="control-label">Postal Code</label>
                    <input name="postal_code" class="form-control" id="postal_code" placeholder="Postal Code" type="text" value="{{ $member->business_postal_code }}">
            </div>

            <div class="form-group">
                <label for="vat_number" class="control-label">Vat Number</label>
                    <input name="vat_number" class="form-control" id="vat_number" placeholder="Vat Number" type="text" value="{{ $member->business_vat }}">
                    <!-- <span v-if="valid_vat_number" id="valid_vat_number">Valid Format <i class="fa fa-check"></i></span>
                    <span v-if="invalid_vat_number" id="invalid_vat_number">Invalid Format <i class="fa fa-close"></i></span>
                    <span v-if="validating_vat" id="validating">Validating...</span> -->
                    <!--<input id="vat_percent" name="vat_percent" type="hidden" v-model="vat_percent">-->
            </div>

            <div class="form-group">
                <label for="country_dial_code_id" class="control-label">Country</label>
                    <select @change="is_eu()" id="country_dial_code_id" class="form-control"  name="country_dial_code_id">
                        <option value="">--Select Country--</option>
                        @foreach (App\Models\CountryDialCode::select('country', 'id', 'vat')->get() as $country_dial_code)
                            <option value="{{ $country_dial_code->country }}" data-vat="{{ $country_dial_code->vat }}"{{ (($country_dial_code->country == $member->business_country) ? ' selected' : '') }}>{{ $country_dial_code->country }}</option>
                        @endforeach
                    </select>
            </div>
        </div>
    </div>
    <div>
        <button type="submit" class="btn btn-primary text-theme"><i class="fa fa-save"></i>Save</button>
        <a href="{{ url('manager/members') }}" class="btn btn-default"><i class="fa fa-ban"></i>Cancel</a>
    </div>
</form>
@endsection
