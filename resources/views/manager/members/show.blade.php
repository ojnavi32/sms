@extends('layouts.manager.layout')

@section('page_styles')
<style type="text/css">
    .label.label-failed {
        background-color: #d9534f;
    }
    .label.label-executed {
        background-color: #f0ad4e;
    }
    .topup_details em {
        color: red;
        font-weight: bold;
    }
    .topup_details p {
        max-width: 700px;
        word-break: break-all;
    }
</style>
@endsection

@section('content')
<div class="panel panel-light text-theme">
    <div class="panel-heading">
        <h3 class="panel-title">&nbsp;</h3>
    </div>
    <div class="panel-body">
        <form class="form-horizontal">
            <div class="form-group">
                <label class="control-label col-sm-2">Register Date</label>
                <div class="col-sm-10">
                     <p class="form-control-static">{{ $member->created_at }}</p>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-2">IP Address</label>
                <div class="col-sm-10">
                    <p class="form-control-static">{{ $member->ip_address }}</p>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-2">Last Login Date</label>
                <div class="col-sm-10">
                    <p class="form-control-static">{{ $member->last_login_time }}</p>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-2">Last IP Address</label>
                <div class="col-sm-10">
                    <p class="form-control-static">{{ $member->last_ip_address }}</p>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-2">Country</label>
                <div class="col-sm-10">
                    <p class="form-control-static">{{ $member->country }}</p>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-2">Sender Name</label>
                <div class="col-sm-10">
                    <p class="form-control-static">{{ $member->name }}</p>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-2">Sender Email</label>
                <div class="col-sm-10">
                    <p class="form-control-static">{{ $member->email }}</p>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-2">Cash Balance</label>
                <div class="col-sm-10">
                    <p class="form-control-static">{{ $member->cash_balance }}</p>
                </div>
            </div>
            <!--<div class="form-group">
                <label class="control-label col-sm-2">Cash Balance</label>
                <div class="col-sm-10" id="vue-cash-balance">
                    <div class="row">
                      <div class="col-xs-2">
                        <input type="text" class="form-control" name="cash_balance" v-model="cash_balance">
                      </div>
                      <div class="col-xs-3">@{{ status }}</div>
                    </div>
                </div>
            </div>-->
            <div class="form-group">
                <label class="control-label col-sm-2">Verified</label>
                <div class="col-sm-10">
                    <p class="form-control-static">{{ ( $member->verified ? 'yes' : 'no' ) }}
                        @if( ! $member->verified )
                            <a href="{{ url('manager/members/verify', $member->id) }}" class="btn btn-primary">Verify</a>
                        @endif
                    </p>
                </div>
            </div>
            <div id="faxes_sent" class="panel panel-default mt-50">
                <div class="panel-heading">SMS Sent</div>
                <table id="fax-jobs-list" class="table">
                    <thead>
                        <tr>
                            <th>Today</th>
                            <th>This Month</th>
                            <th>Last Month</th>
                            <th>Lifetime</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                           <td>{{ $smstoday }}</td>
                           <td>{{ $smsthisMonth }}</td>
                            <td>{{ $smslastMonth }}</td>
                            <td>{{ $smslifeTime }}</td>
                        </tr>
                    </tbody>
                </table>
            </div>

            
            @if ($smsBroadcasts)
            <div id="fax_jobs" class="panel panel-default mt-50">
                <a name="fax_jobs"></a>
                <div class="panel-heading">Fax Jobs</div>
                <table id="fax-jobs-list" class="table">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>Created At</th>
                            <th>Recipient</th>
                            <th class="text-center">Status</th>
                            <th style="max-width:370px">Response</th>
                            <th>&nbsp;</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($smsBroadcasts as $fax_job)
                            <tr>
                                <td>{{ $fax_job->id }}</td>
                                <td>{{ $fax_job->created_at }}</td>
                                <td>{{ $fax_job->fax_number }}</td>
                                <td class="text-center">
                                @if ($fax_job->status != 'success' and $fax_job->status != 'failed')
                                <span class="label label-warning">{{ $fax_job->status }}</span>
                                @else
                                <span class="label label-{{ $fax_job->status }}">{{ $fax_job->status }}</span>
                                @endif
                                    @if ($fax_job->status == 'success')
                                        <br><small><em>Deducted <strong>&euro;{{ $fax_job->cost }}</strong></em></small>
                                    @endif
                                </td>
                                <td>{{ $fax_job->message }}</td>
                                <td>
                                    @if($fax_job->status == 'failed')
                                    <a href="#" class="btn btn-green btn-xs resend-class" title="Resend Fax" resend="{{ $fax_job->id }}"><span class="fa fa-send"></span></a>
                                    @endif
                                    <a href="{{ url('manager/fax_jobs/view/'.$fax_job->id) }}" class="btn btn-green btn-xs" title="View Details"><span class="fa fa-search-plus"></span></a></td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
                @if ( ! request('more'))
                    @if($fax_jobs->hasMorePages() and ! request('more'))
                        <a href="?more=1#fax_jobs">Show more..</a>
                    @endif
                @endif
            </div>
            @endif

            <div id="payment_history" class="panel panel-default mt-50">
                <div class="panel-heading">Payment</div>
                <table id="fax-jobs-list" class="table">
                    <thead>
                        <tr>
                            <th>Created</th>
                            <th>Payment Method</th>
                            <th>Amount</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($payments as $payment)
                            <tr>
                            <td>
                                <span>{{ $payment->created_at }}</span>
                            </td>
                            <td>
                                <span>{{ $payment->paymentMethod->name }}</span>
                            </td>
                            <td>
                                <span>{{ $payment->amount }}</span>
                            </td>                         
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
            
        </form>
    </div>
</div>
@endsection
@section('page_scripts')
<script type="text/javascript" src="https://unpkg.com/vue/dist/vue.js"></script>
<script src="https://unpkg.com/lodash@4.13.1/lodash.min.js"></script>
<script src="https://unpkg.com/axios/dist/axios.min.js"></script>
<script src="https://unpkg.com/vue-toastr@latest/dist/vue-toastr.combine.min.js"></script>
<script type="text/javascript">

$(function(){
    
    $('.resend-class').click(function(){
        var fax_job_id = $(this).attr('resend');
        $.get(
            '{{ url('service/fax/send-fax') }}',
            { 'fax_job_id': fax_job_id },
            function(data) {
                console.log(data);
                if (data.front_end_status === 'failed') {

                } else {
                }
            }
        );

        $(this).text('Sending...');

        return false;
    })
});


</script>
@endsection