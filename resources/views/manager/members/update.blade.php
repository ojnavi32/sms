@extends('layouts.manager.layout')

@section('content')
{!! Form::open(['class' => 'form-bg text-theme']) !!}
    <div class="form-group">
        <label for="name">Name</label>
        <input class="form-control" name="name" id="name" placeholder="Name" type="text" value="{{ $member->name }}">
    </div>
    <div class="form-group">
        <label for="email">Email</label>
        <input class="form-control" name="email" id="email" placeholder="Email" type="text" value="{{ $member->email }}">
    </div>
    <div class="panel panel-default mt-30">
        <div class="panel-heading">
            Leave blank to unchange
        </div>
        <div class="panel-body">
            <div class="form-group">
                <label for="password">Password</label>
                <input class="form-control" name="password" id="password" placeholder="Password" type="password">
            </div>
            <div class="form-group">
                <label for="password_confirmation">Confirm Password</label>
                <input class="form-control" name="password_confirmation" id="password_confirmation" placeholder="Confirm Password" type="password">
            </div>
        </div>
    </div>
    <div class="panel panel-default mt-30">
        <div class="panel-heading">
            Email
        </div>
        <div class="panel-body">
            <div class="form-group">
                <input type="checkbox" value="1" name="disable_email" id="disable_email" {{ ($member->disable_email) ? 'checked' : '' }}> <label for="disable_email">Disable Email</label>
            </div>
        </div>
    </div>
    <div class="panel panel-default mt-30">
        <div class="panel-heading">
            Billing Details
        </div>
        <div class="panel-body">
            <div class="form-group">
                <label for="company" class="control-label col-sm-3">{{ trans('buttons_links.labels.company_name') }}</label>
                <div class="col-sm-9">
                    <input @blur="validateVatNumber()" v-model="company" name="company" class="form-control" id="company" placeholder="{{ trans('buttons_links.labels.company_name') }}" type="text" value="{{ @$billingDetail->company }}">
                </div>
            </div>

            <div class="form-group">
                <label for="firstname" class="control-label col-sm-3">{{ trans('buttons_links.labels.first_name') }}</label>
                <div class="col-sm-9">
                    <input name="firstname" class="form-control" id="firstname" placeholder="{{ trans('buttons_links.labels.first_name') }}" type="text" value="{{ @$billingDetail->firstname }}">
                </div>
            </div>
            <div class="form-group">
                <label for="lastname" class="control-label col-sm-3">{{ trans('buttons_links.labels.last_name') }}</label>
                <div class="col-sm-9">
                    <input name="lastname" class="form-control" id="lastname" placeholder="{{ trans('buttons_links.labels.last_name') }}" type="text" value="{{ @$billingDetail->lastname }}">
                </div>
            </div>
            <div class="form-group">
                <label for="street_address" class="control-label col-sm-3">{{ trans('buttons_links.labels.street_address') }}</label>
                <div class="col-sm-9">
                    <textarea name="street_address" class="form-control" id="street_address" placeholder="{{ trans('buttons_links.labels.street_address') }}">{{ @$billingDetail->street_address }}</textarea>
                </div>
            </div>
            <div class="form-group">
                <label for="postal_code" class="control-label col-sm-3">{{ trans('buttons_links.labels.postal_code') }}</label>
                <div class="col-sm-9">
                    <input name="postal_code" class="form-control" id="postal_code" placeholder="{{ trans('buttons_links.labels.postal_code') }}" type="text" value="{{ @$billingDetail->postal_code }}">
                </div>
            </div>

            <div class="form-group" v-if="show_vat_number_field">
                <label for="vat_number" class="control-label col-sm-3">{{ trans('buttons_links.labels.vat_number') }}</label>
                <div class="col-sm-9">
                    <input @blur="validateVatNumber()" v-model="vat_number" name="vat_number" class="form-control" id="vat_number" placeholder="{{ trans('buttons_links.labels.vat_number') }}" type="text" value="{{ @$billingDetail->vat_number }}">
                    <!-- <span v-if="valid_vat_number" id="valid_vat_number">Valid Format <i class="fa fa-check"></i></span>
                    <span v-if="invalid_vat_number" id="invalid_vat_number">Invalid Format <i class="fa fa-close"></i></span>
                    <span v-if="validating_vat" id="validating">Validating...</span> -->
                    <input id="vat_percent" name="vat_percent" type="hidden" v-model="vat_percent">
                </div>
            </div>

            <div class="form-group">
                <label for="country_dial_code_id" class="control-label col-sm-3">{{ trans('buttons_links.labels.country') }}</label>
                <div class="col-sm-9">
                    <select @change="is_eu()" id="country_dial_code_id" class="form-control" required="required" name="country_dial_code_id">
                        <option value="">{{ trans('buttons_links.labels.select_country') }}</option>
                        @foreach (App\Models\CountryDialCode::select('country', 'id', 'vat')->get() as $country_dial_code)
                            <option value="{{ $country_dial_code->id }}" data-vat="{{ $country_dial_code->vat }}"{{ (($country_dial_code->id == @$billingDetail->country_dial_code_id) ? ' selected' : '') }}>{{ $country_dial_code->country }}</option>
                        @endforeach
                    </select>
                </div>
            </div>
        </div>
    </div>
    <div>
        <button type="submit" class="btn btn-primary text-theme"><i class="fa fa-save"></i>Save</button>
        <a href="{{ url('manager/members') }}" class="btn btn-default"><i class="fa fa-ban"></i>Cancel</a>
    </div>
</form>
@endsection
