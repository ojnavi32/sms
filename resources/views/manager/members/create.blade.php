@extends('layouts.manager.layout')

@section('content')
{!! Form::open(['class' => 'form-bg text-theme']) !!}
    <div class="form-group">
        <label for="name">Name</label>
        <input class="form-control" name="name" id="name" placeholder="Name" type="text" value="{{ @$name }}">
    </div>
    <div class="form-group">
        <label for="email">Email</label>
        <input class="form-control" name="email" id="email" placeholder="Email" type="text" value="{{ @$email }}">
    </div>
    <div class="form-group">
        <label for="password">Password</label>
        <input class="form-control" name="password" id="password" placeholder="Password" type="password">
    </div>
    <div class="form-group">
        <label for="password_confirmation">Confirm Password</label>
        <input class="form-control" name="password_confirmation" id="password_confirmation" placeholder="Password" type="password">
    </div>
    <div>
        <button type="submit" class="btn btn-primary text-theme"><i class="fa fa-save"></i>Save</button>
        <a href="{{ url('manager/members') }}" class="btn btn-default"><i class="fa fa-ban"></i>Cancel</a>
    </div>
</form>
@endsection
