@extends('layouts.manager.layout')

@section('page_styles')

@endsection

@section('content')
<h1 class="title">Members</h1>
<!--<a href="{{ url('manager/members/create') }}" class="button is-primary">Create</a><br/>-->

{!! Form::open(['id' => 'list-filter-form', 'method' => 'GET', 'class' => 'form-horizontal form-bg text-theme']) !!}
<div class="columns">
  <div class="col-sm-4">
    <!--<label for="inputPassword" class="control-label col-sm-2">Name or Email</label>-->
    <input type="text" name="name_or_email" value="{{ @$name_or_email }}" placeholder="Name or Email" class="input form-control">
  </div>
  <div class="column">
    <!--<input class="checkbox" value="topuped_credits" type="checkbox" name="tags[]" id="topuped_credits" {{ (in_array('topuped_credits', request('tags', [])) ? 'checked' : '') }}>
                        <label for="topuped_credits">Topuped Credits</label><br>
                    <input value="bought_fax_number_trial" type="checkbox" name="tags[]" id="bought_fax_number_trial" {{ (in_array('bought_fax_number_trial', request('tags', [])) ? 'checked' : '') }}>
                        <label for="sent_fax_success">Sent an SMS</label><br>
                    <input value="sent_fax_failed" type="checkbox" name="tags[]" id="sent_fax_failed" {{ (in_array('sent_fax_failed', request('tags', [])) ? 'checked' : '') }}>
                        <label for="sent_fax_failed">SMS Failed</label><br>
                    <br>
                    <input value="banned" type="checkbox" name="banned" id="banned" {{ (request('banned') ? 'checked' : '') }}>
                        <label for="banned">Banned Users</label><br>
                    <input value="valid_vat" type="checkbox" name="valid_vat" id="valid_vat" {{ (request('valid_vat') ? 'checked' : '') }}>
                        <label for="valid_vat">Valid VAT</label><br>
                    <input value="developer" type="checkbox" name="developer" id="developer" {{ (request('developer') ? 'checked' : '') }}>
                        <label for="developer">Developer Account</label><br>
                    @if(request()->path() == 'manager/members/ios')
                    <input value="1" type="checkbox" name="innapp-purchase" id="innapp-purchase" {{ (request('innapp-purchase') ? 'checked' : '') }}>
                        <label for="innapp-purchase">Made Inapp Purchase</label><br>
                    @endif -->
                    <button class="btn btn-primary">Search</button>
  </div>
</div><br/>
</form>

<table class="table is-bordered is-narrow">
    <thead>
        <tr>
            <th>Joined</th>
            <th>Name/Email</th>
            <th>Cash Balance</th>
            <th>SMS Jobs</th>
            <th>&nbsp;</th>
        </tr>
    </thead>
    <tbody>
        @foreach ($members as $member)
            <tr>
                <td>
                    <span>{{ $member->created_at }}</span><br>
                    <small>{{ $member->ip_address }}</small><br>
                    <small>{{ $member->country }}</small><br>
                </td>
                <td>
                    <span>{{ $member->name }}</span><br>
                    <a href="mailto:{{ $member->email }}" class="small">&lt;{{ $member->email }}&gt;</a>
                    <br><small>{{ $member->created_app }}</small>
                </td>
                <td>
                    <span>{{ $member->cash_balance }}</span>
                </td>
                <td class="text-center">
                    <span>{{ $member->broadcasts_count }}</span>
                </td>
                <td class="">
                    <a href="{{ route('manager.members.show', $member->id) }}" class="btn btn-green btn-xs update-btn" title="View Member"><i class="fa fa-search-plus"></i></a>
                    <a href="{{ url('manager/members/edit-funds/'.$member->id) }}" class="btn btn-green btn-xs update-btn" title="Edit Funds"><i class="fa fa-dollar"></i></a>
                    <a href="{{ url('manager/members/'.$member->id.'/settings/') }}" class="btn btn-green btn-xs update-btn" title="Settings (API/Broadcast)"><i class="fa fa-gear"></i></a><br>
                   <!-- <a href="#" class="btn btn-danger btn-xs delete-btn" title="Ban Email" data-rec-id="{{ $member->id }}" data-toggle="modal" data-target="#modalConfirmBan"><i class="fa fa-ban"></i></a>-->
                    <a href="{{ route('manager.members.edit', $member->id) }}" class="btn btn-green btn-xs update-btn" title="Update"><i class="fa fa-edit"></i></a>
                    <a href="{{ route('manager.members.delete', $member->id) }}" class="btn btn-danger btn-xs delete-btn" title="Delete" onclick="return confirm('Are you sure?')" data-toggle="modal"><i class="fa fa-trash-o"></i></a>
                </td>
                <td>
                   <a href="{{ route('manager.members.download', $member->id) }}">
                    <button type="button" class="btn btn-primary">download</button></a>
                </td>    
            </tr>
        @endforeach
    </tbody>
</table>
{!! $members->appends($requests)->render() !!}
@endsection
