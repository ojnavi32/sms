@extends('layouts.manager.layout')

@section('content')
{!! Form::open(['class' => 'form-bg text-theme']) !!}
    <div class="form-group">
        <label for="name">Billing Type</label>
        <select class="form-control" name="billing_type">
              <option  @if($billingType == 'PER_COUNTRY') selected  @endif>PER_COUNTRY</option>
              <option @if($billingType == 'PER_CARRIER') selected  @endif>PER_CARRIER</option>
        </select>
    </div>
    
    <div style="height:20px;"></div>
    <div>
        <button class= "button is-primary" type="submit" class="btn btn-primary text-theme"><i class="fa fa-save"></i>Save</button>
        <a href="{{ url('manager/members') }}" class="btn btn-default"><i class="fa fa-ban"></i>Cancel</a>
    </div>
</form>
@endsection