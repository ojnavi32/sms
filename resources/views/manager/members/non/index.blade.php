@extends('layouts.manager.layout')

@section('page_styles')

@endsection

@section('content')
<h1 class="title">Non-User Login Attempts</h1>
<div class="panel panel-default">
    <table class="table is-narrow is-bordered">
        <thead>
            <tr>
                <th>Date</th>
                <th>Email</th>
                <th>Country</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($members as $member)
                <tr>
                    <td>
                        <span>{{ $member->created_at }}</span>
                    </td>
                    <td>
                        <a href="mailto:{{ $member->email }}" class="small">&lt;{{ $member->email }}&gt;</a>
                        <br><small>{{ $member->created_app }}</small>
                    </td>
                    <td>
                       <span>{{ $member->country }}</span>
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>
    <div class="panel-footer text-center">
        {!! $members->appends($requests)->render() !!}
    </div>
</div>
@endsection