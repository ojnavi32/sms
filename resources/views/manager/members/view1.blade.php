@extends('layouts.manager.layout')

@section('page_styles')
<style type="text/css">
    .label.label-failed {
        background-color: #d9534f;
    }
    .label.label-executed {
        background-color: #f0ad4e;
    }
    .topup_details em {
        color: red;
        font-weight: bold;
    }
    .topup_details p {
        max-width: 700px;
        word-break: break-all;
    }
</style>
@endsection

@section('content')
<vue-toastr ref="toastr"></vue-toastr>
<div class="panel panel-light text-theme">
    <div class="panel-heading">
        <h3 class="panel-title">&nbsp;</h3>
    </div>
    <div class="panel-body">
        <form class="form-horizontal">
            <div class="form-group">
                <label class="control-label col-sm-2">Register Date</label>
                <div class="col-sm-10">
                    <p class="form-control-static">{{ $member->created_at }}</p>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-2">IP Address</label>
                <div class="col-sm-10">
                    <p class="form-control-static">{{ $member->ip_address }}</p>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-2">Last Login Date</label>
                <div class="col-sm-10">
                    <p class="form-control-static">{{ $member->last_login_time }}</p>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-2">Last IP Address</label>
                <div class="col-sm-10">
                    <p class="form-control-static">{{ $member->last_ip_address }}</p>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-2">Country</label>
                <div class="col-sm-10">
                    <p class="form-control-static">{{ $member->country }}</p>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-2">Sender Name</label>
                <div class="col-sm-10">
                    <p class="form-control-static">{{ $member->name }}</p>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-2">Sender Email</label>
                <div class="col-sm-10">
                    <p class="form-control-static">{{ $member->email }}</p>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-2">Cash Balance</label>
                <div class="col-sm-10" id="vue-cash-balance">
                    <div class="row">
                      <div class="col-xs-2">
                        <input type="text" class="form-control" name="cash_balance" v-model="cash_balance">
                      </div>
                      <div class="col-xs-3">@{{ status }}</div>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-2">Verified</label>
                <div class="col-sm-10">
                    <p class="form-control-static">{{ ( $member->verified ? 'yes' : 'no' ) }}
                        @if( ! $member->verified )
                            <a href="{{ url('manager/members/verify', $member->id) }}" class="btn btn-primary">Verify</a>
                        @endif
                    </p>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-2">Documents</label>
                <div class="col-sm-10">
                    <p class="form-control-static">{{ $member->documents->count() }}</p>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-2">Fax Jobs</label>
                <div class="col-sm-10">
                    <p class="form-control-static">{{ $fax_jobs->count() }}</p>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-4">Number of Pending Fax Allowed</label>
                <div class="col-sm-6" id="vue-allowed_pending">
                    <div class="row">
                      <div class="col-xs-4">
                        <input type="text" class="form-control" name="allowed_pending" v-model="allowed_pending">
                      </div>
                      <div class="col-xs-3">@{{ allowed_pending_status }}</div>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-2">Banned</label>
                <div class="col-sm-10">
                    <p class="form-control-static">
                        <span id="banned_text">@{{ banned_text }}</span><br>
                        <span>@{{ date_banned }}</span>
                
                        <!-- If not banned -->
                        @if ($member->verified != 2)
                            <a href="#" class="btn btn-danger" title="Ban User" data-rec-id="{{ $member->id }}" data-toggle="modal" data-target="#modalConfirmBan">Ban</a>
                        @else
                            <a href="{{ url('manager/members/unban', $member->id) }}" class="btn btn-default">Unban</a>
                        @endif
                        <a href="{{ url('manager/members/trust', $member->id) }}" class="btn btn-success">Trust</a>
                        <a href="{{ url('manager/members/impersonate', $member->id) }}" class="btn btn-success">Impersonate</a>
                        <a class="btn btn-success" @click="sendPasswordReset()"><i v-if="loadingapi" class="fa fa-spinner fa-spin"></i>Reset Password Email</a>
                        <!-- <a class="btn btn-danger" @click="deactivate()">Deactivate</a> -->
                    </p>
                </div>
            </div>
            <div id="faxes_sent" class="panel panel-default mt-50">
                <div class="panel-heading">Faxes Sent</div>
                <table id="fax-jobs-list" class="table">
                    <thead>
                        <tr>
                            <th>Today</th>
                            <th>This Month</th>
                            <th>Last Month</th>
                            <th>Lifetime</th>
                            <th>&nbsp;</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td><span>{{ $faxes['faxToday'] }}</span></td>
                            <td><span>{{ $faxes['faxMonth'] }}</span></td>
                            <td><span>{{ $faxes['faxLastMonth'] }}</span></td>
                            <td><span>{{ $faxes['faxLifeTime'] }}</span></td>
                            <td class="text-right action-btns"></td>
                        </tr>
                    </tbody>
                </table>
            </div>

            <div id="faxes_received" class="panel panel-default mt-50">
                <div class="panel-heading">Faxes Received</div>
                <table id="fax-jobs-list" class="table">
                    <thead>
                        <tr>
                            <th>Today</th>
                            <th>This Month</th>
                            <th>Last Month</th>
                            <th>Lifetime</th>
                            <th>&nbsp;</th>
                        </tr>
                    </thead>
                    <tbody>
                            <tr>
                            <td><span>{{ $faxes['faxReceivedToday'] }}</span></td>
                            <td><span>{{ $faxes['faxReceivedMonth'] }}</span></td>
                            <td><span>{{ $faxes['faxReceivedLastMonth'] }}</span></td>
                            <td><span>{{ $faxes['faxReceivedLifeTime'] }}</span></td>
                            <td class="text-right action-btns"></td>
                        </tr>
                    </tbody>
                </table>
            </div>
            
            @if ($fax_jobs)
            <div id="fax_jobs" class="panel panel-default mt-50">
                <a name="fax_jobs"></a>
                <div class="panel-heading">Fax Jobs</div>
                <table id="fax-jobs-list" class="table">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>Created At</th>
                            <th>Recipient</th>
                            <th class="text-center">Status</th>
                            <th style="max-width:370px">Response</th>
                            <th>&nbsp;</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($fax_jobs as $fax_job)
                            <tr>
                                <td>{{ $fax_job->id }}</td>
                                <td>{{ $fax_job->created_at }}</td>
                                <td>{{ $fax_job->fax_number }}</td>
                                <td class="text-center">
                                @if ($fax_job->status != 'success' and $fax_job->status != 'failed')
                                <span class="label label-warning">{{ $fax_job->status }}</span>
                                @else
                                <span class="label label-{{ $fax_job->status }}">{{ $fax_job->status }}</span>
                                @endif
                                    @if ($fax_job->status == 'success')
                                        <br><small><em>Deducted <strong>&euro;{{ $fax_job->cost }}</strong></em></small>
                                    @endif
                                </td>
                                <td>{{ $fax_job->message }}</td>
                                <td>
                                    @if($fax_job->status == 'failed')
                                    <a href="#" class="btn btn-green btn-xs resend-class" title="Resend Fax" resend="{{ $fax_job->id }}"><span class="fa fa-send"></span></a>
                                    @endif
                                    <a href="{{ url('manager/fax_jobs/view/'.$fax_job->id) }}" class="btn btn-green btn-xs" title="View Details"><span class="fa fa-search-plus"></span></a></td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
                @if ( ! request('more'))
                    @if($fax_jobs->hasMorePages() and ! request('more'))
                        <a href="?more=1#fax_jobs">Show more..</a>
                    @endif
                @endif
            </div>
            @endif

            <div id="fax_jobs1" class="panel panel-default mt-50">
                <div class="panel-heading">Account Changes</div>
                <table id="fax-jobs-list" class="table">
                    <thead>
                        <tr>
                            <th>Created At</th>
                            <th>Name</th>
                            <th class="text-center">Email</th>
                            <th>&nbsp;</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($changes as $change)
                            <tr>
                                <td>{{ $change->created_at }}</td>
                                <td>{{ $change->name }}</td>
                                <td>{{ $change->email }}</td>
                                <td></td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>

            <div id="fax_jobs2" class="panel panel-default mt-50">
                <div class="panel-heading">User Numbers <a href="{{ url('manager/members/numbers/assign', $member->id) }}" class="btn btn-green btn-xs update-btn" title="Edit"><i class="fa fa-edit"></i>Assign DID</a></div>
                <table id="fax-jobs-list" class="table">
                    <thead>
                        <tr>
                            <th>Created At</th>
                            <th>Number</th>
                            <th>Status</th>
                            <th>Payment Status</th>
                            <th>Fax Received</th>
                            <th>&nbsp;</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($numbers as $number)
                            <tr>
                                <td>{{ $number->created_at }}</td>
                                <td>{{ $number->did_number }}</td>
                                <td>{{ $number->is_on_trial }}</td>
                                <td>{{ $number->payment_status }}</td>
                                <td>
                                    @if($number->inbox()->count() > 0)
                                    <span class="badge">{{ $number->inbox()->count() }}</span>
                                    @endif
                                </td>
                                <td>
                                @if ($number->status != 2)
                                <a href="#" class="btn btn-danger btn-xs delete-btn" title="Delete" data-rec-id="{{ $number->id }}" data-toggle="modal" data-target="#disconnectConfirm"><i class="fa fa-trash"></i></a>
                                @endif
                                <a href="{{ url('manager/members/numbers/edit', [$number->user_id, $number->id]) }}" class="btn btn-green btn-xs update-btn" title="Edit"><i class="fa fa-edit"></i></a>
                                @if($number->is_free)
                                    <a class="btn btn-danger btn-xl">{{ $number->is_free }}</a>
                                @endif
                                @if($number->status_word == 'Expired')
                                <a href="{{ route('m.get.paybalance', [$number->user_id, $number->id]) }}" class="btn btn-success btn-xs"><i class="fa fa-check"></i> {{ trans('buttons_links.buttons.pay_with_balance') }}</a>
                                @endif
                                
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>

            <div id="payment_history" class="panel panel-default mt-50">
                <div class="panel-heading">Payment <a href="{{ url('manager/members/create-invoice', $member->id) }}" class="btn btn-green btn-xs update-btn" title="Add Invoice"><i class="fa fa-plus"></i>Add Invoice</a></div>
                <table id="fax-jobs-list" class="table">
                    <thead>
                        <tr>
                            <th>Created</th>
                            <th>Payment Method</th>
                            <th>Amount</th>
                            <th>Type</th>
                            <th>&nbsp;</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($payments as $payment)
                            <tr>
                            <td>
                                <span>{{ $payment->created_at }}</span>
                            </td>
                            <td>
                                <span>{{ $payment->payment_method->name }}</span>
                            </td>
                            <td>
                                <span>{{ $payment->amount }}</span>
                            </td>
                            <td>
                                <span>{{ ($payment->subscription_id) ? 'DID Number' : 'Top up' }}</span>
                            </td>
                            <td class="text-right action-btns">
                                <a href="{{ url('manager/members/payment-view/'.$payment->id) }}" class="btn btn-green btn-xs" title="View Details"><span class="fa fa-search-plus"></span></a>
                                <a href="{{ url('manager/members/payment-edit', $payment->id) }}" class="btn btn-green btn-xs update-btn" title="Edit"><i class="fa fa-edit"></i></a>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
            
            <div id="failed_topp_logs" class="panel panel-default mt-50">
                <div class="panel-heading">Failed Topup Logs</div>
                <table id="fax-jobs-list" class="table">
                    <thead>
                        <tr>
                            <th>Time</th>
                            <th>Error & Topup Details</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($failedTopupLogs as $row)
                            <tr>
                            <td>
                                <span>{{ $row->created_at }}</span>
                            </td>
                            <td class="topup_details">
                                <em>{{ $row->error_message }}</em><br>
                                <p>{{ $row->topup_details }}</p>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
            
            <div id="failed_subscription_logs" class="panel panel-default mt-50">
                <div class="panel-heading">Failed Subscription Logs</div>
                <table id="fax-jobs-list" class="table">
                    <thead>
                        <tr>
                            <th>Time</th>
                            <th>Error & Topup Details</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($failedSubscriptionsLogs as $row)
                            <tr>
                            <td>
                                <span>{{ $row->created_at }}</span>
                            </td>
                            <td class="topup_details">
                                <em>{{ $row->error_message }}</em><br>
                                <p>{{ $row->topup_details }}</p>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>

        </form>
    </div>
</div>
@endsection
@section('page_scripts')
<script type="text/javascript" src="https://unpkg.com/vue/dist/vue.js"></script>
<script src="https://unpkg.com/lodash@4.13.1/lodash.min.js"></script>
<script src="https://unpkg.com/axios/dist/axios.min.js"></script>
<script src="https://unpkg.com/vue-toastr@latest/dist/vue-toastr.combine.min.js"></script>
<script type="text/javascript">

$(function(){
    
    $('.resend-class').click(function(){
        var fax_job_id = $(this).attr('resend');
        $.get(
            '{{ url('service/fax/send-fax') }}',
            { 'fax_job_id': fax_job_id },
            function(data) {
                console.log(data);
                if (data.front_end_status === 'failed') {

                } else {
                }
            }
        );

        $(this).text('Sending...');

        return false;
    })
});

new Vue({
  el: '#vue',
  data: {
    loading: false,
    loadingapi: false,
    cash_balance: '{{ $member->cash_balance  }}',
    allowed_pending: '{{ $member->allowed_pending }}',
    status: '',
    allowed_pending_status: '',
    banned_text: '{{ $member->banned_text }}',
    date_banned: '{{ $member->date_banned }}',
    email: "{{ $member->email }}",
  },
  
  components: {
        'vue-toastr': window.vueToastr
    },
    
  watch: {
    // whenever cash_balance changes, this function will run
    cash_balance: function (newQuestion) {
      this.status = 'Saving ...'
      this.updateCashBalance()
    },
    allowed_pending: function (newQuestion) {
      this.allowed_pending_status = 'Saving ...'
      this.updateAllowedPending()
    }
  },
  methods: {
    
    sendPasswordReset: function() {
        var self = this;
        this.loadingapi = true;
        axios.post('{{ request()->root() . '/manager/members/password/reset' }}', {
          _token: "{{ csrf_token() }}",
          email: this.email
        })
        .then(function (response) {
            self.toast("Password reset email has been sent!")
            self.loadingapi = false;
        })
        .catch(function (error) {
          console.log(error);
        });
    },
    
    toast: function(message) {
        this.$root.$refs.toastr.Add({
            msg: message,
            clickClose: true,
            timeout: 8000,
            position: "toast-top-center",
            type: "success"
        });
    },
    
    deactivate: function() {
        var self = this;
        this.loadingapi = true;
        axios.post('{{ url('m/api/v1/deactivate') }}', {
          _token: "{{ csrf_token() }}",
          user_id: {{ $member->id }}
        })
        .then(function (response) {
            self.banned_text = 'Yes (Deactivated)';
            //self.toast("Password reset email has been sent!")
            self.loadingapi = false;
        })
        .catch(function (error) {
          console.log(error);
        });
    },
    
    // _.debounce is a function provided by lodash to limit how
    // often a particularly expensive operation can be run.
    // In this case, we want to limit how often we access
    // yesno.wtf/api, waiting until the user has completely
    // finished typing before making the ajax request. To learn
    // more about the _.debounce function (and its cousin
    // _.throttle), visit: https://lodash.com/docs#debounce
    updateCashBalance: _.debounce(
      function () {
        var vm = this
        if (this.cash_balance.indexOf('?') === -1) {
            var self = this;
            this.loadingapi = true;
            axios.post('{{ url('m/api/v1/cash') }}', {
              _token: "{{ csrf_token() }}",
              user_id: {{ $member->id }},
              cash_balance: self.cash_balance
            })
            .then(function (response) {
            })
            .catch(function (error) {
              console.log(error);
            });
          vm.status = 'Saved!'
          return
        }
      },
      // This is the number of milliseconds we wait for the
      // user to stop typing. 1 sec for 1000
      3000
    ),
    
    updateAllowedPending: _.debounce(
      function () {
        var vm = this
        if (this.allowed_pending.indexOf('?') === -1) {
          
          var self = this;
            this.loadingapi = true;
            axios.post('{{ url('m/api/v1/allowed-pending') }}', {
              _token: "{{ csrf_token() }}",
              user_id: {{ $member->id }},
              allowed_pending: self.allowed_pending
            })
            .then(function (response) {
            })
            .catch(function (error) {
              console.log(error);
            });
            
          vm.allowed_pending_status = 'Saved!'
          return
        }
      },
      // This is the number of milliseconds we wait for the
      // user to stop typing. 1 sec for 1000
      3000
    )
    
  }
})
</script>
@endsection