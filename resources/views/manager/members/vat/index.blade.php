@extends('layouts.manager.layout')

@section('page_styles')

@endsection

@section('content')
<h1 class="title">VAT Numbers</h1>
<div class="panel panel-default">
    <!-- <div class="panel-heading">
        {!! Form::open(['id' => 'list-filter-form', 'method' => 'GET', 'class' => 'form-horizontal form-bg text-theme']) !!}
            <div class="form-group">
                <label for="inputPassword" class="control-label col-sm-2">Name or Email</label>
                <div class="col-sm-4">
                    <input type="text" name="name_or_email" value="{{ @$name_or_email }}" placeholder="Name or Email" class="form-control">
                </div>
                <div class="col-sm-4">
                    <input value="topuped_credits" type="checkbox" name="tags[]" id="topuped_credits" {{ (in_array('topuped_credits', request('tags', [])) ? 'checked' : '') }}>
                        <label for="topuped_credits">Topuped Credits</label><br>
                    <input value="bought_fax_number_trial" type="checkbox" name="tags[]" id="bought_fax_number_trial" {{ (in_array('bought_fax_number_trial', request('tags', [])) ? 'checked' : '') }}>
                        <label for="bought_fax_number_trial">Bought Number</label><br>
                    <input value="sent_fax_success" type="checkbox" name="tags[]" id="sent_fax_success" {{ (in_array('sent_fax_success', request('tags', [])) ? 'checked' : '') }}>
                        <label for="sent_fax_success">Sent a fax</label><br>
                    <input value="sent_fax_failed" type="checkbox" name="tags[]" id="sent_fax_failed" {{ (in_array('sent_fax_failed', request('tags', [])) ? 'checked' : '') }}>
                        <label for="sent_fax_failed">Fax Failed</label><br>
                    <br>
                    <input value="banned" type="checkbox" name="banned" id="banned" {{ (request('banned') ? 'checked' : '') }}>
                        <label for="banned">Banned Users</label><br>
                    <input value="valid_vat" type="checkbox" name="valid_vat" id="valid_vat" {{ (request('valid_vat') ? 'checked' : '') }}>
                        <label for="valid_vat">Valid VAT</label><br>
                    @if(request()->path() == 'manager/members/ios')
                    <input value="1" type="checkbox" name="innapp-purchase" id="innapp-purchase" {{ (request('innapp-purchase') ? 'checked' : '') }}>
                        <label for="innapp-purchase">Made Inapp Purchase</label><br>
                    @endif
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-10 col-sm-offset-2">
                    <button class="btn btn-primary">Search</button>
                </div>
            </div>
        </form>
    </div> -->
    <table class="table is-narrow is-bordered">
        <thead>
            <tr>
                <th>Joined</th>
                <th>Name/Email</th>
                <th>VAT Number</th>
                <th>Country</th>
                <th>&nbsp;</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($members as $member)
                <tr>
                    <td>
                        <span>{{ $member->created_at }}</span><br>
                        <small>{{ $member->ip_address }}</small><br>
                        <small>{{ $member->country }}</small><br>
                    </td>
                    <td>
                        <span>{{ $member->name }}</span><br>
                        <a href="mailto:{{ $member->email }}" class="small">&lt;{{ $member->email }}&gt;</a>
                        <br><small>{{ $member->created_app }}</small>
                    </td>
                    <td>
                        <span>{{ $member->business_vat }}</span>
                    </td>
                    <td class="text-center">
                        <span>{{ $member->business_country }}</span>
                    </td>
                    <td class="text-right action-btns">
                        <a href="{{ url('manager/members/view/'.$member->id) }}" class="btn btn-green btn-xs update-btn" title="View Member"><i class="fa fa-search-plus"></i></a>
                        <a href="{{ url('manager/members/edit-funds/'.$member->id) }}" class="btn btn-green btn-xs update-btn" title="Edit Funds"><i class="fa fa-dollar"></i></a>
                        <a href="{{ url('manager/members/'.$member->id.'/settings/') }}" class="btn btn-green btn-xs update-btn" title="Settings (API/Broadcast)"><i class="fa fa-gear"></i></a><br>
                        <a href="#" class="btn btn-danger btn-xs delete-btn" title="Ban Email" data-rec-id="{{ $member->id }}" data-toggle="modal" data-target="#modalConfirmBan"><i class="fa fa-ban"></i></a>
                        <a href="{{ url('manager/members/update/'.$member->id) }}" class="btn btn-green btn-xs update-btn" title="Update"><i class="fa fa-edit"></i></a>
                        <a href="#" class="btn btn-danger btn-xs delete-btn" title="Delete" data-rec-id="{{ $member->id }}" data-toggle="modal" data-target="#modalConfirm"><i class="fa fa-trash-o"></i></a>
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>
    <div class="panel-footer text-center">
        {!! $members->appends($requests)->render() !!}
    </div>
</div>
@endsection