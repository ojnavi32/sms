@extends('layouts.manager.layout')

@section('page_styles')
<style type="text/css">
    label[for=broadcast_enabled] {
        text-align: left !important;
    }
    input[type=checkbox] {
        width: 27px;
        margin-left: auto;
    }
</style>
@endsection

@section('content')
<h3>{{ ucwords(str_replace("_", " ", $member->billing_type))}} Adjusted rates for {{ $member->name }}</h3>
{!! Form::open(['method' => 'GET', 'url' => 'manager/members/'.$member->id.'/settings/adjust-rates/create', 'class' => 'form-horizontal form-bg text-theme']) !!}
<div class="panel panel-default">
    <div class="panel-heading">Adjust rates for SMS.to</div>
    <div class="panel-body">
        <div class="form-group">
            <div class="col-sm-8">
                {!! Form::select('rate_id', $rates_select, NULL, ['class' => 'form-control']) !!}
            </div>
            <div class="col-sm-4">
                <button type="submit" class="btn btn-primary text-theme">Add Adjustment</button>
            </div>
        </div>
    </div>
</div>
</form>
{!! Form::open(['method' => 'GET', 'url' => 'manager/members/'.$member->id.'/settings/adjust-rates', 'class' => 'form-horizontal form-bg text-theme']) !!}
<table class="table">
    <thead>
        <tr>
            <th>Country</th>
            @if($member->billing_type == 'PER_CARRIER')
            <th>Carrier</th>
            @endif
            <th>Base Rate</th>
            <th>&nbsp;</th>
        </tr>
    </thead>
    <tbody>
        @foreach ($adjustments as $rate_adjustment)
        <tr>
            <td>
                <span>{{ $rate_adjustment->rate->name }} (+{{ $rate_adjustment->rate->country_dial_code_id }})</span>
            </td>
            @if($member->billing_type == 'PER_CARRIER')
            <td>
                <span>{{ $rate_adjustment->network }}</span>
            </td>
            @endif
            <td>
                <span>{{ $rate_adjustment->base_rate }}</span>
            </td>

            <td class="text-right action-btns">
                <a href="{{ url('manager/members/'. $member->id . '/settings/adjust-rates/update/'.$rate_adjustment->id) }}" class="btn btn-green btn-xs update-btn" title="Update"><i class="fa fa-edit"></i></a>
                <a href="{{ url('manager/members/'. $member->id . '/settings/adjust-rates/delete/'.$rate_adjustment->id) }}" onclick="return confirm('Are you sure?')" class="btn btn-danger btn-xs delete-btn" title="Delete" data-rec-id="{{ $rate_adjustment->id }}" data-toggle="modal"><i class="fa fa-trash-o"></i></a>
            </td>
        </tr>
        @endforeach
    </tbody>
</table>
</form>
@endsection
