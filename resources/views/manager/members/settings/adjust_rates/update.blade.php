@extends('layouts.manager.layout')

@section('content')
{!! Form::open(['class' => 'form-bg text-theme']) !!}
<div class="form-group">
    <label for="name">Country</label>
    <p class="form-control-static">{{ $user_rate_adjustment->rate->name }} (+{{ $user_rate_adjustment->rate->country_dial_code_id}})</p>
</div>
@if($member->billing_type == 'PER_CARRIER')
<div class="form-group">
    <label for="rate">Carrier</label>
    {!! Form::select('network', $carrier_select, NULL, ['class' => 'form-control']) !!}
</div>
@endif
<div class="form-group">
    <label for="rate">Base Rate</label>
    <input class="form-control" name="base_rate" id="base_rate" placeholder="0.0000" type="text" value="{{ @$user_rate_adjustment->base_rate }}">
</div>
<div class="form-group">
    @yield('providers')
</div>
<div>
    <input type="hidden" name="user_rate_adjustment_id" value="{{ $user_rate_adjustment->id }}">
    <button type="submit" class="btn btn-primary text-theme"><i class="fa fa-save"></i>Save</button>
    <a href="{{ url('manager/members/'.$member->id.'/settings/adjust-rates') }}" class="btn btn-default"><i class="fa fa-ban"></i>Cancel</a>
</div>
</form>
@endsection
