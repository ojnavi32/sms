@extends('layouts.manager.layout')

@section('content')
<h3>Settings for {{ $member->name }}</h3>
{!! Form::open(['class' => 'form-bg text-theme']) !!}
<div class="panel panel-default">
    <div class="panel-heading">SMS.to</div>
    <div class="panel-body">
        <div class="form-group">
            <label for="name">Billing Type</label>
            <select class="form-control" name="billing_type">
                <option  @if($member->billing_type == 'PER_COUNTRY') selected  @endif>PER_COUNTRY</option>
                <option @if($member->billing_type == 'PER_CARRIER') selected  @endif>PER_CARRIER</option>
            </select>
        </div>
        <div class="form-group">
            <div class="col-sm-2"><p class="form-control-static text-right"></p></div>
            <div class="col-sm-10">{{ $rate_adjustments->count() }} {{ ucwords(str_replace("_", " ", $member->billing_type))}} Adjusted Rates <a href="{{ url('manager/members/'.$member->id.'/settings/adjust-rates/') }}" class="btn btn-default">Edit</a></div>
        </div>
        <div style="height:20px;"></div>
        <div>
            <button class= "button is-primary" type="submit" class="btn btn-primary text-theme"><i class="fa fa-save"></i>Save</button>
            <a href="{{ url('manager/members') }}" class="btn btn-default"><i class="fa fa-ban"></i>Cancel</a>
        </div>
    </div>
</div> 
</form>
@endsection