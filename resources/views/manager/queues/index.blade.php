@extends('layouts.manager.layout')

@section('page_styles')
<style type="text/css">
    .topup_details em {
        color: red;
        font-weight: bold;
    }
    .topup_details p {
        max-width: 700px;
        word-break: break-all;
    }
</style>
@endsection

@section('content')
<div class="panel panel-default">
    <div class="panel-heading">
    </div>
    <table class="table">
        <thead>
            <tr>
                <th>Queue</th>
                <th>Payload</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($jobs as $row)
                <tr>
                    <td>{{ $row->queue }}</td>
                    <td><em>{{ $row->payload }}</em></td>
                </tr>
            @endforeach
        </tbody>
    </table>
    <div class="panel-footer text-center">
        {!! $jobs->render() !!}
    </div>
</div>
@endsection
