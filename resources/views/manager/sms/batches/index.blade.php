@extends('layouts.manager.layout')

@section('page_styles')

@endsection

@section('content')
<nav class="breadcrumb" aria-label="breadcrumbs">
  <ul>
   <!--  <li><a href="#">Manager</a></li>
    <li><a href="#">SMS Broadcasts</a></li>
    <li><a href="#">Broadcast #</a></li>
    <li class="is-active"><a href="#" aria-current="page">Batch #</a></li> -->

    @foreach ($breadcrumbs as $breadcrumb)
        <li>{!! $breadcrumb !!}</li>
    @endforeach
  </ul>
</nav>
<h1 class="title">SMS Broadcast</h1>

<div class="columns">
  <div class="column is-one-quarter"><b>Initiated</b></div>
  <div class="column is-one-quarter">{{ $broadcasts->created_at }}</div>
  <div class="column is-one-quarter"><b>Sent From IP</b></div>
  <div class="column is-one-quarter">{{ $broadcasts->sent_ip }}</div>
</div>


<div class="columns">
  <div class="column is-one-quarter"><b>Sender</b></div>
  <div class="column is-one-quarter">{{ $broadcasts->user->name }}</div>
  <div class="column is-one-quarter"><b>bBatches</b></div>
  <div class="column is-one-quarter">{{ $broadcasts->batches }}</div>
</div>

<div class="columns">
  <div class="column is-one-quarter"><b>SMS Messages</b></div>
  <div class="column is-one-quarter">{{ $broadcasts->total_messages }}</div>
  <div class="column is-one-quarter"><b>Template</b></div>
  <div class="column is-one-quarter">{{ $broadcasts->template }}</div>
</div>

<div class="columns">
  <div class="column is-one-quarter"><b>Total Cost</b></div>
  <div class="column is-one-quarter">{{ $broadcasts->total_cost }}</div>
  <div class="column is-one-quarter"><b>Our Cost</b></div>
  <div class="column is-one-quarter">{{ $broadcasts->cost }}</div>
</div>

<div class="columns">
  <div class="column is-one-quarter"><b>Status</b></div>
  <div class="column is-one-quarter"><span class="tag is-{{ $broadcasts->status_format}}">{{ $broadcasts->status }}</span></div>
  <div class="column is-one-quarter"><b>SMS Messages Results</b></div>
  <div class="column is-one-quarter"><span class="tag is-danger">Failed</span>
                        <span class="tag is-success">Success</span></div>
</div>

<div class="panel panel-default">
    <table class="table is-bordered is-narrow">
        <thead>
            <tr>
                <th>Created At</th>
                <th>SMS Messages</th>
                <th>Status</th>
                <th>Provider</th>
                <th>SMS Messages Results</th>
                <th>Actions</th>
                <th>&nbsp;</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($broadcasts->broadcastBatches as $row)
                <tr>
                    <td><span>{{ $row->created_at }}</span></td>
                    <td><span><a href="{{ route('manager.sms.batches.messages', [$row->sms_broadcast_id, $row->id]) }}"> {{ $row->total_messages }}</a></span></td>
                    <td><span>{{ $row->status }}</span></td>
                    <td><span>{{ $row->provider->name }}</span></td>
                    <td>
                        <div class="field is-grouped is-grouped-multiline">
                            <div class="tags has-addons">
                              <span class="tag is-danger">Failed</span>
                              <span class="tag is-dark">{{ $row->failed }}</span>
                            </div>
                            <div class="tags has-addons">
                              <span class="tag is-success">Success</span>
                              <span class="tag is-dark">{{ $row->success }}</span>
                            </div>
                            <span class="tag is-warning">Total</span><span class="tag is-dark">{{ $row->total_messages }}</span>
                        </div>
                    </td>
                    <td>
                    </td>
                    <td class="text-right action-btns">
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>
    <div class="panel-footer">

    </div>
</div>
@endsection
