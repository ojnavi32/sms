@extends('layouts.manager.layout')

@section('page_styles')

@endsection

@section('content')
<nav class="breadcrumb" aria-label="breadcrumbs">
  <ul>
    @foreach ($breadcrumbs as $breadcrumb)
        <li>{!! $breadcrumb !!}</li>
    @endforeach
  </ul>
</nav>

<h1 class="title">SMS Batch</h1>
<div class="columns">
  <div class="column is-one-quarter">Initiated</div>
  <div class="column is-one-quarter">{{ $batches->created_at }}</div>
</div>
<div class="columns">
  <div class="column is-one-quarter">Sender</div>
  <div class="column is-one-quarter">{{ $batches->smsBroadcast->user->name }}</div>
</div>

<div class="columns">
  <div class="column is-one-quarter">SMS Messages</div>
  <div class="column is-one-quarter">{{ $batches->total_messages }}</div>
</div>

<div class="columns">
  <div class="column is-one-quarter">Status</div>
  <div class="column is-one-quarter"><span class="tag is-{{ $batches->status_format}}">{{ $batches->status }}</span></div>
</div>

<div class="panel panel-default">
    <table class="table is-bordered is-narrow">
        <thead>
            <tr>
                <th>ID</th>
                <th>Sent to</th>
                <th>Result</th>
                <th>Cost</th>
                <th>Actions</th>
                <th>&nbsp;</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($messages as $row)
                <tr>
                    <td>{{ $row->id }}</td>
                    <td>{{ $row->send_to }}</td>
                    <td><span class="tag is-{{ $row->status_format}}">{{ $row->status }}</span></td>
                    <td>{{ $row->cost }}</td>
                    <td> </td>
                    <td class="text-right action-btns"></td>
                </tr>
                <tr><td colspan="6"><code>{{ $row->payload }}</code></td></tr>
            @endforeach
        </tbody>
    </table>
    <div class="panel-footer">

    </div>
</div>
@endsection
