@extends('layouts.manager.layout')

@section('page_styles')

@endsection

@section('content')
<nav class="breadcrumb" aria-label="breadcrumbs">
  <ul>
    @foreach ($breadcrumbs as $breadcrumb)
        <li>{!! $breadcrumb !!}</li>
    @endforeach
  </ul>
</nav>
<h1 class="title">SMS Jobs</h1>
<!-- <a href="{{ route('manager.rates.create') }}?provider_id={{ request('provider_id', 1) }}" class="button is-primary">Create</a>

<div class="field is-horizontal">
  <div class="field-label is-normal">
    <label class="label">Select Provider</label>
  </div>
  <div class="field-body">
   
  </div>
</div> -->

<div class="panel panel-default">
    <table class="table is-bordered is-narrow">
        <thead>
            <tr>
                <th>Created At</th>
                <th>Sender</th>
                <th>Cost</th>
                <th>Our Cost</th>
                <th>Status</th>
                <th>Batches</th>
                <th>Messages</th>
                <th>&nbsp;</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($broadcasts as $row)
                <tr>
                    <td>
                        <span><a href="{{ route('manager.sms.batches', $row->id) }}">{{ $row->created_at }}</a></span>
                    </td>
                    <td>
                        <span><a href="{{ route('manager.members.show', $row->user->id) }}">{{ $row->user->name }}</a></span>
                    </td>
                    <td>
                        <span>{{ $row->cost }}</span>
                    </td>
                    <td>
                        <span>{{ $row->total_cost }}</span>
                    </td>
                    <td>
                        <span>{{ $row->status }}</span>
                    </td>
                    <td>
                        <span>{{ $row->batches }}</span>
                    </td>
                    <td>
                        <span class="tag is-danger">Failed {{ $row->failed }}</span>
                        <span class="tag is-success">Success {{ $row->delivered }}</span>
                    </td>
                    <td class="text-right action-btns">
                       
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>
    <div class="panel-footer">
        <div class="pull-right">
            {{$broadcasts->links()}}
        </div>
    </div>
</div>
@endsection
