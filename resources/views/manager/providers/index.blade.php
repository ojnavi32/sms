@extends('layouts.manager.layout')

@section('page_styles')
@endsection

@section('content')
<a href="{{ url('manager/providers/create') }}" class="btn btn-primary">Create</a>
<h1 class="title">Providers</h1>
<table class="table is-bordered is-narrow">
    <thead>
        <tr>
            <th>Name</th>
            <th>Slug</th>
            <!-- <th>SMS Per Second</th> -->
            <th>Rates</th>
            <th>Active</th>
            <th>Actions</th>
        </tr>
    </thead>
    <tbody>
        @foreach ($providers as $provider)
            <tr>
                <td>
                    <span>{{ $provider->name }}</span>
                </td>
                <td>
                    <span>{{ $provider->slug }}</span>
                </td>
                <!-- <td>
                    <span>{{ $provider->sms_second }}</span>
                </td> -->
                <td>
                    <a class="button is-link" href="{{ route('manager.rates') }}?provider_id={{ $provider->id }}">Rates</a>
                    <a class="button" href="{{ route('manager.rates.ratesUpload') }}">Import Rates</a>
                </td>
                <td>
                    <span>{{ ($provider->active ? 'yes' : 'no') }}</span>
                </td>
                <td class="text-right action-btns">
                        <a href="{{ route('manager.providers.edit', $provider->id) }}" class="btn btn-green btn-xs update-btn" title="Update"><i class="fa fa-edit"></i></a>
                        <a href="{{ route('manager.providers.delete', $provider->id) }}" class="btn btn-danger btn-xs delete-btn" title="Delete" onclick="return confirm('Are you sure?')"><i class="fa fa-trash-o"></i></a>
                    </td>
            </tr>
        @endforeach
    </tbody>
</table>
@endsection
