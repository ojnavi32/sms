@extends('layouts.manager.layout')

@section('content')

<form action="{{ route('manager.providers.update', $id) }}" method="POST">

<input type="text" class="form-control" name="name" placeholder="Name" value="{{$provider->name}}">

<input type="text" class="form-control" name="slug" placeholder="Slug" value="{{$provider->slug}}">

<input type="text" class="form-control" name="sms_second" placeholder="SMS Per Second" value="{{$provider->sms_second}}">
{{ csrf_field() }}

    <div class="form-group">
    @yield('providers')
    </div>
    <div>
        <button type="submit" class="btn btn-primary"><i class="fa fa-save"></i>Save</button>
        <a href="{{ route('manager.providers') }}" class="btn btn-default"><i class="fa fa-ban"></i>Cancel</a>
    </div>
</form>
@endsection
