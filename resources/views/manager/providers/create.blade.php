@extends('layouts.manager.layout')

@section('page_styles')

@endsection

@section('content')
                <div class="panel-heading">Providers</div>
                <div class="columns">
                    <form class="form-horizontal" role="form" method="POST" action="{{ route('manager.providers.store') }}" autocomplete="off">
                        {{ csrf_field() }}
                        <div class="column">
                            <label class="control-label col-sm-2">Name</label>
                            <div class="col-md-6">
                                <input class="input" type="text" name="name" placeholder="Enter the name of Provider" value="{{ old('name') }}">
                            </div>
                        </div>

                         <div class="column">
                            <label class="control-label col-sm-2">Slug</label>
                            <div class="col-md-6">
                                <input class="input" type="text" name="slug" placeholder="Enter the Slug" value="{{ old('slug') }}">
                            </div>
                        </div>

                        <div class="column">
                            <label class="control-label col-sm-2">Url</label>
                            <div class="col-md-6">
                                <input class="input" type="text" name="url" placeholder="Enter the Url" value="{{ old('url') }}">
                            </div>
                        </div>

                        <div class="column">
                            <label class="control-label col-sm-2">Currency</label>
                            <div class="col-md-6">
                                <input class="input" type="text" name="currency" placeholder="Enter the Currency" value="{{ old('currency') }}">
                            </div>
                        </div>
                       
                        <div class="column">
                            <div class="col-md-8 col-md-offset-4">
                                <button type="submit" class="button is-primary">
                                    Submit
                                </button>
                                <button type="button" class="button is-primary" onclick="window.location='{{ route("manager.providers") }}'">Providers Lists</button>
                            </div>
                        </div>
                    </form>
                </div>

@endsection
