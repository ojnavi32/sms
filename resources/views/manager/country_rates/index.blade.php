@extends('layouts.manager.layout')

@section('page_styles')

@endsection

@section('content')
<h1 class="title">Country Master Rates</h1>
<div class="row">
    <div class="col-md-12">
        <div class="pull-left">
            <a href="{{ route('manager.country-rates.create') }}" class="btn btn-primary">Create</a> 
        </div>
        <div class="pull-right">
            <a href="{{ route('manager.ratesUpload') }}" class="btn btn-primary">Import Rates</a>
        </div><br/><br/>

        <form method="GET" action="">
            <input class="form-control" type="text" name="country" value="" placeholder="Country" >
            <br/>
            <button class="btn btn-primary">Search</button>
        </form>
    </div>

        <div class="panel panel-default col-md-12">
            <table class="table is-bordered is-narrow table-responsive">
                <thead>
                    <tr>
                        <th>Dial Code</th>
                        <th>Country</th>
                        <th>Cost Per SMS</th>
                        <th>Currency</th>
                        <th>&nbsp;</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($rates as $rate)
                        <tr>
                            <td>
                                <span>{{ $rate->country_dial_code_id }}</span>
                            </td>
                            <td>
                                <span>{{ $rate->name }}</span>
                            </td>
                            <td>
                                <span>{{ $rate->base_rate }}</span>
                            </td>
                            <td>
                                <span>Euro</span>
                            </td>
                            <td class="text-right action-btns">
                                <a href="{{ route('manager.country-rates.edit', $rate->id) }}" class="btn btn-green btn-xs update-btn" title="Update"><i class="fa fa-edit"></i></a>
                                <a href="{{ route('manager.country-rates.providers',$rate->id) }}" class="btn btn-green btn-xs" title="View Providers"><i class="fa fa-chain"></i></a>
                                <a href="{{ route('manager.country-rates.delete', $rate->id) }}" class="btn btn-danger btn-xs delete-btn" title="Delete" onclick="return confirm('Are you sure?')"><i class="fa fa-trash-o"></i></a>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>    
        <div class="panel-footer text-center"
            {{ $rates->links() }}
        </div>    
    </div>
</div>
@endsection
