@extends('layouts.manager.layout')

@section('page_styles')

@endsection

@section('content')
<h1 class="title">Providers</h1>

<div class="columns">
    <form>
    {{ csrf_field() }}
        <div class="field is-narrow">
            <div class="control">
                <div class="select">
                    <select name="provider_select" id="provider_select" class="input">
                        @foreach(App\Models\Provider::all() as $provider)
                        <option value="{{ $provider->id }}">{{ $provider->name }}</option>
                        @endforeach
                    </select>
                </div>
                <button type="submit" class="button is-primary btn-submit">
                    Add provider
                </button>
                <input type="hidden" id="rate_id" value="{{$rate_id}}">
            </div>
        </div>
    </form>
</div>

<div class="panel panel-default">
    <table class="table is-bordered is-narrow">
        <thead>
            <tr>
                <th>Priority</th>
                <th>Name</th>
                <th>&nbsp;</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($providers as $provider)
                <tr>
                    <td>
                       <span>{{ $provider->priority}}</span>
                    </td>    
                    <td>
                        <span>{{ $provider->provider['name']}}</span>
                    </td>
                    <td>
                        <a href="{{ route('manager.country-rates.delete-provider', $provider->id) }}" class="btn btn-danger btn-xs delete-btn" title="Delete" onclick="return confirm('Are you sure?')"><i class="fa fa-trash-o"></i></a>
                        @if($provider->priority != 1)
                        <a href="#" class="btn btn-green btn-xs move-up-btn" title="Move Up" data-rec-id="{{ $provider->id }}"><i class="fa fa-sort-up"></i></a>
                        @endif
                    </td>    
                </tr>
            @endforeach
        </tbody>
    </table>

    <div class="panel-footer">
        <div class="pull-right">
          
        </div>
    </div>
</div>

{!! Form::open(['id' => 'form-action']) !!}
    <input type="hidden" name="action">
    <input type="hidden" name="rate_provider_priority_id">
</form>
@endsection

@section('page_scripts')
<script type="text/javascript">
        $(document).ready(function() { 
        $(document).on('click', 'a.move-up-btn',function(e){ 

            e.preventDefault();
            $.executeAction('move_up', $(this).data('recId'))
        });
        
    });

    $.executeAction = function(action, rate_provider_priority_id){ 
        $('#form-action').find('input[name=action]').val(action);
        $('#form-action').find('input[name=rate_provider_priority_id]').val(rate_provider_priority_id);
        $('#form-action').submit();
    };
</script>
@endsection


 
