@extends('layouts.manager.layout')

@section('content')
<div class="columns">
    <form class="form-horizontal" role="form" method="POST" action="{{ route('manager.country-rates.postCreate') }}">
        {{ csrf_field() }}
        <div class="column">
            <label class="control-label col-sm-2">COUNTRY DIAL CODE ID</label>
            <div class="col-md-6">
                <input type="text" class="input" id="code" name="code" placeholder="Enter Dial Code Id">
            </div>
        </div>
        <div class="column">
            <label class="control-label col-sm-2">COUNTRY</label>
            <div class="col-md-6">
                <input type="text" class="input" id="country" name="country" placeholder="Enter Country">
            </div>
        </div>
        <div class="column">
            <label class="control-label col-sm-2">COST PER SMS</label>
            <div class="col-md-6">
                <input type="text" class="input" id="cost" name="cost" placeholder="Enter Cost Per SMS">
            </div>
        </div>
        <div class="column">
            <div class="col-sm-offset-2 col-sm-10">
                <button type="submit" class="button is-primary"><i class="fa fa-save"></i>Create</button>
            </div>
        </div>    
        
       </div> 
    </form>
</div>
@endsection
