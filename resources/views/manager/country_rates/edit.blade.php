@extends('layouts.manager.layout')

@section('content')
<div class="col-md-12">
    <div class="row">
        <form class="form-horizontal" role="form" method="POST" action="{{ route('manager.country-rates.update',$rates->id) }}">
            {{ csrf_field() }}
            <div class="col-md-12">
                <label class="control-label col-sm-2">COUNTRY DIAL CODE ID</label>
                <div class="col-md-6">
                    <input type="text" class="input form-control" id="code" name="code" placeholder="Enter Dial Code Id" value="{{ @$rates->country_dial_code_id }}">
                </div>
            </div>
            <div class="col-md-12">
                <label class="control-label col-sm-2">COUNTRY</label>
                <div class="col-md-6">
                    <input type="text" class="input form-control" id="country" name="country" placeholder="Enter Country" value="{{ @$rates->name }}">
                </div>
            </div>
            <div class="col-md-12">
                <label class="control-label col-sm-2">COST PER SMS</label>
                <div class="col-md-6">
                    <input type="text" class="input form-control" id="cost" name="cost" placeholder="Enter Cost Per SMS" value="{{ @$rates->base_rate }}">
                </div>
            </div>
            <div class="col-md-12">
                <div class="col-sm-offset-2 col-sm-10">
                    <button type="submit" class="btn btn-primary"><i class="fa fa-save"></i>Update</button>
                </div>
            </div>    
            
           </div> 
        </form>
    </div>    
</div>
@endsection
