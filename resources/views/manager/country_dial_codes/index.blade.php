@extends('layouts.manager.layout')

@section('page_styles')

@endsection

@section('content')
<h1 class="title">Country Dial Codes</h1>
<div class="panel panel-default">
    <table class="table is-bordered is-narrow">
        <thead>
            <tr>
                <th>Country</th>
                <th>Slug</th>
                <th>A2 Code</th>
                <th>A3 Code</th>
                <th>Dial Code</th>
                <th>Exchange Rate</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($country_dial_codes as $country_dial_code)
                <tr>
                    <td>
                        <span>{{ $country_dial_code->country }}</span>
                    </td>
                    <td>
                        <span>{{ $country_dial_code->slug }}</span>
                    </td>
                    <td>
                        <span>{{ $country_dial_code->a2_code }}</span>
                    </td>
                    <td>
                        <span>{{ $country_dial_code->a3_code }}</span>
                    </td>
                    <td>
                        <span>{{ $country_dial_code->dial_code }}</span>
                    </td>
                    <td>
                        <span>
                            @if ($country_dial_code->exchange_rate)
                                <a href="{{ url('manager/exchange_rates#' . $country_dial_code->exchange_rate->currency_code) }}">{{ $country_dial_code->exchange_rate->currency_code . ' ' . @$country_dial_code->exchange_rate->rate }}</a>
                            @endif
                        </span>
                        <a href="{{ url('manager/country_dial_codes/edit-exchange-rate/'.$country_dial_code->id) }}" class="btn btn-xs update-btn" title="Change Exchange Rate"><i class="fa fa-edit"></i></a>
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>
    <div class="panel-footer">

    </div>
</div>
@endsection
