@extends('layouts.manager.layout')

@section('content')
{!! Form::open(['class' => 'form-horizontal form-bg text-theme']) !!}
    <div class="form-group">
        <label for="cash_balance" class="control-label col-sm-2">Exchange Rate</label>
        <div class="col-sm-10">
            {!! Form::select('exchange_rate_id', $exchange_rates, @$exchange_rate, ['class' => 'form-control']) !!}
        </div>
    </div>
    <div class="form-group">
        <div class="col-sm-offset-2 col-sm-10">
            <button type="submit" class="btn btn-primary text-theme"><i class="fa fa-save"></i>Set Exchange Rate</button>
            <a href="{{ url('manager/country_dial_codes') }}" class="btn btn-default"><i class="fa fa-ban"></i>Cancel</a>
        </div>
    </div>
</form>
@endsection
