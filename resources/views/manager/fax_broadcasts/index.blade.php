@extends('layouts.manager.layout')

@section('page_styles')
<style type="text/css">
    #fax-jobs-list thead tr th:first-child {
        width: 126px;
    }
    #fax-jobs-list td {
        vertical-align: middle;
    }
    .fax-statuses span.label {
        padding: 4px 6px;
    }
    .fax-statuses span.label:not(:last-child) {
        margin-right: 4px;
    }
    .fax-statuses span.label span.badge {
        margin-left: 3px;
    }
    .label.label-failed {
        background-color: #d9534f;
    }
    .label.label-executed {
        background-color: #f0ad4e;
    }
</style>
@endsection

@section('content')
<div class="panel panel-default">
    <table id="fax-jobs-list" class="table">
        <thead>
            <tr>
                <th>Created At</th>
                <th>Sender</th>
                <th class="text-center">Fax Jobs</th>
                <th>&nbsp;</th>
                <th class="text-center">&nbsp;</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($fax_broadcasts as $fax_broadcast)
            <tr>
                <td>{{ $fax_broadcast->created_at }}</td>
                <td>
                    @if ($fax_broadcast->user)
                    {{ $fax_broadcast->user->name }}<br>
                    <a href="mailto:{{ $fax_broadcast->user->email }}" class="small">&lt;{{ $fax_broadcast->user->email }}&gt;</a>
                    @endif
                </td>
                <td class="text-center">{{ $fax_broadcast->fax_jobs->count() }}</td>
                <td class="fax-statuses">
                    <span class="label label-executed">executed <span class="badge">{{ $fax_broadcast->fax_jobs()->whereStatus('executed')->count() }}</span></span>
                    <span class="label label-failed">failed <span class="badge">{{ $fax_broadcast->fax_jobs()->whereStatus('failed')->count() }}</span></span>
                    <span class="label label-success">success <span class="badge">{{ $fax_broadcast->fax_jobs()->whereStatus('success')->count() }}</span></span>
                </td>
                <td class="text-center">
                    <a href="{{ url('manager/fax-broadcasts/view/'.$fax_broadcast->id) }}" class="btn btn-green btn-xs" title="View Details"><span class="fa fa-search-plus"></span></a>
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
    <div class="panel-footer text-center">
    </div>
</div>
@endsection


@section('page_scripts')
@endsection