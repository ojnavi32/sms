@extends('layouts.manager.layout')

@section('page_styles')
<style type="text/css">
    #fax-jobs-list thead tr th:first-child {
        width: 126px;
    }
    #fax-jobs-list td {
        vertical-align: middle;
    }
    .fax-statuses span.label {
        margin: auto 4px;
        padding: 4px 6px;
    }
    .fax-statuses span.label span.badge {
        margin-left: 3px;
    }
    .label.label-failed {
        background-color: #d9534f;
    }
    .label.label-executed,
    .label.label-requeued,
    .label.label-blocked {
        background-color: #f0ad4e;
    }
</style>
@endsection

@section('content')
<div class="panel panel-default">
    <div class="panel-heading">Fax Broadcast</div>
    <div class="panel-body">
        <form class="form-horizontal">
            <div class="form-group">
                <label class="control-label col-sm-2">Created</label>
                <div class="col-sm-10">
                    <p class="form-control-static">{{ $fax_broadcast->created_at }}</p>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-2">Sender</label>
                <div class="col-sm-10">
                    <p class="form-control-static">
                        {{ $fax_broadcast->user->name }}<br>
                        <a href="mailto:{{ $fax_broadcast->user->email }}" class="small">&lt;{{ $fax_broadcast->user->email }}&gt;</a>
                    </p>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-2">Statuses</label>
                <div class="col-sm-10">
                    <p class="form-control-static fax-statuses">
                        <span class="label label-executed">executed <span class="badge">{{ $executing_faxes }}</span></span>
                        <span class="label label-failed">failed <span class="badge">{{ $failed_faxes }}</span></span>
                        <span class="label label-success">success <span class="badge">{{ $successful_faxes }}</span></span>
                    </p>
                </div>
            </div>
        </form>
    </div>
    <table id="fax-jobs-list" class="table">
        <thead>
            <tr>
                <th>Created At</th>
                <th>Document</th>
                <th>Recipient</th>
                <th class="text-center">Status</th>
                <th>&nbsp;</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($fax_jobs as $fax_job)
            <tr>
                <td>{{ $fax_job->created_at }}</td>
                <td>{!! @$fax_job->document->filename_display ?: '<em style="color:red">deleted</em>' !!}</td>
                <td>{{ $fax_job->fax_number }}</td>
                <td class="text-center">
                    <span class="label label-{{ $fax_job->status }}">{{ $fax_job->status }}</span>
                    @if ($fax_job->status == 'success' && ($fax_job->cost != 0))
                    <br><small><em>Deducted <strong>&euro;{{ $fax_job->cost }}</strong></em></small>
                    @elseif ($fax_job->status == 'failed')
                    <br><small><em>{{ $fax_job->message }}</em></small>
                    @endif
                </td>
                <td><a href="{{ url('manager/fax-broadcasts/view/'.$fax_broadcast->id.'/fax-job/'.$fax_job->id) }}" class="btn btn-green btn-xs" title="View Details"><span class="fa fa-search-plus"></span></a></td>
            </tr>
            @endforeach
        </tbody>
    </table>
    <div class="panel-footer text-center">
        {!! $fax_jobs->render() !!}
    </div>
</div>
@endsection


@section('page_scripts')
@endsection