@extends('layouts.manager.layout')

@section('page_styles')
<style type="text/css">
    .btn-xs .fa {
        margin-right: 0;
    }
    #priority-list tbody tr:first-child .action-btns a:nth-child(1),
    #priority-list tbody tr:last-child .action-btns a:nth-child(2) {
        display: none;
    }
    #priority-list tbody tr:first-child .action-btns a:nth-child(2),
    #priority-list tbody tr:last-child .action-btns a:nth-child(1) {
        margin-right: 10px;
    }
</style>
@endsection

@section('content')
<div class="clearfix mb-20">
    <h3 class="pull-left" style="margin:0"><a href="{{ url('manager/rates/update/'.$rate->id) }}">{{ $rate->name }}</a> Providers</h3>
    <div class="pull-right">
        {!! Form::open(['class' => 'form-inline']) !!}
            <input type="hidden" name="action" value="add">
            {!! Form::select('rate_provider', $comm_providers, @$rate_provider, ['class' => 'form-control']) !!}
            <button type="submit" class="btn btn-primary">Add Provider</button>
        </form>
    </div>
</div>
<div class="panel panel-default">
    <table id="priority-list" class="table">
        <thead>
            <tr>
                <th>Priority</th>
                <th>Provider</th>
                <th>&nbsp;</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($rate->provider_priorities()->orderBy('priority')->get() as $provider)
            <tr>
                <td>
                    <span>{{ Html::ordinal($provider->pivot->priority) }}</span>
                </td>
                <td>
                    <span>{{ $provider->name }}</span>
                </td>
                <td class="text-right action-btns">
                    <a href="#" class="btn btn-green btn-xs move-up-btn" title="Move Up" data-rec-id="{{ $provider->pivot->id }}"><i class="fa fa-sort-up"></i></a>
                    <!-- <a href="#" class="btn btn-green btn-xs move-down-btn" title="Move Down" data-rec-id="{{ $provider->pivot->id }}"><i class="fa fa-sort-down"></i></a> -->
                    <a href="#" class="btn btn-danger btn-xs delete-btn" title="Delete" data-rec-id="{{ $provider->pivot->id }}"><i class="fa fa-trash-o"></i></a>
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
    <div class="panel-footer">

    </div>
</div>

{!! Form::open(['id' => 'form-action']) !!}
    <input type="hidden" name="action">
    <input type="hidden" name="rate_provider_priority_id">
</form>
@endsection

@section('page_scripts')
<script type="text/javascript">
    $(function(){
        $('a.move-up-btn').on('click', function(e){
            e.preventDefault();
            $.executeAction('move_up', $(this).data('recId'))
        });
        $('a.move-down-btn').on('click', function(e){
            e.preventDefault();
            $.executeAction('move_down', $(this).data('recId'))
        });

        $('a.delete-btn').on('click', function(e){
            e.preventDefault();
            $.executeAction('delete', $(this).data('recId'))
        });
    });

    $.executeAction = function(action, rate_provider_priority_id){
        $('#form-action').find('input[name=action]').val(action);
        $('#form-action').find('input[name=rate_provider_priority_id]').val(rate_provider_priority_id);
        $('#form-action').submit();
    };
</script>
@endsection
