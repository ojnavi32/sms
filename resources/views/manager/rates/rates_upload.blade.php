@extends('layouts.manager.layout')

@section('content')


{!! Form::open(array('route' => 'manager.rates.rateFileUpload','enctype' => 'multipart/form-data')) !!}
<h1 class="title">Rates Upload</h1>
  <div class="column">
    <div class="col-md-4">
      {!! Form::file('csv', array('class' => 'image, button is-primary')) !!}
    </div>
  </div>

  <h1 class="title">Select Provider</h1>
  <div class="field is-narrow">
      <div class="control">
        <div class="select">
        <select name="provider_id" class="input" required>
            @foreach(App\Models\Provider::all() as $provider)
            <option value="{{ $provider->id }}" {{ (request('provider_id') == $provider->id ? 'selected' : '') }}>{{ $provider->name }}</option>
            @endforeach
        </select>
      </div>
      </div>
  </div>

  <div class="col-md-2" style="height:20px;"></div>
    <div class="col-md-4">
      <button type="submit" class="button is-primary">Create</button>
  </div>
{!! Form::close() !!}
@endsection
