@extends('layouts.manager.layout')

@section('page_styles')

@endsection

@section('content')
<h1 class="title">Carrier Rates</h1>
<!-- <a href="{{ route('manager.rates.create') }}?provider_id={{ request('provider_id', 1) }}" class="button is-primary">Create</a> -->
<div class="pull-right">
<a href="{{ route('manager.rates.ratesUpload') }}" class="btn btn-primary">Import Rates</a>
</div>
<div class="col-md-12">
<form>
  <div class="field is-narrow">
      <div class="control">
        <div class="select col-md-6">
          <select name="country" class="input form-control" onchange="this.form.submit()">
            <option value="">Select Country</option>
            @foreach(App\Models\Rate::all() as $rate)
            <option value="{{ $rate->name }}" {{ (request('country') == $rate->name ? 'selected' : '') }}>{{ $rate->name }}</option>
            @endforeach
          </select>
        </div>

        <div class="select col-md-6">
          <select name="provider_id" class="input form-control" onchange="this.form.submit()">
            <option value="">Select Provider</option>
            @foreach(App\Models\Provider::all() as $provider)
            <option value="{{ $provider->id }}" {{ (request('provider_id') == $provider->id ? 'selected' : '') }}>{{ $provider->name }}</option>
            @endforeach
          </select>
        </div>
      </div>
   </div>
</form>
</div>

<div class="panel panel-default">
    <table class="table is-bordered is-narrow">
        <thead>
            <tr>
                <th>Dial Code</th>
                <th>Country</th>
                <th>Network Name</th>
                <th>Cost Per SMS</th>
                <th>Currency</th>
                <th>&nbsp;</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($rates as $rate)
                <tr>
                    <td>
                        <span>{{ $rate->rate->country_dial_code_id }}</span>
                    </td>
                    <td>
                        <span>{{ $rate->rate->name }}</span>
                    </td>
                    <td>
                        <span>{{ $rate->network }}</span>
                    </td>
                    <td>
                        <span>{{ $rate->base_rate }}</span>
                    </td>
                    <td>
                        <span>{{ $rate->provider->currency }}</span>
                    </td>
                    <td class="text-right action-btns">
                        <a href="{{ route('manager.rates.edit', $rate->id) }}?provider_id={{ $provider_id }}" class="btn btn-green btn-xs update-btn" title="Edit"><i class="fa fa-edit"></i></a>
                        <a href="{{ route('manager.rates.delete', $rate->id) }}?provider_id={{ $provider_id }}" class="btn btn-danger btn-xs delete-btn" title="Delete" onclick="return confirm('Are you sure?')"><i class="fa fa-trash-o"></i></a>
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>
    <div class="panel-footer">
        <div class="pull-right">
            {{ $rates->appends(['country'=>request('country'),'provider_id'=>request('provider_id')])->links() }}
        </div>
    </div>
</div>
@endsection
