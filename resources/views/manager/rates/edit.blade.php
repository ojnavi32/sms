@extends('layouts.manager.layout')

@section('content')
<div class="columns">
    <form class="form-horizontal" role="form" method="POST" action="{{ route('manager.rates.update',$providerRates->id) }}">
        {{ csrf_field() }}
        <div class="column">
            <label class="control-label col-sm-2">COUNTRY DIAL CODE ID</label>
            <div class="col-md-6">
                <input type="text" class="input" id="code" name="code" placeholder="Enter Dial Code Id" value="{{ @$providerRates->rate->country_dial_code_id }}">
            </div>
        </div>
        <div class="column">
            <label class="control-label col-sm-2">COUNTRY</label>
            <div class="col-md-6">
                <input type="text" class="input" id="country" name="country" placeholder="Enter Country" value="{{ @$providerRates->rate->name }}">
            </div>
        </div>
        <div class="column">
            <label class="control-label col-sm-2">NETWORK NAME</label>
            <div class="col-md-6">
                <input type="text" class="input" id="network" name="network" placeholder="Enter Network Name" value="{{ @$providerRates->network }}">
            </div>
        </div>
        <div class="column">
            <label class="control-label col-sm-2">COST PER SMS</label>
            <div class="col-md-6">
                <input type="text" class="input" id="cost" name="cost" placeholder="Enter Cost Per SMS" value="{{ @$providerRates->base_rate }}">
            </div>
        </div>
        <div class="column">
            <div class="col-sm-offset-2 col-sm-10">
                <button type="submit" class="button is-primary"><i class="fa fa-save"></i>Update</button>
            </div>
        </div>    
        
       </div> 
    </form>
</div>
@endsection
