@extends('layouts.manager.layout')

@section('content')

<form action="{{ route('manager.rates.update', @$id) }}" method="POST">
<div class="field is-horizontal">
  <div class="field-label is-normal">
    <label class="label">Provider</label>
  </div>
  <div class="field-body">
    <div class="field is-narrow">
      <div class="control">
        {{ $provider->name }}
        <input type="hidden" name="provider_id" value="{{ @$provider_id }}">
        {{ csrf_field() }}
      </div>
    </div>
  </div>
</div>

<div class="field is-horizontal">
  <div class="field-label is-normal">
    <label class="label">Country</label>
  </div>
  <div class="field-body">
    <div class="field is-narrow">
      <div class="control">
      <div class="select">
        {!! Form::select('country_dial_code_id', $country_dial_codes->pluck('CountryCode', 'id')->all(), @$country_dial_code_id, ['id' => 'country_dial_code_id', 'class' => 'form-control']) !!}
      </div>
    </div>
    </div>
    <!-- manager.rates.edit -->
  </div>
</div>
<div class="field is-horizontal">
  <div class="field-label is-normal">
    <label class="label">Name</label>
  </div>
  <div class="field-body">
    <div class="field is-narrow">
      <div class="control">
        <input class="input" name="name" id="name" placeholder="Rate Name" type="text" value="{{ @$name }}">
      </div>
    </div>
  </div>
</div>

<div class="field is-horizontal">
  <div class="field-label is-normal">
    <label class="label">Plan</label>
  </div>
  <div class="field-body">
    <div class="field is-narrow">
      <div class="control">
        <input class="input" name="plan" id="plan" placeholder="Plan" type="text" value="{{ @$plan }}">
      </div>
    </div>
  </div>
</div>

<div class="field is-horizontal">
  <div class="field-label is-normal">
    <label class="label">Provider Rate</label>
  </div>
  <div class="field-body">
    <div class="field is-narrow">
      <div class="control">
        <input class="input" name="base_rate" id="base_rate" placeholder="Base Rate" type="text" value="{{ @$base_rate }}">
      </div>
    </div>
  </div>
</div>


<div class="field is-horizontal">
  <div class="field-label is-normal">
    <label class="label">Our Rate</label>
  </div>
  <div class="field-body">
    <div class="field is-narrow">
      <div class="control">
        <input class="input" name="our_rate" id="our_rate" placeholder="Our Rate" type="text" value="{{ @$our_rate }}">
      </div>
    </div>
  </div>
</div>

<div class="field is-horizontal">
  <div class="field-label is-normal">
    <label class="label">Credits Per SMS</label>
  </div>
  <div class="field-body">
    <div class="field is-narrow">
      <div class="control">
        <input class="input" name="credits" id="credits" placeholder="Credits" type="text" value="{{ @$credits }}">
      </div>
    </div>
  </div>
</div>

{{ bulma_text('sms_second', $sms_second, 'SMS Per Second') }}

<!-- <div class="field is-horizontal">
  <div class="field-label is-normal">
    <label class="label">Multiplier</label>
  </div>
  <div class="field-body">
    <div class="field is-narrow">
      <div class="control">
        <input class="input" name="multiplier" id="multiplier" placeholder="Multiplier" type="text" value="{{ @$multiplier }}">
      </div>
    </div>
  </div>
</div> -->

    <div class="form-group">
    @yield('providers')
    </div>
    <div>
        <button type="submit" class="button is-primary"><i class="fa fa-save"></i>Save</button>
        <a href="{{ url('manager/rates') }}" class="button is-link"><i class="fa fa-ban"></i>Cancel</a>
    </div>
</form>
@endsection
