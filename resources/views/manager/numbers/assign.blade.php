@extends('layouts.manager.layout')

@section('content')
{!! Form::open(['class' => 'form-bg text-theme']) !!}
    <div class="form-group">
        <label for="">Number</label>
        @if(count($numbers) != 0)
            {{ Form::select('numbers', $numbers)}}
        @else
            <a href="{{ url('manager/dids/provision', $user_id) }}" class="btn btn-success text-theme"><i class="fa fa-check"></i>Provision</a>
        @endif
    </div>
    <div class="form-group">
        <label for="name">Name the Number</label>
        <input class="form-control" name="name" id="name" placeholder="" type="text">
    </div>
    <div class="form-group">
        <label for="name">Email</label>
        <input class="form-control" name="email" id="email" placeholder="" type="text">
    </div>
    <div class="form-group">
        <label for="Status">Status</label>
        {{ Form::select('status', [1 => 'Active', '5' => 'Inactive'])}}
    </div>
    <!-- <div class="form-group">
        <label for="expiration_date">Expiration Date</label>
        <input class="form-control" name="expiration_date" id="expiration_date" placeholder="Expiration Date" type="text" value="">
    </div> -->
    <div class="form-group">
        <label for="Status">Contract Term</label>
        {{ Form::select('contract_type', ['3m' => '3 Months', '512' => '12 Months']) }}
    </div>
    <div class="form-group">
        <label for="cash_balance_sub">Subscription Credit Allowance</label>
        <input class="form-control" name="cash_balance_sub" id="cash_balance_sub" placeholder="" type="text" value="">
    </div>
    <div class="form-group">
        <label for="monthly_cash">Recurring Cash Balance</label>
        <input class="form-control" name="monthly_cash" id="monthly_cash" placeholder="" type="text" value="">
    </div>
    <div>
        <button type="submit" class="btn btn-primary text-theme"><i class="fa fa-save"></i>Save</button>
        <input type="hidden" name="user_id" value="{{ $user_id }}">
        <a href="{{ url('manager/members/view', $user_id) }}" class="btn btn-default"><i class="fa fa-ban"></i>Cancel</a>
    </div>
</form>
<hr>
<div>
</div>
@endsection

@section('page_scripts')
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/js/bootstrap-datepicker.min.js"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/css/bootstrap-datepicker3.min.css">
<script type="text/javascript">
$(function(){
	$('#expiration_date').datepicker({
	    format: 'yyyy-mm-dd'
	});
});
</script>
@endsection


