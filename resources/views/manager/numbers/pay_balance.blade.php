@extends('layouts.manager.layout')

@section('content')
{!! Form::open(['class' => 'form-bg text-theme']) !!}
    <div class="form-group">
        <label for="">Number: </label>
        {{ $number->did_number }}
    </div>
    <div class="form-group">
        <label for="Status">Months: </label>
        <select name="contract_type">
        @foreach(config('voxbone.contract_types') as $contract)
        <option value="{{ $contract['type'] }}">{{ $contract['label'] . ' - €' . $contract['price'] }}</option>
        @endforeach
        </select>
    </div>
    <div class="form-group">
        <label for="">User Cash Balance: </label>
        €{{ $number->user->cash_balance }}
    </div>
    <div>
        <button type="submit" class="btn btn-primary text-theme"><i class="fa fa-save"></i>Save</button>
        <a href="{{ url('manager/members/view', $user_id) }}" class="btn btn-default"><i class="fa fa-ban"></i>Cancel</a>
    </div>
</form>
<hr>
<div>

@if ($number->payment_failed == 1)
<a href="{{ url('manager/members/numbers/retry-charge', $number->id) }}" class="btn btn-success text-theme"><i class="fa fa-check"></i>Retry Payment</a>
@endif
</div>
@endsection

@section('page_scripts')
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/js/bootstrap-datepicker.min.js"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/css/bootstrap-datepicker3.min.css">
<script type="text/javascript">
$(function(){
	$('#expiration_date').datepicker({
	    format: 'yyyy-mm-dd'
	});
});
</script>
@endsection


