@extends('layouts.manager.layout')

@section('content')
{!! Form::open(['class' => 'form-bg text-theme']) !!}
    <div class="form-group">
        <label for="">Number</label>
        {{ $number->did_number }}
    </div>
    <!-- <div class="form-group">
        <label for="">Payment Status</label>
        {{ $number->payment_status }}
    </div> -->
    <div class="form-group">
        <label for="Status">Status</label>
        {{ Form::select('status', [1 => 'Active', '5' => 'Inactive'], $number->status) }}
    </div>
    <div class="form-group">
        <label for="payment_failed">Payment Failed</label>
        {{ Form::checkbox('payment_failed', 1, $number->payment_failed) }}
    </div>
    <div class="form-group">
        <label for="expiration_date">Expiration Date</label>
        <input class="form-control" name="expiration_date" id="expiration_date" placeholder="Expiration Date" type="text" value="{{ $number->expiration_date }}">
    </div>
    <div class="form-group">
        <label for="cash_balance_sub">Subscription Credit Allowance</label>
        <input class="form-control" name="cash_balance_sub" id="cash_balance_sub" placeholder="" type="text" value="{{ $number->user->userCashBalance()->sum('amount') }}">
    </div>
    <div class="form-group">
        <label for="monthly_cash">Recurring Cash Balance</label>
        <input class="form-control" name="monthly_cash" id="monthly_cash" placeholder="" type="text" value="{{ $number->user->monthly_cash }}">
    </div>
    <div>
        <button type="submit" class="btn btn-primary text-theme"><i class="fa fa-save"></i>Save</button>
        <a href="{{ url('manager/members/view', $user_id) }}" class="btn btn-default"><i class="fa fa-ban"></i>Cancel</a>
    </div>
</form>
<hr>
<div>
<a class="btn btn-danger text-theme" data-rec-id="{{ $number->id }}" data-toggle="modal" data-target="#cancelSubscriptionConfirm"><i class="fa fa-ban"></i>Cancel Subscription</a><br><br>
<a class="btn btn-danger text-theme" data-rec-id="{{ $number->id }}" data-toggle="modal" data-target="#disconnectConfirm"><i class="fa fa-trash"></i>Delete Subscription</a>
@if ($number->payment_failed == 1)
<a href="{{ url('manager/members/numbers/retry-charge', $number->id) }}" class="btn btn-success text-theme"><i class="fa fa-check"></i>Retry Payment</a>
@endif
</div>
@endsection

@section('page_scripts')
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/js/bootstrap-datepicker.min.js"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/css/bootstrap-datepicker3.min.css">
<script type="text/javascript">
$(function(){
	$('#expiration_date').datepicker({
	    format: 'yyyy-mm-dd'
	});
});
</script>
@endsection


