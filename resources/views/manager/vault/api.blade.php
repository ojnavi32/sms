@extends('layouts.manager.layout')

@section('page_styles')
<style type="text/css">
    .btn-xs .fa {
        margin-right: 0;
    }
</style>
@endsection

@section('content')

<div class="panel-heading">
        {!! Form::open(['id' => 'list-filter-form', 'method' => 'GET', 'class' => 'form-horizontal form-bg text-theme']) !!}
            <div class="form-group">
                <label for="inputPassword" class="control-label col-sm-2">Number</label>
                <div class="col-sm-4">
                    <input type="text" name="number" value="{{ request('number') }}" placeholder="Number" class="form-control">
                    <input type="hidden" name="status" value="{{ request('status') }}">
                </div>
                @if(request('status') == 'active')
                <div class="col-sm-4">
                    <input value="canceled" type="checkbox" name="tags[]" id="5" {{ (in_array('canceled', request('tags', [])) ? 'checked' : '') }}>
                        <label for="5">Canceled</label><br>
                    <input value="payment_failed" type="checkbox" name="tags[]" id="8" {{ (in_array('payment_failed', request('tags', [])) ? 'checked' : '') }}>
                        <label for="8">Unpaid</label><br>
                    <input value="expired" type="checkbox" name="tags[]" id="9" {{ (in_array('expired', request('tags', [])) ? 'checked' : '') }}>
                        <label for="9">Expired</label><br>
                </div>
                @endif
            </div>
            <div class="form-group">
                <div class="col-sm-10 col-sm-offset-2">
                    <button class="btn btn-primary" name="search" value="search">Search</button>
                </div>
            </div>
        </form>

        <div class="mt-10 text-right">
            @if(request('status') == 'expired' or request('status') == 'active')
                <button id="btn-free" class="btn btn-green">Free</button>
            @endif
            <button id="btn-download-csv" class="btn btn-green">Download .CSV of Numbers</button>
        </div>
    </div>

<div class="btn-group btn-group-justified" role="group" aria-label="...">
  <div class="btn-group" role="group">
    <a href="?status=active" class="btn btn-default {{ (request('status') == 'active') ? 'active': '' }}">Active<br> Numbers</a>
  </div>
  <div class="btn-group" role="group">
    <a href="?status=canceled" class="btn btn-default {{ (request('status') == 'canceled') ? 'active': '' }}">Canceled<br> Numbers</a>
  </div>
  <div class="btn-group" role="group">
    <a href="?status=payment_failed" class="btn btn-default {{ (request('status') == 'payment_failed') ? 'active': '' }}">Payment<br> Failed</a>
  </div>
  <div class="btn-group" role="group">
    <a href="?status=expired" class="btn btn-default {{ (request('status') == 'expired') ? 'active': '' }}">Disconnected/<br>Expired</a>
  </div>
<!--   <div class="btn-group" role="group">
    <a href="?status=deleted" class="btn btn-default {{ (request('status') == 'deleted') ? 'active': '' }}">Free<br>Numbers</a>
  </div> -->
</div>

@if(request('status') == 'expired' or request('status') == 'active')
    <input id="select_all" class="select_all" name="select_all" type="checkbox" value="1"> <label for="select_all">Select All</label>
@endif
<div class="panel panel-default">
    {!! Form::open(['id' => 'vault-form', 'method' => 'POST', 'class' => 'form-horizontal form-bg text-theme']) !!}
    <table class="table">
        <thead>
            <tr>
                @if(request('status') == 'expired' or request('status') == 'active')
                <th></th>
                @endif
                <th>ID</th>
                <th>DID Number</th>
                <th>Owner</th>
                <th>Labels</th>
                <th><a href="{{ column_sort('expiration_date') }}">Expiration Date</a></th>
                @if (request('status') == 'expired')
                    <th>Days to Delete</th>
                @endif
                <th>&nbsp;</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($dids as $did)
                <tr>
                    @if(request('status') == 'expired' or request('status') == 'active')
                    <td><input class="expired_ids" name="expired_ids[]" type="checkbox" value="{{ $did->id }}"></td>
                    @endif
                    <td>
                        <span>{{ $did->did }}</span>
                    </td>
                    <td>
                        @if ($did->country != '')
                            <span>{{ $did->country }} ({{ $did->area_code }})</span><br>
                        @endif
                        <span>{{ $did->did_number }}</span><br>
                        @if ($did->name != '')
                            <span>({{ $did->name }})</span>
                        @endif
                    </td>
                    <td>
                        @if($did->user)
                            @if ($did->user->name == '')
                                <span><a href="{{ url('manager/members/view/'.$did->user->id) }}">No Name</a></span><br>
                            @else
                                <span><a href="{{ url('manager/members/view/'.$did->user->id) }}">{{ $did->user->name }}</a></span><br>
                            @endif
                        @endif
                    </td>
                    <td>
                        @foreach($did->labels as $labels)
                        <span class="label label-{{ $labels['label']}}">{{ $labels['value'] }}</span>
                        @endforeach
                    </td>
                    <td><span>{{ $did->expiration_date }}</span></td>
                    @if (request('status') == 'expired')
                        <td>
                            @if ($did->days_remaining)
                                @if ($did->days_remaining <= 0)
                                    <span>Ready to delete</span>
                                @else
                                    <span>{{ $did->days_remaining }}</span>
                                @endif
                            @endif
                        </td>
                    @endif
                    <td class="text-right action-btns">
                        @if($did->address_id != null)
                            <a href="{{ url('manager/vault/regulation', $did->id) }}" class="btn btn-success btn-xs delete-btn" title="View Regulation" data-rec-id="{{ $did->id }}" data-target="#authorizeConfirm">View Regulation</a>
                        @endif
                        @if (request('status') == 'trial' or request('status') == 'active')
                        <a href="#" class="btn btn-danger btn-xs delete-btn" title="Delete" data-rec-id="{{ $did->id }}" data-toggle="modal" data-target="#disconnectConfirm">Delete</a>
                        @endif

                        @if (request('status') == 'expired')
                            <a href="#" class="btn btn-success btn-xs delete-btn" title="Reactivate" data-rec-id="{{ $did->id }}" data-toggle="modal" data-target="#reactivateConfirm">Reactivate</a>
                            <a href="#" class="btn btn-danger btn-xs delete-btn" title="Free" data-rec-id="{{ $did->id }}" data-toggle="modal" data-target="#freeConfirm">Free</a>
                            @if($did->did == '')
                            <a href="#" class="btn btn-danger btn-xs delete-btn" title="Free" data-rec-id="{{ $did->id }}" data-toggle="modal" data-target="#deleteBlankConfirm">Delete</a>
                            @endif
                        @endif
                        
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>
    </form>
    <div class="panel-footer text-center">
        {{ $dids->appends(request()->except('_token'))->render() }}
    </div>
</div>
@endsection
@section('page_scripts')
<script type="text/javascript">
$(function(){
    $('#btn-download-csv').on('click',function(){
        var vaultForm = $('#vault-form');
        vaultForm.prop('method', 'POST');
        vaultForm.prop('action', 'vault/download-csv?status={{ request('status', 'pending') }}');
        vaultForm.submit();
    });

    $('#btn-free').on('click',function(){
        var vaultForm = $('#vault-form');
        vaultForm.prop('method', 'POST');
        vaultForm.prop('action', 'vault/free');
        vaultForm.submit();
    });

    $(".select_all").change(function(){  //"select all" change 
        var status = this.checked; // "select all" checked status
        $('.expired_ids').each(function(){ //iterate all listed checkbox items
            this.checked = status; //change ".checkbox" checked status
        });
    });
});
</script>
@endsection