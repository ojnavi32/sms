@extends('layouts.manager.layout')

@section('page_styles')
<style type="text/css">
    .label.label-failed {
        background-color: #d9534f;
    }
    .label.label-executed {
        background-color: #f0ad4e;
    }
</style>
@endsection

@section('content')
<div class="panel panel-light text-theme">
    <div class="panel-heading">
        <h3 class="panel-title">Porting Request Details</h3>
    </div>
    <div class="panel-body">
        <form class="form-horizontal" action="{{ url('manager/vault/details', $dids->id) }}" method="POST">
            <div class="form-group">
                <label class="control-label col-sm-6">Country:</label>
                <div class="col-sm-6">
                    <p class="form-control-static">{{ $details->country }}</p>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-6">Area Code:</label>
                <div class="col-sm-6">
                    <p class="form-control-static">{{ $details->area_code }}</p>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-6">Number or Numbers to port:</label>
                <div class="col-sm-6">
                    <p class="form-control-static">{{ $details->numbers }}</p>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-6">Current Provider:</label>
                <div class="col-sm-6">
                    <p class="form-control-static">{{ $details->current_provider }}</p>
                </div>
            </div>

            @if($dids->ongoing )
            <div class="form-group">
                <label class="control-label col-sm-6">Status:</label>
                <div class="col-sm-6">
                    <p class="form-control-static">Pending Documents</p>
                </div>
            </div>
            @endif

            <hr>
            <!-- <p>After you successfuly port the number from voxbone you may enter number here and save</p> -->
            <div class="form-group">
                <label class="control-label col-sm-3">Number to Port/Activate</label>
                <div class="col-sm-5">
                    <h2>{{ $dids->did_number }}</h2>
                    <!-- <textarea class="form-control" name="numbers" readonly>{{ $dids->did_number }}</textarea> -->
                </div>
            </div>
            {{ csrf_field() }}
            <button type="submit" class="btn btn-primary align-center">Activate Number</button>
            @if( ! $dids->ongoing )
            <button type="submit" class="btn btn-primary align-center" name="ongoing" value="ongoing">Set as Ongoing</button>
            @endif

        </form>

        <!-- <a href="{{ url('manager/vault/', $details->id) }}" class="btn btn-primary align-center">Enter Ported Numbers</a> -->
    </div>
</div>
@endsection
