@extends('layouts.manager.layout')

@section('page_styles')
<style type="text/css">
    .label.label-failed {
        background-color: #d9534f;
    }
    .label.label-executed {
        background-color: #f0ad4e;
    }
</style>
@endsection

@section('content')
<div class="panel panel-light text-theme">
    <div class="panel-heading">
        <h3 class="panel-title">Regulation Address</h3>
    </div>
    <div class="panel-body">
        <form class="form-horizontal" action="{{ url('manager/vault/regulation', $dids->id) }}" method="POST">
            <div class="form-group">
                <label class="control-label col-sm-6">Country:</label>
                <div class="col-sm-6">
                    <p class="form-control-static">{{ $address->country }}</p>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-6">Salutation:</label>
                <div class="col-sm-6">
                    <p class="form-control-static">{{ $address->salutation }}</p>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-6">First Name:</label>
                <div class="col-sm-6">
                    <p class="form-control-static">{{ $address->first_name }}</p>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-6">Last Name:</label>
                <div class="col-sm-6">
                    <p class="form-control-static">{{ $address->last_name }}</p>
                </div>
            </div>

            <div class="form-group">
                <label class="control-label col-sm-6">Company Name:</label>
                <div class="col-sm-6">
                    <p class="form-control-static">{{ $address->company_name }}</p>
                </div>
            </div>

            <div class="form-group">
                <label class="control-label col-sm-6">Building Number:</label>
                <div class="col-sm-6">
                    <p class="form-control-static">{{ $address->building_number }}</p>
                </div>
            </div>

            <div class="form-group">
                <label class="control-label col-sm-6">Street Name:</label>
                <div class="col-sm-6">
                    <p class="form-control-static">{{ $address->street_name }}</p>
                </div>
            </div>

            <div class="form-group">
                <label class="control-label col-sm-6">Zip Code:</label>
                <div class="col-sm-6">
                    <p class="form-control-static">{{ $address->zip_code }}</p>
                </div>
            </div>

            <div class="form-group">
                <label class="control-label col-sm-6">City:</label>
                <div class="col-sm-6">
                    <p class="form-control-static">{{ $address->city }}</p>
                </div>
            </div>

            <div class="form-group">
                <label class="control-label col-sm-6">Status:</label>
                <div class="col-sm-6">
                    <p class="form-control-static">{{ $address->status }}</p>
                </div>
            </div>

            <hr>
            <div class="form-group">
                <label class="control-label col-sm-3">Proof of Address</label>
                <div class="col-sm-5">
                    <a href="{{ url('manager/vault/regulation-file', $address->id) }}" target="_blank">View File</span>
                </div>
            </div>
            {{ csrf_field() }}
            <button type="submit" class="btn btn-primary align-center">Submit to Voxbone</button>
        </form>
    </div>
</div>
@endsection
