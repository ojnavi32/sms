@extends('layouts.manager.layout')

@section('page_styles')
<style type="text/css">
    .btn-xs .fa {
        margin-right: 0;
    }
</style>
@endsection

@section('content')
<div class="mb-16"><a href="{{ url('manager/number_banlist/create') }}" class="button">Add New</a></div>
<div class="panel panel-default">
    <table class="table is-narrow is-bordered">
        <thead>
            <tr>
                <th>Created</th>
                <th>Match String</th>
                <th>&nbsp;</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($banlist as $banned)
                <tr>
                    <td>
                        <span>{{ $banned->created_at }}</span>
                    </td>
                    <td>
                        <span>{{ $banned->number }}</span>
                    </td>
                    <td class="text-right action-btns">
                        <a href="{{ route('manager.banlist.numbers.edit', $banned->id) }}" class="btn btn-green btn-xs update-btn" title="Update"><i class="fa fa-edit"></i></a>
                        <a href="#" class="btn btn-danger btn-xs delete-btn" title="Delete" data-rec-id="{{ $banned->id }}" data-toggle="modal" data-target="#modalConfirm"><i class="fa fa-trash-o"></i></a>
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>
    <div class="panel-footer">

    </div>
</div>
@endsection
