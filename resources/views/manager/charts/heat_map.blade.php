@extends('layouts.manager.layout')

@section('page_styles')
<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/bootstrap/latest/css/bootstrap.css">
<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.css">
@endsection

@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-sm-5 col-sm-offset-7">
            <div id="reportrange" class="pull-right" style="background: #fff; cursor: pointer; padding: 5px 10px; border: 1px solid #ccc; width: 100%">
                <i class="glyphicon glyphicon-calendar fa fa-calendar"></i>&nbsp;
                <span></span> <b class="caret"></b>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-12">
            <div id="regions_div"></div>
        </div>
    </div>
    <div class="row text-center">
        <div class="col-sm-12">
            <div class="btn-group text-center">
                <button class="btn btn-default" id="view_all_faxes_map">All faxes</button>
                <button class="btn btn-default" id="view_failed_faxes_map">Failed faxes.</button>
                <button class="btn btn-default" id="view_successful_faxes_map">Successful faxes.</button>
                <button class="btn btn-default" id="view_total_pages_map">Total Pages</button>
            </div>
        </div>
    </div>
</div>

<div class="container-fluid mt-40">
    <div class="row">
        <p>
            <em style="color:red;">Note: Reports before March 3 have discrepancies.</em>
        </p>
        <div class="panel panel-default">
            <div class="panel-heading">
            </div>
            <table class="table">
                <thead>
                    <tr>
                        <th>Country</th>
                        <th>All Faxes</th>
                        <th>Successful Faxes</th>
                        <th>Failed Faxes</th>
                        <th>Failed Faxes Rate</th>
                        <th>Total Pages</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($countries_fax_counts as $country_fax_count)
                        <tr>
                            <td>
                                <span>{{ $country_fax_count->a2_code }} - {{ $country_fax_count->country }}</span>
                            </td>
                            <td class="text-center">
                                <span>{{ $country_fax_count->all_faxes }}</span>
                            </td>
                            <td class="text-center">
                                <span>{{ $country_fax_count->successful_faxes }}</span>
                            </td>
                            <td class="text-center">
                                <span>{{ $country_fax_count->failed_faxes }}</span>
                            </td>
                            <td class="text-center">
                                <span>{{ (int)$country_fax_count->fail_rate }}%</span>
                            </td>
                            <td class="text-center">
                                <span>{{ (int)$country_fax_count->faxes_total_pages }}</span>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
@endsection

@section('page_scripts')
<script type="text/javascript" src="https://cdn.jsdelivr.net/jquery/1/jquery.min.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.js"></script>

<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<script type="text/javascript" src="https://www.google.com/jsapi"></script>
<script type="text/javascript">
$(function(){
    function cb(start, end) {
        $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
    }
    cb(moment('{{ $start_date }}'), moment('{{ $end_date }}'));

    $('#reportrange').daterangepicker({
        startDate: moment('{{ $start_date }}'),
        endDate: moment('{{ $end_date }}'),
        ranges: {
           'Today': [moment(), moment()],
           'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
           'Last 7 Days': [moment().subtract(6, 'days'), moment()],
           'Last 30 Days': [moment().subtract(29, 'days'), moment()],
           'This Month': [moment().startOf('month'), moment().endOf('month')],
           'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
        }
    }, cb);

    google.charts.load('current', {'packages': ['geochart']});
    google.charts.setOnLoadCallback(function(){
        $.drawRegionsMap({!! $all_faxes !!})
    });

    $.drawRegionsMap = function(data_feed) {
        var data = google.visualization.arrayToDataTable(data_feed);
        var options = {};
        var chart = new google.visualization.GeoChart(document.getElementById('regions_div'));
        chart.draw(data, options);
    };


    $('#reportrange').on('apply.daterangepicker', function(e, picker){
        start_date = picker.startDate.format('YYYY-MM-DD');
        end_date = picker.endDate.format('YYYY-MM-DD');
        window.location.href = '?start_date=' + start_date + '&end_date=' + end_date;
    });

    $('#view_all_faxes_map').on('click',function(e){
        e.preventDefault();
        $.drawRegionsMap({!! $all_faxes !!});
    });

    $('#view_failed_faxes_map').on('click',function(e){
        e.preventDefault();
        $.drawRegionsMap({!! $failed_faxes !!});
    });

    $('#view_successful_faxes_map').on('click',function(e){
        e.preventDefault();
        $.drawRegionsMap({!! $successful_faxes !!});
    });

    $('#view_total_pages_map').on('click',function(e){
        e.preventDefault();
        $.drawRegionsMap({!! $total_pages !!});
    });
});
</script>
@endsection