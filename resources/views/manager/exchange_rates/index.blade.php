@extends('layouts.manager.layout')

@section('page_styles')

@endsection

@section('content')
<h1 class="title">Exchange Rates</h1>
<div class="panel panel-default">
    <table class="table is-bordered is-narrow">
        <thead>
            <tr>
                <th>Currency Code</th>
                <th>Currency Name</th>
                <th>Rate</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($exchange_rates as $exchange_rate)
                <tr>
                    <td>
                        <span id="{{ $exchange_rate->currency_code }}">{{ $exchange_rate->currency_code }}</span>
                    </td>
                    <td>
                        <span>{{ $exchange_rate->currency_name }}</span>
                    </td>
                    <td>
                        <span>{{ $exchange_rate->rate }}</span>
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>
    <div class="panel-footer">

    </div>
</div>
@endsection
