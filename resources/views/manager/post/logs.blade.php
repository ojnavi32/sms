@extends('layouts.manager.layout')

@section('page_styles')
<style type="text/css">
    .topup_details em {
        color: red;
        font-weight: bold;
    }
    .topup_details p {
        max-width: 700px;
        word-break: break-all;
    }
</style>
@endsection

@section('content')
<div class="panel panel-default">
    <div class="panel-heading">
    </div>
    <table class="table">
        <thead>
            <tr>
                <th>Time</th>
                <th>Content</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($logs as $log)
                <tr>
                    <td>{{ $log->created_at }}</td>
                    <td><em>{{ $log->content }}</em></td>
                </tr>
            @endforeach
        </tbody>
    </table>
    <div class="panel-footer text-center">
        {!! $logs->render() !!}
    </div>
</div>
@endsection
