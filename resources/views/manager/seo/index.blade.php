@extends('layouts.manager.layout')

@section('page_styles')
<style type="text/css">
    .action-btns .btn-xs {
        margin: 2px 0;
    }
    .action-btns .btn-xs .fa {
        margin-right: 0;
        min-width: 14px;
    }
</style>
@endsection

@section('content')
<div class="panel panel-default">
    <div class="panel-heading">
        {!! Form::open(['id' => 'list-filter-form', 'method' => 'GET', 'class' => 'form-horizontal form-bg text-theme']) !!}
            <div class="form-group">
                <label for="inputPassword" class="control-label col-sm-2"></label>
                <div class="col-sm-4">
                    <select name="locale" id="locale">
                    @foreach(LaravelLocalization::getSupportedLocales() as $localeCode => $properties)
                    <option value="{{ $localeCode }}" {{ (request('locale') == $localeCode) ? 'selected' : '' }}>{{ ucfirst($properties['name']) }}</option>
                    @endforeach
                    </select>
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-10 col-sm-offset-2">
                </div>
            </div>
        </form>
    </div>
    <table class="table">
        <thead>
            <tr>
                <th>ID</th>
                <th>URL</th>
                <th>Locale</th>
                <th>Meta Title</th>
                <th>Meta Description</th>
                <th>H1</th>
                <th>&nbsp;</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($rows as $row)
                <tr>
                    <td>
                        <span>{{ $row->id }}</span>
                    </td>
                    <td>
                        <small>{{ 'https://fax.to/' . $row->url }}</small>
                    </td>
                    <td>
                       <span>{{ $row->locale }}</span>
                    </td>
                    <td>
                       <!-- <span>{{ $row->title }}</span> -->
                       <a class="pUpdate" data-name="title" data-type="textarea" data-id="{{ $row->id }}">{{ $row->title }}</a>
                    </td>
                    <td>
                       <!-- <span>{{ $row->description }}</span> -->
                       <a class="pUpdate" data-name="description" data-type="textarea" data-id="{{ $row->id }}">{{ $row->description }}</a>
                    </td>
                    <td>
                       <!-- <span>{{ $row->h1 }}</span> -->
                       <a class="pUpdate" data-name="h1" data-type="textarea" data-id="{{ $row->id }}">{{ $row->h1 }}</a>
                    </td>
                    <td class="text-center">  
                    </td>
                    <td class="text-right action-btns">
                        <!-- <a href="{{ url('manager/members/view/'.$row->id) }}" class="btn btn-green btn-xs update-btn" title="View Member"><i class="fa fa-search-plus"></i></a> -->
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>
    <div class="panel-footer text-center">
        
    </div>
</div>
@endsection
@section('page_scripts')
<link href="//cdnjs.cloudflare.com/ajax/libs/x-editable/1.5.0/bootstrap3-editable/css/bootstrap-editable.css" rel="stylesheet"/>
<script src="//cdnjs.cloudflare.com/ajax/libs/x-editable/1.5.0/bootstrap3-editable/js/bootstrap-editable.min.js"></script>
<script type="text/javascript">
$(function(){
        $.fn.editable.defaults.mode = 'popup';
        $('.pUpdate').editable({
            validate: function(value) {
                if($.trim(value) == '') 
                    return 'Value is required.';
        },
        url:'{{ URL::to("/") }}/manager/seo/update',
        title: 'Edit',
        placement: 'top', 
        send:'always',
        ajaxOptions: {
        dataType: 'json'
        }
     });

    $('#locale').change(function() {
        $('#list-filter-form').submit()
    })
});
</script>
@endsection