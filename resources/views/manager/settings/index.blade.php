@extends('layouts.manager.layout')

@section('content')

<form action="{{ route('manager.post.settings') }}" method="POST">
    <div>
        <button type="submit" class="btn btn-primary"><i class="fa fa-save"></i>Save</button>
        {{ csrf_field() }}
        <!-- <a href="{{ url('manager/rates') }}" class="button is-link"><i class="fa fa-ban"></i>Cancel</a> -->
    </div>
</form>
@endsection

