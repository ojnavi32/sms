@extends('layouts.manager.layout')

@section('page_styles')
<style type="text/css">
    .btn-xs .fa {
        margin-right: 0;
    }
</style>
@endsection

@section('content')
<div class="panel panel-default">
    <div class="panel-heading">
        <h4>Filter by:</h4>
        {!! Form::open(['id' => 'list-filter-form', 'method' => 'GET', 'class' => 'form-horizontal form-bg text-theme']) !!}
            <div class="form-group">
                <label for="inputPassword" class="control-label col-sm-2">Month</label>
                <div class="col-sm-4">
                    {!! Form::select('month', [''=>'Choose a month'] + $month_range, request('month'), ['class' => 'form-control']) !!}
                </div>
            </div>
            <div class="form-group">
                <label for="pending" class="control-label col-sm-2">Show Pending</label>
                <div class="col-sm-4">
                    {!! Form::checkbox('pending', 'pending', request('pending')) !!}
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-10 col-sm-offset-2">
                    <button class="btn btn-primary">Submit</button>
                </div>
            </div>
            <div class="mt-10 text-right">
                <button name="download" value="download" id="btn-download-csv" class="btn btn-green">Download current list as CSV</button>
                <button name="comma" value="comma" id="comma" class="btn btn-green">Download comma separated Text File</button>
            </div>
        </form>
        
    </div>
    <table class="table">
        <thead>
            <tr>
                <th>Created</th>
                <th>Member</th>
                <th>Payment Method</th>
                <th>VAT Exempt</th>
                <th>Amount</th>
                <th>&nbsp;</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($payment_history as $payment)
            <tr>
                <td>
                    <span>{{ $payment->created_at }}</span>
                </td>
                <td>
                    {{ $payment->user->name }}<br>
                    <a href="mailto:{{ $payment->user->email }}" class="small">&lt;{{ $payment->user->email }}&gt;</a>
                </td>
                <td>
                    <span>{{ $payment->payment_method->name }}</span>
                </td>
                <td>
                    <span>{{ $payment->vat_exemption }}</span>
                </td>
                <td>
                    <span>{{ computeAmountTotal($payment->amount, $payment->vat)['pay_amount'] }}</span>
                </td>
                <td class="text-right action-btns">
                    <a href="{{ url('manager/payments/payment-history/view/'.$payment->id) }}" class="btn btn-green btn-xs" title="View Details"><span class="fa fa-search-plus"></span></a>
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
    <div class="panel-footer text-center">
        {!! $payment_history->appends($request)->render() !!}
    </div>
</div>
@endsection

@section('page_scripts')
<script type="text/javascript">
$(function(){
    $('#btn-download-csv').on('click',function(){
        var tmp_form = $('<form>');
        tmp_form.prop('method', 'POST');
        tmp_form.prop('action', 'payment-history/download-csv');

        tmp_form.prepend('{{ csrf_field() }}');

        $('#list-filter-form').find('select, input').each(function(ind, elem){
            tmp_form.append($(elem).clone());
        });

        tmp_form.submit();
    });
});
</script>
@endsection