@extends('layouts.manager.layout')

@section('page_styles')
<style type="text/css">
    .action-btns .btn-xs {
        margin: 2px 0;
    }
    .action-btns .btn-xs .fa {
        margin-right: 0;
        min-width: 14px;
    }
</style>
@endsection

@section('content')
<div class="mb-16"><a href="{{ url('manager/payments/vat-settings/create') }}" class="btn btn-primary">Create</a></div>
<div class="panel panel-default">
    <div class="panel-heading">
    </div>
    <table class="table">
        <thead>
            <tr>
                <th>Country</th>
                <th>VAT</th>
                <th>&nbsp;</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($country_vats as $country_vat)
            <tr>
                <td>
                    <span>{{ $country_vat->country }}</span>
                </td>
                <td>
                    <span>{{ $country_vat->vat }}%</span>
                </td>
                <td class="text-right action-btns">
                    <a href="{{ url('manager/payments/vat-settings/update/'.$country_vat->id) }}" class="btn btn-green btn-xs update-btn" title="Update"><i class="fa fa-edit"></i></a>
                    <a href="#" class="btn btn-danger btn-xs delete-btn" title="Delete" data-rec-id="{{ $country_vat->id }}" data-toggle="modal" data-target="#modalConfirm"><i class="fa fa-trash-o"></i></a>
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
    <div class="panel-footer text-center">
    </div>
</div>
@endsection