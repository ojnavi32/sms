@extends('layouts.manager.layout')

@section('content')
{!! Form::open(['class' => 'form-bg text-theme']) !!}
    <div class="form-group">
        <label for="country_dial_code_id">Country</label>
        <p class="form-control-static">{{ $country_dial_code->country }}</p>
    </div>
    <div class="form-group">
        <label for="vat">VAT %</label>
        <div class="">
        </div>
        <input class="form-control" name="vat" id="vat" placeholder="0.00" type="text" value="{{ $country_dial_code->vat }}">
    </div>
    <div>
        <button type="submit" class="btn btn-primary text-theme"><i class="fa fa-save"></i>Save</button>
        <a href="{{ url('manager/payments/vat-settings') }}" class="btn btn-default"><i class="fa fa-ban"></i>Cancel</a>
    </div>
</form>
@endsection
