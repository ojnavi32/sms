@extends('layouts.manager.layout')
@section('page_styles')
<style type="text/css">
    .btn-xs .fa {
        margin-right: 0;
    }
</style>
@endsection
@section('content')
<div class="panel panel-default">
    <table class="table">
        @if(Session::has('success'))
        <div class="alert alert-success">{{ Session::get('success') }}</div>
        @endif
        <thead>
            <tr>
                <th>Created</th>
                <th>Member</th>
                <th>Payment Method</th>
                <th>Amount</th>
                <th>Vat</th>
                <th>Reference Number</th>
                <th>Desciption</th>
                <th>Paid</th>
                <th>&nbsp;</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($payment_history as $payment)
            <tr>
                <td>
                    <span>{{ $payment->created_at }}</span>
                </td>
                <td>
                    @if(isset($payment->user->name)){{ $payment->user->name }}<br>
                    <a href="mailto:{{ $payment->user->email }}" class="small">&lt;{{ $payment->user->email }}&gt;</a>
                    @endif
                </td>
                <td>
                    <span>@if(isset($payment->paymentMethod)){{ $payment->paymentMethod->name }}@endif</span>
                </td>
                <td>
                    <span>{{ $payment->amount }}</span>
                </td>
                <td>
                    <span>{{ $payment->vat }}</span>
                </td>
                <td>
                    <span>{{ $payment->reference_number }}</span>
                </td>
                <td>
                    <span>{{ $payment->details }}</span>
                </td>
                <td class="text-right action-btns">
                    @if(!$payment->is_paid)
                    <a href="{{ route('manager.members.payments.confirm', $payment->id) }}" class="btn btn-primary"  onclick="return confirm('Are you sure want to approve this payment?')" data-toggle="modal">CONFIRM</a>
                    @else
                    <span><center>Paid</center></span>
                    @endif
                </td>
                <td class="text-right action-btns">
                    <a href="{{ url('manager/payment_history/view/'.$payment->id) }}" class="btn btn-green btn-xs" title="View Details"><span class="fa fa-search-plus"></span></a>
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
    <div class="panel-footer text-center">
        {!! $payment_history->appends($request)->render() !!}
    </div>
</div>
@endsection
