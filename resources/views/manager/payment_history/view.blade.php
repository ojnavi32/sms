@extends('layouts.manager.layout')

@section('page_styles')
<style type="text/css">
    .label.label-failed {
        background-color: #d9534f;
    }
    .label.label-executed {
        background-color: #f0ad4e;
    }
</style>
@endsection

@section('content')
<div class="panel panel-light text-theme">
    <div class="panel-heading">
        <h3 class="panel-title">&nbsp;</h3>
    </div>
    <div class="panel-body">
        <form class="form-horizontal">
            <div class="form-group">
                <label class="control-label col-sm-3">Created</label>
                <div class="col-sm-9">
                    <p class="form-control-static">{{ $payment->created_at }}</p>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-3">Member Name</label>
                <div class="col-sm-9">
                    <p class="form-control-static">{{ $payment->user->name }}</p>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-3">Member Email</label>
                <div class="col-sm-9">
                    <p class="form-control-static">{{ $payment->user->email }}</p>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-3">Payment Method</label>
                <div class="col-sm-9">
                    <p class="form-control-static">{{ $payment->paymentMethod->name }}</p>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-3">Amount</label>
                <div class="col-sm-9">
                    <p class="form-control-static">{{ $payment->amount }}</p>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-3">Is Paid</label>
                <div class="col-sm-9">
                    @if(!$payment->is_paid)
                    <a href="{{ route('manager.members.payments.confirm', $payment->id) }}" class="btn btn-primary" onclick="return confirm('Are you sure want to approve this confirmation?')" data-toggle="modal">CONFIRM</a>
                    @else
                    <p class="form-control-static">Paid</p>
                    @endif
                </div>
            </div>    
            @if ($payment->payment_history_details)
            <div class="panel panel-default mt-50">
                <div class="panel-heading">Payment Details</div>
                @foreach ($payment->payment_history_details as $payment_details)
                    <div class="form-group">
                        <label class="control-label col-sm-3">{{ $payment_details->name }}</label>
                        <div class="col-sm-9">
                            <p class="form-control-static">{{ $payment_details->info }}</p>
                        </div>
                    </div>
                @endforeach
            </div>
            @endif
        </form>
    </div>
</div>
@endsection
