@extends('layouts.manager.layout')

@section('content')
{!! Form::open(['class' => 'form-horizontal']) !!}
    <div class="form-group">
        <label for="company" class="control-label col-sm-3">Company</label>
        <div class="col-sm-9">
            <input name="company" class="form-control" id="company" placeholder="Company" type="text" value="{{ @$billing_detail->company }}">
        </div>
    </div>
    <div class="form-group">
        <label for="firstname" class="control-label col-sm-3">First Name</label>
        <div class="col-sm-9">
            <input name="firstname" class="form-control" id="firstname" placeholder="First Name" type="text" value="{{ @$billing_detail->firstname }}">
        </div>
    </div>
    <div class="form-group">
        <label for="lastname" class="control-label col-sm-3">Last Name</label>
        <div class="col-sm-9">
            <input name="lastname" class="form-control" id="lastname" placeholder="Last Name" type="text" value="{{ @$billing_detail->lastname }}">
        </div>
    </div>
    <div class="form-group">
        <label for="street_address" class="control-label col-sm-3">Street Address</label>
        <div class="col-sm-9">
            <textarea name="street_address" class="form-control" id="street_address" placeholder="Street Address">{{ @$billing_detail->street_address }}</textarea>
        </div>
    </div>
    <div class="form-group">
        <label for="postal_code" class="control-label col-sm-3">Postal Code</label>
        <div class="col-sm-9">
            <input name="postal_code" class="form-control" id="postal_code" placeholder="Postal Code" type="text" value="{{ @$billing_detail->postal_code }}">
        </div>
    </div>
    <div class="form-group">
        <label for="country_dial_code_id" class="control-label col-sm-3">Country</label>
        <div class="col-sm-9">
            <select id="country_dial_code_id" class="form-control" required="required" name="country_dial_code_id">
                <option value="">Select a country</option>
                @foreach ($country_dial_codes as $country_dial_code)
                    <option value="{{ $country_dial_code->id }}" data-vat="{{ $country_dial_code->vat }}"{{ (($country_dial_code->id == @$me->billing_detail->country_dial_code_id) ? ' selected' : '') }}>{{ $country_dial_code->country }}</option>
                @endforeach
            </select>
        </div>
    </div>
    <div class="form-group">
        <label for="amount" class="control-label col-sm-3">Amount</label>
        <div class="col-sm-3">
            <input name="amount" class="form-control" id="amount" placeholder="Amount" type="text" value="">
        </div>
    </div>

    <div class="form-group">
        <label for="amount" class="control-label col-sm-3">Subscription ID (Optional)</label>
        <div class="col-sm-3">
            <select id="subscription_id" class="form-control" name="subscription_id">
                <option value="">Select subscription</option>
                @foreach ($subscriptions as $key => $val)
                    <option value="{{ $val }}">{{ $val }} ({{ $key }})</option>
                @endforeach
            </select>
        </div>
    </div>

    <div id="topup-total-box" class="row topup-info text-center">
        <div class="col-sm-4">
            <span>Amount:</span><br />
             &euro; <span class="topup-amount"></span>
        </div>
        <div class="col-sm-1 calculus">
            +
        </div>
        <div class="col-sm-1">
            <span>VAT: </span><br />
            <span class="add-vat"></span>%
        </div>
        <div class="col-sm-1 calculus">
            =
        </div>
        <div class="col-sm-1">
            <span>Total:</span><br />
            <span id="total_eur" class="topup-total-row"><strong>&euro;<span class="topup-total-amount"></span></strong></span>
            <span id="total_usd" class="topup-total-row hide"><strong>$<span class="topup-total-amount-usd"></span></strong></span>
        </div>
    </div>

    <div class="form-group">
        <label for="payment_method_id" class="control-label col-sm-3">Payment Method</label>
        <div class="col-sm-3">
            <select name="payment_method_id" id="payment_method_id">
                <option value="1">Paypal</option>
                <option value="2">Credit Card</option>
                <option value="3">Cash</option>
                <option value="4">Bank Wire</option>
            </select>
        </div>
    </div>

    <div class="form-group">
        <div class="col-sm-offset-2 col-sm-10">
            <button type="submit" class="btn btn-primary">Save</button>
            <a href="{{ url('manager/members/view/' . $user_id) }}" class="btn btn-primary">Cancel</a>
        </div>
    </div>
</form>
<hr>
<div>
</div>
@endsection

@section('page_scripts')
<script>
$.computeTotal = function(){
    var topup_total = $('#topup-total-box');
    var topup_amount = Number($('#amount').val());
    var add_vat = Number($('#country_dial_code_id').find('option:selected').data('vat'));
    if (isNaN(add_vat)) {
        add_vat = 0;
    }
    topup_total.find('.topup-amount').text(topup_amount);
    topup_total.find('.add-vat').text(add_vat);
    topup_total.find('.topup-total-amount').text(topup_amount + ((add_vat/100)*topup_amount));

    $.computeConvert("{{ url('service/rate/conversion') }}/");
};

$(function(){
    $('#country_dial_code_id').on('change', function(){
        $.computeTotal();
    });

    $('#amount').on('keyup', function(){
        $.computeTotal();
    });

    $.computeTotal();
});
</script>
@endsection