@extends('layouts.manager.layout')

@section('content')
{!! Form::open(['class' => 'form-horizontal']) !!}
    <div class="form-group">
        <label for="invoice_date" class="control-label col-sm-3">Date Time</label>
        <div class="col-sm-3">
            <input name="invoice_date" class="form-control" id="invoice_date" placeholder="Invoice Date" type="text" value="{{ $payment->created_at }}">
        </div>
    </div>
    <div class="form-group">
        <label for="country_dial_code_id" class="control-label col-sm-3">Country</label>
        <div class="col-sm-9">
            <select id="country_dial_code_id" class="form-control" required="required" name="country_dial_code_id">
                <option value="">Select a country</option>
                @foreach ($country_dial_codes as $country_dial_code)
                    <option value="{{ $country_dial_code->id }}" data-vat="{{ $country_dial_code->vat }}"{{ (($country_dial_code->id == @$me->billing_detail->country_dial_code_id) ? ' selected' : '') }}>{{ $country_dial_code->country }}</option>
                @endforeach
            </select>
        </div>
    </div>
    <div class="form-group">
        <label for="amount" class="control-label col-sm-3">Amount</label>
        <div class="col-sm-3">
            <input name="amount" class="form-control" id="amount" placeholder="Amount" type="text" value="{{ $payment->amount }}">
            <input name="vat" class="form-control" id="vat" type="hidden" value="{{ $payment->vat }}">
            <input name="user_id" class="form-control" id="user_id" type="hidden" value="{{ $payment->user_id }}">
        </div>
    </div>

    <div id="topup-total-box" class="row topup-info text-center">
        <div class="col-sm-4">
            <span>Amount:</span><br />
             &euro; <span class="topup-amount"></span>
        </div>
        <div class="col-sm-1 calculus">
            +
        </div>
        <div class="col-sm-1">
            <span>VAT: </span><br />
            <span class="add-vat"></span>%
        </div>
        <div class="col-sm-1 calculus">
            =
        </div>
        <div class="col-sm-1">
            <span>Total:</span><br />
            <span id="total_eur" class="topup-total-row"><strong>&euro;<span class="topup-total-amount"></span></strong></span>
            <span id="total_usd" class="topup-total-row hide"><strong>$<span class="topup-total-amount-usd"></span></strong></span>
        </div>
    </div>

    <!-- <div class="form-group">
        <label for="payment_method_id" class="control-label col-sm-3">Payment Method</label>
        <div class="col-sm-3">
            <select name="payment_method_id" id="payment_method_id">
                <option value="1">Paypal</option>
                <option value="2">Credit Card</option>
            </select>
        </div>
    </div> -->

    <div class="form-group">
        <div class="col-sm-offset-2 col-sm-10">
            <button type="submit" class="btn btn-primary">Save</button>
            <a href="{{ url('manager/members/view/' . $user_id) }}" class="btn btn-primary">Cancel</a>
        </div>
    </div>
</form>
<hr>
<div>
</div>
@endsection

@section('page_scripts')
<script>
$.computeTotal = function(){
    var topup_total = $('#topup-total-box');
    var topup_amount = Number($('#amount').val());
    var add_vat = Number($('#country_dial_code_id').find('option:selected').data('vat'));
    if (isNaN(add_vat)) {
        add_vat = 0;
    }
    topup_total.find('.topup-amount').text(topup_amount);
    topup_total.find('.add-vat').text(add_vat);
    $('#vat').val(add_vat);
    topup_total.find('.topup-total-amount').text(topup_amount + ((add_vat/100)*topup_amount));

    $.computeConvert("{{ url('service/rate/conversion') }}/");
};

$(function(){
    $('#country_dial_code_id').on('change', function(){
        $.computeTotal();
    });

    $('#amount').on('keyup', function(){
        $.computeTotal();
    });

    $.computeTotal();
});
</script>
@endsection