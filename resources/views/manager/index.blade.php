
@extends('layouts.manager.layout')

@section('content')
<h1 class="title">Stats HERE</h1>
<div id="count-wrapper-2" class="row">
 @foreach($stats as $key=>$value)

         <div class="col-sm-4">
            <div class="icon-box bg" style="padding: 16px;text-align: center">
                <i class="{{$value['icon']}}"></i>
                <h3 class="title-count title-lg text-theme-sm">{{$value['value']}}</h3>
                <h3 class="title-sm text-theme-sm">{{$value['title']}}</h3>
            </div>
        </div>

@endforeach
 </div>
@endsection