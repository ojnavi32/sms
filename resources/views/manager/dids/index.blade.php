@extends('layouts.manager.layout')

@section('page_styles')
<style type="text/css">
    .btn-xs .fa {
        margin-right: 0;
    }
</style>
@endsection

@section('content')

<div class="panel-heading">
        {!! Form::open(['id' => 'list-filter-form', 'method' => 'GET', 'class' => 'form-horizontal form-bg text-theme']) !!}
            <div class="form-group">
                <label for="inputPassword" class="control-label col-sm-2">Number</label>
                <div class="col-sm-4">
                    <input type="text" name="number" value="{{ request('number') }}" placeholder="Number" class="form-control">
                    <input type="hidden" name="status" value="{{ request('status') }}">
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-10 col-sm-offset-2">
                    <button class="btn btn-primary">Search</button>
                </div>
            </div>
        </form>
    </div>

<div class="btn-group btn-group-justified" role="group" aria-label="...">
  <!-- <div class="btn-group" role="group">
    <a href="?status=free" class="btn btn-default {{ (request('status') == 'free') ? 'active': '' }}">Free<br> Numbers</a>
  </div>
  <div class="btn-group" role="group">
    <a href="?status=occupied" class="btn btn-default {{ (request('status') == 'occupied') ? 'active': '' }}">Occupied<br> Numbers</a>
  </div> -->
  {{ $dids->count() }} Numbers
  <div class="mt-10 text-right">
            @if(request('status') == 'expired')
                <button id="btn-free" class="btn btn-green">Free</button>
            @endif
            <a type="button" href="{{ url('manager/dids/download') }}" class="btn btn-green">Download .CSV of Numbers</a>
        </div>
</div>

<div class="panel panel-default">
    <table class="table">
        <thead>
            <tr>
                <th>ID</th>
                <th>DID Number</th>
                <th>Subscription ID</th>
                <th>&nbsp;</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($dids as $did)
                <tr>
                    <td>
                        <span>{{ $did->did }}</span>
                    </td>
                    <td>
                        @if ($did->country != '')
                            <span>{{ $did->country }} ({{ $did->area_code }})</span><br>
                        @endif
                        <span>{{ $did->did_number }}</span><br>
                        @if($did->user)
                            @if ($did->user->name == '')
                                <span><a href="{{ url('manager/members/view/'.$did->user->id) }}">No Name</a></span><br>
                            @else
                                <span><a href="{{ url('manager/members/view/'.$did->user->id) }}">{{ $did->user->name }}</a></span><br>
                            @endif
                        @endif
                       <!--  @if ($did->name != '')
                            <span>({{ $did->name }})</span>
                        @endif -->
                    </td>
                    <td>{{ $did->subscription_id }}<!-- {{ $did->voxbone_status }} --></td>
                    
                    <td class="text-right action-btns">
                        @if (request('status') == 'trial' or request('status') == 'active')
                        <a href="#" class="btn btn-danger btn-xs delete-btn" title="Delete" data-rec-id="{{ $did->id }}" data-toggle="modal" data-target="#disconnectConfirm">Disconnect</a>
                        <a href="#" class="btn btn-success btn-xs delete-btn" title="Email" data-rec-id="{{ $did->id }}" data-toggle="modal" data-target="#emailConfirm">Email</a>
                        @endif
                        @if (request('status') == 'pending')
                            @if($did->porting_request_id != null)
                                <a href="{{ url('manager/vault/details', $did->id) }}" class="btn btn-success btn-xs delete-btn" title="View Details" data-rec-id="{{ $did->id }}">Porting Details</a>
                                <a href="#" class="btn btn-danger btn-xs" title="Decline" data-rec-id="{{ $did->id }}" data-toggle="modal" data-target="#declineConfirm">Decline</a>
                            @else
                                <a href="#" class="btn btn-success btn-xs delete-btn" title="Authorize" data-rec-id="{{ $did->id }}" data-toggle="modal" data-target="#authorizeConfirm">Authorize</a>
                                <a href="#" class="btn btn-danger btn-xs" title="Decline" data-rec-id="{{ $did->id }}" data-toggle="modal" data-target="#declineConfirm">Decline</a>
                            @endif
                        @endif
                        @if (request('status') == 'expired')
                            <a href="#" class="btn btn-success btn-xs delete-btn" title="Reactivate" data-rec-id="{{ $did->id }}" data-toggle="modal" data-target="#reactivateConfirm">Reactivate</a>
                            @if ($did->days_remaining)
                                @if ($did->days_remaining <= 0)
                                    <!-- Permanent delete -->
                                    <a href="#" class="btn btn-danger btn-xs delete-btn" title="Permanent Delete" data-rec-id="{{ $did->id }}"  data-permanent="1" data-toggle="modal" data-target="#modalConfirm"><i class="fa fa-trash-o"></i></a>
                                @endif
                            @else
                                <a href="#" class="btn btn-danger btn-xs delete-btn" title="Mark for Deletion" data-rec-id="{{ $did->id }}" data-toggle="modal" data-target="#modalConfirm"><i class="fa fa-trash-o"></i></a>
                            @endif
                        @endif
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>
    <div class="panel-footer text-center">
        <?php echo $dids->appends(['status' => request('status')])->render(); ?>
    </div>
</div>
@endsection
@section('page_scripts')
<script type="text/javascript">

$(function(){
    $('#btn-download-csv').on('click',function(){
        var tmp_form = $('<form>');
        tmp_form.prop('method', 'POST');
        tmp_form.prop('action', 'vault/download-csv');

        $('#list-filter-form').find('select, input').each(function(ind, elem){
            tmp_form.append($(elem).clone());
        });

        tmp_form.submit();
    });
});

</script>
@endsection