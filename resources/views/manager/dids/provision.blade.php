@extends('layouts.manager.layout')

@section('content')
{!! Form::open(['class' => 'form-bg text-theme']) !!}
    <div class="form-group">
        <label for="Status">Country</label>
        <select id="country" name="country" @change="getAreaCodes()">
            @foreach($countries as $country)
            <option value="{{ $country['a2_code'] }}">{{ $country['country'] }}</option>
            @endforeach
        </select>
    </div>
    <div class="form-group">
        <label for="number">Choose Area:</label>
        <div class="area-code">
            <select name="area_code" id="areaCode" class="form-control" v-model="selected.didGroupId" options="idsCodes" @change="getDidGroupId()">
                <option value="@{{ key }}" v-for="(key, val) in idsCodes">@{{ val }}</option>
            </select>
            <input type="hidden" name="area_code_hidden" v-model="selected.areaCode">
            <input type="hidden" name="user_id" value="{{ $user_id }}">
        </div>
    </div>
    <div class="form-group fax-number-group">
        <label for="number">Your number will be like:</label>
        <div class="text" data-toggle="tooltip" data-placement="top" title="A number will be randomly assigned to you!">
            +@{{selectedCountry.dial_code}} (@{{selected.areaCode}}) ... <i class="fa fa-question-circle"></i> ...
        </div>
    </div>
    <div>
        <button type="submit" class="btn btn-primary text-theme"><i class="fa fa-check"></i>Provision</button>
        <a href="{{ url('manager/members/numbers/assign', $user_id) }}" class="btn btn-default"><i class="fa fa-ban"></i>Cancel</a>
    </div>
</form>
<hr>
<div>
</div>
@if(Auth::user()->email == 'italos.marios@gmail.com')
<p><pre>data: @{{$data.selected | json 2}}</pre></p>
@endif
@endsection

@section('page_scripts')
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/vue/1.0.16/vue.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/vue-resource/0.7.0/vue-resource.min.js"></script>
<script type="text/javascript">
var vm = new Vue({
    el: 'body',
    data: {
        countries: {!! json_encode($countries) !!},
        areaCodes: [],
        didgroups: [],
        idsCodes: [],

        isLoading: null,
        numberName: "",

        selected: { }
    },
    ready: function() {
        this.refresh();
    },
    methods: {
        refresh: function() {
            this.setDefaultSelected();
            this.getAreaCodes();
        },

        setDefaultSelected: function() {
            this.selected = {
                country: 'ARG',
                numberType: 'GEOGRAPHIC',
                areaCode: this.areaCodes[0] || '55',
                didGroupId: '1',
                numberName: '',
                status: 'available',
                contractType: '3m',
                destinationEmail: '',
            }
        },

        getAreaCodes: function() {
            var self = this;

            var country = $('#country').val();
            
            var url = '{{ URL::to('/') }}/service/voxbone/countries/'+country+'/areacodes?type='+this.selected.numberType+'';
            self.selected.areaCode = "";
            self.selected.didGroupId = "";
            self.areaCodes = null;
            // self.isLoading = true;
            return jQuery.get(url).done(function(result) {
                //self.isLoading = false;
                console.log(result);
                self.selected.areaCode = result.codes[0] || [];
                self.selected.didGroupId = result.ids[0] || [];
                self.idsCodes = result.ids_codes || [];
                self.areaCodes = result.codes || null;
                self.didgroups = result.ids || null;
            }).fail(function() {
                self.isLoading = false;
                // result = [];
                // self.selected.areaCode = result[0];
                // self.areaCodes = result;
            });
        },
        getDidGroupId: function() {
            var index = this.didgroups.indexOf(this.selected.didGroupId);
            this.selected.areaCode = this.areaCodes[index];
        },
    }
});
</script>
@endsection