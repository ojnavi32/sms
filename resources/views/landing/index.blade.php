@extends('layouts.default')

@section('content')
	@include('components.landing.sms-gateway')
	@include('components.landing.features')
	@include('components.landing.cost-calculator')
	@include('components.landing.rest-api')
	@include('components.landing.customers')
@endsection

