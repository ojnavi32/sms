@extends('layouts.default')

@section('title')
<title>SMS Getaway API for Business | SMS Rest Api for Developers | SMS.To</title>
@endsection	
@section('description') 
	<meta name="description" content="Intergrate our SMS Rest API and implement SMS Notifications, OTP in your workflows. Get a fully Secure SSL SMS API for enviroment or development language now."/>
@endsection
@section('keywords') 
	<meta name="keywords" content="sms api, sms rest api, bulk sms notifications, bulk sms otp">
@endsection

@section('content')
	@include('components.landing.sms-gateway', [
		'title' => 'Powerful SMS API Gateway.',
		'description' => 'Intergrate our <strong>SMS Rest API</strong> and implement SMS Notifications, <br />OTP, Reminders, 2FA (2 Factor Authentication) in your workflows. Fully Secure SSL SMS API for any <br />enviroment or development language.'
	])
	@include('components.landing.rest-api')
@endsection
