@component('mail::message')
<p><b>Hi {{$data['name']}},</b> </p>
<p>
  @if($data['status'] === 'success')
      You successful added {{$data['amount']}}  to Your Account.<br/>
      
      Your Current Balance is: {{$data['balance']}}.
      @else
      Failed Transaction.
  @endif
</p>

@component('mail::button', ['url' => $url])
Send SMS
@endcomponent

<p>
	Thanks for choosing {{ config('app.name') }}<br>
    -- The {{ config('app.name') }} Team!
</p>
@endcomponent