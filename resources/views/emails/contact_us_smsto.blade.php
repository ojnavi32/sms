@component('mail::message')
<p><b>Hello Admin, </b></p>
<h2 style="line-height: 30px;">We got an Inquiry</h2>
<p style="line-height: 30px;">Inquiry from <strong>{{ $name }} &lt;{{ $email }}&gt;</strong></p>
<p>
        <span style="font-weight: bold;">Message: </span>
        {{ $message }}
</p>  
<p>      
        <span style="font-weight: bold;">Phone:</span>
        {{ $phone }}
</p>

<p>
	Please reply as soon as possible!<br>
	Thank you.<br>
    -- The {{ config('app.name') }} Team!
</p>
@endcomponent



