@component('mail::message')

<p><b>Hi {{$user->name}},</b></p>
<h2 style="line-height: 30px;">Welcome to SMS.to</h2>
<p>We're glad to see you here!<br/>
You can use following information to login into your account:</p>
<p class="email"><span style="font-weight: bold;">Email address:</span> {{$user->email}}<br/>
<span style="font-weight: bold;">Password:</span> {{$user->tempPassword}}</p>


@component('mail::button', ['url' => $url])
Login
@endcomponent

<p>Don't working the password? <a href="{{ $passwordResetlink }}" style="text-decoration:none;font-weight: bold;color:#5affab!important;">Reset here</a></p>

<p>
	Thanks for choosing {{ config('app.name') }}<br>
    -- The {{ config('app.name') }} Team!
</p>
@endcomponent