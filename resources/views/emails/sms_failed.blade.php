@component('mail::message')
<p style="line-height: 30px;">{{$subMessage}}</p>
<p>
   {{$message}}    
</p>  

<p>
	Thanks for choosing {{ config('app.name') }}<br>
    -- The {{ config('app.name') }} Team!
</p>
@endcomponent