@component('mail::message')
<p><b>Hi {{$userName}}, </b></p>
@if($listName)
<p>Your SMS broadcast to <span style="font-weight: bold;">{{ $listName }}</span> finished successfully!</p>
@else
<p>Your SMS broadcast finished Successfully!</p>
@endif
<p><span style="font-weight: bold;">Delivered</span> - {{$deliveryCount}} <br/>

<span style="font-weight: bold;">Failed</span> - {{$failedCount}} </p>

@component('mail::button', ['url' => $url])
Check Report
@endcomponent

<p>
	Thanks for choosing {{ config('app.name') }}<br>
    -- The {{ config('app.name') }} Team!
</p>
@endcomponent