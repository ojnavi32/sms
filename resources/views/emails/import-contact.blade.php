@component('mail::message')
<p><b>Hi {{$user->name}}, </b></p>
@if($listName)
<p>Your contacts imported to the list: <span style="font-weight: bold;">{{ $listName }}</span> Successfully!<br/>
@else
<p>Your contacts imported to the list Successfully!<br/>
@endif
You can broadcast SMS to the list now.</p>

@component('mail::button', ['url' => $url])
Send SMS
@endcomponent

<p>
	Thanks for choosing {{ config('app.name') }}<br>
    -- The {{ config('app.name') }} Team!
</p>
@endcomponent
