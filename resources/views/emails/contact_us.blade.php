@component('mail::message')
<p><b>Hi {{$name}}, </b></p>
<h2 style="line-height: 30px;">Thank you for your interest in SMS.to</h2>

<p>We received you inquiry. You will hear from us soon!</p>

<p>
	Thanks for choosing {{ config('app.name') }}<br>
    -- The {{ config('app.name') }} Team!
</p>
@endcomponent



