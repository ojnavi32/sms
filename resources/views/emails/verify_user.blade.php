@component('mail::message')
<p><b>Hi {{$user->name}},</b></p>

<p>Thank you for choosing SMS.to. Please verify your email address and you are ready to send SMS world-wide.</p>

@component('mail::button', ['url' => $url])
Verify Email 
@endcomponent

<p>If you’re having trouble clicking the "Verify Email" button, copy and paste the URL below into your web browser: <span class="verify">{{$url}}</span></p>

<br>
<p>
	Thanks for choosing {{ config('app.name') }}<br>
    -- The {{ config('app.name') }} Team!
</p>
@endcomponent
