@extends('layouts.base')

@section('structure_content')
	@yield('content')
@endsection

@section('structure')
	@yield('structure_content')
@endsection

@section('title')
	<title>{{ config('app.name', 'Laravel') }}</title>
@endsection

@section('head_script')
	<script type='text/javascript' src='https://www.gstatic.com/charts/loader.js'></script>
@endsection

@section('head_style')
	<link href="{{ mix('css/app.css') }}" rel="stylesheet" />
@endsection

@section('bottom_script')
	<script>
        window.echoConfig = {
            host: {!! json_encode(env('LARAVEL_ECHO_SERVER_HOST')) !!},
            port: {!! json_encode(env('LARAVEL_ECHO_SERVER_PORT')) !!}
        };
	</script>
	<script src="{{mix('js/app.js')}}"></script>
@endsection
