<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
	<meta charset="utf-8" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<meta name="viewport" content="width=device-width, initial-scale=1" />
	<meta name="csrf-token" content="{{ csrf_token() }}" />
	<meta name="auth-token" content="{{ auth_token() }}" />
	<meta name="api-base" content="{{ api_base() }}" />
	<title>{{ config('app.name', 'Laravel') }}</title>
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" integrity="sha256-eZrrJcwDc/3uDhsdt61sL2oOBY362qM3lon1gyExkL0=" crossorigin="anonymous" />
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/simple-line-icons/2.4.1/css/simple-line-icons.min.css" integrity="sha256-7O1DfUu4pybYI7uAATw34eDrgQaWGOfMV/8erfDQz/Q=" crossorigin="anonymous" />
	<link href="{{ asset('css/manager.css') }}" rel="stylesheet" />
</head>
<body>
	<div id="app" class="container is-fluid">
		<h1 class="title">SMS.To</h1>
		<!-- <h2 class="subtitle">A simple <strong>menu</strong>, for any type of vertical navigation</h2> -->
		<hr />

		<div class="row">
			<div class="col-md-3">
				<div class="row">
					<div class= "col-md-12">
						<aside class="sidebar-nav">
							<p class="menu-label">
								<!-- General -->
							</p>
							<ul class="nav nav-list">
								<li><a href="{{ route('manager.dashboard')}}" class="{{ route_active('manager.dashboard') }}"><i class="fa fa-tasks"></i>  Dashboard</a></li>
								<li><a href="{{ route('manager.members')}}" class="{{ route_active('manager.members') }}"><i class="fa fa-tasks"></i>  Members</a></li>
								<li><a href="{{ route('manager.members.vat')}}" class="{{ route_active('manager.members.vat') }}"><i class="fa fa-tasks"></i>  Member VAT Numbers</a></li>
								<li><a href="{{ route('manager.deleteRequest')}}" class="{{ route('manager.deleteRequest')}}"><i class="fa fa-tasks"></i>  Delete Requests</a></li>
								<li><a href="{{ route('manager.members.non')}}" class="{{ route_active('manager.members.non') }}"><i class="fa fa-tasks"></i>  Non-User Login Attempts</a></li>
								<li><a href="{{ route('manager.providers')}}" class="{{ route_active('manager.providers') }}"><i class="fa fa-tasks"></i>  Providers</a></li>
								<li><a href="{{ route('manager.country-rates')}}" class="{{ route_active('manager.country-rates') }}"><i class="fa fa-tasks"></i>  Country Master Rates</a></li>
								<li><a href="{{ route('manager.rates')}}" class="{{ route_active('manager.rates') }}"><i class="fa fa-tasks"></i>  Carrier Rates</a></li>
								<li><a href="{{ route('manager.sms.jobs')}}" class="{{ route_active('manager.sms.jobs') }}"><i class="fa fa-tasks"></i>  SMS Jobs</a></li>
								<!--<li><a href="{{ route('manager.sms.jobs.api')}}" class="{{ route_active('manager.sms.jobs.api') }}">SMS Jobs (API)</a></li>-->
								<li><a href="{{ route('manager.members.payments')}}" class="{{ route_active('manager.members.payments') }}"><i class="fa fa-tasks"></i>  Member Payments</a></li>
								<!--<li><a href="{{ route('manager.banlist.email')}}" class="{{ route_active('manager.banlist.email') }}">Email Banlist</a></li>-->
								<!--<li><a href="{{ route('manager.banlist.ip')}}" class="{{ route_active('manager.banlist.ip') }}">IP Banlist</a></li>-->
								<li><a href="{{ route('manager.banlist.numbers')}}" class="{{ route_active('manager.banlist.numbers') }}"><i class="fa fa-tasks"></i>  Number Banlist</a></li>
								<li><a href="{{ route('manager.dial.codes')}}" class="{{ route_active('manager.dial.codes') }}"><i class="fa fa-tasks"></i>  Country Dial Codes</a></li>
								<li><a href="{{ route('manager.exchange.rates')}}" class="{{ route_active('manager.exchange.rates') }}"><i class="fa fa-tasks"></i>  Exchange Rates</a></li>
								<li><a href="{{ route('manager.settings')}}" class="{{ route_active('manager.settings') }}"><i class="fa fa-tasks"></i>  Settings</a></li>
								<li><a href="{{ route('manager.creditsPackages.show')}}" class="{{ route_active('manager.creditsPackages.show') }}"><i class="fa fa-tasks"></i>  Credits</a></li>
								<li><a href="{{ route('manager.currency.show')}}" class="{{ route_active('manager.currency.show') }}"><i class="fa fa-tasks"></i>  Currencies</a></li>
								<li><a href="{{ route('manager.logout')}}"><i class="fa fa-tasks"></i>  Logout</a></li>
							</ul>
						</aside>
				    </div>
			    </div>
			</div>
			<div class="col-md-9">
				<div class="row">
			<!--   <section class="section">
						<div class="container"> -->
						@include('manager.notification')
						
						@yield('content')
					 <!--  </div>
				</section> -->
				<div>
			</div>
		</div>
		<footer class="footer">
			<div class="container">
				<div class="content has-text-centered">
					<p>
						<strong>2017 © SMS.to Online SMS. All Rights Reserved.</strong>
					</p>
				</div>
			</div>
		</footer>
	</div>
	<script src="{{ asset('js/manager.js') }}"></script>
	@yield('page_scripts')
</body>
</html>
