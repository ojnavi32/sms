<div id="nav" class="navbar navbar-default nav-dropdown-v1" role="navigation">
    <div class="menu-top menu-top-inverse">
        <div class="container">
            <div class="row">
                <div class="col-sm-5 hidden-xs">
                    <a class="title-menu-top display-inline-block" href="{{ url('/') }}">Back to site</a>
                </div>
                <div class="col-sm-7 col-xs-12">
                    <div class="pull-right">
                        @if (Auth::check())
                            <div class="dropdown dropdown-login pull-left">
                                <a href="{{ url('auth/logout') }}" class="btn-menu-top"><i class="fa fa-user"></i> Logout</a>
                            </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
