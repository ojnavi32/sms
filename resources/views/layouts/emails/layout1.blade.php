@extends('layouts.emails.layout')

@section('content')
<table style="margin:0px auto;border-spacing:0px" width="600" cellpadding="0" cellspacing="0" bgcolor="#ffffff" border="0" class="tb_wrap">
    <tbody>
        <tr>
            <td style="background-color:rgb(51,51,51);padding:20px 0px 17px 50px;border-collapse:collapse" align="left" bgcolor="#333333">
                <span style="color:rgb(255,255,255);font-family:Helvetica,sans-serif;font-size:23px;line-height:1.5;text-decoration:none"></span>
            </td>
        </tr>
        <tr>
            <td style="border:1px solid rgb(238,238,238);background-color:rgb(255,255,255);padding:35px 50px;font-size:13px;line-height:20px;font-family:Helvetica,sans-serif;color:rgb(51,51,51);border-collapse:collapse" align="left" bgcolor="#ffffff">
                <table style="margin:0px auto;border-spacing:0px;color:rgb(51,51,51);font-family:Helvetica,sans-serif;font-size:13px" width="100%" cellpadding="0" cellspacing="0" border="0">
                    <tbody>
                        <tr>
                            <td>
                                <p>
                                    @yield('message')<br />
                                    <br />
                                    <div dir="ltr"><p style="font-size:small"><font face="verdana, sans-serif" color="#999999" size="2">Intergo Telecom Ltd.</font></p><p style="font-size:small"><font face="verdana, sans-serif" size="2"><font color="#ff9900">Tel:</font>&nbsp;+1 (646) 520-0680</font></p><p style="font-size:small"><font face="verdana, sans-serif" size="2"><font color="#ff9900">Email:</font>&nbsp;<a href="mailto:support@sms.to" target="_blank">support@sms.to</a></font></p><p style="font-size:small"><font face="verdana, sans-serif" size="2"><font color="#ff9900">Site:&nbsp;</font><a href="https://sms.to/" style="color:rgb(17,85,204)" target="_blank">www.sms.to</a></font></p><div>
                                        <!-- <img src="/images/logo.svg" style="font-size:12.8px" class="CToWUd"> -->
                                        <div class="yj6qo"></div><div class="adL"><br /></div></div></div>
                                </p>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </td>
        </tr>
        <tr>
            <td style="background-color:rgb(57,54,51);border-collapse:collapse;background-repeat:initial initial">&nbsp;</td>
        </tr>
    </tbody>
</table>
@endsection
