<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"
      xmlns:v="urn:schemas-microsoft-com:vml"
      xmlns:o="urn:schemas-microsoft-com:office:office">
    <head>
        <!--[if gte mso 9]><xml>
            <o:OfficeDocumentSettings>
                <o:AllowPNG/>
                <o:PixelsPerInch>96</o:PixelsPerInch>
            </o:OfficeDocumentSettings>
        </xml><![endif]-->
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="viewport" content="width=device-width" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta name="format-detection" content="telephone=no" />
        <title>SMS.to</title>
        <style type="text/css">
            body {
                mso-line-height-rule: exactly;
                -webkit-text-size-adjust: none;
                -ms-text-size-adjust: none;
            }
            a {
                mso-line-height-rule: exactly;
            }
            table {
                border-collapse: collapse;
                padding: 0px !important;
                mso-table-lspace: 0pt;
                mso-table-rspace: 0pt;
            }

            div[class=mobile_only] {
                display: none;
                height: 0;
                max-height: 0;
                overflow: hidden;
            }

            @media only screen and (max-width:480px) {
                table[class=tb_wrap] {
                    width: 100% !important;
                }
                div[class=desktop_only],
                div[class="mobile_only screen_max_640"] {
                    display: none;
                    height: 0;
                    max-height: 0;
                    overflow: hidden;
                }
                div[class="mobile_only screen_max_480"] {
                    display: block !important;
                    height: auto !important;
                    max-height: none !important;
                    overflow: visible !important;
                }
                div[class="mobile_only screen_max_480"] table {
                    display: table !important;
                }
            }

            @media only screen and (min-width:481px) and (max-width:640px) {
                table[class=tb_wrap] {
                    width: 100% !important;
                }
                div[class=desktop_only],
                div[class="mobile_only screen_max_480"] {
                    display: none;
                    height: 0;
                    max-height: 0;
                    overflow: hidden;
                }
                div[class="mobile_only screen_max_640"] {
                    display: block !important;
                    height: auto !important;
                    max-height: none !important;
                    overflow: visible !important;
                }
                div[class="mobile_only screen_max_640"] table {
                    display: table !important;
                }
            }
        </style>
    </head>

    <body style="width: 100%; margin: 0; padding: 0; -webkit-text-size-adjust: none; -ms-text-size-adjust: none; -webkit-font-smoothing: antialiased;">
        <table width="100%" border="0" cellpadding="0" cellspacing="0">
            <tbody>
                <tr>
                    <td valign="top" style="padding: 0 4px;">
                        <table style="font-family:Helvetica;font-size:12px;font-style:normal;font-variant:normal;font-weight:normal;letter-spacing:normal;line-height:normal;text-align:start;text-indent:0px;text-transform:none;white-space:normal;word-spacing:0px;border-spacing:0px" height="100%" width="100%" cellpadding="0" cellspacing="0" bgcolor="#fff" border="0">
                            <tbody>
                                <tr>
                                    <td style="background-color:rgb(255,255,255);border-collapse:collapse">
                                        <br />
                                        <table style="margin:0px auto;border-spacing:0px" width="600" cellpadding="0" cellspacing="0" bgcolor="#fff" border="0" class="tb_wrap">
                                            <tbody>
                                                <tr>
                                                    <td style="background-color:rgb(255,255,255);font-size:13px;line-height:20px;font-family:Helvetica,sans-serif;color:rgb(51,51,51);border-collapse:collapse" align="left" bgcolor="#fff">
                                                        <table style="margin:0px auto;border-spacing:0px" width="600" cellpadding="0" cellspacing="0" bgcolor="#fff" border="0" class="tb_wrap">
                                                            <tbody>
                                                                <tr>
                                                                    <td style="border-collapse:collapse">
                                                                        <a href="https://sms.to" style="text-decoration:none;color:rgb(91,212,188)" target="_blank">
                                                                            <!-- <img class="CToWUd" alt="sms.to" src="../../../assets/images/logo.svg" style="color:rgb(0,153,153)" height="60" > -->
                                                                        </a>
                                                                    </td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                        <br />
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>

                                        @yield('content')

                                        <table style="margin:0px auto;border-spacing:0px" width="600" cellpadding="0" cellspacing="0" bgcolor="#fff" border="0" class="tb_wrap">
                                            <tbody>
                                                <tr>
                                                    <td style="background-color:rgb(255,255,255);padding-left:10px;padding-right:10px;font-size:12px;line-height:20px;font-family:Helvetica,sans-serif;color:rgb(51,51,51);border-collapse:collapse" align="left" bgcolor="#fff">
                                                        <table style="border-spacing:0px" width="580" cellpadding="0" cellspacing="0" bgcolor="#fff" border="0" class="tb_wrap">
                                                            <tbody>
                                                                <tr>
                                                                    <td style="border-bottom-width:1px;border-bottom-style:solid;border-bottom-color:rgb(218,217,211);border-collapse:collapse;font-size:12px;font-family:Helvetica,sans-serif;padding:15px 0px" align="center">
                                                                        Connect with us: <a href="https://twitter.com/smstoapp" rel="nofollow" style="text-decoration:none;color:rgb(0,119,204)" target="_blank">@smstoapp</a> on Twitter, <a href="https://www.facebook.com/smstoapp" rel="nofollow" style="text-decoration:none;color:rgb(0,119,204)" target="_blank">smstoApp</a> on Facebook Page
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td style="color:rgb(153,153,153);font-family:Helvetica,sans-serif;font-size:11px;line-height:14px;border-collapse:collapse;padding:15px 0px" align="center">
                                                                        <b>SMS.to</b>
                                                                        <br />
                                                                        Omonoias 1, Geroskipou, Cyprus<br />Website: <a href="https://sms.to" target="_blank">www.sms.to</a>
                                                                    </td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
            </tbody>
        </table>
    </body>
</html>