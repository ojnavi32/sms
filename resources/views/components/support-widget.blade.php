<div class="menu-label is-small">Contact us</div>
<nav class="level is-mobile style-support-item">
	<div class="level-left">
		<a href="#" class="level-item">
			<span class="icon">
				<span class="icon-bubbles"></span>
			</span>
			<span>Chat with us</span>
		</a>
	</div>
	<div class="level-right">
		<div class="level-item is-status">online</div>
	</div>
</nav>
<nav class="level is-mobile style-support-item">
	<div class="level-left">
		<a href="mailto:support@sms.to" class="level-item">
			<span class="icon">
				<span class="icon-envelope"></span>
			</span>
			<span>Send Email</span>
		</a>
	</div>
	<div class="level-right">
		<div class="level-item is-status">online</div>
	</div>
</nav>
{{-- <nav class="level is-mobile style-support-item">
	<div class="level-left">
		<a href="#" class="level-item">
			<span class="icon">
				<span class="icon-phone"></span>
			</span>
			<span>+1 238 782 898</span>
		</a>
	</div>
	<div class="level-right">
		<div class="level-item is-status">online</div>
	</div>
</nav> --}}
<div class="menu-label is-small">Support</div>
<nav class="level is-mobile style-support-item">
	<div class="level-left">
		<a href="{{ route('faq') }}" class="level-item">
			<span class="icon">
				<span class="icon-question"></span>
			</span>
			<span>FAQ</span>
		</a>
	</div>
	<div class="level-right"></div>
</nav>
{{-- <nav class="level is-mobile style-support-item">
	<div class="level-left">
		<a href="#" class="level-item">
			<span class="icon">
				<span class="icon-notebook"></span>
			</span>
			<span>Knowledge Base</span>
		</a>
	</div>
	<div class="level-right"></div>
</nav> --}}
<div class="menu-label is-small">Connect</div>
<div class="social">
	@include('components.social')
</div>
