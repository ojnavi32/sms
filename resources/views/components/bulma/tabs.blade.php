<div class="tabs">
	<ul>
		@if (isset($tabs))
			@foreach ($tabs as $tab)
				@if (Route::is($tab['route']))
					<li class="is-active"><a>{{$tab['label']}}</a></li>
				@else
					<li><a href="{{route($tab['route'])}}">{{$tab['label']}}</a></li>
				@endif
			@endforeach
		@endif
	</ul>
</div>
