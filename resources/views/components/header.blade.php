<header class="header">	
	<div class="container">
		<nav class="navbar style-main">
			<div class="navbar-brand">
				<a class="navbar-item" href="{{ url('/') }}">
					@include('components.logo')
				</a>
				<div class="navbar-burger burger" data-target="navbarMenu">
					<span></span>
					<span></span>
					<span></span>
				</div>
			</div>

			<div id="navbarMenu" class="navbar-menu">
				<div class="navbar-start">
					<a class="navbar-item" href="{{ url('/') }}">Send Bulk SMS</a>
					<a class="navbar-item" href="{{ url('sms-api') }}">SMS API</a>
					<a class="navbar-item" href="{{ route('pricing') }}">Pricing</a>

					<div class="navbar-item has-dropdown is-hoverable">
						<a class="navbar-link has-no-arrow" href="#more">
							<span></span>
							<span class="icon">
								<span class="icon-options"></span>
							</span>
						</a>
						<div class="navbar-dropdown is-boxed">
							<a class="navbar-item" href="{{ statelessRoute('about') }}">About</a>
							<a class="navbar-item" href="https://blog.sms.to/">Blog</a>
							<a class="navbar-item" href="{{ statelessRoute('contact-us') }}">Contact Us</a>
						</div>
					</div>
				</div>

				<div class="navbar-end">
					
					<div class="navbar-item has-dropdown is-hoverable">
						<a href="#" class="navbar-link has-no-arrow">
							<span class="icon-earphones-alt"></span>
						</a>
						<div class="navbar-dropdown is-boxed is-right is-detailed">
							@include('components.support-widget')
						</div>
					</div>

					@if (Auth::guest())
						<div class="navbar-item">
							<a href="{{ route('login') }}" class="button is-light">
								<span class="icon">
									<span class="icon-login"></span>
								</span>
								<span>Sign in / Sign up</span>
							</a>
						</div>
					@else
						{{-- <a href="#" class="navbar-item">
							<span class="icon-bell has-updates"></span>
						</a> --}}
						<div class="navbar-item has-dropdown is-hoverable">
							<a class="navbar-link" href="#">
								<figure class="image is-avatar is-32x32">
									@if (Auth::user()->avatarUrl)
										<img src="{{Auth::user()->avatarUrl}}" class="avatar-photo" />
									@endif
								</figure>
							</a>
							<div class="navbar-dropdown is-boxed is-right">
								<form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
									{{ csrf_field() }}
								</form>
								<a class="navbar-item" href="{{ route('app.dashboard') }}">
									<span class="icon">
										<span class="icon-speedometer"></span>
									</span>
									<span>Dashboard</span>
								</a>
								<a class="navbar-item" href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
									<span class="icon">
										<span class="icon-logout"></span>
									</span>
									<span>Logout</span>
								</a>
							</div>
						</div>
					@endif
				</div>
			</div>
		</nav>
	</div>
</header>

