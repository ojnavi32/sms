<form class="navbar-item searchbox">
	<div class="field">
		<div class="control has-icons-right">
			<input type="text" class="input" placeholder="Search..." />
			<span class="icon is-small is-right"><i class="icon-magnifier"></i></span>
		</div>
	</div>
</form>
