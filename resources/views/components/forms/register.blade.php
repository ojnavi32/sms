 <form method="POST" action="{{ route('register') }}">
	{{ csrf_field() }}

	@include('components.form.field', [
		'icon' => 'icon-people',
		'name' => 'name',
		'type' => 'text',
		'placeholder' => 'Your full name',
		'value' => old('name'),
		'required' => true,
		'autofocus' => true
	])
	@include('components.form.field', [
		'icon' => 'icon-envelope',
		'name' => 'email',
		'type' => 'email',
		'placeholder' => 'Email address',
		'value' => old('email'),
		'required' => true
	])
	@include('components.form.field', [
		'icon' => 'icon-lock',
		'name' => 'password',
		'type' => 'password',
		'placeholder' => 'Password (min. 6 characters)',
		'value' => '',
		'required' => true
	])
	@include('components.form.field', [
		'icon' => 'icon-lock',
		'name' => 'password_confirmation',
		'type' => 'password',
		'placeholder' => 'Confirm password',
		'value' => '',
		'required' => true
	])

	@include('components.form.checkbox', [
		'name' => 'terms_conditions',
		'label' => 'I have read and agree to SMS.to\'s <a href="'.url('terms').'">terms and conditions</a> and the <a href="'.url('privacy-policy').'">privacy policy</a>.',
		'value' => '1',
		'required' => true
	])
	@include('components.form.checkbox', [
		'name' => 'special_offers',
		'label' => 'I wish to receive the latest news, special offers, and money off coupons from SMS.to. <br /><i>We\'ll always treat your personal details with utmost care and will never be sold to 3rd parties for marketing purposes. You can remove your consent at any time.</i>',
		'value' => '1',
	])

	<div class="field">
		<div class="control">
			<button type="submit" class="button is-info is-fullwidth">Register</button>
		</div>
	</div>
</form>
