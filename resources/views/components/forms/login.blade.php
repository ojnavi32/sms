<form method="POST" action="{{ route('login') }}">
	{{ csrf_field() }}
	
	@include('components.form.field', [
		'icon' => 'icon-envelope',
		'name' => 'email',
		'type' => 'email',
		'placeholder' => 'Email address',
		'value' => old('email'),
		'required' => true
	])
	@include('components.form.field', [
		'icon' => 'icon-lock',
		'name' => 'password',
		'type' => 'password',
		'placeholder' => 'Password (min. 6 characters)',
		'value' => '',
		'required' => true
	])

	<div class="has-text-right">
		<a href="{{ route('password.request') }}">Forgot password?</a>
	</div>
	<br />
	<div class="field is-hidden">
		<p class="control">
			{{-- <label class="checkbox"><input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}> Remember me </label> --}}
			<label class="checkbox"><input type="checkbox" name="remember" checked> Remember me </label>
		</p>
	</div>

	<div class="field">
		<div class="control">
			<button type="submit" class="button is-info is-fullwidth">Login</button>
		</div>
	</div>

	<div class="social-auth">
		<div class="subtitle">- Or continue with -</div>
		<div class="columns is-mobile">
			<div class="column">
				<a href="{{route('auth.google')}}" class="button is-block">
					<span class="icon">
						<span class="icon-social-google"></span>
					</span>
				</a>
			</div>
			<div class="column">
				<a href="{{route('auth.facebook')}}" class="button is-block">
					<span class="icon">
						<span class="icon-social-facebook"></span>
					</span>
				</a>
			</div>
			<div class="column">
				<a href="{{route('auth.twitter')}}" class="button is-block">
					<span class="icon">
						<span class="icon-social-twitter"></span>
					</span>
				</a>
			</div>
		</div>
	</div>
</form>
