<form method="POST" action="{{ route('password.request') }}">
	{{ csrf_field() }}

    <input type="hidden" name="token" value="{{ $token }}" />
	
	@include('components.form.field', [
		'icon' => 'icon-envelope',
		'name' => 'email',
		'type' => 'email',
		'placeholder' => 'Email address',
		'value' => old('email'),
		'required' => true
	])
	@include('components.form.field', [
		'icon' => 'icon-lock',
		'name' => 'password',
		'type' => 'password',
		'placeholder' => 'Password (min. 6 characters)',
		'value' => '',
		'required' => true
	])
	@include('components.form.field', [
		'icon' => 'icon-lock',
		'name' => 'password_confirmation',
		'type' => 'password',
		'placeholder' => 'Confirm password',
		'value' => '',
		'required' => true
	])

	<div class="field">
		<div class="control">
			<button type="submit" class="button is-info is-fullwidth">Reset Password</button>
		</div>
	</div>
</form>
