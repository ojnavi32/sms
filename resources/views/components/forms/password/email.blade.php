<form method="POST" action="{{ route('password.email') }}">
	{{ csrf_field() }}
	
	@include('components.form.field', [
		'icon' => 'icon-envelope',
		'name' => 'email',
		'type' => 'email',
		'placeholder' => 'Email address',
		'value' => old('email'),
		'required' => true
	])

		

	<div class="field">
		<div class="control">
			<button type="submit" class="button is-info is-fullwidth">Send Password Reset Link</button>
		</div>
	</div>

	@if(Session::has('warning'))
		<p class="notification is-danger">{{Session::get('warning')}}</p>
	@elseif(Session::has('status'))
		<p class="notification" style="color:green">{{Session::get('status')}}</p>
	@elseif(Session::has('message'))
		<p class="notification is-danger">{{Session::get('message')}}</p>
	@endif
</form>



