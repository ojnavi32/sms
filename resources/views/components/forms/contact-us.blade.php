 @if(!Session::has('success'))
 <form method="POST" action="{{ route('post.contact-us') }}">

	@include('components.form.field', [
		'icon' => 'icon-people',
		'name' => 'name',
		'type' => 'text',
		'label' => 'Full name',
		'placeholder' => 'Your full name here...',
		'value' => old('name'),
		'required' => true,
		'autofocus' => true
	])
	@include('components.form.field', [
		'icon' => 'icon-envelope',
		'name' => 'email',
		'type' => 'email',
		'label' => 'Email address',
		'placeholder' => 'Email address here...',
		'value' => old('email'),
		'required' => true
	])
	@include('components.form.field', [
		'icon' => 'icon-phone',
		'name' => 'phone',
		'type' => 'phone',
		'label' => 'Phone',
		'placeholder' => 'Your phone here...',
		'value' => old('phone')
	])
	@include('components.form.field', [
		'icon' => 'icon-text',
		'name' => 'message',
		'type' => 'textarea',
		'label' => 'Message',
		'placeholder' => 'Your message...',
		'value' => old('message'),
		'required' => true
	])

	{!! Form::token() !!}
	
	<br />
	{!! app('captcha')->display() !!}
	<span style="color:red">{{ $errors->first('g-recaptcha-response') }}</span>
	<br/>
	<br/>
	<div class="control columns is-mobile is-centered">
		<button type="submit" class="button is-info">Send email now</button>
	</div>
	@endif
	@if ($message = Session::get('success'))
		<strong style="color:green"><center>{{ $message }}</center></strong>
	@endif	
</form>
