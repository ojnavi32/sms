@section('title')
<title>SMS.To FAQ | Frequently Asked Questions about Buk SMS</title>
@endsection
@section('description') 
	<meta name="description" content="Here are some frequently asked questions by users. If you can't find a solution for your bulk sms problem here at SMS.To, feel free to contact us." />
@endsection
@section('keywords') 
	<meta name="keywords" content="bulk sms, sms faq">
@endsection
<section class="hero is-info is-bold">
	<div class="hero-body">
		<div class="container">
			<h1 class="title">Frequently Asked Questions</h1>
			<p class="subtitle">Here are some frequently asked questions by users. If you can't find a solution for your problem here, feel free to contact us.</p>
		</div>
	</div>
</section>

<section class="section is-medium style-legal">
	<div class="container">
		<div class="content">
			<h2>Can I send a text message from my company’s name?</h2>
			<p>
				Yes! Technically it’s possible with your company name, but keep in mind that this is limited to between 3 and 11 characters and subject to any country restrictions.
			</p>
			<h2>What browsers can I use?</h2>
			<p>
				We support all major web browsers (Chrome, Firefox, Edge, etc) - besides Internet Explorer 8, 9 and 10, but anything from Internet Explorer 11 and over is supported. You can find out which version your browser is by going to the 'about' section of your browser.
			</p>
			<h2>How can I change my password?</h2>
			<p>
				You can change your password by requesting a Password reset and then clicking on the link sent to your email. Or, you can change the password from within your SMS.to account by going to Account > General and enter your new password in there!
			</p>
		</div>
	</div>
</section>
