@section('title')
<title>Robust API's and intuitive Web Platforms | SMS.To</title>
@endsection
@section('description') 
	<meta name="description" content="SMS.to Mission is to simplify business communications and allow a quick and efficient go to market strategy by our Robust API's and intuitive Web Platforms." />
@endsection
@section('keywords') 
	<meta name="keywords" content="robust api, robust apis">
@endsection
<div class="card">
	<div class="card-content">
		<p class="title">
			“There are two hard things in computer science: cache invalidation, naming things, and off-by-one errors.”
		</p>
		<p class="subtitle">
			Jeff Atwood
		</p>
	</div>
	<footer class="card-footer">
		<p class="card-footer-item">
			<span>
				View on <a href="https://twitter.com/codinghorror/status/506010907021828096">Twitter</a>
			</span>
		</p>
		<p class="card-footer-item">
			<span>
				Share on <a href="#">Facebook</a>
			</span>
		</p>
	</footer>
</div>
