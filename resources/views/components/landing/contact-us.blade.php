@section('title')
<title>Contact Bulk SMS Service To Get Started with SMS.To Platform </title>
@endsection
@section('description') 
	<meta name="description" content="Feel free to contact the sophisticated SMS Marketing platform, OTP (One time passwords), 2FA ( 2 Factor Authentication), notifications and Viber Messaging." />
@endsection
@section('keywords') 
	<meta name="keywords" content="bulk sms service, sms marketing">
@endsection

<section class="hero is-info is-bold">
	<div class="hero-body">
		<div class="container">
			<h1 class="title">Contact Us</h1>
			<p class="subtitle">Our expert team is happy to answer any questions you may have, and help get you started with SMS for business.</p>
		</div>
	</div>
</section>

<section class="section is-medium style-legal">
	<div class="container">
		<div class="content">
			
			<div class="columns">
				<div class="column">
					@include('components.forms.contact-us') 
				</div>
				<div class="column">
					
					<div class="menu-label">A more direct Line</div>
					<nav class="level is-mobile style-contactus-item">
						<div class="level-left">
							<div class="level-item">
								<span class="icon is-medium">
									<span class="icon-envelope"></span>
								</span>
							</div>
							<div class="level-item with-title">
								<div>Customer services</div>
								<a href="#">sales@sms.to</a>
							</div>
						</div>
					</nav>
					<nav class="level is-mobile style-contactus-item">
						<div class="level-left">
							<div class="level-item">
								<span class="icon is-medium">
									<span class="icon-envelope"></span>
								</span>
							</div>
							<div class="level-item with-title">
								<div>Support</div>
								<a href="#">support@sms.to</a>
							</div>
						</div>
					</nav>

					<nav class="level is-mobile style-contactus-item">
						<div class="level-left">
							<div class="level-item">
								<span class="icon is-medium">
									<span class="icon-envelope-open"></span>
								</span>
							</div>
							<div class="level-item with-title">
								<div>Mailling address</div>
								<a href="#">Intergo Telecom Ltd, <br />Nikolaou Nikolaidi 3, Office 206, <br />Paphos, 8010, Cyprus</a>
							</div>
						</div>
					</nav>

					<nav class="level is-mobile style-contactus-item">
						<div class="level-left">
							<div class="level-item">
								<span class="icon is-medium">
									<span class="icon-phone"></span>
								</span>
							</div>
							<div class="level-item with-title">
								<div>Phone</div>
								<a href="#">+1 238 782 898</a>
							</div>
						</div>
					</nav>

					<nav class="level is-mobile style-contactus-item">
						<div class="level-left">
							<div class="level-item">
								<span class="icon is-medium">
									<span class="icon-bubbles"></span>
								</span>
							</div>
							<div class="level-item with-title">
								<div>Chat with us</div>
								<a href="#">LIVE</a>
							</div>
						</div>
					</nav>

					<div class="menu-label">Support</div>
					<nav class="level is-mobile style-contactus-item">
						<div class="level-left">
							<div class="level-item">
								<span class="icon is-medium">
									<span class="icon-question"></span>
								</span>
							</div>
							<div class="level-item">
								<a href="#">FAQ</a>
							</div>
						</div>
					</nav>
					<nav class="level is-mobile style-contactus-item">
						<div class="level-left">
							<div class="level-item">
								<span class="icon is-medium">
									<span class="icon-notebook"></span>
								</span>
							</div>
							<div class="level-item">
								<a href="#">Knowledge Base</a>
							</div>
						</div>
					</nav>

				</div>
			</div>
		</div>
	</div>
</section>

<script type="application/ld+json">
      {
        "@context": "http://schema.org",
        "@type": "Organization",
        "address": {
          "@type": "PostalAddress",
          "addressLocality": "Paphos, Cyprus",
          "postalCode": "8010",
          "streetAddress": " Nikolaou Nikolaidi 3, Office 206"
        },
        "sameAs": [
          " https://www.facebook.com/SMSto-Bulk-SMS-SMS-Marketing-1909869432637539/",
          " https://twitter.com/smstoapp",
          " https://www.linkedin.com/company/sms-to",
          " https://www.youtube.com/channel/UC-Qo3znwQKmAAd-ZaUvDfmQ",
        ],
        "name": "Sms.to",
        "url": " https://sms.to/",
        "logo": "https://sms.to/images/logo.svg"
      }
</script>

