@section('title')
<title>Bulk SMS Rates, Pricing and Discounts | SMS.To</title>
@endsection
@section('description') 
	<meta name="description" content="In case of high volume SMS API usage, our sales team is prepared to give out additional discount. No contracts, no commitments, pay only for what you use." />
@endsection
@section('keywords') 
	<meta name="keywords" content="sms api usage, sms rates, bulk sms rates, sms pricing, bulk sms pricing">
@endsection

<section class="hero is-info is-bold">
	<div class="hero-body">
		<div class="container">
			<h1 class="title">Our Prices</h1>
		</div>
	</div>
</section>

<br />
<div id="cost-calculator">
	<section class="section">
		<div class="container">
			<div class="columns pricing-hero-content">
				<div class="column is-full-mobile is-two-thirds-tablet is-half-desktop is-one-third-widescreen">
					<h1 class="title is-5">Destination</h1>
					<form-field
						key="country"
						:model="countries"
						property="selected"
						:options="countries.list"
						option-track-by="iso2"
						option-value="iso2"
						option-label="name"
						:property-is-option-value="true"
						:multiple="false"
						:searchable="true"
						type="multiselect"
						placeholder="Select country"
						
						class="style-light"

						label="SMS to"
					></form-field>

					<div class="field style-light">
						<label class="label">Volume per month</label>
						<label class="label style-secondary sms-count">@{{amount}} SMS</label>
						<p class="control">
							<input class="input-range" type="range" step="100" min="100" max="100000" v-model="amount" />
						</p>
					</div>
				</div>
				<div class="column">
					<h2 class="title is-5">Bulk Rates</h2>
					<p>
						No contracts, no commitments, pay only for what you use. 
						In case  of high volume SMS API usage, our sales team is 
						prepared to give out additional discounts. Contact us to 
						for more details.
					</p>
					<br />
					<a href="{{ route('contact-us') }}" class="button is-info is-outlined" :class="{ 'is-danger': showContactUs }">Contact Us &rarr;</a>
				</div>
			</div>
		</div>
	</section>
	<section class="hero is-medium style-bg-gray">
		<div class="hero-body">
			<div class="container">
				<div class="columns">
					<div class="column">
						<div class="card">
							<div class="card-content">
								<div class="media">
									<div class="media-left"></div>
									<div class="media-content">
										<p class="title is-4">SMS</p>
									</div>
								</div>
								<div class="content">
									Fast, reliable message delivery through our globally connected API.
								</div>
							</div>
						</div>
					</div>
					<div class="column">
						<div class="card">
							<div class="card-content">
								<div class="media">
									<div class="media-left">
										<div class="icon is-medium">
											<div class="icon-call-out"></div>
										</div>
									</div>
									<div class="media-content">
										<p class="title is-4">Outbound</p>
									</div>
								</div>
								<div class="content">
									<span class="is-inline-flex has-text-info" v-if="isLoadingCost">
										<span class="loader"></span>
									</span>
									<span id="estimated" class="title is-4 has-text-info" v-if="!isLoadingCost">&euro;@{{costPerSMS}}</span>
									<div>price per sms</div>
								</div>
							</div>
						</div>
					</div>
					<div class="column">
						<div class="card">
							<div class="card-content">
								<div class="media">
									<div class="media-left">
										<div class="icon is-medium">
											<div class="icon-call-in"></div>
										</div>
									</div>
									<div class="media-content">
										<p class="title is-4">Inbound</p>
									</div>
								</div>
								<div class="content">
									<p class="title is-4 has-text-info">Free<p>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
</div>
