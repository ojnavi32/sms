@section('title')
<title>Robust API's and intuitive Web Platforms | SMS.To</title>
@endsection
@section('description') 
	<meta name="description" content="SMS.to Mission is to simplify business communications and allow a quick and efficient go to market strategy by our Robust API's and intuitive Web Platforms." />
@endsection
@section('keywords') 
	<meta name="keywords" content="robust api, robust apis">
@endsection

<section class="hero is-medium">
	<div class="hero-body">
		<div class="container">
			<h2 class="title is-2 has-text-centered">Mentions in the Media</h2>
			<div class="columns is-multiline is-centered">
				<div class="column is-one-third">
					@include('components.landing.press-media-item')
				</div>
				<div class="column is-one-third">
					@include('components.landing.press-media-item')
				</div>
				<div class="column is-one-third">
					@include('components.landing.press-media-item')
				</div>
				<div class="column is-one-third">
					@include('components.landing.press-media-item')
				</div>
				<div class="column is-one-third">
					@include('components.landing.press-media-item')
				</div>
				<div class="column is-one-third">
					@include('components.landing.press-media-item')
				</div>
			</div>
		</div>
	</div>
</section>
