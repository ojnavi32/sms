@section('title')
<title>SMS Marketing and SMS API Gateway Platform | About SMS.To</title>
@endsection
@section('description') 
	<meta name="description" content="SMS.to is a complete SMS Marketing & SMS API gateway platform offering Enterprise grade Omni-Channel digital communication services to businesses." />
@endsection
@section('keywords') 
	<meta name="keywords" content="sms api gateway, sms marketing platform, otp. sms api gateway platform">
@endsection
<section class="hero is-info is-bold">
	<div class="hero-body">
		<div class="container">
			<h1 class="title">About Us</h1>
		</div>
	</div>
</section>

<section class="section is-medium style-legal">
	<div class="container">
		<div class="content">
			<h2>Who We Are</h2>
			<p>
				SMS.to is a complete SMS Marketing & SMS API gateway platform offering Enterprise grade Omni-Channel digital communication services to businesses.
			</p>
			<p>
				That includes a sophisticated SMS Marketing platform, OTP (One time passwords), 2 Way SMS, 2FA ( 2 Factor Authentication), notifications and Viber Messaging.
			</p>

			<h2>Our Mission</h2>
			<p>
				SMS.to Mission is to simplify business communications by disrupting the aged telecom model of lengthy negotiations and processes and allow a quick and efficient 
				go to market strategy through our Robust API's and intuitive Web Platforms.
			</p>
			<p>
				Businesses can integrate and reach their customers in hours not months from the day of setup, 
				achieving cost and time efficiency and most importantly competitiveness through all the value added features SMS.to provides.
			</p>
		</div>
	</div>
</section>
