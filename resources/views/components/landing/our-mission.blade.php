@section('title')
<title>Robust API's and intuitive Web Platforms | SMS.To</title>
@endsection
@section('description') 
	<meta name="description" content="SMS.to Mission is to simplify business communications and allow a quick and efficient go to market strategy by our Robust API's and intuitive Web Platforms." />
@endsection
@section('keywords') 
	<meta name="keywords" content="robust api, robust apis">
@endsection
<section class="hero is-medium style-bg-gray">
	<div class="hero-body">
		<div class="container">
			<h2 class="title is-2 has-text-centered">Our Mission</h2>
			<p>
				SMS.to Mission is to simplify business communications by disrupting the aged telecom model of lengthy negotiations and processes and allow a quick and efficient 
				go to market strategy through our Robust API's and intuitive Web Platforms.
			</p>
			<p>
				Businesses can integrate and reach their customers in hours not months from the day of setup, a
				chieving cost and time efficiency and most importantly competitiveness through all the value added features SMS.to provides.
			</p>
		</div>
	</div>
</section>
