@section('title')
<title>Bulk SMS Refund Policy | Bulk SMS Service Provider | SMS.To</title>
@endsection
@section('description') 
	<meta name="description" content="If you are not 100% satisfied with your purchase, contact SMS.To within 14 days from the purchase date. We will provide a full refund of your unused credits." />
@endsection
@section('keywords') 
	<meta name="keywords" content="bulk sms refund, bulk sms credits, sms credits">
@endsection


<section class="hero is-info is-bold">
	<div class="hero-body">
		<div class="container">
			<h1 class="title">Refund Policy</h1>
		</div>
	</div>
</section>

<section class="section is-medium style-legal">
	<div class="container">
		<div class="content">
			<h2>REFUND POLICY</h2>
			<p>If you are not 100% satisfied with your purchase, you may contact us within 14 days from the purchase date and we will provide a full refund of your unused credits.</p>
			<p>Please note that used credits are not refundable.</p>
			<p>Additionally after 14 days there are no refunds for Credit in your account.</p>
		</div>
	</div>
</section>
