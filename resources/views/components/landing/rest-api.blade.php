<section class="section style-rest-api">
	@include('components.coming-soon')
	<div class="container">
		<div class="columns is-gapless">
			<div class="column is-half">
				<div class="section-desc">
					<h1 class="title">SMS REST API for Developers</h1>
					<p class="description">
						Implement SMS notifications, OTP, reminders etc. into your workflow and build apps that send SMS with our redundant SSL SMS API.
					</p>
					<br />
					<a href="#" class="button is-link">GET API KEY &rarr;</a>
					<div class="actions">
						<a href="#">
							<span class="icon is-small">
								<span class="icon-book-open"></span>
							</span>
							<span>Read Docs</span>
						</a>
						<a href="#">
							<span class="icon is-small">
								<span class="icon-rocket"></span>
							</span>
							<span>Get The SDKs</span>
						</a>
						<a href="#">
							<span class="icon is-small">
								<span class="icon-social-github"></span>
							</span>
							<span>Git</span>
						</a>
					</div>
				</div>
			</div>
			<div class="column is-half cutout">
				<div class="tabs-wrapper">
					<div class="tabs is-centered is-boxed is-fullwidth">
						<ul>
							<li class="is-active"><a href="#apiCurl" data-toggle="tab">cURL</a></li>
							<li><a href="#apiPhp" data-toggle="tab">PHP</a></li>
							<li><a href="#apiPython" data-toggle="tab">Python</a></li>
							<li><a href="#apiRuby" data-toggle="tab">Ruby</a></li>
							<li><a href="#apiCsharp" data-toggle="tab">C#</a></li>
						</ul>
					</div>
					<div class="tabs-content">
						<div class="tab is-active" id="apiCurl">
							<pre>#!/bin/bash
Lorem ipsum dolor sit amet, consectetur adipisicing elit. Doloremque sint, cumque ad, eos exercitationem dicta fuga sequi voluptate, iste nisi mollitia alias non, minus nemo repellat obcaecati magnam. Ullam, animi?
<br />
Lorem ipsum dolor sit amet, consectetur adipisicing elit. Rem porro laudantium, aut veritatis neque quidem error blanditiis odit similique beatae saepe inventore non voluptas, quae et mollitia nobis accusantium! Ratione!
							</pre>
						</div>
						<div class="tab" id="apiPhp">
							<pre>PHP code here...</pre>
						</div>
						<div class="tab" id="apiPython">
							<pre>Python code here...</pre>
						</div>
						<div class="tab" id="apiRuby">
							<pre>Ruby code here...</pre>
						</div>
						<div class="tab" id="apiCsharp">
							<pre>C# code here...</pre>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
