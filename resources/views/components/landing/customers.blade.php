<section class="section style-customers">
	<div class="container">
		<h1 class="title is-5">Trusted by 15,000 customers</h1>
		<div class="customers">
			<div class="customer microsoft"></div>
			<div class="customer electronic-arts"></div>
			<div class="customer soundcloud"></div>
			<div class="customer shopify"></div>
			<div class="customer huge"></div>
		</div>
	</div>
</section>
