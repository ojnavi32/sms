<section id="features" class="section style-features">
	<div class="container">
		<div class="columns is-gapless-around">
			<div class="column is-8">
				<div class="section-desc">
					<h1 class="title">SMS Gateway Features</h1>
					<o-features @update-active="setActiveFeature" :content="features" :active-feature="activeFeature">
						<a href="#" class="feature">
							<span class="icon">
								<span class="icon-paper-plane"></span>
							</span>
							<span class="detail">
								<span class="name">Bulk SMS &amp; SMS Marketing</span>
								<span class="desc">Send an SMS broadcast to your contact list with merge, tracking, and other powerful features</span>
							</span>
						</a>
						<a href="#" class="feature">
							<span class="icon">
								<span class="icon-bell"></span>
							</span>
							<span class="detail">
								<span class="name">Notification SMS</span>
								<span class="desc">Send SMS to your customers when they purchase, make an order, or when their Pizza is ready!</span>
							</span>
						</a>
						<a href="#" class="feature">
							<span class="icon">
								<span class="icon-calendar"></span>
							</span>
							<span class="detail">
								<span class="name">Appointment Reminders</span>
								<span class="desc">Schedule SMS for the future and send reminders to your users to avoid no-shows</span>
							</span>
						</a>
						<a href="#" class="feature">
							<span class="icon">
								<span class="icon-key"></span>
							</span>
							<span class="detail">
								<span class="name">Two factor verification &amp; One time passwords</span>
								<span class="desc">Powerful SMS API gateway to enable Two Factor Authentication and OTP SMS passwords</span>
							</span>
						</a>
						<a href="#" class="feature">
							<span class="icon">
								<span class="icon-note"></span>
							</span>
							<span class="detail">
								<span class="name">Surveys &amp; Feedback Requests</span>
								<span class="desc">Quickly run SMS Polls, Surveys, and request feedback with 2-way SMS</span>
							</span>
						</a>
					</o-features>

					<a href="#" class="button is-link">EXPLORE MORE &rarr;</a>
				</div>
			</div>
			<div class="column phone-column">
				<o-phone v-if="currentFeature" :messages="currentFeature.messages" @messages-ended="setNextFeature" @go-back="goBack"></o-phone>
			</div>
		</div>
	</div>
</section>
