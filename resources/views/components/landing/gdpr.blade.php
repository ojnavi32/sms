<section class="hero is-info is-bold">
	<div class="hero-body">
		<div class="container">
			<h1 class="title">GDPR EU Directive Compliance</h1>
		</div>
	</div>
</section>

<section class="section is-medium style-legal">
	<div class="container">
		<div class="content">
			<h2>GDPR Compliance at SMS.to</h2>

			<p>Here at SMS.to.to is fully compliant and also offers a full GDPR EU Directive compliance as an extention to our customers.</p>

			<p>Our GDPR statement is available below.</p>

			<h2>SMS.to GDPR Compliance Statement</h2>

			<p>The following is an addendum to SMS.to Privacy Policy and Terms and Conditions to cover the EU General Data Protection Regulation which comes into effect on May 25th 2018.</p>

			<p>As a SMS.to customer, the GDPR gives you new protection rights and assures better access to your personal data.</p>

			<p><b>-Right to rectification: </b>Rectify your personal information at any time from your account settings. You can also contact us directly to edit or rectify your information.</p>
			
			<p><b>-Right to be forgotten: </b>Cancel your SMS.to account or subscription and close your account at any time. You can send us a request to erase all your data, which we will complete within 30 days.</p>

			<p><b>-Right to portability: </b>Upon request, we will export your data so that it can be transferred to a third party or competitor.</p>

			<p><b>-Right to object: </b>Unsubscribe at any time to any specific use of your information (newsletter, automatic emails, etc.).</p>

			<p><b>-Right of access: </b>We are transparent about the data that we collect and what we do with it. To familiarize yourself with this, please refer to our privacy policy.  Lastly, you can contact us at any time to access and modify any of your personal data.</p>

			<h2>Accountability</h2>

			<p>We have conducted an audit of all information we hold on our customers and for our customers.</p>

			<p>SMS.to holds names, email address, IP addresses/session and data subjects including name, surname and mobile number on behalf of our customers.</p>
		
			<p>The data is held only as long as our customers account remains open. If a customer’s wishes to close their account, all data is then deleted.</p>

			<h2>Communicating with Staff and Service Users</h2>

			<p>SMS.to servers are based in the EU and in the US. We operate multiple SMS servers in both the US and in Europe. Our users have an option to select wether their data will be processed globally or within Europe only routed and processed through our EU partner telecom companies.</p>

			<p>SMS.to has direct connections to operators in the EU and when processing our customers’ data it is sent directly to our operators and is not transferred outside the EU.</p>

			<h2>Personal Privacy Rights</h2>

			<p>All customers have access to view their data using their secure login and password. They can add, delete or modify any inaccuracies in this data. Customers have full control over their data</p>

			<p>SMS.to provides facilities for companies to package and export their data in the interests of data portability.</p>

			<h2>Data Access Requests</h2>

			<p>SMS.to provides for data access requests from our customers. This information will be returned to the customer within one month of request.</p>

			<p>SMS.to will also provides full control over email notifications over all our customers. Consent for email notifications, email events and email marketing consents can be retracted at any time</p>

			<h2>Legal Basis for Processing</h2>

			<p>SMS.to is processing SMS termination on behalf of our customers to send A2P SMS campaigns or notifications</p>

			<h2>Consent</h2>

			<p>SMS.to takes consent from all our customers on signup before sending marketing emails. This consent is separate than the terms and conditions and has to be actively given. Customers can at any time retract their consent for the different types of emails from their account.</p>

			<p>SMS.to makes all customers aware of their Data Protection responsibilities and that they have received consent from their data subjects to contact them. After May 25th 2018 there will be checks in place for customers to ensure they have obtained consent before they can import into their accounts.</p>

			<h2>Data Protection by Design</h2>

			<p>SMS.to operates servers both Production and Disaster Recovery that are located within the EU. If choosen no data on either environment will leave  the EU at any point. The data centre services provider who hosts and manages the secure environment for our servers is ISO 27001 certified.</p>

			<p>The SMS.to System employs security protocols to block illegal application requests such as SQL injection. All access to system backend is locked down by specific IP whitelist.</p>

			<p>The SMS.to System is monitored 24/7 by our own engineers. The engineers receive pager alerts to any suspicious activity or unusual network traffic. On a positive identification of a data breach our policy is to alert all Data Controllers immediately.</p>

			<h2>Reporting Data Breaches</h2>

			<p>Any data breaches will be reported to both our customers and the DPC within 72 hours.</p>

			<h2>Data Protection Officer</h2>

			<p>SMS.to have designated Italos Marios as Data Protection Officer. Any questions relating to SMS.to GDPR compliance should be sent to italosm@sms.to</p>
			
		</div>
	</div>
</section>
