<section class="hero is-medium">
	<div class="hero-body">
		<div class="container">
			<h2 class="title is-2 has-text-centered">Our Logos</h2>
			<div class="columns is-multiline is-centered">
				<div class="column is-one-third">
					<div class="card">
						<div class="card-content has-text-centered">
							<p class="title is-4">
								Scalable
								<small>(.SVG)</small>
							</p>
							<span class="icon is-large">
								<span class="fa fa-picture-o"></span>
							</span>
						</div>
						<footer class="card-footer">
							<p class="card-footer-item">
								<span>
									<a href="{{ asset('images/logo.svg') }}">Download</a>
								</span>
							</p>
						</footer>
					</div>
				</div>
				<div class="column is-one-third">
					<div class="card">
						<div class="card-content has-text-centered">
							<p class="title is-4">
								Various sizes
								<small>(.PNG)</small>
							</p>
							<span class="icon is-large">
								<span class="fa fa-picture-o"></span>
							</span>
						</div>
						<footer class="card-footer">
							<p class="card-footer-item">
								<span>
									<a href="{{ asset('images/logo-pngs.zip') }}">Download</a>
								</span>
							</p>
						</footer>
					</div>
				</div>
				<div class="column is-one-third">
					<div class="card">
						<div class="card-content has-text-centered">
							<p class="title is-4">
								Icon only
								<small>(.PNG)</small>
							</p>
							<span class="icon is-large">
								<span class="fa fa-picture-o"></span>
							</span>
						</div>
						<footer class="card-footer">
							<p class="card-footer-item">
								<span>
									<a href="{{ asset('images/logoicon-pngs.zip') }}">Download</a>
								</span>
							</p>
						</footer>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
