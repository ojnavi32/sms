<section class="section style-sms-gateway">
	<div class="container">
		<div class="section-desc">
			<h1 class="title">
				@if(isset($title))
					{!! $title !!}
				@else
					Powerful SMS Gateway
				@endif
			</h1>
			<p class="description">
				@if(isset($description))
					{!! $description !!}
				@else
					<strong>Send Bulk SMS</strong> or use our <strong>SMS API</strong> to implement SMS notifications, <br />
					OTP, reminders into your workflow and build apps that send SMS <br />
					with our redundant SSL SMS API.
				@endif
			</p>
			<form action="{{statelessRoute('user.getstarted')}}" method="POST">
				{{csrf_field()}}
				@if(!Session::has('status'))
					<div class="field has-addons" id="started-btn">
						<div class="control">
							<input class="input is-medium" id="email-input" type="text" name="email" placeholder="Enter your email address" style="font:10px;" required/>
						</div>
						<div class="control">
							<button type="submit" class="button is-info">Get Started</button>
						</div>
					</div>
				@endif
				<div class="box-content">
					@if(Session::has('status'))
						<p style="color:green">{{Session::get('status')}}</p>
					@endif
				</div>

				<div class="checkboxDiv width400">
					@include('components.form.checkbox', [
						'name' => 'terms_conditions',
						'label' => 'I have read and agree to SMS.to\'s <a href="'.url('terms').'">terms and conditions</a> and the <a href="'.url('privacy-policy').'">privacy policy</a>.',
						'value' => '1',
						'required' => true
					])
					@include('components.form.checkbox', [
						'name' => 'special_offers',
						'label' => 'I wish to receive the latest news, special offers, and money off coupons from SMS.to. <br /><i>We\'ll always treat your personal details with utmost care and will never be sold to 3rd parties for marketing purposes. You can remove your consent at any time.</i>',
						'value' => '1',
					])
				</div>

				@if(Session::has('error'))
					<p style="color:red">{{Session::get('error')}}</p>
				@endif
			</form>
		</div>
	</div>
</section>

