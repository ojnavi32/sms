<section class="section style-cost-calculator">
	<div class="container" id="cost-calculator">
		<div class="columns is-gapless-around">
			<div class="column cutout style-light">
				<div class="section-desc">
					<h1 class="title is-5">Cost Calculator</h1>
					<p class="subtitle is-small">Fill details below to calculate your<br /> SMS campaign cost</p>

					<form>
						<form-field
							key="country"
							:model="countries"
							property="selected"
							:options="countries.list"
							option-track-by="iso2"
							option-value="iso2"
							option-label="name"
							:property-is-option-value="true"
							:multiple="false"
							:searchable="true"
							type="multiselect"
							placeholder="Select country"
							
							class="style-light"

							label="SMS to"
						></form-field>

						<div class="field style-light">
							<label class="label">Volume per month</label>
							<label class="label style-secondary sms-count">@{{amount}} SMS</label>
							<p class="control">
								<input class="input-range" type="range" step="100" min="100" max="100000" v-model="amount" />
							</p>
						</div>
						<hr />
						<div class="field style-light">
							<label class="label">Estimated Cost</label>
							<div class="level">
								<div class="level-left">
									<div class="level-item">
										<div class="price">
											<span class="is-inline-flex has-text-info" v-if="isLoadingCost">
												<span class="loader"></span>
											</span>
											<span id="estimated" v-if="!isLoadingCost">&euro;@{{costPerSMS}}</span>
											<i>price per sms</i>
										</div>
									</div>
								</div>
								<div class="level-right" v-if="showContactUs">
									<div class="level-item">
										<a href="{{ route('contact-us') }}" class="button is-small is-info is-outlined">Contact Us &rarr;</a>
									</div>
								</div>
							</div>
						</div>
					</form>
				</div>
			</div>
			<div class="column">
				<div class="section-desc style-light">
					<h1 class="title">Simply Pay As You Go</h1>
					<p class="description">
						No contracts, no commitments, pay only for <br />
						what you use. <br />
						Additional discounts for high volume SMS API usage.
					</p>
					<br />
					<a href="{{ route('pricing') }}" class="button is-link">BUY SMS CREDITS &rarr;</a>
				</div>
			</div>
		</div>
	</div>
</section>
