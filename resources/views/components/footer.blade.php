<footer class="footer">
	<div class="container">
		<div class="columns">
			<div class="column">
				<a class="logo-footer" href="{{ url('/') }}" title="{{ config('app.name', 'Laravel') }}">
					@include('components.logo')
				</a>
				<p>
					Bulk SMS Marketing Platform &amp; SMS Gateway API<br />
					for all your SMS needs. OTP, Notifications, <br />
					Newsletters, Reminders, Polling, 2 WAY SMS, 2FA.
				</p>

				<div class="secondary-menu">
					<a href="{{ route('privacy-policy') }}">Privacy</a> - <a href="{{ route('terms') }}">Terms</a>
				</div>

				<div class="menu-label style-footer is-small">Connect</div>
				<div class="social">
					@include('components.social')
				</div>
				<br />
				<div class="menu-label style-footer is-small">Corporate</div>
				<p class="corporate is-paddingless">Intergo Telecom Ltd, <br />Nikolaou Nikolaidi 3, Office 206, <br />Paphos, 8010, Cyprus</p>
			</div>
			<div class="column is-2">
				<nav class="menu">
					<div class="menu-label style-footer">About</div>
					<ul>
						<li><a href="{{ route('about') }}">About us</a></li>
						<li><a href="{{ route('contact-us') }}">Contact us</a></li>
						<li><a href="#">Career<small> (soon)</small></a></li>
						<li><a href="{{ route('press') }}">Press</a></li>
						<li><a href="{{ route('privacy-policy') }}">Privacy Policy</a></li>
						<li><a href="{{ route('terms') }}">Terms and Conditions</a></li>
						<li><a href="{{ route('refund-policy') }}">Refund Policy</a></li>
						<li><a href="{{ route('gdpr-compliance') }}">GDPR EU Directive Compliance</a></li>
					</ul>
				</nav>
			</div>
			<div class="column is-2">
				<nav class="menu">
					<div class="menu-label style-footer">Products</div>
					<ul>
						<li><a href="{{ url('/') }}">SMS Gateway</a></li>
						<li><a href="{{ route('sms-api') }}">API</a></li>
						<li><a href="#">SDKs</a></li>
					</ul>
				</nav>
			</div>
			<div class="column is-2">
				<nav class="menu">
					<div class="menu-label style-footer">Resources</div>
					<ul>
						<li><a href="https://blog.sms.to/">Blog</a></li>
						<li><a href="{{ route('faq') }}">FAQ</a></li>
						{{-- <li><a href="#">Reports</a></li>
						<li><a href="#">Case Studies</a></li>
						<li><a href="#">Sample apps</a></li>
						<li><a href="#">One-click install</a></li> --}}
					</ul>
				</nav>
			</div>
		</div>
		<div class="level">
			<div class="level-left">
				<div class="level-item">
					<span class="icon">
						<span class="icon-lock"></span>
					</span>
					<span>Secure Payments</span>
				</div>
				<div class="level-item payment-systems">
					<div class="payment-system mastercard"></div>
					<div class="payment-system visa"></div>
					<div class="payment-system jcc"></div>
				</div>
			</div>
			{{-- <div class="level-right">
				<a href="#preferences" class="button is-light" data-toggle="display">
					<span class="icon">
						<span class="flag-icon flag-icon-gb"></span>
					</span>
					<span>English - Euro</span>
					<span class="icon">
						<span class="icon-arrow-down"></span>
					</span>
				</a>
			</div> --}}
		</div>
	</div>
	{{-- <section id="preferences" class="section section-preferences toggleable is-hidden">
		<div class="container">
			<h6 class="title">Select your preferences</h6>
			<form action="#">
				<div class="columns">
					<div class="column is-a-third">
						<div class="field">
							<label class="label is-small">Country</label>
							<div class="control has-icons-left">
								<span class="select is-fullwidth">
									<select>
										<option selected>United Kingdom</option>
										<option>Select dropdown</option>
										<option>With options</option>
									</select>
								</span>
								<span class="icon is-small is-left">
									<span class="icon-location-pin"></span>
								</span>
							</div>
						</div>
					</div>
					<div class="column is-a-third">
						<div class="field">
							<label class="label is-small">Language</label>
							<div class="control has-icons-left">
								<span class="select is-fullwidth">
									<select>
										<option selected>English (UK)</option>
										<option>Select dropdown</option>
										<option>With options</option>
									</select>
								</span>
								<span class="icon is-small is-left">
									<span class="icon-location-pin"></span>
								</span>
							</div>
						</div>
					</div>
					<div class="column is-a-third">
						<div class="field">
							<label class="label is-small">Currency</label>
							<div class="control has-icons-left">
								<span class="select is-fullwidth">
									<select>
										<option selected>EURO</option>
										<option>Select dropdown</option>
										<option>With options</option>
									</select>
								</span>
								<span class="icon is-small is-left">
									<span class="icon-location-pin"></span>
								</span>
							</div>
						</div>
					</div>
				</div>
				<div class="columns">
					<div class="column">
						<button type="submit" class="button is-info">
							<span class="icon is-small">
								<span class="icon-check"></span>
							</span>
							<span>Save Settings</span>
						</a>
					</div>
				</div>
				<a href="#" class="close" data-action="hide-container">
					<span>close</span>
					<span class="icon-close"></span>
				</a>
			</form>
		</div>
	</section> --}}
</footer>



















