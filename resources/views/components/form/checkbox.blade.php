<div class="field">
	<label for="{{$name}}" class="checkbox">
		<span class="columns is-mobile">
			<span class="column is-narrow">
				<input id="{{$name}}" name="{{$name}}" type="checkbox" value="{{$value}}" {{ isset($required) && $required == true ? 'required' : '' }} />
			</span>
			<span class="column">
				{!! $label !!}
			</span>
		</span>
	</label>
</div>
{!! $errors->first($name, '<p class="help is-danger">:message</p>') !!}
