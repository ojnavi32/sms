<div class="field">
	@if(isset($label))
		<label class="label">{{$label}}</label>
	@endif
	<p class="control{{ isset($icon) ? ' has-icons-left' : '' }}">
		@if($type === 'textarea')
			<textarea class="textarea{{ $errors->has($name) ? ' is-danger' : '' }}" type="{{ $type ? $type : 'text' }}" name="{{$name}}" {!! isset($placeholder) ? ' placeholder="'.$placeholder.'"' : '' !!} {!! isset($value) ? ' value="'.$value.'"' : '' !!} {{ isset($required) && $required == true ? 'required' : '' }} {{ isset($autofocus) && $autofocus == true ? 'autofocus' : '' }}></textarea>
		@else
			<input class="input{{ $errors->has($name) ? ' is-danger' : '' }}" type="{{ $type ? $type : 'text' }}" name="{{$name}}" {!! isset($placeholder) ? ' placeholder="'.$placeholder.'"' : '' !!} {!! isset($value) ? ' value="'.$value.'"' : '' !!} {{ isset($required) && $required == true ? 'required' : '' }} {{ isset($autofocus) && $autofocus == true ? 'autofocus' : '' }} />
		@endif
		@if (isset($icon))
			<span class="icon is-small is-left">
				<span class="{{$icon}}"></span>
			</span>
		@endif
		{!! $errors->first($name, '<p class="help is-danger">:message</p>') !!}
	</p>
</div>


