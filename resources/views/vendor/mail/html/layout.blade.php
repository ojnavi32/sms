<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"
      xmlns:v="urn:schemas-microsoft-com:vml"
      xmlns:o="urn:schemas-microsoft-com:office:office">
    <head>
        <!--[if gte mso 9]><xml>
            <o:OfficeDocumentSettings>
                <o:AllowPNG/>
                <o:PixelsPerInch>96</o:PixelsPerInch>
            </o:OfficeDocumentSettings>
        </xml><![endif]-->
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="viewport" content="width=device-width" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta name="format-detection" content="telephone=no" />
        <title>SMSTO EMAIL</title>
        <style type="text/css">
            body {
                mso-line-height-rule: exactly;
                -webkit-text-size-adjust: none;
                -ms-text-size-adjust: none;
            }
            a {
                mso-line-height-rule: exactly;
          
            }
            table {
                border-collapse: collapse;
                padding: 0px !important;
                mso-table-lspace: 0pt;
                mso-table-rspace: 0pt;
                font-family: verdana,sans-serif!important;
            }

            div[class=mobile_only] {
                display: none;
                height: 0;
                max-height: 0;
                overflow: hidden;
            }

            td[class=main_content] p, td[class=main_content] p a, td[class=main_content] p a:visited{
                color: #fff!important;
                font-size: 14px!important;
                font-family: verdana,sans-serif!important;
            }

            td[class=main_content] p a{
                margin-bottom: 5px; margin-top: 5px;
            }

            td[class=main_content] p{
                max-width: 500px;                
            }

            .verify, .email{color: #5affab!important; }
            .verify a, .verify a:visited, .verify a:link{ color: #5affab!important;} 
            .email a, .email a:visited, .email a:link{ color: #5affab!important;}
            .ii a[href] { color:#5affab!important; text-decoration:none;}

            td[class*="main_content"] p a,
            *[class="main_content"] p a:link,
            *[class="main_content"] p a:visited,
            *[class="main_content"] p a.link,
            *[class="main_content"] p a.visited {
                color: #fff!important;
                text-decoration: underline;
            }


            td[class=main_content] h2{
                font-size: 26px!important;
                font-family: verdana,sans-serif!important;
                color: #fff!important;
            }

            td[class=main_content] h1{
                font-size: 14px!important;
                font-family: verdana,sans-serif!important;
                color: #fff!important;
            }

          

            td[class=footer-address] p{
                font-size: 13px!important;
                font-family: verdana,sans-serif!important;
               
            } 

            table[class=subcopy] {
                border-top: 0px!important;
            }
            
            @media only screen and (max-width:480px) {
                table[class=tb_wrap] {
                    width: 100% !important;
                }
                div[class=desktop_only],
                div[class="mobile_only screen_max_640"] {
                    display: none;
                    height: 0;
                    max-height: 0;
                    overflow: hidden;
                }
                div[class="mobile_only screen_max_480"] {
                    display: block !important;
                    height: auto !important;
                    max-height: none !important;
                    overflow: visible !important;
                }
                div[class="mobile_only screen_max_480"] table {
                    display: table !important;
                }
            }

            @media only screen and (min-width:481px) and (max-width:640px) {
                table[class=tb_wrap] {
                    width: 100% !important;
                }
                div[class=desktop_only],
                div[class="mobile_only screen_max_480"] {
                    display: none;
                    height: 0;
                    max-height: 0;
                    overflow: hidden;
                }
                div[class="mobile_only screen_max_640"] {
                    display: block !important;
                    height: auto !important;
                    max-height: none !important;
                    overflow: visible !important;
                }
                div[class="mobile_only screen_max_640"] table {
                    display: table !important;
                }
            }
        </style>
    </head>

    <body style="width: 100%; margin: 0px; padding: 0px; -webkit-text-size-adjust: none; -ms-text-size-adjust: none; -webkit-font-smoothing: antialiased;">
        <table width="100%" border="0" cellpadding="0" cellspacing="0" bgcolor="#ebeef1">
            <tbody>
                <tr>
                    <td valign="top" style="padding: 0 4px;">
                       <table style="font-family:Helvetica;font-size:12px;font-style:normal;font-variant:normal;font-weight:normal;letter-spacing:normal;line-height:normal;text-align:start;text-indent:0px;text-transform:none;white-space:normal;word-spacing:0px;border-spacing:0px" height="100%" width="100%" cellpadding="0" cellspacing="0"  border="0">
                            <tbody>
                                <tr>
                                    <td style="border-collapse:collapse;" width="600">
                                        <br />
                                        <table style="margin:0px auto;border-spacing:0px;  border-radius: 10px 10px 0px 0px; overflow: hidden; " width="600" cellpadding="0" cellspacing="0" bgcolor="#ffffff" border="0" class="tb_wrap">
                                            <tbody>
                                                <tr>
                                                    <td style="background-color:rgb(255,255,255);font-size:13px;line-height:20px;font-family:Helvetica,sans-serif;color:rgb(51,51,51);border-collapse:collapse; " align="left" bgcolor="#fff">
                                                        <table style="margin:0px auto;border-spacing:0px" width="600" cellpadding="0" cellspacing="0" bgcolor="#ffffff" border="0" class="tb_wrap">
                                                            <tbody>
                                                                <tr>

                                                                    {{ $header or '' }}

                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                        <br />
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
										
										
										<!-- Main Content -->
										<table style="margin:0px auto;border-spacing:0px" width="600" cellpadding="0" cellspacing="0" bgcolor="#ffffff" border="0" class="tb_wrap">
										<tbody>
											
											<tr>
												<td  style="max-width: 500px; padding:35px 50px!important; background: #6091f2 url('{{ asset('images/baguette-wannabe.png') }}') no-repeat 100% bottom; background-size: 50%; font-size:13px;line-height:20px;font-family:Helvetica,sans-serif;color:rgb(255,255,255);border-collapse:collapse" align="left">
													<table style="margin:0px auto;border-spacing:0px;color:rgb(255,255,255);font-family:verdana,sans-serif;font-size:18px; text-align: center; " width="100%" cellpadding="0" cellspacing="0" border="0">
														<tbody>
															<tr>
																<td class="main_content" style="word-break: break-all;">
																
                                                                        {{ Illuminate\Mail\Markdown::parse($slot) }}                                                                    
																	
																</td>
															</tr>
														</tbody>
													</table>
												</td>
											</tr>
										</tbody>
									</table>	
									<!-- Main Content End -->
									
									<table style="margin:0px auto;border-spacing:0px; border-radius: 0px 0px 10px 10px; overflow: hidden; margin-bottom: 20px;" width="600" cellpadding="0" cellspacing="0" bgcolor="#fff" border="0" class="tb_wrap">
										<tbody>
											<tr>
												<td style="background-color:rgb(245,245,245);font-size:12px;line-height:20px;font-family:Helvetica,sans-serif;color:rgb(51,51,51);border-collapse:collapse" align="left" bgcolor="#fff">													
                                                    
                                                    {{ $footer or '' }} 

												</td>
											</tr>
										</tbody>
									</table>
										
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
            </tbody>
        </table>
    </body>
</html>
