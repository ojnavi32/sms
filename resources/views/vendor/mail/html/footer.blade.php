
<table style="border-spacing:0px" width="600" cellpadding="0" cellspacing="0" bgcolor="#d3d6d8" border="0" class="tb_wrap">
    <tbody>
        <tr>
            <td colspan="2" style="background-color:rgb(255,255,255); border-bottom-width:1px;border-bottom-style:solid;border-bottom-color:rgb(218,217,211);border-collapse:collapse;font-size:16px;font-family:Helvetica,sans-serif;padding:20px 20px 5px 20px; text-align: center;">
                <p style="font-size:15px;line-height:20px;font-family:Helvetica,sans-serif;color:rgb(51,51,51); margin-top: 0; margin-botom: 0!important;">If you have any questions or issues with your account, visit our <a href="http://support.sms.to/" style="text-decoration:none;color:rgb(0,119,204); font-weight: bold;">Support Center.</a></p>
            </td>
        </tr>
    
    
        <tr>
            <td class="footer-address" style="width: 65%; border-collapse:collapse;font-size:13px; font-family:Helvetica,sans-serif;padding: 20px; color:rgb(142,142,142); line-height: 20px;">
                <p><span style="font-weight:bold;">SMS.to is a product of Intergo Telecom Ltd</span> <br /><span style="font-style: italic;">Nikolaou Nikolaidi 3, Office 206, Paphos, 8010, Cyprus</span></p> 
            </td>
            <td class="footer-icon" style="width: 35%;">
                <b style="font-size:15px; font-weight: bold; font-family:Helvetica,sans-serif;color:rgb(142,142,142); line-height: 20px;">Follow Us:</b><br />
                <a href="https://www.facebook.com/SMSto-Bulk-SMS-SMS-Marketing-1909869432637539/" rel="nofollow" style="text-decoration:none;color:rgb(0,119,204)" target="_blank"><img src="{{ asset('images/social-media-icons/facebook-icon.png') }}" width="32"></a>
                <a href="#"><img src="{{ asset('images/social-media-icons/googleplus-icon.png') }}" width="32"></a>
                <a href="https://www.linkedin.com/company/sms-to"><img src="{{ asset('images/social-media-icons/linkedin-icon.png') }}" width="32"></a>
                <a href="https://twitter.com/smstoapp" rel="nofollow" style="text-decoration:none;color:rgb(0,119,204)" target="_blank"><img src="{{ asset('images/social-media-icons/twitter-icon.png') }}" width="32"></a>
            </td>
        </tr>
        
    </tbody>
</table>
