<td style="border-collapse:collapse; padding: 20px 20px 0px 20px; clear:both">
    <a href="{{ $url }}" style="text-decoration:none;color:rgb(91,212,188); float:left;" target="_blank"><img class="CToWUd" alt="{{ config('app.name', 'Laravel') }}" src="{{ asset('images/logo-email.png') }}" style="color:rgb(0,153,153)"></a>
    <span style="float:right; font-size: 20px; margin-top: 5px;">Visit <a href="{{ $url }}" style="text-decoration:none; color:rgb(96, 145, 242); font-weight: bold;">SMS.to</a></span>
</td>
