<table class="socialicon" width="100%" cellpadding="0" cellspacing="0">
    <tr>
        <td>
            <a href="https://www.facebook.com/SMSto-Bulk-SMS-SMS-Marketing-1909869432637539/"><img src="{{ asset('images/social-media-icons/facebook.png') }}" alt="{{ config('app.name', 'Laravel') }}" class="socialIcon" /></a>
            <a href="https://twitter.com/smstoapp"><img src="{{ asset('images/social-media-icons/twitter.png') }}" alt="{{ config('app.name', 'Laravel') }}" class="socialIcon" /></a>
            <a href="https://www.linkedin.com/company/sms-to"><img src="{{ asset('images/social-media-icons/linkdln.png') }}" alt="{{ config('app.name', 'Laravel') }}" class="socialIcon" /></a> 
            <a href="https://www.youtube.com/channel/UC-Qo3znwQKmAAd-ZaUvDfmQ"><img src="{{ asset('images/social-media-icons/youtube.png') }}" alt="{{ config('app.name', 'Laravel') }}" class="socialIcon" /></a>
        </td>
    </tr>
</table>