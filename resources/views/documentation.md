FORMAT: 1A

# APIDocs

# Contacts [/contacts]

## Show all contacts for particular users [GET /contacts]
Get a JSON representation of all the contacts of the users.

+ Parameters
    + page: (integer, optional) - The page of results to view.
        + Default: 1
    + limit: (integer, optional) - The amount of results per page.
        + Default: 10

+ Response 200 (application/json)
    + Body

            {
                "id": 10,
                "username": "foo"
            }

## Get avatar equivalent of the email [GET /contacts/{email}/gravatar]
Get avatar equivalent of the email

+ Response 200 (application/json)
    + Body

            {
                "url": ""
            }