<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    'password' => 'Passwords must be at least six characters and match the confirmation.',
    'reset' => 'Your password has been reset!',
    'sent' => 'An email has been sent to your email address. It can take a few minutes before you have it in your inbox. Click on the RESET link to reset your password!',
    'token' => 'This password reset token is invalid.',
    'user' => "We can't find a user with that e-mail address.",

];
