# sms.to

Use .env.example for sample .env file

### URL Mapping

Sample records:

| Local | Dev | Production |
| ------ | ------ | ------ |
| http://localhost/public/app | https://smsto.devhost.intergo.com.cy | https://app.sms.to |
| http://localhost/public/app/send | https://smsto.devhost.intergo.com.cy/send | https://app.sms.to/send |


### Needed packages

GeoIP2

Install on Local/Dev:
1. Download and extract database file from this URL:
http://geolite.maxmind.com/download/geoip/database/GeoLite2-Country.tar.gz
2. Place it on this path: ${Laravel_Storage}/app/geoip2/GeoLite2-Country.mmdb


Install on Ubuntu systems (16.04+) with weekly auto-update:

```sh
$ apt-get install geoipupdate
```

After that, use following config file on `/etc/GeoIP.conf`:

```sh
# The following AccountID and LicenseKey are required placeholders.
# For geoipupdate versions earlier than 2.5.0, use UserId here instead of AccountID.
UserId 0
LicenseKey 000000000000

# Include one or more of the following edition IDs:
# * GeoLite2-City - GeoLite 2 City
# * GeoLite2-Country - GeoLite2 Country
# For geoipupdate versions earlier than 2.5.0, use ProductIds here instead of EditionIDs.
ProductIds GeoLite2-City GeoLite2-Country
```

You can run it for update:

```sh
$ geoipupdate [-v]
```

Create GeoIP directory on project and link database:
```sh
$ mkdir /srv/smsto.devhost.intergo.com.cy/storage/app/geoip2
$ ln -s /var/lib/GeoIP/GeoLite2-Country.mmdb /srv/smsto.devhost.intergo.com.cy/storage/app/geoip2/GeoLite2-Country.mmdb
```

Configure crontab `/etc/crontab`:
```sh
# Update GeoIP
24 0    * * 4   root    /usr/bin/geoipupdate
```




### References

  - https://m.dotdev.co/test-driven-api-development-using-laravel-dingo-and-jwt-with-documentation-ae4014260148
  - https://github.com/vue-bulma/vue-admin
  - https://www.dunebook.com/brief-overview-of-design-patterns-used-in-laravel/
  - http://webtechsharing.com/factory-design-pattern/
