<?php

/*
  |--------------------------------------------------------------------------
  | Web Routes
  |--------------------------------------------------------------------------
  |
  | Here is where you can register web routes for your application. These
  | routes are loaded by the RouteServiceProvider within a group which
  | contains the "web" middleware group. Now create something great!
  |
 */

Auth::routes();

Route::get('/get-list-test', function () {
    $user = App\User::find(1);
    $mongo = new App\SmstoMongo;
    $db = $mongo->db("smsto");
    // Lists with paginator
    $db->getCollection()->transform(function ($list) use ($user, $db) {

        $collection_name = "lists_" . $list->id . "_user_" .$user->id . "_contacts";
        $collection = $db->{$collection_name};
        $count = $collection->count();
        $list->number_contacts = $count;
        $list->save();
        return $list;
    });
});

Route::post('user/get-started', 'Auth\RegisterController@getStartedRequest')->name('user.getstarted');
Route::get('/logout', 'Auth\LoginController@logout')->name('logout');
Route::get('logs', '\Rap2hpoutre\LaravelLogViewer\LogViewerController@index')->middleware('auth');
$api = app('Dingo\Api\Routing\Router');

// look at .env API_STANDARDS_TREE
// use below to authenticate user
//$api->version('v1', ['middleware' => 'api.auth'], function ($api) {
$api->version('v1', [], function ($api) {
    $api->group(['namespace' => 'App\Http\Controllers\Api'], function ($api) {

        // For authentications
        $api->post('authenticate', 'AuthenticateController@authenticate');
        $api->get('authenticated_user', 'AuthenticateController@authenticatedUser');
        $api->post('logout', 'AuthenticateController@logout');
        $api->get('token', 'AuthenticateController@getToken');

        // All routes below requires authentications
        $api->group(['middleware' => ['api.auth', 'api_log']], function ($api) {

            $api->group(['prefix' => 'sms'], function ($api) {
                $api->get('sent', 'DashboardController@smsSend')->name('api.sms.send');
                // /Dashboard
                $api->get('info', 'DashboardController@smsLogs')->name('api.dashboard.sms.logs');
                // /send
                $api->post('send', 'SendController@index')->name('api.send');
                $api->get('costs', 'SendController@index')->name('api.costs');

                $api->post('send/preview', 'SendController@previewCost')->name('api.send.preview');

                $api->post('shortlink', 'SendController@shortLink')->name('api.shortlink');
            });

            $api->group(['prefix' => 'chart'], function ($api) {

                // chartsmsSearch
                $api->get('geochart/sms-reach', 'DashboardController@chartSmsReach')->name('api.dashboard.heatmap.sms');

                $api->group(['prefix' => 'corechart'], function ($api) {
                    // chartCostActivity
                    $api->get('cost-activity', 'DashboardController@chartCostActivity')->name('api.dashboard.heatmap.cost');

                    // sending activity chart
                    $api->get('sending-activity', 'DashboardController@chartSendActivity')->name('api.dashboard.heatmap.cost');
                });
            });

            $api->group(['prefix' => 'users'], function ($api) {
                $api->get('/{id}', 'UserController@show')->name('api.users');
                $api->get('/', 'UserController@getUsers')->name('api.users');
                $api->post('{id}/update', 'UserController@update')->name('api.users.update');
                $api->post('{id}/change-password', 'UserController@changePassword');
                $api->post('delete-request', 'UserController@deleteRequest')->name('api.delete.request');

                // notifications settings
                $api->get('{id}/settings/notifications', 'NotificationSettingController@show');
                $api->post('{id}/settings/notifications', 'NotificationSettingController@update');

                // Billing Details
                //$api->post('billing/{id}','\BillingController@updateContacts')->name('api.billing.update');
                $api->post('{id}/billing', 'BillingController@store')->name('api.billing.store');
                $api->post('{id}/billing/update', 'UserController@billingupdate')->name('api.users.billing.update');
                $api->post('reset-password', 'UserController@resetPassword');
                $api->post('avatar', 'UserController@avatar')->name('api.users.avatar');
            });

            // phone-prefixes
            $api->get('phone-prefixes', 'DashboardController@phonePrefix')->name('api.phone.prefixes');

            // countries
            $api->get('countries', 'DashboardController@phonePrefix')->name('api.countries');

            // timezones
            $api->get('timezones', 'DashboardController@timezones')->name('api.timezones');

            $api->get('user/info', 'DashboardController@userInfo')->name('api.user.info');

            $api->get('payments', 'DashboardController@payments')->name('api.payments');

            // payment view
            $api->get('payments/{id}', 'DashboardController@getPaymentDetails')->name('api.payments.details');


            $api->get('payments/download', 'DashboardController@downloadPayment')->name('payments.download');

            $api->group(['prefix' => 'lists'], function ($api) {

                $api->get('/', 'ListController@getLists')->name('api.lists');
                $api->post('/', 'ListController@store')->name('api.lists.store');
                $api->get('/{id}', 'ListController@show')->name('api.lists.show');
                $api->post('/{id}', 'ListController@update')->name('api.lists.update');
                $api->delete('/{id}', 'ListController@delete')->name('api.lists.delete');
                $api->get('/{id}/contacts', 'ContactController@contacts')->name('api.lists.contacts');
                $api->post('/{id}/contacts', 'ListController@postContacts')->name('api.lists.post.contacts');
                $api->delete('/{id}/contacts/{contact_id}', 'ListController@deleteContact')->name('api.lists.delete.contacts');
                $api->post('/{id}/favorite', 'ListController@favorite')->name('api.lists.favorite');
            });


            $api->group(['prefix' => 'templates'], function ($api) {

                $api->get('/', 'TemplateController@index')->name('api.templates');
                $api->post('/', 'TemplateController@store')->name('api.templates.store');
                $api->get('/{id}', 'TemplateController@show')->name('api.templates.show');
                $api->post('/{id}', 'TemplateController@update')->name('api.templates.update');
                $api->delete('/{id}', 'TemplateController@delete')->name('api.templates.delete');
            });


            $api->group(['prefix' => 'payments'], function ($api) {

                $api->get('methods', 'PaymentMethodController@index')->name('api.payments.methods');
                $api->get('methods/{id}', 'PaymentMethodController@show')->name('api.payments.methods.show');
                $api->post('methods', 'PaymentMethodController@store')->name('api.payments.methods.store');
                $api->post('methods/{id}', 'PaymentMethodController@update')->name('api.payments.methods.update');
                $api->delete('methods/{id}', 'PaymentMethodController@delete')->name('api.payments.methods.delete');
            });

            $api->group(['prefix' => 'contacts'], function ($api) {

//				$api->post('import-upload', ['as' => 'api.contacts.import.upload', 'uses' =>'ContactController@importUploadContacts']);
                $api->post('import-upload', ['as' => 'api.contacts.import.upload', 'uses' => 'UploadContactListController@upload']);
                $api->get('info', 'ContactController@contactsInfo')->name('api.contacts.info');
                $api->get('/', ['as' => 'api.contacts', 'uses' => 'ContactController@index'])->name('api.contacts');
                $api->post('/', ['as' => 'api.contacts.store', 'uses' => 'ContactController@store']);
                $api->get('/{id}', 'ContactController@show')->name('api.contacts.show');
                $api->post('/{id}', 'ContactController@update')->name('api.contacts.update');
                $api->delete('/{id}', 'ContactController@deleteContacts')->name('api.contacts.delete');
                $api->post('/{id}/sms', 'ContactController@sendSms')->name('api.contacts.sms');
                $api->post('/{id}/email', 'ContactController@sendEmail')->name('api.contacts.mail');
                $api->post('/{id}/lists', 'ContactController@addList')->name('api.contacts.addList');
                $api->get('/{email}/gravatar', ['as' => 'api.contacts.gravatar', 'uses' => 'ContactController@gravatar']);
                $api->post('merge', 'ContactController@mergeContacts')->name('api.contacts.merge');
                $api->post('move', 'ContactController@moveContacts')->name('api.contacts.move');
                $api->get('duplicates', ['as' => 'api.contacts.duplicates', 'uses' => 'ContactController@duplicates'])->name('api.contacts.duplicates');
            });

            $api->group(['prefix' => 'broadcasts'], function ($api) {

                $api->get('/', 'SmsBroadcastController@index')->name('api.broadcasts');
                $api->post('/', 'SmsBroadcastController@store')->name('api.broadcasts.store');
                $api->get('/{id}', 'SmsBroadcastController@show')->name('api.broadcasts.show');
                $api->post('/{id}', 'SmsBroadcastController@update')->name('api.broadcasts.update');
                $api->delete('/{id}', 'SmsBroadcastController@delete')->name('api.broadcasts.delete');

                $api->get('{id}/csv/download', 'SmsBroadcastController@downloadBroadcastAsCsv')->name('api.broadcasts.csv.download');
                $api->get('{id}/excel/download', 'SmsBroadcastController@downloadBroadcastAsExcel')->name('api.broadcasts.excel.download');
            });

            // /broadcasts batches
            $api->get('broadcasts-messages/{id}', 'SmsBroadcastController@broadcastDetails')->name('api.broadcasts.details');
            // $api->get('broadcast/{id}/csv/download','SmsBroadcastController@downloadBroadcastAsCsv')->name('api.broadcasts.csv.download');
            // $api->get('broadcast/{id}/excel/download','SmsBroadcastController@downloadBroadcastAsExcel')->name('api.broadcasts.excel.download');
            // $api->get('batches/{id}/messages','\SmsBroadcastBatchController@messages')->name('api.batches.messages');

            $api->group(['prefix' => 'credit-packages'], function ($api) {
                $api->get('/', 'PackagesController@index')->name('api.packages');
                $api->get('/{id}', 'PackagesController@show')->name('api.packages.show');
            });

            $api->group(['prefix' => 'sessions'], function ($api) {
                $api->get('/', 'SessionController@index')->name('api.sessions');
                $api->post('/', 'SessionController@store')->name('api.sessions.store');
                $api->get('/{id}', 'SessionController@show')->name('api.sessions.show');
                $api->post('/{id}', 'SessionController@update')->name('api.sessions.update');
                $api->delete('/{id}', 'SessionController@delete')->name('api.sessions.delete');
                $api->delete('all', 'SessionController@deleteAll')->name('api.sessions.delete.all');
            });

            $api->group(['prefix' => 'keys'], function ($api) {
                $api->get('/', 'ApiKeyController@index')->name('api.keys');
                $api->post('/', 'ApiKeyController@store')->name('api.keys.store');
                $api->get('/{id}', 'ApiKeyController@show')->name('api.keys.show');
                $api->post('/{id}', 'ApiKeyController@update')->name('api.keys.update');
                $api->delete('/{id}', 'ApiKeyController@delete')->name('api.keys.delete');
            });
        });
    });
});

// $middleware = ['auth', 'not_api_auth', 'ban', 'localeSessionRedirect'];
// app.sms.to
$middleware = ['auth'];
// $group = [
// 	'domain' => config('app.url'),
// 	'middleware' => $middleware,
// ];

$group = [
    'middleware' => $middleware,
    'prefix' => 'app'
];

if (config('app.env') == 'local') {
    $group = [
        'domain' => 'app.sms.dev',
        'middleware' => $middleware,
    ];
    if (env('APP_MACHINE') != 'manny') {
        $group['prefix'] = 'app';
        unset($group['domain']);
    }
}

if (config('app.env') == 'local') {
    $group = [
        'domain' => 'app.sms.dev',
        'middleware' => $middleware,
    ];
    if (env('APP_MACHINE') != 'manny') {
        $group['prefix'] = 'app';
        unset($group['domain']);
    }
}

// app dashboard
Route::group($group, function () {

    Route::get('/', 'App\DashboardController@index')->name('app.dashboard');
    //Route::get('send', 'App\MessageController@send')->name('app.send');
});


// api.sms.to
$middleware = ['auth'];
// $group = [
// 	'domain' =>  config('api.domain'),
// 	'middleware' => $middleware,
// ];
$group = [
    'domain' => config('app.url'),
    'middleware' => $middleware,
    'prefix' => 'app'
];

if (config('app.env') == 'local') {
    $group = [
        'domain' => 'api.sms.dev',
        'middleware' => $middleware,
    ];

    if (env('APP_MACHINE') != 'manny') {
        $group['prefix'] = 'api';
        unset($group['domain']);
    }
}

// api dashboard
Route::group($group, function () {

    Route::get('/', 'Api\DashboardController@index')->name('api.dashboard');
    Route::get('api', 'Api\ApiController@index')->name('api.api');
    Route::get('api/key', 'Api\ApiController@apiKey')->name('api.api.key');
    Route::get('api/key/log', 'Api\ApiController@apiKeyLog')->name('api.api-key.log');
    // app.sms.to/api/key/delete
    Route::get('api/data', 'Api\ApiController@data')->name('api.api.data');
    Route::get('api/log', 'Api\ApiController@log')->name('api.api.log');
});

Route::group(['prefix' => 'manager', 'namespace' => 'Manager'], function () {
    Route::get('/login', 'ManagerController@getLogin')->name('manager.login');
    Route::post('/login', 'ManagerController@postLogin')->name('manager.post.login');
    Route::get('payment_history/view/{id}', 'PaymentHistoryController@getView')->name('manager.payment_history.view');

    Route::group(['middleware' => 'auth.manager'], function () {
        Route::get('/', 'ManagerController@getIndex')->name('manager.dashboard');
        Route::group(['prefix' => 'members'], function () {
            Route::get('/', 'Members\MembersController@getIndex')->name('manager.members');
            Route::get('/create', 'Members\MembersController@getCreate')->name('manager.members.create');
            Route::post('/create', 'Members\MembersController@postCreate')->name('manager.members.store');
            //Route::get('/{id}', 'Members\MembersController@show')->name('manager.members.show');
            Route::get('/{id}/edit', 'Members\MembersController@edit')->name('manager.members.edit');
            Route::get('/{id}/update', 'Members\MembersController@getUpdate')->name('manager.members.update');
            Route::post('/{id}/update', 'Members\MembersController@postUpdate')->name('manager.members.postupdate');

            Route::get('vat', 'Members\MembersController@vatMembers')->name('manager.members.vat');
            Route::get('non', 'Members\MembersController@nonMembers')->name('manager.members.non');
            Route::get('payments', 'PaymentHistoryController@getIndex')->name('manager.members.payments');
            Route::get('verify/{id}', 'Members\MembersController@getVerified')->name('manager.members.verify');
            Route::get('download-memberDetails/{id}', 'Members\MembersController@downloadMemberDetails')->name('manager.members.download');
            Route::get('payments/{id}/confirm', 'PaymentHistoryController@confirmPayment')->name('manager.members.payments.confirm');
        });


        //Route::get('members/{id}/edit', 'Members\MembersController@edit')->name('manager.members.edit');
        //Route::get('members/{id}/update', 'Members\MembersController@getUpdate')->name('manager.members.update');
        Route::get('members/{id}', 'Members\MembersController@show')->name('manager.members.show');
        Route::get('members/{id}/settings', 'Members\Settings\SettingsController@getIndex')->name('manager.members.settings');
        Route::post('members/{id}/settings', 'Members\MembersController@storeSettings')->name('manager.members.store.settings');
        Route::get('members/{id}/settings/adjust-rates', 'Members\Settings\AdjustRatesController@getIndex')->name('manager.members.settings.adjust-rates');
        Route::get('members/{id}/settings/adjust-rates/create', 'Members\Settings\AdjustRatesController@getCreate')->name('manager.members.settings.adjust-rates.create');
        Route::post('members/{id}/settings/adjust-rates/create', 'Members\Settings\AdjustRatesController@postCreate')->name('manager.members.settings.adjust-rates.post');
        Route::get('members/{id}/settings/adjust-rates/update/{adjustId}', 'Members\Settings\AdjustRatesController@getUpdate')->name('manager.members.settings.adjust-rates.edit');
        Route::post('members/{id}/settings/adjust-rates/update/{adjustId}', 'Members\Settings\AdjustRatesController@postUpdate')->name('manager.members.settings.adjust-rates.update');
        Route::get('members/{id}/settings/adjust-rates/delete/{adjustId}', 'Members\Settings\AdjustRatesController@postDelete')->name('manager.members.settings.adjust-rates.delete');
        Route::get('members/edit-funds/{id}', 'Members\MembersController@getEditFunds')->name('manager.members.edit.fund');
        Route::get('members/edit-funds/{id}', 'Members\MembersController@getEditFunds')->name('manager.members.edit.fund');
        Route::post('members/edit-funds/{id}', 'Members\MembersController@postEditFunds')->name('manager.members.store.fund');
        Route::get('members/{id}/delete', 'Members\MembersController@postDelete')->name('manager.members.delete');

        Route::group(['prefix' => 'providers'], function () {
            Route::get('/', 'ProviderController@getIndex')->name('manager.providers');
            Route::get('/create', 'ProviderController@create')->name('manager.providers.create');
            Route::post('/store', 'ProviderController@store')->name('manager.providers.store');
            Route::get('/{id}/edit', 'ProviderController@edit')->name('manager.providers.edit');
            Route::post('/{id}/update', 'ProviderController@update')->name('manager.providers.update');
            Route::get('/delete/{id}', 'ProviderController@delete')->name('manager.providers.delete');
        });

        Route::group(['prefix' => 'rates'], function () {
            Route::get('/', 'RatesController@getIndex')->name('manager.rates');
            Route::get('/create', 'RatesController@create')->name('manager.rates.create');
            Route::get('/{id}/edit', 'RatesController@edit')->name('manager.rates.edit');
            Route::get('/{id}/delete', 'RatesController@postDelete')->name('manager.rates.delete');
            Route::post('/{id}/update', 'RatesController@update')->name('manager.rates.update');

            Route::get('providerRates-import', 'RatesController@ratesUpload')->name('manager.rates.ratesUpload');
            Route::post('providerRates', 'RatesController@providerRateFileUpload')->name('manager.rates.rateFileUpload');
        });

        Route::group(['prefix' => 'country-rates'], function () {
            Route::get('/', 'CountryRatesController@getIndex')->name('manager.country-rates');
            Route::get('/create', 'CountryRatesController@create')->name('manager.country-rates.create');
            Route::post('/create', 'CountryRatesController@postCreate')->name('manager.country-rates.postCreate');
            Route::get('/{id}/edit', 'CountryRatesController@edit')->name('manager.country-rates.edit');
            Route::get('/{id}/delete', 'CountryRatesController@postDelete')->name('manager.country-rates.delete');
            Route::post('/{id}/update', 'CountryRatesController@update')->name('manager.country-rates.update');
            Route::get('/{id}/providers', 'CountryRatesController@getproviders')->name('manager.country-rates.providers');
            Route::post('/{id}/providers', 'CountryRatesController@postUpdateProviders')->name('manager.country-rates.update-providers');
            Route::post('/add-providers', 'CountryRatesController@addProviders')->name('manager.country-rates.add-provider');
            Route::get('/{id}/delete-providers', 'CountryRatesController@deleteProviders')->name('manager.country-rates.delete-provider');
        });

        Route::group(['prefix' => 'sms'], function () {
            Route::get('/', 'SmsController@index')->name('manager.sms.jobs');
            Route::get('/{id}/batches', 'SmsBatchController@index')->name('manager.sms.batches');
            Route::get('/{id}/batches/{batch_id}/messages', 'SmsBatchMessageController@index')->name('manager.sms.batches.messages');
            Route::get('/api', 'RatesController@getIndex')->name('manager.sms.jobs.api');
        });

        Route::group(['prefix' => 'banlist'], function () {
            Route::get('/email', 'EmailBanlistController@getIndex')->name('manager.banlist.email');
            Route::get('/ip', 'IpBanlistController@getIndex')->name('manager.banlist.ip');
            Route::get('/numbers', 'NumberBanlistController@getIndex')->name('manager.banlist.numbers');
            Route::get('/numbers/{id}/edit', 'NumberBanlistController@getIndex')->name('manager.banlist.numbers.edit');
        });

        Route::get('/dial-codes', 'CountryDialCodesController@getIndex')->name('manager.dial.codes');
        Route::get('/exchange-rates', 'ExchangeRatesController@getIndex')->name('manager.exchange.rates');

        Route::group(['prefix' => 'settings'], function () {
            Route::get('settings', 'SettingsController@getIndex')->name('manager.settings');
            Route::post('settings', 'SettingsController@postIndex')->name('manager.post.settings');
        });

        Route::get('rates-import', 'CountryRatesController@ratesUpload')->name('manager.ratesUpload');

        Route::post('rates', 'CountryRatesController@rateFileUpload')->name('manager.rateFileUpload');

        Route::get('credits-packages/create', 'CreditPackagesController@create')->name('manager.creditsPackages.create');

        Route::post('credits-packages', 'CreditPackagesController@store')->name('manager.creditsPackages.store');

        Route::get('credits-packages/show', 'CreditPackagesController@show')->name('manager.creditsPackages.show');

        Route::get('credits-packages/edit/{id}', 'CreditPackagesController@edit')->name('manager.creditsPackages.edit');

        Route::post('credits-packages/update/{id}', 'CreditPackagesController@update')->name('manager.creditsPackages.update');

        Route::get('credits-packages/delete/{id}', 'CreditPackagesController@delete')->name('manager.creditsPackages.delete');

        Route::post('credits-packages/update/{id}', 'CreditPackagesController@update')->name('manager.creditsPackages.update');

        Route::get('currency/create', 'CurrencyController@create')->name('manager.currency.create');

        Route::get('currency/show', 'CurrencyController@show')->name('manager.currency.show');

        Route::post('currency', 'CurrencyController@store')->name('manager.currency.store');

        Route::get('currency/edit/{id}', 'CurrencyController@edit')->name('manager.currency.edit');

        Route::post('currency/update/{id}', 'CurrencyController@update')->name('manager.currency.update');

        Route::get('currency/delete/{id}', 'CurrencyController@delete')->name('manager.currency.delete');

        Route::get('delete-requests', 'DeleteRequestController@show')->name('manager.deleteRequest');

        Route::get('delete-requests/{id}/delete', 'DeleteRequestController@postDelete')->name('manager.deleteRequest.delete');

        Route::get('/logout', 'ManagerController@logout')->name('manager.logout');
    });
});

Route::get('/contact-us', 'ContactUsController@showContactUs')->name('contact-us');
Route::post('/contact-us', 'ContactUsController@submitContactUs')->name('post.contact-us');

// Route::get('/home', 'HomeController@index')->name('home');
Route::get('/import', 'HomeController@import')->name('import.test');

// Social Login
Route::get('/auth/facebook/redirect', 'Auth\SocialAuthController@redirectFaceBook')->name('auth.facebook');
Route::get('/auth/facebook/callback', 'Auth\SocialAuthController@callbackFaceBook');

Route::get('/auth/google/redirect', 'Auth\SocialAuthController@redirectGoogle')->name('auth.google');
Route::get('/auth/google/callback', 'Auth\SocialAuthController@callbackGoogle');

Route::get('/auth/twitter/redirect', 'Auth\SocialAuthController@redirectTwitter')->name('auth.twitter');
Route::get('/auth/twitter/callback', 'Auth\SocialAuthController@callbackTwitter');

Route::get('/user/verify/{token}', 'Auth\RegisterController@verifyUser')->name('user.verify');


// sms.to
Route::get('/l/{shortlink}', 'HomeController@shortLink')->name('short_link');
Route::get('/lo/{var}', 'HomeController@opOut')->name('opout');
Route::get('/', 'HomeController@index')->name('home');


Route::get('/pricing', 'HomeController@showPricingPlans')->name('pricing');
Route::get('/sms-api', 'HomeController@showSmsApi')->name('sms-api');
Route::get('/faq', 'HomeController@showFaq')->name('faq');
Route::get('/about', 'HomeController@showAbout')->name('about');
Route::get('/privacy-policy', 'HomeController@showPrivacyPolicy')->name('privacy-policy');
Route::get('/refund-policy', 'HomeController@showRefundPolicy')->name('refund-policy');
Route::get('/terms', 'HomeController@showTerms')->name('terms');
Route::get('/press', 'HomeController@showPress')->name('press');
Route::get('/countries', 'CostCalculatorController@getAvailableCountries');
Route::get('/estimated/{country}', 'CostCalculatorController@getLowestRate');
Route::get('/gdpr-compliance', 'HomeController@showGDPRCompliance')->name('gdpr-compliance');


Route::get('/payment/{id}/pdf/download', 'Api\DashboardController@pdfDownload')->name('generate-pdf');

// optout
Route::get('/o/{optout_string}', 'App\OptOutManagementController@optout')->name('optout');
Route::post('/o/post', 'App\OptOutManagementController@optoutPost')->name('optout.post');


/* Route::get('/', function () {
  // $client = new GuzzleHttp\Client(['base_uri' => 'https://sms.voxbone.com:4443']);
  // return $client->request('POST', '/sms/v1/639175023198', [
  //     'auth' => ['intergo', 'e&Dez0&1Pv', 'digest'],
  //     'json' => ["from" => "+447441922903", "msg" => "Hello dolly!", "frag" => null],
  //     'headers' => [
  //         'Content-Type' => 'application/json',
  //         'Accept'     => 'application/json',
  //     ]
  // ]);
  //dd(md5('95W9YMdgBf4W'));
  //SMS::driver('plivo'); // working on UK numbers
  //SMS::driver('nexmo'); // working
  //SMS::driver('smsapi'); // working
  //SMS::driver('voxbone'); // working
  //SMS::driver('twilio'); // working
  SMS::driver('cheapglobalsms'); //
  SMS::driver('websms'); //
  // SMS::send('This is my message. websms?', [], function($sms) {
  //     $sms->from('SMS.to'); // websms
  //     $sms->to('639175023198');
  // });
  // SMS::send('This is my message. You received this?', [], function($sms) {
  //     //$sms->from('+16177588594'); // twilio
  //     $sms->from('SMS.to'); // nexmo
  //     //$sms->from('+447441922903'); // voxbone
  //     //$sms->from('Info'); // smsapi
  //     $sms->to('639175023198');
  //     // $sms->to('+639175023198');
  //     //$sms->to('+447464808919'); // plivo
  // });
  // SMS::send('This is my message. You received this?', [], function($sms) {
  //     //$sms->from('+16177588594'); // twilio
  //     //$sms->from('SMS.to'); // nexmo
  //     $sms->from('+447441922903'); // voxbone
  //     //$sms->from('Info'); // smsapi
  //     $sms->to('639175023198');
  //     // $sms->to('+639175023198');
  //     //$sms->to('+447464808919'); // plivo
  // });

  return view('welcome');
  }); */

// app dashboard
Route::group(['prefix' => 'services'], function () {

    Route::post('plivo', 'App\DashboardController@index')->name('services.dashboard');

    // stripe
    Route::post('stripe/charge', 'Services\PaymentController@stripePayment')->name('services.stripe.charge');
    Route::post('bankwire/charge', 'Services\PaymentController@bankWirePayment')->name('services.bankwire.charge');
});

use App\Events\ImportFailedEvent;

Route::get('testMark123Pogi/{user_id}/{list_id}', function ($user_id, $list_id) {
    $user = \App\Models\User::find($user_id);
    // $list = $user->lists()->where('id', 7)->first();
    $list = \App\Lists::find($list_id);

    event(new \App\Events\ImportFailedEvent($user, $list, "testing import error"));
//    ImportFailedEvent::dispatch($user, $list, "error", "gago");
    \App\Events\ImportSuccessEvent::dispatch($user, $list);
    echo "Hello world!";
});