<?php

return [

	/*
	|--------------------------------------------------------------------------
	| oAuth Config
	|--------------------------------------------------------------------------
	*/

	/**
	 * Storage
	 */
	'storage' => '\\OAuth\\Common\\Storage\\Session',

	/**
	 * Consumers
	 */
	'consumers' => [

		'Facebook' => [
			'client_id'     => '',
			'client_secret' => '',
			'scope'         => [],
		],
		// https://console.developers.google.com/apis/credentials?project=sms-to&authuser=1
		'Google' => [
		    'client_id'     => env('GOOGLE_CLIENT_ID', '353380095058-2jrld8suqdduldud406i611utatp421g.apps.googleusercontent.com'),
		    'client_secret' => env('GOOGLE_CLIENT_SECRET', 'TWPAS20YaeTeYdm4_fYFUScp'),
		    'scope'         => ['userinfo_email', 'userinfo_profile', 'https://www.google.com/m8/feeds/'],
		    'redirect' 		=> env('GOOGLE_REDIRECT', 'https://sms.to/import/callback'),
		],

	]

];