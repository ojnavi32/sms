<?php

return [

    'notification_types' => [
        'sms_activity' => 'Send me email about my SMS.to activity',
        'personalized' => 'Personalized recommendations',
        'general' => 'General updates announcements',
        'invitations' => 'Marketing Consent',
        'browser_login' => 'A new browser login',
        'email_account_notifications ' => 'Email Account Notifications',
        'email_event_triggers' => 'Email Event Triggers',
        'email_marketing_consent  ' => 'Email Marketing Consent',
    ],
    'vat' => env('VAT', 19),
    
];
