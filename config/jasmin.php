<?php

return [
    'server' => env('JASMIN_SERVER', ''),
    'port' => env('JASMIN_PORT', ''),
    'username' => env('JASMIN_USERNAME', ''),
    'password' => env('JASMIN_PASSWORD', ''),
];