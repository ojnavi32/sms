<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Third Party Services
    |--------------------------------------------------------------------------
    |
    | This file is for storing the credentials for third party services such
    | as Stripe, Mailgun, SparkPost and others. This file provides a sane
    | default location for this type of information, allowing packages
    | to have a conventional place to find your various credentials.
    |
    */

    'mailgun' => [
        'domain' => env('MAILGUN_DOMAIN'),
        'secret' => env('MAILGUN_SECRET'),
    ],

    'ses' => [
        'key' => env('SES_KEY'),
        'secret' => env('SES_SECRET'),
        'region' => 'us-east-1',
    ],

    'sparkpost' => [
        'secret' => env('SPARKPOST_SECRET'),
    ],

    'stripe' => [
        'model' => App\User::class,
        'key' => env('STRIPE_KEY', 'pk_test_hswfpAK1X7zEWutQT70lcyFn'),
        'secret' => env('STRIPE_SECRET', 'sk_test_lSna2ul2PYwJ9vl0GW6ai06R'),
    ],
    
    'open_exchange_rates' => [
        'api_url' => 'https://openexchangerates.org/api/',
        'app_id' => 'cf1bb7680d194e9ea41381f07fb4210a',
    ],

    'intercom' => [
        'app_id' => env('INTERCOM_APP_ID', 'j99o0xb6'),
        'api_key' => env('INTERCOM_API_KEY', 'ae45fcd29d13200230f7b7e64782b6883537503c'),
        'admin_id' => '347483',
    ],

    'vat_layer' => [
        'api_key' => env('VAT_LAYER_API_KEY', '345d372cc739b313e7454aa5f1c6ae86'),
        'api_url' => env('VAT_LAYER_API_URL', 'http://apilayer.net/api/'),
    ],

    'nexmo' => [
        'key' => env('NEXMO_KEY', '976555c1'),
        'secret' => env('NEXMO_SECRET', '7b69badce516979d'),
        'sms_from' => env('SMS_FROM', '447464808919'),
    ],
    
    'braintree' => [
        'environment' => env('BRAINTREE_ENVIRONMENT', 'production'),
        'merchant_id' => env('BRAINTREE_MERCHANT_ID', '77sq3f4tvtczs53m'),
        'merchant_id_usd' => env('BRAINTREE_MERCHANT_ID_USD', 'intergointeractiveUSD'),
        'public_key' => env('BRAINTREE_PUBLIC_KEY', 'b3xzxc6kw82nmq53'),
        'private_key' => env('BRAINTREE_PRIVATE_KEY', 'aa3f0124e357ff837d6b12e303981800'),
    ],

    'facebook' => [
        'client_id' => env('FACEBOOK_CLIENT_ID', '1883703491956335'),
        'client_secret' => env('FACEBOOK_CLIENT_SECRET', 'dc9e631102c6070016667fb19acfabf4'),
        'redirect' => env('FACEBOOK_REDIRECT', 'https://sms.to/auth/facebook/callback'),
    ],

    'google' => [
        'analytics_property_id' => env('GOOGLE_ANALYTICS_PROPERTY_ID', 'UA-61225511-1'),
        'google_events_tracking' => env('GOOGLE_EVENTS_TRACKING', false),
        'client_id' => env('GOOGLE_CLIENT_ID', '257109385889-adsr0njfeai1tnpph0s178q7g5km07nt.apps.googleusercontent.com'),
        'client_secret' => env('GOOGLE_CLIENT_SECRET', 'fbFLmpzw-kimW00nj1QENIJO'),
        'redirect' => env('GOOGLE_REDIRECT', 'https://sms.to/auth/google/callback'),
        'sign_redirect' => env('GOOGLE_SIGNIN_REDIRECT', 'https://sms.to/auth/google/callback'),
    ],

    'twitter' => [
        'client_id' => env('TWITTER_CLIENT_ID', 'OmBp8gVMfIrMOZHlr2VwOyDl8'),
        'client_secret' => env('TWITTER_CLIENT_SECRET', 'Jqvj6U68ZjFKPKPGMm2Cahfd8GThHb5R50K0Cbo3ZWXWFUfoxj'),
        'redirect' => env('TWITTER_REDIRECT', 'https://sms.to/auth/twitter/callback'),
    ],

    'linkedin' => [
        'client_id' => env('LINKEDIN_CLIENT_ID', 'OmBp8gVMfIrMOZHlr2VwOyDl8'),
        'client_secret' => env('LINKEDIN_CLIENT_SECRET', 'ffDjUEgWiBGfQJI6'),
        'redirect' => env('LINKEDIN_REDIRECT', 'https://sms.to/auth/linkedin/callback'),
    ],
    'infobip' => [
        'username' => env('INFOBIP_USERNAME', 'WholesaleBulkSms'),
        'password' => env('INFOBIP_PASSWORD', 'Vh1VCNfG9te2'),
    ],
    'routee' => [
        'applicationId' => env('ROUTEE_APP_ID', '5ae03db1e4b044bb10f8addc'),
        'secret' => env('ROUTEE_SECRET', 'gbcuPmV8FB'),
    ],

];
