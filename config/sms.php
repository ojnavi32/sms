<?php

/*
 * https://simplesoftware.io/docs/simple-sms#docs-configuration for more information.
 */
return [
    'driver' => env('SMS_DRIVER', 'nexmo'),

    'from' => env('SMS_FROM', 'Your Number or Email'),

    'callfire' => [
        'app_login' => env('CALLFIRE_LOGIN', 'Your CallFire API Login'),
        'app_password' => env('CALLFIRE_PASSWORD', 'Your CallFire API Password')
    ],

    'eztexting' => [
        'username' => env('EZTEXTING_USERNAME', 'Your EZTexting Username'),
        'password' => env('EZTEXTING_PASSWORD', 'Your EZTexting Password')
    ],

    'flowroute' => [
        'access_key' => env('FLOWROUTE_ACCESS_KEY', 'Your Flowroute Access Key'),
        'secret_key' => env('FLOWROUTE_SECRET_KEY', 'Your Flowroute Secret Key')
    ],

    'infobip'=> [
         'username' => env('INFOBIP_USERNAME', 'Your Infobip Username'),
         'password' => env('INFOBIP_PASSWORD', 'Your Infobip Password')
    ],

    'labsmobile' => [
        'client_id' => env('LABSMOBILE_CLIENT_ID', 'Your Labsmobile Client ID'),
        'username' => env('LABSMOBILE_USERNAME', 'Your Labsmobile Username'),
        'password' => env('LABSMOBILE_PASSWORD', 'Your Labsmobile Password'),
        'test' => env('LABSMOBILE_TEST', false)
    ],

    'mozeo' => [
        'company_key' => env('MOZEO_COMPANY_KEY', 'Your Mozeo Company Key'),
        'username' => env('MOZEO_USERNAME', 'Your Mozeo Username'),
        'password' => env('MOZEO_PASSWORD', 'Your Mozeo Password')
    ],

    'nexmo' => [
        'from' => env('NEXMO_FROM', 'SMS.to'),
        'api_key' => env('NEXMO_KEY', '69e1a564'),
        'api_secret' => env('NEXMO_SECRET', '0d90202d6697a98b'),
        'sms_per_second' => env('NEXMO_PER_SECOND', 30),

        'smpp_ip_domain' => env('NEXMO_SMPP_IP_DOMAIN'),
        'smpp_port' => env('NEXMO_SMPP_PORT'),
        'smpp_username' => env('NEXMO_SMPP_USERNAME'),
        'smpp_password' => env('NEXMO_SMPP_PASSWORD'),
    ],

    'plivo' => [
        'auth_id' => env('PLIVO_AUTH_ID', 'MAMWI0MZLJMZGZZMMXZT'),
        'auth_token' => env('PLIVO_AUTH_TOKEN', 'Y2FlYjFmOWUyNTAzMGY2Nzc1YTEwYTQ3ZDE3OTQ1'),
        'sms_per_second' => env('PLIVO_PER_SECOND', 30),

        'smpp_ip_domain' => env('PLIVO_SMPP_IP_DOMAIN'),
        'smpp_port' => env('PLIVO_SMPP_PORT'),
        'smpp_username' => env('PLIVO_SMPP_USERNAME'),
        'smpp_password' => env('PLIVO_SMPP_PASSWORD'),
    ],
    
    'voxbone' => [
        'username' => env('VOXBONE_USERNAME', 'intergo'),
        'password' => env('VOXBONE_PASSWORD', 'e&Dez0&1Pv'),
        'sms_per_second' => env('VOXBONE_PER_SECOND', 30),

        'smpp_ip_domain' => env('VOXBONE_SMPP_IP_DOMAIN'),
        'smpp_port' => env('VOXBONE_SMPP_PORT'),
        'smpp_username' => env('VOXBONE_SMPP_USERNAME'),
        'smpp_password' => env('VOXBONE_SMPP_PASSWORD'),
    ],
    
    'sms_api' => [
        'auth_token' => env('SMS_API_AUTH_TOKEN', 'hnA0CZWYyF60uw2RFK1buOVMWP2N91P1APLIod2D'),
        'username' => env('SMS_API_USERNAME', 'accounts@intergo.com.cy'),
        'password' => env('SMS_API_PASSWORD', '95W9YMdgBf4W'),
        'sms_per_second' => env('SMS_API_PER_SECOND', 30),

        'smpp_ip_domain' => env('SMS_API_SMPP_IP_DOMAIN'),
        'smpp_port' => env('SMS_API_SMPP_PORT'),
        'smpp_username' => env('SMS_API_SMPP_USERNAME'),
        'smpp_password' => env('SMS_API_SMPP_PASSWORD'),
    ],
    
    'cheap_global_sms' => [
        'from' => env('CHEAP_GLOBAL_FROM', 'SMSto'),
        'sub_account' => env('SUB_ACCOUNT', '2140_smsto'),
        'sub_account_pass' => env('SUB_ACCOUNT_PASS', 'b8w47p27--8987'),
        'sms_per_second' => env('SMS_API_PER_SECOND', 30),

        'smpp_ip_domain' => env('CHEAP_GLOBAL_SMPP_IP_DOMAIN'),
        'smpp_port' => env('CHEAP_GLOBAL_SMPP_PORT'),
        'smpp_username' => env('CHEAP_GLOBAL_SMPP_USERNAME'),
        'smpp_password' => env('CHEAP_GLOBAL_SMPP_PASSWORD'),
    ],
    
    'web_sms' => [
        'from' => env('WEB_SMS_FROM', 'SMSto'),
        'key' => env('WEB_SMS_KEY', '0bb19-9b88b-ea11e-7bb31-be2e4-4b06b-3468b'),
        'sms_per_second' => env('WEB_SMS_PER_SECOND'),

        'smpp_ip_domain' => env('WEB_SMS_SMPP_IP_DOMAIN'),
        'smpp_port' => env('WEB_SMS_SMPP_PORT'),
        'smpp_username' => env('WEB_SMS_SMPP_USERNAME'),
        'smpp_password' => env('WEB_SMS_SMPP_PASSWORD'),
    ],

    'twilio' => [
        'from' => env('TWILIO_SMS_FROM', 'Your Twilio SID'),
        'account_sid' => env('TWILIO_SID', 'Your Twilio SID'),
        'auth_token' => env('TWILIO_TOKEN', 'Your Twilio Token'),
        'verify' => env('TWILIO_VERIFY', true),
        'sms_per_second' => env('TWILIO_PER_SECOND', 30),

        'smpp_ip_domain' => env('TWILIO_SMPP_IP_DOMAIN'),
        'smpp_port' => env('TWILIO_SMPP_PORT'),
        'smpp_username' => env('TWILIO_SMPP_USERNAME'),
        'smpp_password' => env('TWILIO_SMPP_PASSWORD'),
    ],

    'zenvia' => [
        'account_key' => env('ZENVIA_KEY','Your Zenvia account key'),
        'passcode' => env('ZENVIA_PASSCODE','Your Zenvia Passcode'),
        'call_back_option' => env('ZENVIA_CALLBACK_OPTION', 'NONE'),

        'smpp_ip_domain' => env('ZENVIA_SMPP_IP_DOMAIN'),
        'smpp_port' => env('ZENVIA_SMPP_PORT'),
        'smpp_username' => env('ZENVIA_SMPP_USERNAME'),
        'smpp_password' => env('ZENVIA_SMPP_PASSWORD'),
    ],
    
    'globe' => [
        'app_id' => env('GLOBE_APP_ID','8jpXU8qRKpu4RTkx48iRaxuGzj54UA54'),
        'app_secret' => env('GLOBE_APP_SECRET','2db0e48c53ecdbd707c0a89bc191efeedb8ac75a21185bcc46e729222ca79221'),
        'call_back_option' => env('ZENVIA_CALLBACK_OPTION', 'NONE'),
        'sms_per_second' => env('GLOBE_PER_SECOND', 40),

        'smpp_ip_domain' => env('GLOBE_SMPP_IP_DOMAIN'),
        'smpp_port' => env('GLOBE_SMPP_PORT'),
        'smpp_username' => env('GLOBE_SMPP_USERNAME'),
        'smpp_password' => env('GLOBE_SMPP_PASSWORD'),
    ],
    
    'providers' => [
        'nexmo' => 'Nexmo',
        'plivo' => 'Plivo',
        'voxbone' => 'Voxbone',
        'sms_api' => 'SMS API',
        'twilio' => 'Twilio',
    ],
    'sms_per_batch' => env('SMS_PER_BATCH', 1000),
];
