<?php

return [
    
    'contacts' => [
        'id',
        'name',
        'email',
    ],
    
    'users' => [
        'id',
        'name',
        'email',
    ],
    
    'lists' => [
        'id',
        'name',
    ],
    
    'payments' => [
        'id',
        'name',
    ],

];
