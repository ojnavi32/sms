<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnsToNotificationsSettingTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('notifications_settings', function (Blueprint $table) {
            $table->timestamp('email_Marketing_consent_timestamp')->after('val')->nullable();
            $table->string('email_Marketing_consent_ip')->after('email_Marketing_consent_timestamp')->nullable();
            $table->timestamp('email_Marketing_consent_unsubscribe_timestamp')->after('email_Marketing_consent_ip')->nullable();
            $table->string('email_Marketing_consent_unsubscribe_ip')->after('email_Marketing_consent_unsubscribe_timestamp')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('notifications_settings', function (Blueprint $table) {
            $table->dropColumn(['email_Marketing_consent_timestamp', 'email_Marketing_consent_ip', 'email_Marketing_consent_unsubscribe_timestamp', 'email_Marketing_consent_unsubscribe_ip']);
        });
    }
}
