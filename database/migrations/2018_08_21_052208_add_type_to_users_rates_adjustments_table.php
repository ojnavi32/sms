<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTypeToUsersRatesAdjustmentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users_rates_adjustments', function (Blueprint $table) {
            $table->enum('billing_type',array('PER_COUNTRY', 'PER_CARRIER'))->after('network');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users_rates_adjustments', function (Blueprint $table) {
            $table->dropColumn('billing_type');
        });
    }
}
