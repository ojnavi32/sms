<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddIndexesToSmsToTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0');

        Schema::table('contacts', function (Blueprint $table) {
            $table->integer('user_id')->unsigned()->change();
            $table->foreign('user_id')->references('id')->on('users');
        });

        Schema::table('cash_history', function (Blueprint $table) {
            $table->integer('client_id')->unsigned()->change(); 
            $table->foreign('client_id')->references('id')->on('users');
        });

        Schema::table('lists_contacts', function (Blueprint $table) {
            $table->integer('user_id')->unsigned()->change();
            $table->foreign('user_id')->references('id')->on('users');
        });

        Schema::table('providers_rates', function (Blueprint $table) {
            $table->integer('rate_id')->unsigned()->change();
            $table->integer('provider_id')->unsigned()->change();
            $table->foreign('rate_id')->references('id')->on('rates');
            $table->foreign('provider_id')->references('id')->on('providers');
        });

        Schema::table('sms_broadcasts_messages', function (Blueprint $table) {
            $table->integer('sms_broadcast_id')->unsigned()->change();
            $table->integer('sms_broadcast_batch_id')->unsigned()->change();
            $table->foreign('sms_broadcast_id')->references('id')->on('sms_broadcasts');
            $table->foreign('sms_broadcast_batch_id')->references('id')->on('sms_broadcasts_batches');
        });

        Schema::table('lists', function (Blueprint $table) {
            $table->integer('user_id')->unsigned()->change();
            $table->foreign('user_id')->references('id')->on('users');
        });

        DB::statement('SET FOREIGN_KEY_CHECKS=1');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        
    }
}
