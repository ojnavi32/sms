<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ModifyListIdBroadcastsTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('sms_broadcasts', function (Blueprint $table) {
            $table->dropForeign('sms_broadcasts_lists_id_foreign');
        });
        
        Schema::table('sms_broadcasts', function (Blueprint $table) {
            $table->string('lists_id', 128)->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        
    }
}
