<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCashHistoryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cash_history', function(Blueprint $table) {
            $table->increments('id');
            $table->integer('client_id')->nullable();
            $table->double('original_amount')->nullable();
            $table->double('amount')->nullable();
            $table->integer('fax_broadcast_id')->nullable();
            $table->boolean('type')->nullable();
            $table->integer('by')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
