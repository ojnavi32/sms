<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddBroadcastIdSmsBroadcastsMessagesTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('sms_broadcasts_messages', function (Blueprint $table) {
            $table->integer('sms_broadcast_id')->after('id')->nullable();
            $table->integer('sms_broadcast_batch_id')->after('sms_broadcast_id')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        
    }
}
