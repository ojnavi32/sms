<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddNetworkToUsersRatesAdjustmentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users_rates_adjustments', function (Blueprint $table) {
            $table->string('network', 255)->nullable()->after('rate_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users_rates_adjustments', function (Blueprint $table) {
             $table->dropColumn(['network']);
        });
    }
}
