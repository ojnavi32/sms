<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCountryDialCodesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('country_dial_codes', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('country');
			$table->string('slug')->nullable();
			$table->char('a2_code');
			$table->char('a3_code');
			$table->mediumInteger('dial_code')->unsigned();
            $table->integer('exchange_rate_id')->unsigned()->nullable();
            $table->foreign('exchange_rate_id')->references('id')->on('exchange_rates');
			$table->timestamps();
            $table->softDeletes();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('country_dial_codes');
	}

}
