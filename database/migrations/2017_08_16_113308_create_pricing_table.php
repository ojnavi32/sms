<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePricingTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pricing', function(Blueprint $table) {
            $table->increments('id');
            $table->string('country', 64)->nullable();
            $table->string('country_iso', 8)->nullable();
            $table->string('country_code', 8)->nullable();
            $table->string('network', 64)->nullable();
            $table->string('network_alias', 64)->nullable();
            $table->string('network_name', 64)->nullable();
            $table->integer('prefix')->nullable();
            $table->double('price')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('sms_broadcasts_messages_logs');
    }

}
