<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSmsBatchesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sms_batches', function(Blueprint $table) {
            $table->increments('id');
            $table->integer('sms_broadcast_id')->unsigned();
            $table->foreign('sms_broadcast_id')->references('id')->on('sms_broadcasts');
            $table->string('status')->nullable();
            $table->text('message')->nullable();
            $table->dateTime('time_run')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
