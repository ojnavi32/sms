<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddLastLoginTimeUsersTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function($table)
        {
            $table->string('ip_address', 32)->after('credits')->nullable();
            $table->string('last_login_time', 32)->after('ip_address')->nullable();
            $table->string('last_ip_address', 32)->after('last_login_time')->nullable();
            $table->string('email_domain', 128)->after('email')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        
    }

}
