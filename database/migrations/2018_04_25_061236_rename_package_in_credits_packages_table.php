<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RenamePackageInCreditsPackagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('credits_packages', function (Blueprint $table) {
                $table->renameColumn('package', 'name');
                });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
         Schema::table('credits_packages', function(Blueprint $table) {
                        $table->renameColumn('name', 'package');
                });
    }
}
