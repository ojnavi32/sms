<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddIsDeletedToDeleteRequestsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('delete_requests', function (Blueprint $table) {
            $table->boolean('is_deleted')->after('user_id')->default(0)->nullable();
            $table->timestamp('member_deleted_at')->after('is_deleted')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('delete_requests', function (Blueprint $table) {
            $table->dropColumn(['is_deleted', 'member_deleted_at']);
        });
    }
}
