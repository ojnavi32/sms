<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnsToUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->text('address')->after('billing_type')->nullable();
            $table->string('country')->after('address')->nullable();
            $table->string('postal_code')->after('country')->nullable();
            $table->string('vat_number')->after('postal_code')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $table->dropColumn('address');
        $table->dropColumn('country');
        $table->dropColumn('postal_code');
        $table->dropColumn('vat_number');
    }
}
