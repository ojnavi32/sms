<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProvidersRatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('providers_rates', function(Blueprint $table) {
            $table->increments('id');
            $table->integer('provider_id')->nullable();
            $table->integer('rate_id')->nullable();
            $table->double('base_rate')->nullable();
            $table->double('our_rate')->nullable();
            $table->string('network', 64)->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('sms_broadcasts_messages_logs');
    }

}
