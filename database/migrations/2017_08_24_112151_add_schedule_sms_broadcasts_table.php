<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddScheduleSmsBroadcastsTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('sms_broadcasts', function($table)
        {
            $table->date('sched_date')->after('total_cost')->nullable();
            $table->string('sched_time', 16)->after('sched_date')->nullable();
            $table->string('interval', 64)->after('sched_time')->nullable(); // we can use something like cron jobs * * * *
            $table->dateTime('run')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        
    }

}
