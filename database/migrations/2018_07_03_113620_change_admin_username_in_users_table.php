<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

use Carbon\carbon;

class ChangeAdminUsernameInUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $userArray = [
            'name'=>'admin',
            'type' => 'admin',
            'first_name' => 'admin',
            'last_name' => 'admin',
            'surname' => 'admin', 
            'username' => 'smsadmin', 
            'email' => 'admin@sms.to',
            'password'=>bcrypt('0022#@fs@L'),
            'created_at'=>carbon::Now()
        ];
        
        $user = DB::table('users')->where('username','admin@sms.to')->update($userArray);

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
