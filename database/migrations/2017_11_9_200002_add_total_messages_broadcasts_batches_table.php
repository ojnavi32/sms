<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTotalMessagesBroadcastsBatchesTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if ( ! Schema::hasColumn('sms_broadcasts_batches', 'total_messages')) {
            Schema::table('sms_broadcasts_batches', function (Blueprint $table) {
                $table->integer('total_messages')->after('message')->nullable();
                $table->integer('failed')->after('total_messages')->nullable();
                $table->integer('success')->after('failed')->nullable();
            });
        }

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        
    }
}
