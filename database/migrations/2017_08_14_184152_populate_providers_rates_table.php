<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class PopulateProvidersRatesTable extends Migration {

/**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        foreach(App\Models\Provider::all() as $provider) {
            
            $rates =  App\Models\Rate::join('country_dial_codes', 'country_dial_codes.id', '=', 'rates.country_dial_code_id')
                        ->orderBy('dial_code')
                        ->orderBy('country')
                        ->get([DB::raw('`rates`.`id` AS `rate_id`'), 'rates.*', 'country_dial_codes.*']);
            
            foreach($rates as $rate) {
                App\Models\ProviderRate::firstOrCreate(['provider_id' => $provider->id, 'rate_id' => $rate->rate_id, 'base_rate' => 0, 'our_rate' => 0, 'network' => null]);
            }
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        
    }

}
