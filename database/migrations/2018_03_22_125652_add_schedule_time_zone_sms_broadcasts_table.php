<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddScheduleTimeZoneSmsBroadcastsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('sms_broadcasts', function (Blueprint $table) {

               $table->string('scheduled_server_time', 16)->after('sched_time')->nullable();
               $table->string('time_zone', 100)->after('scheduled_server_time')->nullable();

    });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $table->dropColumn('scheduled_server_time');
        $table->dropColumn('time_zone');
    }
}
