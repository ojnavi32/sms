<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterBilingDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('billing_details', function($table)
        {
            $table->string('first_name', 32)->after('address')->nullable();
            $table->string('last_name', 32)->after('first_name')->nullable();
            $table->string('company', 64)->after('last_name')->nullable();
            $table->string('vat_number', 32)->after('company')->nullable();
            $table->boolean('vat_number_validated')->after('vat_number')->nullable();
            $table->integer('vat_number_tries')->after('vat_number_validated')->nullable();
            $table->string('city', 32)->after('vat_number_tries')->nullable();
            $table->string('state', 32)->after('city')->nullable();
            $table->string('postal_code', 16)->after('state')->nullable();
            $table->integer('country_dial_code_id')->after('postal_code')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        
    }
}
