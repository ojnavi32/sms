<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRatesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('rates', function(Blueprint $table)
		{
			$table->increments('id');
            $table->integer('country_dial_code_id')->unsigned();
            $table->foreign('country_dial_code_id')->references('id')->on('country_dial_codes');
			$table->string('name');
			$table->string('slug')->nullable();
            $table->integer('plan')->unsigned();
            $table->float('base_rate', 4, 4);
            $table->decimal('multiplier', 5, 2);
			$table->timestamps();
            $table->softDeletes();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('rates');
	}

}
