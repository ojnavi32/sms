<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeListContactsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0');

        Schema::table('contacts', function (Blueprint $table) {
              $table->increments('id')->unsigned()->change();
        });

        Schema::table('lists_contacts', function (Blueprint $table) {
              $table->integer('list_id')->unsigned()->change();
              $table->integer('contact_id')->unsigned()->change();
              $table->foreign('list_id')->references('id')->on('lists');
              $table->foreign('contact_id')->references('id')->on('contacts');
        });

        DB::statement('SET FOREIGN_KEY_CHECKS=1');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        
    }
}
