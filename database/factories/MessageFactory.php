<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

/** @var \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(App\Message::class, function (Faker\Generator $faker) {
   
    return [
        'user_id' => function () {
            //return factory(App\User::class)->create()->id;
            return App\User::inRandomOrder()->first()->id;
        },
        'delivered' => 10,
        'failed' => 10,
        'message' => $faker->sentence,
        'status' => 'success',
    ];
});
