<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

/** @var \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(App\Models\ApiKey::class, function (Faker\Generator $faker) {
   
    return [
    	'user_id' => function () {
            return App\User::inRandomOrder()->first()->id;
        },
        'description' => $faker->company,
        'mode' => 'Gateway',
        'status' => 'Active',
        'access_key' => 'dfdshfdsfhdl',
    ];
});
