<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

/** @var \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(App\ContactMessage::class, function (Faker\Generator $faker) {
   $user = App\User::inRandomOrder()->first();
    return [
        'user_id' => function () use ($user) {
            return $user->id;
        },
       'list_id' => function () use ($user) {
            return $user->lists()->inRandomOrder()->first()->id;
        },
        'contact_id' => function () use ($user) {
            return $user->contacts()->inRandomOrder()->first()->id;
        },
        'message_id' => function () use ($user) {
            return $user->messages()->inRandomOrder()->first()->id;
        },
        'status' => 'success',
        'created_at' => $faker->dateTimeBetween($startDate = '-2 months', $endDate = 'now'),
    ];
});
