<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

/** @var \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(App\BillingDetail::class, function (Faker\Generator $faker) {
   
   $faker->addProvider(new \Faker\Provider\at_AT\Payment($faker));
    return [
        'user_id' => null,
        'address' => $faker->address,
        'first_name' => $faker->firstName,
        'last_name' => $faker->lastName,
        'company' => $faker->company,
        'vat_number' => $faker->vat(false),
        'postal_code' => $faker->postcode,
    ];
});
