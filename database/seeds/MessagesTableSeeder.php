<?php

use Illuminate\Database\Seeder;

class MessagesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	// This will create 50 users with 
        factory(App\User::class, 50)->create()->each(function ($u) {
	        $u->messages()->save(factory(App\Message::class)->make()); // create messages
	        $u->contacts()->save(factory(App\Contact::class)->make()); // create contacts
	        $u->lists()->save(factory(App\Lists::class)->make()); // create lists
            $u->billingDetail()->save(factory(App\BillingDetail::class)->make()); // create lists
            
	    });
	    
	    factory(App\Message::class, 50)->create();
        factory(App\ContactMessage::class, 5000)->create();
    }
}
