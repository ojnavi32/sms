<?php

use Illuminate\Database\Seeder;

class AdministratorsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'type' => 'admin',
            'email' => 'admin@sms.to',
            'username' => 'admin@sms.to',
            'password' => bcrypt('0022#@fs@L'),
        ]);
    }
}