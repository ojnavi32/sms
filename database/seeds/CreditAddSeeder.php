<?php

use Illuminate\Database\Seeder;

use App\Models\User;

class CreditAddSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $users = User::where('credit_balance', 0)->get();

        foreach($users as $user){
            $user->credit_balance = $user->cash_balance/0.01;
            $user->save();
        }
        
    }
}