<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);

        // $this->call(MessagesTableSeeder::class);
        // $this->call(RateTableSeeder::class);
        $this->call(PaymentsTableSeeder::class);
        $this->call(AdministratorsTableSeeder::class);
        $this->call(CreditAddSeeder::class);
        $this->call(ProvidersTableSeeder::class);
    }
}
