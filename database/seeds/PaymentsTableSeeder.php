<?php

use Illuminate\Database\Seeder;

use App\Models\PaymentMethod;

class PaymentsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        DB::table('payment_methods')->delete();

        $paymentMethods = [
            ['name' => 'credit card', 'slug' => 'credit_card', 'active' => 1 ],
            ['name' => 'cash', 'slug' => 'cash', 'active' => 1 ],
            ['name' => 'bank wire', 'slug' => 'bank_wire', 'active' => 1 ]
        ];

        foreach($paymentMethods as $paymentMethod){
            PaymentMethod::create($paymentMethod);
        }

        
    }
}

