const { mix } = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/assets/js/app.js', 'public/js')
	.js('resources/assets/js/landing.js', 'public/js')
    .js('resources/assets/js/manager.js', 'public/js')
	.sass('resources/assets/sass/app.scss', 'public/css')
	.sass('resources/assets/sass/landing.scss', 'public/css')
	.sass('resources/assets/sass/manager.scss', 'public/css')
	.options({ processCssUrls: false })
	.version()
	.copy('resources/assets/images', 'public/images')
	.copy('resources/assets/favicon', 'public')
	.browserSync({
		proxy: {
			target: 'http://127.0.0.1:8000/'
		}
	});
