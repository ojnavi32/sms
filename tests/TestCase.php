<?php

namespace Tests;

use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\TestCase as BaseTestCase;
use JWTAuth;
use App\User;
use Faker\Factory as Faker;

abstract class TestCase extends BaseTestCase
{
    use CreatesApplication, DatabaseMigrations;
    
  	protected $faker;  
  
    // https://www.neontsunami.com/posts/changing-the-base-url-with-laravel-54-testing
    function setUp()
	{
	    parent::setUp();
	    
	    $this->faker = Faker::create();
	    // Migrate database
	    //Artisan::call('migrate --database=sqlite'); // testing database which is sqlite
	    //Artisan::call('migrate:refresh', ['--database' => 'sqlite']); // testing database which is sqlite
	 	// rm database/sms.sqlite 
		// touch database/sms.sqlite
	    
	    // testing database which is sqlite. sqlite is the database connection in database.php
	    $this->artisan('migrate', ['--database' => 'sqlite']);
	    config(['app.url' => 'https://api.sms.dev']);
	}
	
	public $user = null;
	
    public function user()
    {
        if ($this->user == null) {
            $this->user = factory(User::class)->create(['password' => bcrypt('foo')]);
        }
        
        return $this->user;
    }
    
    public function tearDown()
    {
    	// $this->artisan('migrate:reset', ['--database' => 'sqlite']);
    	// parent::tearDown();
    }
    
    /**
	 * Return request headers needed to interact with the API.
	 *
	 * @return Array array of headers.
	 */
	protected function headers($user = null)
	{
	    $headers = ['Accept' => 'application/json'];

	    if (!is_null($user)) {
	        $token = JWTAuth::fromUser($user);
	        JWTAuth::setToken($token);
	        $headers['Authorization'] = 'Bearer '.$token;
	    }

	    return $headers;
	}
}
