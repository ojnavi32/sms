<?php

namespace Tests\Browser;

use App\User;
use Tests\DuskTestCase;
use Laravel\Dusk\Browser;
use Illuminate\Foundation\Testing\DatabaseMigrations;

class LoginTest extends DuskTestCase
{
    use DatabaseMigrations;
    
    /**
     * A Dusk test login.
     *
     * @return void
     */
    public function testLogin()
    {
        $user = factory(User::class)->create([
            'email' => 'manny.isles@sms.to',
        ]); // default password is secret
        
        $this->browse(function ($browser) use ($user) {
            $browser->visit('/login')
                    ->type('email', $user->email)
                    ->type('password', 'secret')
                    ->press('Login')
                    ->assertPathIs('/');
        });
    }
    
    public function testRegister()
    {
        $this->browse(function ($browser) {
            $browser->visit('/register')
                    ->type('name', 'Mann Isles')
                    ->type('email', 'manny.isles@sms.to')
                    ->type('password', 'secret')
                    ->type('password_confirmation', 'secret')
                    ->press('Register')
                    ->assertPathIs('/');
        });
    }
    
    public function testForgotPassword()
    {
        $user = factory(User::class)->create([
            'email' => 'manny.isles@sms.to',
        ]); // default password is secret
        
        $this->browse(function ($browser) use ($user) {
            $browser->visit('/password/reset')
                    ->type('email', $user->email)
                    ->press('Send Password')
                    ->assertPathIs('/password/reset');
        });
    }
}
