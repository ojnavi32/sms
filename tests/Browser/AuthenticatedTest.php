<?php

namespace Tests\Browser;

use App\User;
use Tests\DuskTestCase;
use Laravel\Dusk\Browser;
use Illuminate\Foundation\Testing\DatabaseMigrations;

class AuthenticatedTest extends DuskTestCase
{
    use DatabaseMigrations;
    
    /**
     * A Dusk test login.
     *
     * @return void
     */
    public function testLogin()
    {
        $user = factory(User::class)->create([
            'email' => 'manny.isles@sms.to',
        ]); // default password is secret
        
        $this->browse(function ($browser) {
            $browser->loginAs(User::find(1))
                  ->visit('/');
        });
    }
}
