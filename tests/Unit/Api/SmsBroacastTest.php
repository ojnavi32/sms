<?php

namespace Tests\Unit\Api;

use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Support\Facades\Mail;
use App\User;
use App\Lists;
use App\Models\SmsBroadcast;
use App\ListContact;

class SmsBroacastTest extends TestCase
{
    //use DatabaseMigrations;
    
	public $apiUrl = '/api/v1/';
    
	public function testUnauthorized()
    {
    	$response = $this->json('GET', $this->apiUrl . 'broadcasts');
        $response->assertStatus(401);
    }
    
	public function testGet()
    {
    	$user = $this->user();
    	
        $response = $this->json('GET', $this->apiUrl . 'broadcasts', [], $this->headers($user));

        $response->assertStatus(200);
    }
    
    public function testShow()
    {
        $user = $this->user();
        
        $sms = factory(SmsBroadcast::class)->create(['user_id' => $user->id]);
        
        $response = $this->json('GET', $this->apiUrl . 'broadcasts/' . $sms->id, [], $this->headers($user));

        $response->assertStatus(200);
    }
    
    public function testNotFound()
    {
        $user = $this->user();
        
        $response = $this->json('GET', $this->apiUrl . 'broadcasts/-1', [], $this->headers($user));

        $response->assertStatus(400);
    }
    
    public function testStore()
    {
        $user = $this->user();
        
        $list = factory(Lists::class)->create(['user_id' => $user->id]);
        
        $response = $this->json('POST', $this->apiUrl . 'broadcasts', ['list_id' => $list->id, 'message' => 'Test Messages', 'total_cost' => 50], $this->headers($user));
        
        $response->assertStatus(201);
    }
    
    public function testFailedStore()
    {
        $user = $this->user();
        
        $response = $this->json('POST', $this->apiUrl . 'broadcasts', ['name' => null],
            $this->headers($user)
        );

        $response
            ->assertStatus(500)
            ->assertJson([
                'status_code' => 500,
            ]);
    }
    
    public function testUpdate()
    {
        $user = $this->user();
        $sms = factory(SmsBroadcast::class)->create(['user_id' => $user->id]);
        
        $response = $this->json('POST', $this->apiUrl . 'broadcasts/' . $sms->id, ['message' => 'updated messages'], $this->headers($user));

        $response
            ->assertStatus(200)
            ->assertJson([
                'updated' => true,
            ]);
    }
    
    public function testDelete()
    {
        $user = $this->user();
        $sms = factory(SmsBroadcast::class)->create(['user_id' => $user->id]);
        
        $response = $this->json('DELETE', $this->apiUrl . 'broadcasts/' . $sms->id, [], $this->headers($user));

        $response
            ->assertStatus(204);
    }
    
    public function t1estFailedDelete()
    {
        $user = $this->user();
        
        $response = $this->json('DELETE', $this->apiUrl . 'templates/' . '-1', [], $this->headers($user));

        $response->assertStatus(400);
    }
}
