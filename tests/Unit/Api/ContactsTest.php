<?php

namespace Tests\Unit\Api;

use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Mail;
use App\User;
use App\Contact;
use App\Lists;
use App\ListContact;

class ContactsTest extends TestCase
{
    //use DatabaseMigrations;
    
	public $apiUrl = '/api/v1/';
	
	public function testUnauthorized()
    {
    	$response = $this->json('GET', $this->apiUrl . 'contacts');
        $response->assertStatus(401);
    }
    
	public function testGetContacts()
    {
    	// use for setting up session
    	// $response = $this->withSession(['foo' => 'bar'])
     //                     ->get('/');
    	// $this->headers() // is located at Test/TestCase.php
    	$user = $this->user();
    	
        $response = $this->json('GET', $this->apiUrl . 'contacts', [], $this->headers($user));

        $response->assertStatus(200);
    }
    
    public function testGetSingleContact()
    {
    	$user = $this->user();
    	
    	$contact = factory(Contact::class)->create(['user_id' => $user->id]);
    	
        $response = $this->json('GET', $this->apiUrl . 'contacts/' . $contact->id, [], $this->headers($user));

        $response->assertStatus(200);
    }
    
    public function testContactNotFound()
    {
    	$user = $this->user();
            	
        $response = $this->json('GET', $this->apiUrl . 'contacts/-1', [], $this->headers($user));

        $response->assertStatus(404);
    }
    
    public function testCreateContacts()
    {
    	$user = $this->user();
    	
        $response = $this->json('POST', $this->apiUrl . 'contacts', ['name' => 'Sally', 'email' => 'mail.' . time() .'@gmail.com', 'phone' => '+123' . time()], $this->headers($user));
        
        $response->assertStatus(201);
    }
    
    public function testCreateErrorOnDuplicate()
    {
        $user = $this->user();
        $phoneNumber = '+123' . time();
        $data = ['name' => 'Sally', 'email' => 'mail.' . time() .'@gmail.com', 'phone' => $phoneNumber];
        $response = $this->json('POST', $this->apiUrl . 'contacts', $data, $this->headers($user));
        $response = $this->json('POST', $this->apiUrl . 'contacts', $data, $this->headers($user));
        
        $response->assertStatus(422);
    }
    
    public function testCreateContactsWithList()
    {
    	$user = $this->user();
         
    	$list = factory(Lists::class)->create(['user_id' => $user->id]);
    	
        $response = $this->json('POST', $this->apiUrl . 'contacts', [
        		'name' => 'Sally', 
        		'email' => 'mail.' . time() .'@gmail.com',
        		'phone' => '+123' . time(),
        		'list_id' => [$list->id, 100],
        		
        ],
        $this->headers($user)
        );
        
        $response->assertStatus(201);
    }
    
    public function testCreateContactsFailedValidation()
    {
    	$user = $this->user();
    	
        $response = $this->json('POST', $this->apiUrl . 'contacts', ['name' => 'Sally', 'email' => 'mail.' . time() .'@gmail.com', 'phone' => null],
        	$this->headers($user)
        );

        $response
            ->assertStatus(422)
            ->assertJson([
                'status_code' => 422,
            ]);
    }
    
    public function testUpdateContacts()
    {
    	$user = $this->user();
    	$contact = factory(Contact::class)->create(['user_id' => $user->id]);
    	
        $response = $this->json('POST', $this->apiUrl . 'contacts/' . $contact->id, ['name' => 'Sally', 'email' => 'mail.' . time() .'@gmail.com', 'phone' => '+123updated' . time()],
        	$this->headers($user)
        	);

        $response
            ->assertStatus(200)
            ->assertJson([
                'updated' => true,
            ]);
    }
    
    public function testDeleteContacts()
    {
    	$user = $this->user();
    	$contact = factory(Contact::class)->create(['user_id' => $user->id]);
    	
        $response = $this->json('DELETE', $this->apiUrl . 'contacts/' . $contact->id, [], $this->headers($user));

        $response
            ->assertStatus(204);
    }
    
    public function testFailedDeleteContacts()
    {
    	$user = $this->user();
    	$contact = factory(Contact::class)->create(['user_id' => $user->id]);
    	
        $response = $this->json('DELETE', $this->apiUrl . 'contacts/' . '-1', [], $this->headers($user));

        $response->assertStatus(400);
    }
    
    public function testFailedDeleteContactsNotOwned()
    {
    	$user = $this->user();
    	$contact = factory(Contact::class)->create(['user_id' => $user->id]);
    	
    	$otherUser = factory(User::class)->create(['password' => bcrypt('foo')]);
    	
        $response = $this->json('DELETE', $this->apiUrl . 'contacts/' . $contact->id, [], $this->headers($otherUser));

        $response->assertStatus(400);
    }
    
    public function testMoveContacts()
    {
    	$user = $this->user();
    	// Create 2 lists
    	$list = factory(Lists::class)->create(['user_id' => $user->id]);
    	$list2 = factory(Lists::class)->create(['user_id' => $user->id]);
    	
    	$contact = factory(Contact::class)->create(['user_id' => $user->id]);
    	$contact2 = factory(Contact::class)->create(['user_id' => $user->id]);
    	
    	$listContact = factory(ListContact::class)->create(['user_id' => $user->id, 'list_id' => $list->id, 'contact_id' => $contact->id]);
    	$listContact2 = factory(ListContact::class)->create(['user_id' => $user->id, 'list_id' => $list2->id, 'contact_id' => $contact2->id]);
    	
        $response = $this->json('POST', $this->apiUrl . 'contacts/move', ['lists_contact_ids' => [$listContact->id, $listContact2->id], 'list_id' => $list2->id ], $this->headers($user));
        
        $response
            ->assertStatus(200)
            ->assertJson([
                'moved' => true,
            ]);
    }
    
    public function testMergeContacts()
    {
        $user = $this->user();
        factory(Contact::class)->create(['user_id' => $user->id, 'phone' => '+7057482261091']);
        factory(Contact::class)->create(['user_id' => $user->id, 'phone' => '+7057482261091']);
        factory(Contact::class)->create(['user_id' => $user->id, 'phone' => '+7057482261091']);
        
        $response = $this->json('POST', $this->apiUrl . 'contacts/merge', [], $this->headers($user));

        $response
            ->assertStatus(200)
            ->assertJson([
                'merge' => true,
            ]);
    }
    
    public function testSendSms()
    {
        $user = $this->user();
        
        $contact = factory(Contact::class)->create(['user_id' => $user->id]);
        
        $response = $this->json('POST', $this->apiUrl . 'contacts/' . $contact->id . '/sms', ['message' => 'Test message'], $this->headers($user));
        
        $response->assertStatus(201);
    }
    
    public function testSendEmail()
    {
        $user = $this->user();
        
        $contact = factory(Contact::class)->create(['user_id' => $user->id]);
        
        $response = $this->json('POST', $this->apiUrl . 'contacts/' . $contact->id . '/email', ['message' => 'Test email'], $this->headers($user));
        
        $response->assertStatus(201);
    }
    
    public function testImportUploadValidationFailed()
    {
        $user = $this->user();
        
        $response = $this->json('POST', $this->apiUrl . 'contacts/import-upload', [], $this->headers($user));
        
        $response->assertStatus(500);
    }
    
    public function testImportUploadAcceptedMime()
    {
        $user = $this->user();
        
        $response = $this->json('POST', $this->apiUrl . 'contacts/import-upload', [
            'file' => UploadedFile::fake()->create('data.xls')
            ], 
        $this->headers($user));
        
        $response->assertStatus(201);
        
        $response = $this->json('POST', $this->apiUrl . 'contacts/import-upload', [
            'file' => UploadedFile::fake()->create('data.csv')
            ], 
        $this->headers($user));
        
        $response->assertStatus(201);
        
        $response = $this->json('POST', $this->apiUrl . 'contacts/import-upload', [
            'file' => UploadedFile::fake()->create('data.ods')
            ], 
        $this->headers($user));
        
        $response->assertStatus(201);
        
        // Failed if txt
        $response = $this->json('POST', $this->apiUrl . 'contacts/import-upload', [
            'file' => UploadedFile::fake()->create('data.txt')
            ], 
        $this->headers($user));
        
        $response->assertStatus(500);
    }
    
    public function testContactGravatar()
    {
        $user = $this->user();
        $response = $this->json('GET', $this->apiUrl . 'contacts/manny.isles@gmail.com/gravatar', [], $this->headers($user));
        $response->assertStatus(200);
    }
}
