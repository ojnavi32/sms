<?php

namespace Tests\Unit\Api;

use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Support\Facades\Mail;
use App\User;
use App\Contact;
use App\Models\ApiKey;
use App\ListContact;

class ApiKeyTest extends TestCase
{
    //use DatabaseMigrations;
    
	public $apiUrl = '/api/v1/';
	
	public function testUnauthorized()
    {
    	$response = $this->json('GET', $this->apiUrl . 'keys');
        $response->assertStatus(401);
    }
    
	public function testGet()
    {
    	$user = $this->user();
    	
        $response = $this->json('GET', $this->apiUrl . 'keys', [], $this->headers($user));

        $response->assertStatus(200);
    }
    
    public function testShow()
    {
        $user = $this->user();
        
        $api = factory(ApiKey::class)->create(['user_id' => $user->id]);
        
        $response = $this->json('GET', $this->apiUrl . 'keys/' . $api->id, [], $this->headers($user));

        $response->assertStatus(200);
    }
    
    public function testNotFound()
    {
        $user = $this->user();
        
        $response = $this->json('GET', $this->apiUrl . 'keys/-1', [], $this->headers($user));

        $response->assertStatus(400);
    }
    
    public function testStore()
    {
        $user = $this->user();
        
        $response = $this->json('POST', $this->apiUrl . 'keys', [
                'description' => 'Mobile app',
                'mode' => 'Gateway',
                'status' => 'Active',
                'access_key' => 'Moxcxcxbilecxcxgfgcxccxcxcapp',
                ], 
            $this->headers($user));
        
        $response->assertStatus(201);
    }
    
    public function testFailedStore()
    {
        $user = $this->user();
        
        $response = $this->json('POST', $this->apiUrl . 'keys', ['name' => null],
            $this->headers($user)
        );

        $response
            ->assertStatus(500)
            ->assertJson([
                'status_code' => 500,
            ]);
    }
    
    public function testUpdate()
    {
        $user = $this->user();
        $api = factory(ApiKey::class)->create(['user_id' => $user->id]);
        
        $response = $this->json('POST', $this->apiUrl . 'keys/' . $api->id, [
                'description' => 'Mobile app',
                'mode' => 'Gateway',
                'status' => 'Active',
                'access_key' => 'Moxcxcxbilecxcxgfgcxccxcxcapp',
                ], 
            $this->headers($user));

        $response
            ->assertStatus(200)
            ->assertJson([
                'updated' => true,
            ]);
    }
    
    public function testDelete()
    {
        $user = $this->user();
        $api = factory(ApiKey::class)->create(['user_id' => $user->id]);
        
        $response = $this->json('DELETE', $this->apiUrl . 'keys/' . $api->id, [], $this->headers($user));

        $response
            ->assertStatus(204);
    }
    
    public function testFailedDelete()
    {
        $user = $this->user();
        
        $response = $this->json('DELETE', $this->apiUrl . 'keys/' . '-1', [], $this->headers($user));

        $response->assertStatus(400);
    }
}
