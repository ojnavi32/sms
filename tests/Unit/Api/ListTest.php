<?php

namespace Tests\Unit\Api;

use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Support\Facades\Mail;
use App\User;
use App\Contact;
use App\Lists;
use App\ListContact;

class ListTest extends TestCase
{
    //use DatabaseMigrations;
    
	public $apiUrl = '/api/v1/';
	
	public function testUnauthorized()
    {
    	$response = $this->json('GET', $this->apiUrl . 'lists');
        $response->assertStatus(401);
    }
    
	public function testGetLists()
    {
    	$user = $this->user();
    	
        $response = $this->json('GET', $this->apiUrl . 'lists', [], $this->headers($user));

        $response->assertStatus(200);
    }
    
    public function testshowList()
    {
        $user = $this->user();
        
        $list = factory(Lists::class)->create(['user_id' => $user->id]);
        
        $response = $this->json('GET', $this->apiUrl . 'contacts/' . $list->id, [], $this->headers($user));

        $response->assertStatus(200);
    }
    
    public function testListNotFound()
    {
        $user = $this->user();
        
        $response = $this->json('GET', $this->apiUrl . 'lists/-1', [], $this->headers($user));

        $response->assertStatus(404);
    }
    
    public function testCreateList()
    {
        $user = $this->user();
        
        $response = $this->json('POST', $this->apiUrl . 'lists', ['name' => 'New List'], $this->headers($user));
        
        $response->assertStatus(201);
    }
    
    public function testFailedCreateList()
    {
        $user = $this->user();
        
        $response = $this->json('POST', $this->apiUrl . 'lists', ['name' => null],
            $this->headers($user)
        );

        $response
            ->assertStatus(500)
            ->assertJson([
                'status_code' => 500,
            ]);
    }
    
    public function testUpdateList()
    {
        $user = $this->user();
        $list = factory(Lists::class)->create(['user_id' => $user->id]);
        
        $response = $this->json('POST', $this->apiUrl . 'lists/' . $list->id, ['name' => 'updated list'], $this->headers($user));

        $response
            ->assertStatus(200)
            ->assertJson([
                'updated' => true,
            ]);
    }
    
     public function testDeleteList()
    {
        $user = $this->user();
        $list = factory(Lists::class)->create(['user_id' => $user->id]);
        
        $response = $this->json('DELETE', $this->apiUrl . 'lists/' . $list->id, [], $this->headers($user));

        $response
            ->assertStatus(204);
    }
    
    public function testFailedDeleteList()
    {
        $user = $this->user();
        $list = factory(Lists::class)->create(['user_id' => $user->id]);
        
        $response = $this->json('DELETE', $this->apiUrl . 'lists/' . '-1', [], $this->headers($user));

        $response->assertStatus(400);
    }
    
    public function testFavoriteList()
    {
        $user = $this->user();
        $list = factory(Lists::class)->create(['user_id' => $user->id]);
        
        $response = $this->json('POST', $this->apiUrl . 'lists/' . $list->id . '/favorite', ['favorite' => 1], $this->headers($user));

        $response
            ->assertStatus(200)
            ->assertJson([
                'favorited' => true,
            ]);
    }
    
    public function testUnfavoriteList()
    {
        $user = $this->user();
        $list = factory(Lists::class)->create(['user_id' => $user->id]);
        
        $response = $this->json('POST', $this->apiUrl . 'lists/' . $list->id . '/favorite', ['favorite' => 0], $this->headers($user));

        $response
            ->assertStatus(200)
            ->assertJson([
                'favorited' => true,
            ]);
    }
}
