<?php

namespace Tests\Unit\Api;

use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Support\Facades\Mail;
use App\User;
use App\Contact;
use App\Models\Template;
use App\ListContact;

class TemplateTest extends TestCase
{   
	public $apiUrl = '/api/v1/';
	
	public function testUnauthorized()
    {
    	$response = $this->json('GET', $this->apiUrl . 'templates');
        $response->assertStatus(401);
    }
    
	public function testGet()
    {
    	$user = $this->user();
    	
        $response = $this->json('GET', $this->apiUrl . 'templates', [], $this->headers($user));

        $response->assertStatus(200);
    }
    
    public function testShow()
    {
        $user = $this->user();
        
        $template = factory(Template::class)->create(['user_id' => $user->id]);
        
        $response = $this->json('GET', $this->apiUrl . 'templates/' . $template->id, [], $this->headers($user));

        $response->assertStatus(200);
    }
    
    public function testNotFound()
    {
        $user = $this->user();
        
        $response = $this->json('GET', $this->apiUrl . 'templates/-1', [], $this->headers($user));

        $response->assertStatus(404);
    }
    
    public function testStore()
    {
        $user = $this->user();
        
        $response = $this->json('POST', $this->apiUrl . 'templates', ['name' => 'My Template', 'messages' => 'Officiis odio aperiam ut numquam invent.'], $this->headers($user));
        
        $response->assertStatus(201);
    }
    
    public function testFailedStore()
    {
        $user = $this->user();
        
        $response = $this->json('POST', $this->apiUrl . 'templates', ['name' => null],
            $this->headers($user)
        );

        $response
            ->assertStatus(500)
            ->assertJson([
                'status_code' => 500,
            ]);
    }
    
    public function testUpdate()
    {
        $user = $this->user();
        $template = factory(Template::class)->create(['user_id' => $user->id]);
        
        $response = $this->json('POST', $this->apiUrl . 'templates/' . $template->id, ['name' => 'updated templates', 'messages' => 'updated messages'], $this->headers($user));

        $response
            ->assertStatus(200)
            ->assertJson([
                'updated' => true,
            ]);
    }
    
    public function testDelete()
    {
        $user = $this->user();
        $template = factory(Template::class)->create(['user_id' => $user->id]);
        
        $response = $this->json('DELETE', $this->apiUrl . 'templates/' . $template->id, [], $this->headers($user));

        $response
            ->assertStatus(204);
    }
    
    public function testFailedDelete()
    {
        $user = $this->user();
        
        $response = $this->json('DELETE', $this->apiUrl . 'templates/' . '-1', [], $this->headers($user));

        $response->assertStatus(400);
    }
    
    public function testMergeTemplate()
    {
        $user = $this->user();
        
        $response = $this->json('POST', $this->apiUrl . 'templates', [
                'name' => 'My Template', 
                'messages' => 'Test messages |*NAME*| |*SURNAME*| Cool.'
            ], $this->headers($user));
        $response->assertStatus(201);
    }
}
