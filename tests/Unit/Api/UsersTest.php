<?php

namespace Tests\Unit\Api;

use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Support\Facades\Mail;
use App\User;
use App\Contact;
use App\Lists;
use App\ListContact;

class UsersTest extends TestCase
{
	public $apiUrl = '/api/v1/';
	
	public function testUnauthorized()
    {
    	$response = $this->json('GET', $this->apiUrl . 'users/1');
        $response->assertStatus(401);
    }
    
    public function testShoWUser()
    {
    	$user = $this->user();
    	
        $response = $this->json('GET', $this->apiUrl . 'users/' . $user->id, [], $this->headers($user));

        $response->assertStatus(200);
    }
    
    public function testUpdateUser()
    {
        $user = $this->user();
        
        $response = $this->json('POST', $this->apiUrl . 'users/' . $user->id . '/update', [], $this->headers($user));

        $response->assertStatus(200);
    }
    
    public function testUserNotFound()
    {
        $user = $this->user();
        
        $response = $this->json('GET', $this->apiUrl . 'users/-1', [], $this->headers($user));

        $response->assertStatus(404);
    }
}
