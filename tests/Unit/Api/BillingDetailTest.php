<?php

namespace Tests\Unit\Api;

use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Support\Facades\Mail;
use App\User;
use App\Contact;
use App\Lists;
use App\ListContact;

class BillingDetailTest extends TestCase
{
    //use DatabaseMigrations;
    
	public $apiUrl = '/api/v1/';
	
	public function testUnauthorized()
    {
    	// $response = $this->json('GET', $this->apiUrl . 'users/1/billing');
        // $response->assertStatus(401);
    }
    
    public function testStoreBillingDetails()
    {
    	$user = $this->user();
    	
        $response = $this->json('POST', $this->apiUrl . 'users/' . $user->id . '/billing', [
                'address' => 'Address',
                'first_name' => 'Manny',
                'last_name' => 'Isles',
                'postal_code' => '4014',
                'country_dial_code_id'=> 1,
                
                ], $this->headers($user));

        $response->assertStatus(200);
    }
    
    
    
    
    
    // public function testUserNotFound()
    // {
    //     $user = factory(User::class)->create(['password' => bcrypt('foo')]);
        
    //     $response = $this->json('POST', $this->apiUrl . 'authenticate', ['email' => $user->email, 'password' => 'foo']);

    //        $response
    //         ->assertStatus(200)
    //         ->assertJsonStructure(['token']);
        
    //     $response = $this->json('GET', $this->apiUrl . 'users/-1', [], $this->headers($user));

    //     $response->assertStatus(404);
    // }
    
}
